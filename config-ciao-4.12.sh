#!/bin/bash
INSTALLDIR=/Users/qw/astro/soft/zhtools/ciao-4.12

CIAOLIB=/Users/qw/miniconda3/envs/ciao-4.12
FUNTOOLS=/Users/qw/astro/soft/funtools
WCS=/Users/qw/miniconda3/envs/ciao-4.12

GCC=/usr/local/Cellar/gcc@9/9.3.0/bin
export CC=$GCC/gcc-9
export CXX=$GCC/g++-9
export FC=$GCC/gfortran-9

echo "Warning---variables CC, CXX, FC have been exported."
echo "Run 'make' and 'make install' in this same session."

./configure --prefix=$INSTALLDIR \
    --with-readline=$CIAOLIB \
    --with-funtools=$FUNTOOLS \
    --with-cfitsio=$CIAOLIB \
    --with-wcs=$WCS \
    FFLAGS="-fbackslash" # -fallow-argument-mismatch"

# Uh.. Didn't work with GCC10.
# Undefined symbols for architecture x86_64: "__gfortran_os_error_at"...
# when compiling rfits.F. Not debugging this today...
