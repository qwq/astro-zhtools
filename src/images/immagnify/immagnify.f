      implicit none
      real img(1000000000)
      pointer (pimg,img)
      real oimg(1000000000)
      pointer (poimg,oimg)
      integer malloc
      integer nx,ny,nox,noy
      
      real magx,magy
      
      integer iunit,newunit
      integer status
      integer naxis,naxes(10)
      integer bitpix,pcount,gcount
      logical simple,extend
      integer hdtype
     
      character data*200,outfile*200
      character value*80
      logical defined
      integer lnblnk
      

c Read command line pars
      call get_command_line_par ("input",1,data)
      if (.not.defined(data)) then
        call usage
        call exit(1)
      endif

      call get_command_line_par ("output",1,outfile)
      if (.not.defined(data)) then
        call usage
        call exit(1)
      endif

      call get_command_line_par ("mag",1,value)
      if (.not.defined(data)) then
        call get_command_line_par ("magx",1,value)
        if (.not.defined(value)) then
          call usage
          call exit(1)
        endif
        read (value,*) magx
        call get_command_line_par ("magy",1,value)
        if (.not.defined(value)) then
          call usage
          call exit(1)
        endif
        read (value,*) magy
      else
        read (value,*) magx
        read (value,*) magy
      endif
      
      
      
c Determine sizes
      call op_fits_img (data,iunit,nx,ny)

c Allocate memory
      pimg=malloc(4*nx*ny)
      nox=int(nx*magx)+1
      noy=int(ny*magy)+1
      poimg=malloc(4*nox*noy)
      
      if (pimg.eq.0.or.poimg.eq.0) then
        write (0,*)'Not enough memory'
        call exit (1)
      endif
      
c Read data
      call read_fits_image_unit (iunit,data,img,nx,ny,'e')
      
c Interpolate data
      call interpolate images (img,oimg,nx,ny,nox,noy,magx,magy)
      
c Write data
      call write_fits_image (outfile,oimg,nox,noy,'e','e')
      
      call exit(0)
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine interpolate images (img,oimg,nx,ny,nox,noy,magx,magy)
      implicit none
      integer nx,ny,nox,noy
      real img(nx,ny), oimg(nox,noy)
      real magx,magy
      
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine usage
      character progname*80
      integer lnblnk
      
      call getarg (0,progname)
      write (0,*) "Usage: ",progname(1:lnblnk(progname)),
     ~    " input=img output=oimg mag=mag (or magx=magx magy=magy) "
      end
      
