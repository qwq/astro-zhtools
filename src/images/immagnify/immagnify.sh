#!/bin/sh

tmp=gov$$
tmpout=govout$$

image=$1
out=$2
magx=$3
magy=$4

shift
shift
shift
shift

cl << EOSCRIPT | grep "ERROR:"
images
dataio
rfits $image $image $tmp mode='h'
imlintran $tmp $tmpout 0.0 0.0 $magx $magy $* ncols=0 nlines=0 mode='h'
del $out
wfits $tmpout $out mode='h'
imdel $tmp,$tmpout mode='h'
logout
EOSCRIPT

#magnify $tmp $tmpout $magx $magy $* mode='h'
