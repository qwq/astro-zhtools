      implicit none
      character arg*80
      integer lnblnk
      integer n,k

      call getarg(0,arg)
      
      n = lnblnk(arg)

      k = max(1,n-3)
      if (arg(k:n).eq.'ph2w') then
        call phys2world
        call exit(0)
      endif

      k = max(1,n-3)
      if (arg(k:n).eq.'w2ph') then
        call world2phys
        call exit(0)
      endif

      k = max(1,n-6)
      if (arg(k:n).eq.'pix2sky') then
        call phys2world
        call exit(0)
      endif

      k = max(1,n-6)
      if (arg(k:n).eq.'sky2pix') then
        call world2phys
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'rad2ad') then
        call radians2ad
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'ad2rad') then
        call ad2radians
        call exit(0)
      endif

      write (0,*) 'use as pix2sky(=ph2w), sky2pix(=w2ph), rad2ad, or ad2rad'

      call exit(1)
      end
      




