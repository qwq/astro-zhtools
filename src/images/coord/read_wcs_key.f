      subroutine read_wcs_keys
     ~    (imgpar,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,ctype1,ctype2)
      implicit none
      character imgpar*(*)
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      double precision crval1img,crval2img,crpix1img,crpix2img,
     ~    cdelt1img,cdelt2img,crota2img
      character ctype1*(*),ctype2*(*)
      character ctypeimg*80

c fitsio stuff      
      integer iunit,status,blocksize
      character comment*80
      
c image
      character filename*200
      logical defined, qimg, fits wcs present
      
      character arg*200

      call get_command_line_par (imgpar,1,filename)
c
c Get the reference image name. Try open this file. If it is opened OK, we
c will use it later. If not, issue a warning and proceed as without the imgpar
c switch.
c
      if (defined(filename)) then

        qimg = .true.
        
        status = 0
        call ftgiou(iunit,status)
        if (status.ne.0) then
          call perror_fitsio (filename,status)
          call exit(1)
        endif

        call ftopen (iunit,filename,0,blocksize,status)
        if (status.ne.0) then
          call perror_fitsio ('Warning: '//filename,status)
          qimg = .false.
          status = 0
          call ftclos (iunit,status)
          status = 0
          call ftfiou (iunit,status)
        endif
      else
        qimg = .false.
      endif

      if (qimg) then
        call FTGICS (iunit,crval1img,crval2img,crpix1img,crpix2img,cdelt1img
     ~      ,cdelt2img,crota2img,ctypeimg,status)
        if (status.eq.0) then
          fits wcs present = .true.
        else
          fits wcs present = .false.
          status = 0
        endif
      endif

c
c CTYPE1 and CTYPE2 are special. If they are not set, use '-TAN'
c
c CROTA2 is special too. if it not set, crota2=0
c

c CTYPE1
      call get_command_line_par ('CTYPE1',1,ctype1)
      if (.not.defined(ctype1)) then
        if (qimg) then
          if (fits wcs present) then
            ctype1='RA--'//ctypeimg
          else
            call ftgkys (iunit,'CTYPE1',ctype1,comment,status)
            if (status.ne.0) then
              status=0
              ctype1='undefined'
            endif
          endif
        endif
      endif
      

c CTYPE2
      call get_command_line_par ('CTYPE2',1,ctype2)
      if (.not.defined(ctype2)) then
        if (qimg) then
          if (fits wcs present) then
            ctype2='DEC-'//ctypeimg
          else
            call ftgkys (iunit,'CTYPE2',ctype2,comment,status)
            if (status.ne.0) then
              status=0
              ctype2='undefined'
            endif
          endif
        endif
      endif

      if (.not.defined(ctype1)) ctype1 = 'RA---TAN'
      if (.not.defined(ctype2)) ctype2 = 'DEC--TAN'

c CRVAL1
      call get_command_line_par ('CRVAL1',1,arg)
      if (defined(arg)) then
        read (arg,*) crval1
      else
        if (qimg) then
          if (fits wcs present) then
            crval1=crval1img
          else
            call ftgkyd(iunit,'CRVAL1',crval1,comment,status)
            if (status.ne.0) call read_wcs_keys_complain ('CRVAL1')
          endif
        else
          call read_wcs_keys_complain ('CRVAL1')
        endif
      endif

c CRVAL2
      call get_command_line_par ('CRVAL2',1,arg)
      if (defined(arg)) then
        read (arg,*) crval2
      else
        if (qimg) then
          if (fits wcs present) then
            crval2=crval2img
          else
            call ftgkyd(iunit,'CRVAL2',crval2,comment,status)
            if (status.ne.0) call read_wcs_keys_complain ('CRVAL2')
          endif
        else
          call read_wcs_keys_complain ('CRVAL2')
        endif
      endif

c CRPIX1
      call get_command_line_par ('CRPIX1',1,arg)
      if (defined(arg)) then
        read (arg,*) crpix1
      else
        if (qimg) then
          if (fits wcs present) then
            crpix1=crpix1img
          else
            call ftgkyd(iunit,'CRPIX1',crpix1,comment,status)
            if (status.ne.0) call read_wcs_keys_complain ('CRPIX1')
          endif
        else
          call read_wcs_keys_complain ('CRPIX1')
        endif
      endif

c CRPIX2
      call get_command_line_par ('CRPIX2',1,arg)
      if (defined(arg)) then
        read (arg,*) crpix2
      else
        if (qimg) then
          if (fits wcs present) then
            crpix2=crpix2img
          else
            call ftgkyd(iunit,'CRPIX2',crpix2,comment,status)
            if (status.ne.0) call read_wcs_keys_complain ('CRPIX2')
          endif
        else
          call read_wcs_keys_complain ('CRPIX2')
        endif
      endif

c CDELT1
      call get_command_line_par ('CDELT1',1,arg)
      if (defined(arg)) then
        read (arg,*) cdelt1
      else
        if (qimg) then
          if (fits wcs present) then
            cdelt1=cdelt1img
          else
            call ftgkyd(iunit,'CDELT1',cdelt1,comment,status)
            if (status.ne.0) call read_wcs_keys_complain ('CDELT1')
          endif
        else
          call read_wcs_keys_complain ('CDELT1')
        endif
      endif

c CDELT2
      call get_command_line_par ('CDELT2',1,arg)
      if (defined(arg)) then
        read (arg,*) cdelt2
      else
        if (qimg) then
          if (fits wcs present) then
            cdelt2=cdelt2img
          else
            call ftgkyd(iunit,'CDELT2',cdelt2,comment,status)
            if (status.ne.0) call read_wcs_keys_complain ('CDELT2')
          endif
        else
          call read_wcs_keys_complain ('CDELT2')
        endif
      endif



c CROTA2
      call get_command_line_par ('CROTA2',1,arg)
      if (defined(arg)) then
        read (arg,*) crota2
      else
        if (qimg) then
          if (fits wcs present) then
            crota2=crota2img
          else
            call ftgkyd(iunit,'CROTA2',crota2,comment,status)
            if (status.ne.0) then
              status=0
              crota2=0
            endif
          endif
        else
          crota2=0
        endif
      endif

      if (qimg) then
        status = 0
        call ftclos (iunit,status)
        status = 0
        call ftfiou (iunit,status)
      endif

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_wcs_keys_complain (keyname)
      implicit none
      character keyname*(*)
      integer lnblnk
      
      write (0,*) keyname(1:lnblnk(keyname)),' not found'
      call exit(1)

      return
      end
