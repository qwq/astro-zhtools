c pix2sky
      subroutine phys2world
      implicit none
      
      integer status
      character arg*80
      
      
      character listname*200
      
      
      integer iunit,newunit

      character ctype1*80,ctype2*80
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      character pcode*4

      double precision x,y
      
      double precision xx,yy
      double precision ra8,dec8
      double precision pi
      parameter (pi=3.14159265358979d0)
      
      integer ierr,worldpos
      logical defined
      integer outunit
      integer ndp
      character sign*1,sgn*1
      integer ihmsf(4)
      integer idmsf(4)
      integer i
      
c--------------------------------------------------------------
      
      listname=' '
      
      call get_command_line_par ('list',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        listname=arg
      endif
      
      call read_wcs_keys
     ~    ('img',
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,ctype1,ctype2)

      if (ctype1(5:8).ne.ctype2(5:8)) then
        write (0,*) "Different wcs types in X and Y are not supported"
        call exit(1)
      endif
      
      pcode=ctype1(5:8)
      
      if (listname.ne.' ') then
        iunit = newunit()
        open(iunit,file=listname,status='old')
      else
        iunit=5
      endif
      
      status=0
          
      outunit=1
      
      call get_command_line_par ("out",1,arg)
      if (defined(arg)) then
        call lowstring(arg)
        if (arg(1:3).eq.'rad') then
          outunit=1
        else if (arg(1:3).eq.'deg') then
          outunit=2
        else if (arg(1:3).eq.'hex'.or.arg(1:6).eq.'hmsdms') then
          outunit=3
        else
          write (0,*) 'you can only use rad,deg,hmsdms for out='
          write (0,*) ' rad - output in radians (default)'
          write (0,*) ' deg - output in degrees'
          write (0,*) ' hmsdms - output in hms dms'
          call exit(1)
        endif
      endif

      call get_cl_par ('c',arg)
      if (defined(arg)) then
        read (arg,*) x,y
        xx=x
        yy=y
        ierr = worldpos (xx, yy, crval1, crval2, crpix1, crpix2, cdelt1,
     ~      cdelt2, crota2, pcode, ra8, dec8)
        if (ierr.ne.0) then
          write(0,*)'Ierr = ',ierr, 'in  worldpos'
        endif
        
        if (outunit.eq.1) then
          print '(f11.9,1x,f12.9)',ra8*pi/180.0d0,dec8*pi/180.0d0
        else if (outunit.eq.2) then
          print '(f11.7,1x,f11.7)',ra8,dec8
        else if (outunit.eq.3) then
          ra8=ra8*pi/180.0d0
          dec8=dec8*pi/180.0d0
          ndp=3
          call sla_dr2tf(ndp,ra8,sign,ihmsf)
          ndp=2
          call sla_dr2af(ndp,dec8,sgn,idmsf)
          print 782,(ihmsf(i),i=1,4),sgn,(idmsf(i),i=1,4)
        endif
        call exit(0)
      endif

      do while (status.eq.0)
        read (iunit,*,iostat=status) x,y
        if (status.eq.0) then
          
          xx=x
          yy=y
          ierr = worldpos (xx, yy, crval1, crval2, crpix1, crpix2, cdelt1,
     ~        cdelt2, crota2, pcode, ra8, dec8)
          if (ierr.ne.0) then
            write(0,*)'Ierr = ',ierr, 'in  worldpos'
          endif
          
          if (outunit.eq.1) then
            print '(f11.9,1x,f12.9)',ra8*pi/180.0d0,dec8*pi/180.0d0
          else if (outunit.eq.2) then
            print '(f11.7,1x,f11.7)',ra8,dec8
          else if (outunit.eq.3) then
            ra8=ra8*pi/180.0d0
            dec8=dec8*pi/180.0d0
            ndp=3
            call sla_dr2tf(ndp,ra8,sign,ihmsf)
            ndp=2
            call sla_dr2af(ndp,dec8,sgn,idmsf)
            print 782,(ihmsf(i),i=1,4),sgn,(idmsf(i),i=1,4)
 782        format (i2.2,1x,i2.2,1x,i2.2,'.',
     ~          i3.3,2x,a1,i2.2,1x,i2.2,1x,i2.2,'.',i2.2)
          endif
          call flush(6)
          
        endif
      enddo
      close(iunit)
      
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c sky2pix       
      subroutine world2phys
      implicit none
      
      character listname*200
      integer iunit,newunit,status
      
      character ctype1*80,ctype2*80
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      character pcode*4

      double precision ra,dec
      
      
      double precision xx,yy
      double precision ra8,dec8
      
      integer ierr,xypix
      logical qdeg
      character arg*80
      logical defined

c--------------------------------------------------------------
      
      listname=' '
      
      call read_wcs_keys
     ~    ('img',
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,ctype1,ctype2)

      if (ctype1(5:8).ne.ctype2(5:8)) then
        write (0,*) "Different wcs types in X and Y are not supported"
        call exit(1)
      endif
      
      pcode=ctype1(5:8)
      
      if (listname.ne.' ') then
        iunit = newunit()
        open(iunit,file=listname,status='old')
      else
        iunit=5
      endif
      
      status=0
      

      call get_command_line_par ('deg',1,arg)
      qdeg = defined(arg)
      if (.not.qdeg) then
        call get_cl_par ('input',arg)
        if (arg.eq.'deg') then
          qdeg = .true.
        endif
      endif
      if (.not.qdeg) then
        call get_cl_par ('in',arg)
        if (arg.eq.'deg') then
          qdeg = .true.
        endif
      endif

      call get_cl_par ('c',arg)
      if (defined(arg)) then
        read (arg,*) ra,dec
        if (.not.qdeg) then
          ra8=ra*180.0d0/3.14159265358979d0
          dec8=dec*180.0d0/3.14159265358979d0
        else
          ra8 = ra
          dec8 = dec
        endif
        ierr = xypix (ra8, dec8, crval1, crval2, crpix1, crpix2, cdelt1,
     ~      cdelt2, crota2, pcode, xx, yy)
        if (ierr.ne.0) then
          write(0,*)'Ierr = ',ierr, 'in  xypix'
        endif
        print '(f9.3,2x,f9.3)',xx,yy
        call exit(0)
      endif

      do while (status.eq.0)
        read (iunit,*,iostat=status) ra,dec
        if (status.eq.0) then
          
          if (.not.qdeg) then
            ra8=ra*180.0d0/3.14159265358979d0
            dec8=dec*180.0d0/3.14159265358979d0
          else
            ra8 = ra
            dec8 = dec
          endif
          
          ierr = xypix (ra8, dec8, crval1, crval2, crpix1, crpix2, cdelt1,
     ~        cdelt2, crota2, pcode, xx, yy)
          if (ierr.ne.0) then
            write(0,*)'Ierr = ',ierr, 'in  xypix'
          endif

          print '(f9.3,2x,f9.3)',xx,yy
*          
          
          call flush(6)
          
        endif
      enddo
      close(iunit)
      
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine radians2ad
      implicit none
      double precision ra,dec
      character listname*200
      character arg*80
      integer iunit,status
      
      integer ndp
      character sign*1,sgn*1
      integer ihmsf(4)
      integer idmsf(4)
      integer i

      listname=' '
      call get_command_line_par('list',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        listname=arg
      endif
      
      iunit=1
      if (listname.ne.' ') then
        open(iunit,file=listname,status='old')
      else
        iunit=5
      endif
      
      status=0
      do while (status.eq.0)
        read(iunit,*,iostat=status) ra,dec
        if (status.eq.0) then
          
          ndp=1
          call sla_dr2tf(ndp,ra,sign,ihmsf)
          ndp=0
          call sla_dr2af(ndp,dec,sgn,idmsf)

          print 782,(ihmsf(i),i=1,4),sgn,(idmsf(i),i=1,3)
 782      format (i2.2,1x,i2.2,1x,i2.2,'.',i1,2x,a1,i2.2,1x,i2.2,1x,i2.2)
          call flush(6)
        endif
      enddo
      close(iunit)
      return
      end
      
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine ad2radians
      implicit none
      double precision ra,dec
      character listname*200
      character arg*80
      integer iunit,status
      integer test
      
      integer nptr
      character string*200
      integer jf,i,lnblnk

      listname=' '
      call get_command_line_par('list',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        listname=arg
      endif
      
      iunit=1
      if (listname.ne.' ') then
        open(iunit,file=listname,status='old')
      else
        iunit=5
      endif
      
      status=0
      do while (status.eq.0)
        read(iunit,'(a)',iostat=status) string
        if (status.eq.0) then
          nptr=lnblnk(string)
          do i=1,nptr
            test=ichar(string(i:i))
            if (test.gt.57) then ! ":" and all letters
              string(i:i)=' '
            endif
          enddo
          
          nptr=1
          call sla_DAFIN (string,nptr,ra,jf)
          ra=ra*15.0
          
          call sla_DAFIN (string,nptr,dec,jf)
          

          print 782,ra,dec
 782      format (f12.9,2x,f12.9)
          call flush(6)
        endif
      enddo
      close(iunit)
      return
      end


