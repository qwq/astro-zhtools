      character arg*80
      integer lnblnk,n,k,status
      
      call getarg (0,arg)
      n = lnblnk(arg)
      
      k=max(n-7,1)
      if (arg(k:n).eq.'gaussimg') then
        call gaussimage
        call exit(0)
      endif

      k=max(n-6,1)
      if (arg(k:n).eq.'poisson') then
        call poisson
        call exit(0)
      endif

      k=max(n-6,1)
      if (arg(k:n).eq.'kingimg') then
        call kingimg
        call exit(0)
      endif

      k=max(n-8,1)
      if (arg(k:n).eq.'createimg') then
        call createimage
        call exit(0)
      endif

      k=max(n-6,1)
      if (arg(k:n).eq.'scatter') then
        call scatter
        call exit(0)
      endif

      print*,'Choose one:'
      print*,' 1  - simulate a Gaussian'
      print*,' 2  - simulate a beta-model image'
      print*,' 3  - create a constant image'
      print*,' 4  - Poisson scatter of an existing image'
      print*,' 5  - Gaussian scatter of an existing image'
      print*,' else  - exit'

      read (*,'(a)') arg
      
      read (arg,*,iostat=status) k
      if (status.ne.0) call exit(1)
      
      if (k.eq.1) call gaussimage
      if (k.eq.2) call kingimg
      if (k.eq.3) call createimage
      if (k.eq.4) call poisson
      if (k.eq.5) call scatter
      
      call exit(0)
      end
      

