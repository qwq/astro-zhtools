#include <zhtools.h>

      subroutine kingimg
      logical defined
      character*200 refimg
      integer nx,ny
      ZHDECL(real,pimg)
      
      call get_command_line_par ('refimg',1,refimg)
      if (defined(refimg)) then
        call get_image_size (refimg,nx,ny)
      else
        call command_line_or_read_par
     ~      ('nx','X size of the output image',nx,'j')
        call command_line_or_read_par
     ~      ('ny','Y size of the output image',ny,'j')
      endif

      ZHMALLOC(pimg,nx*ny,'e')

      call do_kingimg (nx,ny,ZHVAL(pimg))
      call exit(0)
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_kingimg (nx,ny,img)
      implicit none
      integer nx,ny
      real img(nx*ny)
*#ifdef G77
*      real img(nx*ny)
*#else
*      pointer (pimg,img)
*      real img(1)
*      integer malloc
*#endif

      character*200 imgname,arg
      logical defined
      
      real ax,ay,angle,beta
      real norm
      integer i0,j0
      
c
c Usage: kingimg [img=imgname] [a=a] [ax=ax] [ay=ay] [angle=angle] \
c          [nx=nx] [ny=ny] [i0=i0] [j0=j0] [norm=norm] [beta=beta]
c
c
c--------------------------------------------------------------
      
      ax = -10.0
      ay = -10.0

      call get_command_line_par ('a',1,arg)
      if (defined(arg)) then
        read (arg,*) ax
        ay = ax
      endif
      

      if (ax.lt.0.0) call command_line_or_read_par
     ~    ('ax','core radius on X direction',ax,'e')
      if (ay.lt.0.0) call command_line_or_read_par
     ~    ('ay','core radius on Y direction',ay,'e')

      call command_line_or_read_par ('beta','beta parameter',beta,'e')
      
      i0=nx/2
      j0=ny/2
      
      call get_command_line_par ('i0',1,arg)
      if (defined(arg)) read (arg,*) i0
      call get_command_line_par ('j0',1,arg)
      if (defined(arg)) read (arg,*) j0

      angle = 0
      call get_command_line_par ('angle',1,arg)
      if (defined(arg)) read (arg,*)angle

      norm = 1
      call get_command_line_par ('norm',1,arg)
      if (defined(arg)) read (arg,*)norm


      call command_line_or_read_spar ('img','output image',imgname,'s')

*#ifndef G77      
*c . allocate memory
*      pimg=malloc(4*nx*ny)
*      if (pimg.eq.0) call exiterror ('Not enough memory')
*#endif

      call make_king_img (img,nx,ny,i0,j0,ax,ay,beta,angle,norm)
      
      call write_fits_image (imgname,img,nx,ny,'e','e')
      
      return
      end
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine make_king_img (img,nx,ny,i0,j0,ax,ay,beta,angle,norm0)
      implicit none
      integer nx,ny,i0,j0
      real img(nx,ny)
      real ax,ay,beta,angle
      
      real arg,rr
      real cosa,sina,dx,dy
      
      real*8 norm8
      real norm
      real norm0
      
      
      integer i,j

      arg=angle*3.1415926536/180.0
      cosa=cos(arg)
      sina=sin(arg)
      
      norm8=0.0
      do i=1,nx
        do j=1,ny
          dx=(i-i0)*cosa +(j-j0)*sina
          dy=-(i-i0)*sina+(j-j0)*cosa
          rr=(dx/ax)**2 + (dy/ay)**2
          arg=1.0+rr
          img(i,j)=1.0/arg**(3.0*beta-0.5)
          norm8=norm8+img(i,j)
        enddo
      enddo

      norm=norm0/norm8
      
      do i=1,nx
        do j=1,ny
          img(i,j)=img(i,j)*norm
        enddo
      enddo
      
      return
      end


































