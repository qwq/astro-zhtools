      subroutine saoimage (img,nx,ny,type)
      implicit none
      integer img,nx,ny
      character type*(*)
      character tmpname*80
      character saoprog*200
      
*      if      (type(1:1).eq.'b'.or.type(1:1).eq.'B') then
*        call show_c1(img,nx,ny)
*      else if (type(1:1).eq.'i'.or.type(1:1).eq.'I') then
*        call show_i2(img,nx,ny)
*      else if (type(1:1).eq.'j'.or.type(1:1).eq.'J') then
*        call show_i4(img,nx,ny)
*      else if (type(1:1).eq.'e'.or.type(1:1).eq.'E') then
*        call show_r4(img,nx,ny)
*      else if (type(1:1).eq.'d'.or.type(1:1).eq.'D') then
*        call show_r8(img,nx,ny)
*      else
*        write(0,*)'Unknown type: ',type(1:1),'   in saoimage'
*         call exiterror('')
*      endif

      tmpname = '/tmp/_show_.fits'
      call write_fits_image (tmpname,img,nx,ny,type,type)
      
      call getenv ('ZHSAO',saoprog)
      if (saoprog.eq.' ') then
        saoprog = 'saoimage'
      endif
      
      call strcat (saoprog,' '//tmpname)
      call system (saoprog)
      call unlink (tmpname)

      return
      end
      


