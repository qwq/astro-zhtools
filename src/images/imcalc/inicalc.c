#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int findindex (char *name, char files[100][100], int nfiles);

main (int argc, char *argv[])
{
  char argstring[1000],exprstring[1000],argindex[1000], work[1000];
  register int i, j, l;
  long int k;

  if (argc<2){
    k = 6;
    zhhelp_c_("imcalc",&k);
  }

  strcpy (argstring,argv[1]);
  for (i=2;i<argc;i++)
    {
      strcat (argstring," ");
      strcat (argstring,argv[i]);
    }
  
  /* replace ")" and "(" with " ) " and " ( " */
  fixbraces (argstring);
  /*  printf ("Argstring: |%s|\n",argstring); */

  /* parse expression */
  parseexpr (argstring,exprstring);
  /*  printf ("Exprstring: |%s|\n",exprstring); */

  /* make file index */
  strcpy(work,exprstring);
  filelist (work,argindex);
  /*	  printf ("Argindex: |%s|\n",argindex); */

  inicalc  (exprstring,argindex);
  
  exit(0);

}


int filelist (char *exprstring, char *argindex)
{
  int arg;
  char *args[64];
  
  int narg,iarg,ifile;
  char file[100][200];
  char buff[200];
  int nn,nfiles,is,i;
  

  parse (exprstring,args,&narg);
  strcpy(file[0],args[0]);

  nfiles=1;
  for (iarg=1;iarg<narg;iarg++)
    {
      nn=strlen(args[iarg]);
      if (args[iarg][0] == '$' && args[iarg][nn-1] == '$')
	{
	  for (i=0;i<nn-2;i++)
	    {
	      buff[i]=args[iarg][i+1];
	    }
	  buff[nn-2]='\0';

	  is=0;
	  for(ifile=0;ifile<nfiles;ifile++)
	    {
	      if (strcmp(file[ifile],buff)==0) is=1;
	    }
	  if (is == 0)
	    {
	      nfiles++;
	      strcpy(file[nfiles-1],buff);
	    }
	}
    }
  
  
  strcpy (argindex,"");
  for (ifile=0;ifile<nfiles;ifile++)
    {
      sprintf(buff," %d %s",ifile,file[ifile]);
      strcat (argindex,buff);
    }
  return 1;
}		



int parseexpr (char *argstring, char *exprstring)
{
  int arg;
  char *argv[100];
  int iarg;
  char expression[1024];
  char expression0[1024];
  char word[1024],wword[1024];
  char *s;
  int i,nword,math,n;
  int mode;
  

  /*  parse (argstring,argv,&arg); */
  arg = 2;
  argv[1]=argstring;
  
   /* first argument */
  s=strchr(argv[1],'=');
  if (s==NULL) /* = not present */
    {
      printf("none\n");
      exit(0);
    }

  for (i=0;argv[1][i] != '=';i++)
    expression[i]=argv[1][i];
  expression[i]='\0';
  strcat(expression," ");
  
  for (iarg=1;iarg<arg;iarg++)
    {
      if (iarg == 1)
	{
	  strcpy(word,argv[1]+i+1);
	}
      else
	{
	  strcpy(word,argv[iarg]);
	}
      
      s=word;

/* determine whether the whole argument is an existing file? */
    WORD:
      while (s[0]==' ' || s[0]=='\t' || s[0]=='\n' && s[0] != '\0') s++;
      
/*      printf("expr: %s\n",s); */
      
      if (s[0]=='/')
	{
	/* special cautions for absolute file names */
	  nword=strlen(s);
	  math = 0;
	  for (i=0;i<nword && math == 0 ;i++)
	    {
	      switch (s[i])
		{
		case '+': math=1;break;
		case '-': math=1;break;
		case ' ': math=1;break;
		case '*': math=1;break;
		case '(': math=1;break;
		case ')': math=1;break;
		default:  math=0;
		}
	    }
	  if (math == 1) i--;
	  word[i]='\0';
	  strcpy(wword,"");
	  strncat(wword,s,i);
	  if (strcmp(wword,"/") != 0)
	    {
	      if (access(wword,F_OK) == 0)
		{
		  strcat(expression,"$");
		  strcat(expression,wword);
		  strcat(expression,"$");
		  strcat(expression," ");
		  strncat(expression,s+i,1);
		  strcat(expression," ");
		  s+=i+1;
		  if ( s[0] != '\0') goto WORD;
		}
	    }
	}
	  
      if (access(s,F_OK) == 0)
	{
	  
	  strcat(expression,"$");
	  strcat(expression,s);
	  strcat(expression,"$");
	  strcat(expression," ");
	}
      else
	{
	  /* 	  find the first math symbol */
	  nword=strlen(s);
	  math = 0;
	  for (i=0;i<nword && math == 0 ;i++)
	    {
	      switch (s[i])
		{
		case '+': math=1;break;
		case '-': math=1;break;
		case ' ': math=1;break;
		case '*': math=1;break;
		case '/': math=1;break;
		case '(': math=1;break;
		case ')': math=1;break;
		default:  math=0;
		}
	    }
	  if (math == 1) i--;
	  
	  if (i == nword)
	    {
	      strcat(expression,s);
	      strcat(expression," ");
	    }
	  else if ( i == 0 )
	    {
	      strncat(expression,s+i,1);
	      strcat(expression," ");
	      s+=i+1;
	      if ( s[0] != '\0') goto WORD;
	    }
	  else
	    {
	      wword[0]='\0';
	      strncat(wword,s,i);
	      if(strcmp(wword,"/")==0)
		{
		  strcat(expression,wword);
		  strcat(expression," ");
		}
	      else
		{
		  if (access(wword,F_OK) == 0)
		    {
		      strcat(expression,"$");
		      strcat(expression,wword);
		      strcat(expression,"$");
		      strcat(expression," ");
		      strncat(expression,s+i,1);
		      strcat(expression," ");
		    }
		  else
		    {
		      strcat(expression,wword);
		      strcat(expression," ");
		      strncat(expression,s+i,1);
		      strcat(expression," ");
		    }
		}
	      s+=i+1;
	      if ( s[0] != '\0') goto WORD;
	    }
	}
    }

  i=0;
  n=0;
  for(i=0; expression[i] != 0; i++)
    {
      if (
	  expression[i] == '*' ||
	  expression[i] == '(' ||
	  expression[i] == ')' ||
	  expression[i] == '#' ||
	  expression[i] == '%' ||
	  expression[i] == '[' ||
	  expression[i] == ']' ||
	  expression[i] == '&' ||
	  expression[i] == '<' ||
	  expression[i] == '>' ||
	  expression[i] == '|' ) 
	{
/*	  expression0[n]='\\';
	  n++;*/
	  expression0[n]=expression[i];
	  n++;
	}
      else
	{
	  expression0[n]=expression[i];
	  n++;
	}
    }
  expression0[n]='\0';
  strcpy (exprstring,expression0);
  return 1;
}
 

int fixbraces (char *argstring)
{
  char  work[1000];
  register int i,j,l;
  j = 0;
  l = strlen(argstring);
  for (i=0;i<l;i++)
    {
      switch (argstring[i])
	{
	case '(': work[j]=' '; j++; work[j]='('; j++; work[j]=' '; j++; break;
	case ')': work[j]=' '; j++; work[j]=')'; j++; work[j]=' '; j++; break;
	default:  work[j]=argstring[i]; j++; break;
	}
    }
  work[j]='\0';
  strcpy (argstring,work);
  return 1;
}
    
     

int inicalc (char *exprstring, char *argindex)
{
  char *expr[100];
  char expression[1000],buff[1000],work[1000];
  char word[100];
  int nexpr;
  char *zhtools;
  char source[200];
  char *fileslist1[100];
  char fileslist[100][100];
  int ifile;
  int nfiles1,nfiles;
  int i;
  int j;
  int iarg;
  int pid;
  int nn;
  int stat;

  FILE *prog;
  char progname[100];char rootname[100];char subname[100];
  char command[1000];
  int status;
  
  parse (exprstring,expr,&nexpr);
  strcpy(work,argindex);
  parse (work,fileslist1,&nfiles1);

  nfiles=0;
  for (i=0;i<nfiles1;i+=2)
    {
      sscanf(fileslist1[i],"%d",&ifile);
      strcpy(fileslist[ifile],fileslist1[i+1]);
      if (ifile > nfiles) nfiles=ifile;
    }

  nfiles++;

  strcpy(expression,"arg(1)= ");
  /*	  printf ("%s| %d \n",exprstring,nexpr); */
  for (i=1;i<nexpr;i++)
    {
      if ( expr[i][0] == '$' )
	{
	  iarg = findindex (expr[i],fileslist,nfiles) + 1;
	  sprintf(word,"arg(%d)",iarg);
	}
      else
	strcpy (word,expr[i]);
      
      strcat(expression,word);
    }
  
  /*  Open file with subroutine */
  /* get pid */
  pid=getppid();
  sprintf (rootname,"/tmp/CALC%d",pid);
  strcpy (subname,rootname);
  strcpy (progname,rootname);
  strcat (subname,".f");

  prog=fopen(subname,"w");
  if (prog == NULL )
    {
      printf("Error creating code for CALC subroutine\n");
      exit (1);
    }

  fprintf(prog,"       call imcalc()\n");
  fprintf(prog,"       end\n");
  fprintf(prog,"       subroutine calculator (arg)\n");
  fprintf(prog,"       real arg(*)\n");
  if (strlen(expression)<72)
    fprintf(prog,"       %s\n",expression);
  else
    {
      nn=0;
      strncpy(buff,expression+nn,72);
      fprintf(prog,"       %s\n",buff);
      nn+=72;
      while (nn<strlen(expression))
	{
	  strncpy(buff,expression+nn,72);
	  fprintf(prog,"     ~ %s\n",buff);
	  nn+=72;
	}
    }
	
    
  fprintf(prog,"       return\n");
  fprintf(prog,"       end\n");
  fclose(prog);

  /* compile a program */
  strcpy(command,FORTRANCOMP);
  strcat(command," -o ");
  strcat(command,progname);
  strcat(command," ");
  strcat(command,subname);

  /* if ZHTOOLS is defined, use it */
  if( (zhtools=getenv("ZHTOOLS")) ){
    strcpy(source,zhtools);
    strcat(source,"/lib/imcalc.o");
    if (access(source,F_OK) != 0) {
      fprintf(stderr, "ERROR: imcalc.o not found in zhtools dir: %s.\n",
	      zhtools);
      exit (1);
    }
  }
#ifdef ZHLIB_INSTALL_DIR
  /* if no ZTOOLS env, then use the installed version */
  else{
    snprintf(source, 200, "%s/imcalc.o", ZHLIB_INSTALL_DIR);
    if (access(source,F_OK) != 0){
      fprintf(stderr,
	      "ERROR: imcalc.o not found in zhtools install dir: %s.\n",
	      ZHLIB_INSTALL_DIR);
      exit (1);
    }
  }
#else
  /* if no zhtools env and no installed version, we're in trouble */
  else{
   fprintf(stderr,
   "ERROR: imcalc.o not found (no $ZHTOOLS or install dir available).\n");
  }
#endif

  strcat(command," ");
  strcat(command,source);
  strcat(command," ");

  if( zhtools ){
    strcpy(work,ZHLIB);
    if (strncmp(work,"lib",3)==0) {
      strcat (command,zhtools);
      strcat (command,"/LIB/");
      strcat (command,ZHLIB);
    }
    else
      strcat(command,ZHLIB);
  }
#ifdef ZHLIB_INSTALL_DIR
  else{
    sprintf(work, "%s/libzhtools.a ", ZHLIB_INSTALL_DIR);
    strcat(command, work);
#ifdef ZHLIB_EXTRA
    strcat(command, ZHLIB_EXTRA);
#endif
  }
#else
  /* if no zhtools env and no installed version, we're in trouble */
  else{
   fprintf(stderr,
   "ERROR: zhtools lib not found (no $ZHTOOLS or install dir available).\n");
  }
#endif

  strcat(command," ");
  strcpy(work,FITSLIB);
  if (strncmp(work,"lib",3)==0) {
    strcat (command,zhtools);
    strcat (command,"/LIB/");
    strcat (command,FITSLIB);
  }
  else
    strcat(command,FITSLIB);
  strcat(command," ");
  strcat(command,FITSLIBA);
    
  /* fprintf(stderr, "%s\n", command); */

  strcat(command," 2> /dev/null");  
/*   printf ("Running: %s\n",command); */
  status=system(command);

/*  unlink (subname); */
  if (status != 0)
    {
      fprintf(stderr,"!!! Your expression seem to be erroneous\n");
      fprintf(stderr,"!!! please check it and run again\n");
      prog=fopen(subname,"r");
      while (fgets(buff,900,prog)!=NULL)
	fprintf(stderr,"%s",buff);
      exit(1);
    }

  unlink (subname);

  strcpy (command,progname);
  strcat (command," ");
  strcat (command,argindex);
  
  stat = system (command);
  
  unlink (progname);
   
  exit (stat);
}



/********************************************************************/
int parse (buf,args,narg)
char *buf;
char **args;
int *narg;
{
  *narg=0;
  while (*buf != '\0')
    {
      while ((*buf == ' ') || (*buf == '\t') || (*buf == '\n'))
        *buf++ = '\0';

      *args++=buf;
      *narg+=1;
      while ((*buf != '\0') && (*buf != ' ')
             && (*buf != '\t') && (*buf != '\n')) buf++;
    }
  *args='\0';
  return 1;
}


/*********************************************************************/
int findindex (char *name, char files[100][100], int nfiles)
{
  char buff[100];
  int i=0;

  while (name[i+1]!='$')
    {
      buff[i]=name[i+1];
      i++;
    }
  buff[i]='\0';
  
  for (i=0;i<nfiles;i++)
    if ( strcmp(buff,files[i]) == 0 ) return i;
  return 1;
}

