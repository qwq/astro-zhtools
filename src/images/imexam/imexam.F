#include <zhtools.h>

      program imexam
      implicit none
      character data*200
      integer iargc
      integer unit,nx,ny
      ZHDECL(real,pimg)
      ZHDECL(integer,pmask)
      character arg*200
      character mode(100)*30
      integer nmodes,i
      integer status
      logical defined

      if (iargc().lt.2) call zhhelp ('imexam')
      
c     call getarg(1,data)
      call set_default_par ('img','${ARGV[1]}')
      call get_command_line_par ('img',1,data)
      if (.not.defined(data)) then
        call zhhelp ('imexam')
      endif

c     call getarg(2,arg)
      call set_default_par ('modes','${ARGV[2]}')
      call get_command_line_par ('modes',1,arg)
      if (.not.defined(arg)) then
        call zhhelp ('imexam')
      endif

      if (arg.eq.'-') then ! read list from stdin
        nmodes = 0
        status = 0
        do while (status.eq.0)
          read (*,'(a)',iostat=status) mode(nmodes+1)
          if (status.eq.0) nmodes=nmodes+1
        enddo
      else
         call splitwords_char (arg,mode,nmodes,',')
      endif
      if (nmodes.le.0) then
        call zhhelp ('imexam')
      endif
      
      do i=1,nmodes
        if (
     ~      mode(i).ne.'mean'.and.
     ~      mode(i).ne.'errmean'.and.
     ~      mode(i).ne.'median'.and.
     ~      mode(i).ne.'dispersion'.and.
     ~      mode(i).ne.'stddev'.and.
     ~      mode(i).ne.'absdev'.and.
     ~      mode(i).ne.'square'.and.
     ~      mode(i).ne.'sumsquare'.and.
     ~      mode(i).ne.'sum'.and.
     ~      mode(i).ne.'max'.and.
     ~      mode(i).ne.'min'.and.
     ~      mode(i).ne.'value'.and.
     ~      mode(i).ne.'meancoords'.and.
     ~      mode(i).ne.'area'.and.
     ~      .true.) then
          write(0,*)'Modes available:'
          write(0,*)'   sum, mean, errmean, median, dispersion, stddev, absdev'
          write(0,*)'   square, sumsquare, max, min, value, meancoords, area'
          call exit(1)
        endif
      enddo
      
      call op_fits_img (data,unit,nx,ny)

      ZHMALLOC(pimg,nx*ny,'e')
      ZHMALLOC(pmask,nx*ny,'j')

      call do_imexam (data,mode,nmodes,unit,nx,ny,ZHVAL(pimg),ZHVAL(pmask))
      call exit(0)
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_imexam (data,mode,nmodes,unit,nx,ny,img,mask)
      implicit none
      integer nmodes
      character data*(*), mode(nmodes)*(*)
      integer unit,nx,ny
      character arg*200
      integer iargc,narg
      
      integer lnblnk
      
c image arrays
      real img(nx*ny)
      integer mask(nx*ny)
*#ifdef G77
*      real img(nx*ny)
*      integer mask(nx*ny)
*#else
*      real img(1000000000)
*      pointer (pimg,img)
*      integer mask(1000000000)
*      pointer (pmask,mask)
*      integer malloc
*#endif      

      
      logical clean
      real threshold
      real value
      
      character region*100,exclregion*100,format*100
      logical defined
      
      
      integer x,y,i,imode
      real xm,ym
      logical printit
      logical yespar
      integer maskmin,maskmax,maskvalue

      character buff*80
      
c-----------------------------------------------------------------      
      call get_command_line_par ('reg',1,region)
      if (.not.defined(region)) then
        region='none'
      endif
      
      call get_command_line_par ('exclreg',1,exclregion)
      if (.not.defined(exclregion)) then
        exclregion='none'
      endif

      call get_command_line_par ('format',1,format)
      
      call get_command_line_par ('clean',1,arg)
      if (arg.eq.'yes'.or.arg.eq.'y') then
        clean=.true.
      else
        clean=.false.
      endif
      
      call get_command_line_par ('threshold',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read(arg,*)threshold
      else
        threshold=3.0
      endif

*c Allocate memory
*#ifndef G77
*      pimg=malloc(4*nx*ny)
*      pmask=malloc(4*nx*ny)
*      if (pimg.eq.0.or.pmask.eq.0) then
*        write (0,*)'Not enough memory'
*        call exit (1)
*      endif
*#endif      
      
      if (region.eq.'none') then
        call mset (mask,nx*ny,1,'j')
      else
        call mset (mask,nx*ny,0,'j')
        call xreadmask (unit,mask,nx,ny,1,0,region)
      endif
      
      if (exclregion.ne.'none') then
        call mset (img,nx*ny,0,'j')
        call xreadmask (unit,img,nx,ny,1,0,exclregion)
        call mask_mask (mask,img,nx*ny)
      endif

      if (yespar('debug')) call saoimage (mask,nx,ny,'j')

      
c read image
      call read_fits_image_unit (unit,data,img,nx,ny,'e')
      
      if (yespar('printbyreg')) then
        maskmin = 1
        maskmax = 0
        do i=1,nx*ny
          maskmax = max(mask(i),maskmax)
        enddo
      else
        maskmin = 0
        maskmax = 0
      endif

      do maskvalue = maskmin, maskmax


        if (clean) then
          if (yespar('poisson')) then
            call clean_poisson_img (img,mask,nx*ny,threshold,maskvalue)
          else
            call clean_img (img,mask,nx*ny,threshold,maskvalue)
          endif
        endif
      
        if (maskvalue.ne.0) then
          write (*,'(i4,$)') maskvalue
        endif
        
        do imode=1,nmodes
          printit = .true.
          if (mode(imode).eq.'mean') then
            call img_mean (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'area') then
            call img_area (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'errmean') then
            call img_errmean (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'median') then
            call img_median (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'sum') then
            call img_sum (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'dispersion') then
            call img_disp (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'stddev') then
            call img_disp (img,mask,nx*ny,value,maskvalue)
            value = sqrt(max(value,0.0))
          else if (mode(imode).eq.'absdev') then
            call img_absdev (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'square') then
            call img_square (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'sumsquare') then
            call img_sumsquare (img,mask,nx*ny,value,maskvalue)
          else if (mode(imode).eq.'max') then
            call img_max (img,mask,nx*ny,value,x,maskvalue)
            y=(x-1)/nx+1
            x=x-(y-1)*nx
            write (buff,*) value,x,y
            write (*,'(2x,a,$)') buff(1:lnblnk(buff))
            printit = .false.
          else if (mode(imode).eq.'min') then
            call img_min (img,mask,nx*ny,value,x,maskvalue)
            y=(x-1)/nx+1
            x=x-(y-1)*nx
            write (buff,*) value,x,y
            write (*,'(2x,a,$)') buff(1:lnblnk(buff))
            printit = .false.
          else if (mode(imode).eq.'value') then
            narg=iargc()
c           call getarg(3,arg)
            call set_default_par ('x','${ARGV[3]}')
            call get_command_line_par ('x',1,arg)
            if (.not.defined(arg)) then
               call zhhelp ('imexam')
            endif
            read(arg,*)x
c           call getarg(4,arg)
            call set_default_par ('y','${ARGV[4]}')
            call get_command_line_par ('y',1,arg)
            if (.not.defined(arg)) then
               call zhhelp ('imexam')
            endif
            read(arg,*)y
            value = img(x+nx*(y-1))
            if (defined(format)) then
              print format(1:lnblnk(format)),dble(value)
            else
              print *,value
            endif
            call exit(0)
          else if (mode(imode).eq.'meancoords') then
            call mean_coords (img,mask,nx,ny,xm,ym,maskvalue)
            write (buff,*) xm,ym
            write (*,'(2x,a,$)') buff(1:lnblnk(buff))
            printit = .false.
          else
            call exit(1)
          endif

          if (printit) then
            write (buff,*) value
            write (*,'(2x,a,$)') buff(1:lnblnk(buff))
          endif
        enddo
        print*
      enddo
      call exit(0)
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_mean (img,mask,n,mean,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean
      integer maskvalue
      real*8 sum
      integer i,npix
      logical qreg,useit
      
      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)
        endif
      enddo
      
      if (npix.gt.0) then
        mean=sum/npix
      else
        mean=0
      endif

      return
      end
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_area (img,mask,n,mean,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean
      integer maskvalue
      real*8 sum
      integer i,npix
      logical qreg,useit
      
      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)
        endif
      enddo
      
      mean = npix

      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_median (img,mask,n,mean,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean,mean1,select
      integer maskvalue
      integer i,npix
      logical qreg,useit
      real swap,iswap
      
      qreg = maskvalue.eq.0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          swap = img(npix)
          iswap = mask(npix)
          img(npix)=img(i)
          mask(npix)=mask(i)
          img(i)=swap
          mask(i)=iswap
        endif
      enddo
      
      if (npix.eq.0) then
        mean=0.0
      else if (npix.eq.1) then
        mean=img(1)
      else
*        call sort (npix,img)
*        if ((npix/2)*2.eq.npix) then
*          mean=0.5*(img(npix/2)+img(npix/2+1))
*        else
*          mean=img(npix/2+1)
*        endif
        mean1=select(npix/2+1,npix,img)
        if ((npix/2)*2.eq.npix) then
          call max_ (img,npix/2,mean)
          mean=0.5*(mean+mean1)
        else
          mean=mean1
        endif

      endif

      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_sum (img,mask,n,mean,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean
      real*8 sum
      integer maskvalue
      integer i,npix
      logical qreg,useit

      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)
        endif
      enddo
      
      mean=sum
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_square (img,mask,n,square,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real square
      real*8 sum
      integer maskvalue
      integer i,npix
      logical qreg,useit
      
      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)**2
        endif
      enddo
      
      if (npix.gt.0) then
        square=sum/npix
      else
        square=0
      endif

      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_sumsquare (img,mask,n,square,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real square
      real*8 sum
      integer maskvalue
      integer i,npix
      logical qreg,useit

      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)**2
        endif
      enddo
      
      square = sum
      
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_disp (img,mask,n,disp,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean,disp
      real*8 sum
      integer maskvalue
      integer i,npix
      logical qreg,useit

      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)
        endif
      enddo

      if (npix.eq.0) then
        disp=0
        return
      endif

      mean=sum/npix
      
      sum=0.0d0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          sum=sum+(img(i)-mean)**2
        endif
      enddo
      
      disp=sum/npix
      
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_errmean (img,mask,n,disp,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean,disp
      real*8 sum
      integer maskvalue
      integer i,npix
      logical qreg,useit

      qreg = maskvalue.eq.0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)
        endif
      enddo
      
      if (npix.eq.0) then
        disp=0
        return
      endif

      mean=sum/npix
      
      sum=0.0d0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          sum=sum+(img(i)-mean)**2
        endif
      enddo
      
      disp=dsqrt(sum)/npix
      
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_absdev (img,mask,n,absdev,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real mean,absdev
      real*8 sum
      integer maskvalue
      integer i,npix
      logical qreg,useit

      qreg = maskvalue .eq. 0
      sum=0.0d0
      npix=0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          npix=npix+1
          sum=sum+img(i)
        endif
      enddo
      
      if (npix.eq.0) then
        absdev=0
        return
      endif

      mean=sum/npix
      
      sum=0.0d0
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          sum=sum+abs(img(i)-mean)
        endif
      enddo
      
      absdev=sum/npix
      
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_max (img,mask,n,absdev,ii,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real absdev
      integer maskvalue
      real sum
      integer i
      integer ii
      logical qreg,useit

      qreg = maskvalue.eq.0

      sum=-1.0e10
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          if (img(i).gt.sum) then
            sum=img(i)
            ii=i
          endif
        endif
      enddo
      
      absdev=sum
      
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine img_min (img,mask,n,absdev,ii,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real absdev
      real sum
      integer i,ii
      integer maskvalue
      logical qreg,useit
      
      qreg = maskvalue .eq. 0
      sum=1.0e10
      do i=1,n
        if (qreg) then
          useit = mask(i).ne.0
        else
          useit = mask(i).eq.maskvalue
        endif
        if (useit) then
          if (img(i).lt.sum) then
            ii=i
            sum=img(i)
          endif
        endif
      enddo
      
      absdev=sum
      
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine mean_coords (img,mask,nx,ny,xm,ym,maskvalue)
      implicit none
      integer nx,ny
      real img(nx,ny)
      integer mask(nx,ny)
      real xm,ym
      integer i,j
      real sum
      integer maskvalue
      logical qreg,useit
      
      qreg = maskvalue.eq.0
      sum=0
      xm=0
      ym=0
      
      do i=1,nx
        do j=1,ny
          if (qreg) then
            useit = mask(i,j).ne.0
          else
            useit = mask(i,j).eq.maskvalue
          endif
          if (useit) then
            xm=xm+i*img(i,j)
            ym=ym+j*img(i,j)
            sum=sum+img(i,j)
          endif
        enddo
      enddo
      
      if (sum.gt.0) then
        xm=xm/sum
        ym=ym/sum
      endif
      
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine clean_img (img,mask,n,threshold,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real threshold
      integer maskvalue
      
      real*8 mean,disp
      integer npix
      integer i
      real mean4
      real t
      integer iter
      
      logical qreg,useit
      
      qreg = maskvalue .eq. 0

      do iter=1,3
        mean=0.0d0
        disp=0.0d0
        npix=0
        do i=1,n
          if (qreg) then
            useit = mask(i).ne.0
          else
            useit = mask(i).eq.maskvalue
          endif
          if (useit) then
            mean=mean+img(i)
            disp=disp+img(i)**2
            npix=npix+1
          endif
        enddo
        
        mean = mean/npix
        disp = dsqrt(disp/npix-mean**2)
        
        mean4 = mean
        t = threshold*disp
        
        do i=1,n
          if (qreg) then
            useit = mask(i).ne.0
          else
            useit = mask(i).eq.maskvalue
          endif
          if (useit) then
            if (abs(img(i)-mean4).gt.t) mask(i)=0
          endif
        enddo
        
      enddo
      
      return
      end
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine clean_poisson_img (img,mask,n,threshold,maskvalue)
      implicit none
      real img(*)
      integer mask(*)
      integer n
      real threshold
      integer maskvalue
      
      real*8 mean
      integer npix
      integer i
      real mean4
      real t
      integer iter
      
      logical qreg,useit
      
      qreg = maskvalue .eq. 0

      do iter=1,3
        mean=0.0d0
        npix=0
        do i=1,n
          if (qreg) then
            useit = mask(i).ne.0
          else
            useit = mask(i).eq.maskvalue
          endif
          if (useit) then
            mean=mean+img(i)
            npix=npix+1
          endif
        enddo
        
        mean = mean/npix
        
        mean4 = mean
        t = threshold*(1+sqrt(mean+0.75))
        
        do i=1,n
          if (qreg) then
            useit = mask(i).ne.0
          else
            useit = mask(i).eq.maskvalue
          endif
          if (useit) then
            if (abs(img(i)-mean4).gt.t) mask(i)=0
          endif
        enddo
        
      enddo
      
      return
      end
