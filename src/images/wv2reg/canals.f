c This program should be called on relatively smooth islands.
c make a cond image, then imsmo it whith sigma=1 and than mkcond 
c image .gt. 0.6
      

      subroutine canals (class,nx,ny,border,xborder,yborder,d, dmin, anybuild)
      implicit none
      integer nx,ny
      integer class(nx,ny)
      integer border(nx,ny)
      integer xborder(*), yborder(*), nborder
      real d(*)
      logical anybuild
      integer nclass,iclass
      integer i,j,ii,jj
      integer it
      real r
      integer idistance,nb2,bd
      
      integer xcom(8),ycom(8),icom,jcom
      data xcom / -1,  1,  0,  0, -1,  1,  1, -1/
      data ycom /  0,  0, -1,  1,  1, -1,  1, -1/
      logical not found, found next
      integer nother
      
      integer xstart(1000),ystart(1000)
      integer xknot(1000),yknot(1000),nknots
      integer nborderclass(1000)
      real dknot(1000)
      integer im1,ip1,j0
      real x1,x2,y1,y2,dist,t
      real dmean
      real dmin
      
      logical not left bottom corner 
      logical not right bottom corner
      logical not right upper corner
      logical not left upper corner

C     integer icall /0/
      integer icall
      save icall
      data icall /0/
      character*200 filetosave
      character*1 affix
      logical defined
      integer unit,newunit
      
      anybuild=.false.
      icall=icall+1

      do i=1,nx
        do j=1,ny
          border(i,j)=0
        enddo
      enddo

      nclass=0
      found next = .true.
      do while (found next)
        found next = .false.
       
                                ! ������� "����������"      
        do i=2,nx-1
          do j=2,ny-1
            if (class(i,j).gt.0) then
              iclass=class(i,j)
              nclass=max(nclass,iclass)
              do icom=1,8
                ii=i+xcom(icom)
                jj=j+ycom(icom)
                if (class(ii,jj).ne.iclass) then
                  if (ii.gt.1.and.ii.lt.nx.and.jj.gt.1.and.jj.lt.ny) then
                    nother=0
                    do jcom=1,4
                      if (class(ii+xcom(jcom),jj+ycom(jcom)).ne.iclass) then
                        nother=nother+1
                      endif
                    enddo
                    if (nother.lt.2) then
*                      print*,'%%%%%%%%',ii,jj,nother
                      class(ii,jj)=iclass
                      found next = .true.
                    endif
                  endif
                endif
              enddo
            endif
          enddo
        enddo
                                ! ������� "���������"      
        do i=2,nx-1
          do j=2,ny-1
            if (class(i,j).gt.0) then
              iclass=class(i,j)
              if      (
     ~            class(i-1,j).ne.iclass .or.
     ~            class(i,j+1).ne.iclass .or.
     ~            class(i+1,j).ne.iclass .or.
     ~            class(i,j-1).ne.iclass ) then
                
                nother=0
                do jcom=1,4
                  if (class(i+xcom(jcom),j+ycom(jcom)).eq.iclass) then
                    nother=nother+1
                  endif
                enddo
                if (nother.gt.1) then
*                  
*                  if (class(i-1,j).eq.0.and.class(i+1,j).eq.0) then
*                    class(i-1,j)=iclass
*                    class(i+1,j)=iclass
**                    print*,'$$$$$$$',i,j
*                    found next = .true.
*                  endif
*                  
*                  if (class(i,j-1).eq.0.and.class(i,j+1).eq.0) then
*                    class(i,j-1)=iclass
*                    class(i,j+1)=iclass
**                    print*,'$$$$$$$',i,j
*                    found next = .true.
*                  endif
*                  if (class(i-1,j-1).eq.0.and.class(i+1,j+1).eq.0) then
*                    class(i-1,j-1)=iclass
*                    class(i+1,j+1)=iclass
**                    print*,'$$$$$$$',i,j
*                    found next = .true.
*                  endif
*                  if (class(i+1,j-1).eq.0.and.class(i-1,j+1).eq.0) then
*                    class(i+1,j-1)=iclass
*                    class(i-1,j+1)=iclass
**                    print*,'$$$$$$$',i,j
*                    found next = .true.
*                  endif
*                  

                                ! ��������, ����� ��� ����� ����� ������,
                                ! ������ ���������
                  if ( not left bottom corner (class,nx,ny,i,j,iclass) ) then
                    if ( not right bottom corner (class,nx,ny,i,j,iclass) ) then
                      if ( not right upper corner (class,nx,ny,i,j,iclass) ) then
                        if ( not left upper corner (class,nx,ny,i,j,iclass) ) then
                          do jcom=1,8
                            if (class(i+xcom(jcom),j+ycom(jcom)).ne.iclass)
     ~                          then
                              class(i+xcom(jcom),j+ycom(jcom))=iclass
                              print*,'$$$$$$$',i,j
                              found next = .true.
                            endif
                          enddo
                        endif
                      endif
                    endif
                  endif
                  
                else
                  class(i,j)=0  ! ������ "�������"
                endif
              endif
            endif
          enddo
        enddo
      enddo
      
      do iclass=1,nclass
        nborderclass(iclass)=0
      enddo

      do i=2,nx-1
        do j=2,ny-1
          if (class(i,j).gt.0) then
            iclass=class(i,j)
            if      (class(i-1,j).ne.iclass) then
              border(i,j)=1
              nborderclass(iclass)=nborderclass(iclass)+1
              xstart(iclass)=i
              ystart(iclass)=j
            else if (class(i,j+1).ne.iclass) then
              border(i,j)=1
              nborderclass(iclass)=nborderclass(iclass)+1
              xstart(iclass)=i
              ystart(iclass)=j
            else if (class(i+1,j).ne.iclass) then
              border(i,j)=1
              nborderclass(iclass)=nborderclass(iclass)+1
              xstart(iclass)=i
              ystart(iclass)=j
            else if (class(i,j-1).ne.iclass) then
              border(i,j)=1
              nborderclass(iclass)=nborderclass(iclass)+1
              xstart(iclass)=i
              ystart(iclass)=j
            endif
          endif
        enddo
      enddo

      do iclass=1,nclass
                                ! start
        nborder=1
        xborder(1)=xstart(iclass)
        yborder(1)=ystart(iclass)
        border(xborder(nborder),yborder(nborder))=nborder+1
        
        ii = xborder(nborder)
        jj = yborder(nborder)
        not found = .true.
        icom = 1
        do while (icom.le.8 .and. not found )
          if (border(ii+xcom(icom),jj+ycom(icom)).eq.1) then
            if (class(ii+xcom(icom),jj+ycom(icom)).eq.iclass) then
              not found = .false.
              nborder=nborder+1
              xborder(nborder)=ii+xcom(icom)
              yborder(nborder)=jj+ycom(icom)
              border(xborder(nborder),yborder(nborder))=nborder+1
            endif
          endif
          icom=icom+1
        enddo
        if (nborder.eq.1) then
          write(0,*)'!!!! error while starting for class',iclass
          write(0,*)ii,jj

*          call getenv ("DATASET",filetosave)
*          call strcat (filetosave,"canals.error")
*          unit=newunit()
*          open(unit,file=filetosave,access="append")
*          write(unit,*)'!!!! error while starting for class',iclass
*          write(unit,*)ii,jj
*          close(unit)
          
          foundnext = .false.
          
        else
          found next = .true.
        endif
        
        do while (found next)
          found next = .false.
          ii = xborder(nborder)
          jj = yborder(nborder)
          not found = .true.
          icom = 1
          do while (icom.le.8 .and. not found )
            if (border(ii+xcom(icom),jj+ycom(icom)).eq.1) then
              if (class(ii+xcom(icom),jj+ycom(icom)).eq.iclass) then
                i=ii+xcom(icom)
                j=jj+ycom(icom)
                not found = .false.
                found next = .true.
                nborder=nborder+1
                xborder(nborder)=i
                yborder(nborder)=j
                border(xborder(nborder),yborder(nborder))=nborder+1
              endif
            endif
            icom=icom+1
          enddo
        enddo
        
        if (nborder.ne.nborderclass(iclass)) then

          write(0,*)' Error while ordering border for class',iclass
*          call getenv ("DATASET",filetosave)
*          call strcat (filetosave,"canals.error")
*          unit=newunit()
*          open(unit,file=filetosave,access="append")
*          write(unit,*)' Error while ordering border for class',iclass,
*     ~        ' in call', icall
*          close(unit)
          
        else
          
          nb2 = nborder/2
          if (nb2*2.ne.nborder) then
            nb2=nborder/2+1
          endif
          
          do i=1,nborder
            d(i)=1.0
            do j=1,nborder
              if (i.ne.j) then
                r=idistance(xborder(i),yborder(i),xborder(j),yborder(j))
                bd = iabs(i-j)
                if (bd.gt.nb2) then
                  bd = nb2 + (nb2-bd)
                endif
                
                if (bd.le.0) then
                  write(0,*)'Error with bd'
                  pause
                endif
                d(i)=min(d(i),sqrt(r)/bd)
              endif
            enddo
          enddo
          
          dmean=0
          do i=1,nborder
            dmean=dmean+d(i)
          enddo
          dmean=dmean/nborder
          do i=1,nborder
            d(i)=d(i)/dmean
          enddo
          
          nknots=0
          
          do i=1,nborder
            if (i.gt.1.and.i.lt.nborder) then
              im1=i-1
              ip1=i+1
            else if (i.eq.1) then
              im1=nborder
              ip1=2
            else 
              im1=nborder-1
              ip1=1
            endif
            if (d(i).lt.dmin) then
              if (d(i).lt.d(ip1).and.d(i).lt.d(im1)) then
                nknots=nknots+1
                xknot(nknots)=xborder(i)
                yknot(nknots)=yborder(i)
                dknot(nknots)=d(i)
              endif
            endif
          enddo
          
        

          if (nknots.gt.0) then
            
                                ! will build channels!!
            
            if ((nknots/2)*2.ne.nknots) then
              write(0,*)"WARNING: Nknots �� ������ 2 ��� ������",iclass,' ('
     ~            ,nknots,' )'
            endif
            
            do i=1,nknots
              j0=0
              do j=1,nknots     ! find the nearest knot
                if (i.ne.j) then
                  if (dknot(i).eq.dknot(j)) then
                    j0=j
                  endif
                endif
              enddo
              j=j0
              
              if (j.eq.0) then
                write(0,*)'Warning: cound not find pair to the knot',xknot(i)
     ~              ,yknot(i),'  of class',iclass
              else
                                ! build "channel"
                x1=xknot(i)
                y1=yknot(i)
                x2=xknot(j)
                y2=yknot(j)
                
                dist = sqrt((x1-x2)**2+(y1-y2)**2)
                anybuild=.true.
C               do t=0.0,dist+1.0,0.25
                do it=0,int((dist+1)*4)
                  t = it/4.0
                  ii=nint(x1+min(t,dist)*(x2-x1)/dist)
                  jj=nint(y1+min(t,dist)*(y2-y1)/dist)
                  class(ii,jj)=-1
                  class(ii-1,jj)=-1
                  class(ii+1,jj)=-1
                  class(ii,jj-1)=-1
                  class(ii,jj+1)=-1
                enddo
              endif
            enddo
          endif
        
          do i=1,nborder
            border(xborder(i),yborder(i))=100*d(i)
          enddo
          
          
        endif
        
      enddo


      call get_command_line_par ("saveborder",1,filetosave)
      if (defined(filetosave)) then
        write(affix,'(i1)') icall
        call strcat(filetosave,".")
        call strcat(filetosave,affix)
        call write_fits_image (filetosave,border,nx,ny,'j','j')
      endif

      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      logical function not left bottom corner (img,nx,ny,i,j,value)
      implicit none
      integer i,j,nx,ny,value
      integer img(nx,ny)
      
      if (img(i,j+1).ne.value) then
        not left bottom corner = .true.
      else if (img(i+1,j).ne.value) then
        not left bottom corner = .true.
      else if (img(i+1,j+1).ne.value) then
        not left bottom corner = .true.
      else
        not left bottom corner = .false.
      endif
      return
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      logical function not right bottom corner (img,nx,ny,i,j,value)
      implicit none
      integer i,j,nx,ny,value
      integer img(nx,ny)
      
      if (img(i,j+1).ne.value) then
        not right bottom corner = .true.
      else if (img(i-1,j).ne.value) then
        not right bottom corner = .true.
      else if (img(i-1,j+1).ne.value) then
        not right bottom corner = .true.
      else
        not right bottom corner = .false.
      endif
      return
      end
      
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      logical function not right upper corner (img,nx,ny,i,j,value)
      implicit none
      integer i,j,nx,ny,value
      integer img(nx,ny)
      
      if (img(i,j-1).ne.value) then
        not right upper corner = .true.
      else if (img(i-1,j).ne.value) then
        not right upper corner = .true.
      else if (img(i-1,j-1).ne.value) then
        not right upper corner = .true.
      else
        not right upper corner = .false.
      endif
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      logical function not left upper corner (img,nx,ny,i,j,value)
      implicit none
      integer i,j,nx,ny,value
      integer img(nx,ny)
      
      if (img(i,j-1).ne.value) then
        not left upper corner = .true.
      else if (img(i+1,j).ne.value) then
        not left upper corner = .true.
      else if (img(i+1,j-1).ne.value) then
        not left upper corner = .true.
      else
        not left upper corner = .false.
      endif
      return
      end
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine remove_canals (class,n,m)
      implicit none
      integer n,m
      integer class(n,m)
      integer i,j,iclass
      integer icom,xcom(16),ycom(16)
      data xcom /-2, -2, -2, -1, 0, 1, 2, 2, 2,  2,  2,  1,  0, -1, -2, -2/
      data ycom / 0,  1,  2,  2, 2, 2, 2, 1, 0, -1, -2, -2, -2, -2, -2, -1/


      do i=3,n-3
        do j=3,m-3
          if (class(i,j).lt.0) then ! THIS IS A CANAL
            do icom=1,16
              iclass=class(i+xcom(icom),j+ycom(icom))
              if (iclass.gt.0) then
                class(i,j)=iclass
                goto 1
              endif
            enddo
 1          continue
          endif
        enddo
      enddo
      
      return
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
       
      subroutine mkcond_i (img,rel,val,map,n)
      implicit none
      integer n
      integer img(*),val
      character*(*) rel
      logical map(*)
      integer i
      
      if      (rel.eq.'.eq.') then
        do i=1,n
          map(i) = img(i) .eq. val
        enddo
      else if (rel.eq.'.gt.') then
        do i=1,n
          map(i) = img(i) .gt. val
        enddo
      else if (rel.eq.'.ge.') then
        do i=1,n
          map(i) = img(i) .ge. val
        enddo
      else if (rel.eq.'.le.') then
        do i=1,n
          map(i) = img(i) .le. val
        enddo
      else if (rel.eq.'.lt.') then
        do i=1,n
          map(i) = img(i) .lt. val
        enddo
      else if (rel.eq.'.ne.') then
        do i=1,n
          map(i) = img(i) .ne. val
        enddo
      else
        write(0,*)'Cannot interpret relation: ',rel
        call exit(1)
      endif
      
      return
      end


      subroutine mkcond (img,rel,val,map,n)
      implicit none
      integer n
      real img(*),val
      character*(*) rel
      logical map(*)
      integer i
      
      if      (rel.eq.'.eq.') then
        do i=1,n
          map(i) = img(i) .eq. val
        enddo
      else if (rel.eq.'.gt.') then
        do i=1,n
          map(i) = img(i) .gt. val
        enddo
      else if (rel.eq.'.ge.') then
        do i=1,n
          map(i) = img(i) .ge. val
        enddo
      else if (rel.eq.'.le.') then
        do i=1,n
          map(i) = img(i) .le. val
        enddo
      else if (rel.eq.'.lt.') then
        do i=1,n
          map(i) = img(i) .lt. val
        enddo
      else if (rel.eq.'.ne.') then
        do i=1,n
          map(i) = img(i) .ne. val
        enddo
      else
        write(0,*)'Cannot interpret relation: ',rel
        call exit(1)
      endif
      
      return
      end

      subroutine logical2real (l,x,n)
      implicit none
      logical l(*)
      real x(*)
      integer n
      
      integer i
      
      do i=1,n
        if (l(i)) then
          x(i)=1.0
        else
          x(i)=0.0
        endif
      enddo
      
      return
      end
