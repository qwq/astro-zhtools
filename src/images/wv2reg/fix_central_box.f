      implicit none
      
      integer nx,ny
      integer pimg1,pimg2, pimg3
      character inputname*200, outname*200, img2name*200
      integer unit
      logical defined


c get file names
      call get_cl_par ('i',inputname)
      if (.not.defined(inputname)) call exiterror ('i=???')
      call get_cl_par ('i0',img2name)
      if (.not.defined(img2name)) call exiterror ('i0=???')
        
      call get_cl_par ('o',outname)
      if (.not.defined(outname)) call exiterror ('o=???')
      

c Open first image and allocate memory
      call op_fits_img (inputname,unit,nx,ny)
      call for_malloc (pimg1,4*nx*ny,'e')
      call for_malloc (pimg2,4*nx*ny,'e')
      call for_malloc (pimg3,4*nx*ny,'j')
      if (pimg1.eq.0.or.pimg2.eq.0) call exiterror ('Not enough memory')
      
      call read_fits_image_unit (unit,inputname,%val(pimg1),nx,ny,'e')
      call read_fits_image (img2name,%val(pimg3),nx,ny,'j')

c Do proc.
      call do_fix_central_box (%val(pimg1),%val(pimg2),%val(pimg3),nx,ny)

      call write_fits_image (outname,%val(pimg2),nx,ny,'e','e')

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_fix_central_box (img,oimg,mask,nx,ny)
      implicit none
      integer nx,ny
      real img(nx,ny), oimg(nx,ny)
      integer mask(nx,ny)

      integer i,j,ii,jj,bb,b

      real mean(0:1000)
      integer np(0:1000)
      
      real sum, ww, w
      integer npix

      integer xborder(100000), yborder(100000), nborder

      integer iter, ib

      do i=1,nx
        do j=1,ny
          if (mask(i,j).eq.0) then
            mask(i,j)=1
          else
            mask(i,j)=0
          endif
        enddo
      enddo

      if (mask(nx/2+1,ny/2+1).eq.1) then
        call ini flood fill (mask,nx,ny,1,4)
        call floodfill (nx/2+1,ny/2+1,-1,1)
        do i=1,nx
          do j=1,ny
            if (mask(i,j).lt.0) then
              mask(i,j)=1
            else
              mask(i,j)=0
            endif
          enddo
        enddo

c extend this region by 2 pixels
        do iter=1,2
          do i=2,nx-1
            do j=2,ny-1
              if (mask(i,j).eq.0.and.
     ~            (
     ~            mask(i-1,j-1).eq.1 .or.
     ~            mask(i-1,j).eq.1 .or.
     ~            mask(i-1,j+1).eq.1 .or.
     ~            mask(i,j-1).eq.1 .or.
     ~            mask(i,j+1).eq.1 .or.
     ~            mask(i+1,j-1).eq.1 .or.
     ~            mask(i+1,j).eq.1 .or.
     ~            mask(i+1,j+1).eq.1 ) ) then
                mask(i,j)=-1
              endif
            enddo
          enddo
          do i=1,nx
            do j=1,ny
              if (mask(i,j).lt.0) then
                mask(i,j)=1
              endif
            enddo
          enddo
        enddo
        
        nborder = 0
        do i=2,nx-1
          do j=2,ny-1
            if (mask(i,j).eq.1.and.
     ~          (
     ~          mask(i-1,j-1).eq.0 .or.
     ~          mask(i-1,j).eq.0 .or.
     ~          mask(i-1,j+1).eq.0 .or.
     ~          mask(i,j-1).eq.0 .or.
     ~          mask(i,j+1).eq.0 .or.
     ~          mask(i+1,j-1).eq.0 .or.
     ~          mask(i+1,j).eq.0 .or.
     ~          mask(i+1,j+1).eq.0 ) ) then
              nborder = nborder + 1
              xborder(nborder)=i
              yborder(nborder)=j
              mask(i,j)=-1
            endif
          enddo
        enddo
        
        do i=1,nx
          do j=1,ny
            oimg(i,j) = img(i,j)
          enddo
        enddo
        do i=2,nx-1
          do j=2,ny-1
            if (mask(i,j).eq.1) then
              sum = 0
              w = 0
              do ib=1,nborder
                ww = 1.0/((i-xborder(ib))**2+(j-yborder(ib))**2)
                sum = sum + img(xborder(ib),yborder(ib))*ww
                w = w + ww
              enddo
              oimg(i,j)=sum/w
            endif
          enddo
        enddo
              
      else                      ! no central zero
              

        do bb=0,20
          sum = 0
          npix = 0
          do ii=nx/2+1-bb,nx/2+1+bb
            do jj=nx/2+1-bb,nx/2+1+bb
              sum = sum + img(ii,jj)
              npix = npix + 1
            enddo
          enddo
          mean(bb) = sum
          np(bb) = npix
          if (bb.gt.0) then
            print*,bb,(mean(bb)-mean(bb-1))/(np(bb)-np(bb-1))
          endif
        enddo
        
        do bb = 20,1,-1
          mean(bb) = (mean(bb)-mean(bb-1))/(np(bb)-np(bb-1))
        enddo
        
c detect a sudden drop :
        b = 0
        do bb = 18,0,-1
          if (mean(bb).lt.0.01*mean(bb+1)) then
            b = bb+1
            goto 782
          endif
        enddo
 782    continue
        
        do i=1,nx
          do j=1,ny
            oimg(i,j) = img(i,j)
          enddo
        enddo
        
c fix data in this box
        do ii=nx/2+1-b,nx/2+1+b
          do jj=ny/2+1-b,ny/2+1+b
            sum = 0
            w = 0
            
            i = nx/2+1-b-1
            do j=ny/2+1-b-1,ny/2+1+b+1
              ww = 1.0/((i-ii)**2+(j-jj)**2)
              w = w + ww
              sum = sum + img(i,j)*ww
            enddo
            
            i = nx/2+1+b+1
            do j=ny/2+1-b-1,ny/2+1+b+1
              ww = 1.0/((i-ii)**2+(j-jj)**2)
              w = w + ww
              sum = sum + img(i,j)*ww
            enddo
            
            j = ny/2+1-b-1
            do i=nx/2+1-b-1,nx/2+1+b+1
              ww = 1.0/((i-ii)**2+(j-jj)**2)
              w = w + ww
              sum = sum + img(i,j)*ww
            enddo
            
            j = ny/2+1+b+1
            do i=nx/2+1-b-1,nx/2+1+b+1
              ww = 1.0/((i-ii)**2+(j-jj)**2)
              w = w + ww
              sum = sum + img(i,j)*ww
            enddo
            
            oimg(ii,jj) = sum / w
            
          enddo
        enddo
        
      endif

      return
      end

      
