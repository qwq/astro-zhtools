      subroutine water_fill (img,class,wsp,wsp2,nx,ny,i0,j0)
      implicit none
      integer nx,ny
      real img(nx,ny)
      integer class(nx,ny), wsp(nx,ny)
      real wsp2(nx*ny)

      integer i0,j0

      integer xmin,xmax,ymin,ymax
      integer xmin0,xmax0,ymin0,ymax0
      real v0
      
      integer np,np0
      integer i,j, ii, jj, ip
      logical if_peak
      integer iclass

      integer npeakmax, npeak
      parameter (npeakmax=10000)
      integer xpeak(npeakmax), ypeak(npeakmax)
      real vpeak (npeakmax)
      integer indpeak (npeakmax)
      integer ipeak, ipeak1
      

      
c First class, find all peaks in this island, sort their values
      do i=1,nx
        do j=1,ny
          if (img(i,j).gt.0.0.and.class(i,j).eq.0) then
            wsp(i,j)=1
          else
            wsp(i,j)=0
          endif
        enddo
      enddo

      call ini flood fill (wsp,nx,ny,1,4)
      call floodfill (i0,j0,-1,1)
      
      xmin0 = nx
      xmax0 = 1
      ymin0 = ny
      ymax0 = 1
      
      npeak = 0
      do i=1,nx
        do j=1,ny
          if (wsp(i,j).eq.-1) then
            xmin0 = min(xmin0,i)
            xmax0 = max(xmax0,i)
            ymin0 = min(ymin0,j)
            ymax0 = max(ymax0,j)
            if (i.gt.1.and.i.lt.nx.and.j.gt.1.and.j.lt.ny) then
              if (if_peak(img,nx,ny,i,j)) then
                npeak = npeak + 1
                xpeak(npeak) = i
                ypeak(npeak) = j
                vpeak(npeak) = img(i,j)
              endif
            endif
          endif
        enddo
      enddo
*      call saoimage (wsp,nx,ny,'j')

      if (npeak.gt.1) then
        call indexx (npeak,vpeak,indpeak)
      else
        if (npeak.eq.0) then ! weird but possible case
          iclass = 0
        else
          iclass = xpeak(1) + (ypeak(1)-1)*nx
        endif
        do i=xmin0,xmax0
          do j=ymin0,ymax0
            if (wsp(i,j).eq.-1) then
              class(i,j)=iclass
            endif
          enddo
        enddo
        goto 782
      endif

      do ipeak1 = 1,npeak
        ipeak = indpeak(ipeak1)
        iclass = xpeak(ipeak) + (ypeak(ipeak)-1)*nx
        
c identify its island
        do i=xmin0,xmax0
          do j=ymin0,ymax0
            if (img(i,j).gt.0.0.and.class(i,j).eq.0) then
              wsp(i,j)=1
            else
              wsp(i,j)=0
            endif
          enddo
        enddo
        
        call ini flood fill (wsp,nx,ny,1,4)
        call floodfill (xpeak(ipeak),ypeak(ipeak),-1,1)
        
        if (ipeak1.eq.npeak) then ! this is the last, brightest maximum
          goto 781
        endif

        xmin = nx
        xmax = 1
        ymin = ny
        ymax = 1
        np = 0
        do i=xmin0,xmax0
          do j=ymin0,ymax0
            if (wsp(i,j).eq.-1) then
              np = np + 1
              if (i.eq.xpeak(ipeak).and.j.eq.ypeak(ipeak)) np0 = np
              wsp2(np) = img(i,j)
              xmin = min(xmin,i)
              xmax = max(xmax,i)
              ymin = min(ymin,j)
              ymax = max(ymax,j)
            endif
          enddo
        enddo
        v0 = wsp2(np0)
        
        if(np.gt.1) then
          call sort (np,wsp2)
        else
          class(xpeak(ipeak),ypeak(ipeak))=iclass
          goto 784
        endif

c start from the next-largest pixel and find the level when the basin 
c will contain another maximum

        do ip=np,1,-1
          if (wsp2(ip).lt.v0) then
            do i=xmin,xmax
              do j=ymin,ymax
                if (img(i,j).ge.wsp2(ip).and.class(i,j).eq.0) then
                  wsp(i,j)=1
                else
                  wsp(i,j)=0
                endif
              enddo
            enddo
            call ini flood fill (wsp,nx,ny,1,4)
            call floodfill (xpeak(ipeak),ypeak(ipeak),-1,1)

            do i=xmin,xmax
              do j=ymin,ymax
                if (wsp(i,j).eq.-1 .and.
     ~              i.gt.1.and.i.lt.nx.and.i.ne.xpeak(ipeak) .and.
     ~              j.gt.1.and.j.lt.nx.and.j.ne.ypeak(ipeak) ) then
                  if (if_peak(img,nx,ny,i,j)) then ! there is a secondary
                                ! peak in this region
                    do ii=xmin,xmax
                      do jj=ymin,ymax
                        if (img(ii,jj).ge.wsp2(ip+1).and.class(ii,jj).eq.0)
     ~                      then
                          wsp(ii,jj)=1
                        else
                          wsp(ii,jj)=0
                        endif
                      enddo
                    enddo
                    call ini flood fill (wsp,nx,ny,1,4)
                    call floodfill (xpeak(ipeak),ypeak(ipeak),-1,1)
                    do ii=xmin,xmax
                      do jj=ymin,ymax
                        if (wsp(ii,jj).eq.-1) then
                          class(ii,jj)=iclass
                        endif
                      enddo
                    enddo
                    goto 784
                  endif
                endif
              enddo
            enddo
          endif
        enddo

 781    continue
c We're here if it's the only max in the island
        do ii=xmin,xmax
          do jj=ymin,ymax
            if (wsp(ii,jj).eq.-1) then
              class(ii,jj)=iclass
            endif
          enddo
        enddo
        
 784    continue
      enddo
 782  continue

      return
      end

