*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine classify1 (nclass,class,cond,img,brightnessmin,n,m)
      implicit none
      integer n,m
      integer nclass
      integer class(n*m)
      integer cond(n*m)
      real img(n*m), brightnessmin
      integer ii
      logical if_peak
      
      integer i,j
      
      call ini flood fill (cond,n,m,1,4) ! use 4-connect

      nclass=1
      do i=2,n-1
        do j=2,m-1
          ii=i+(j-1)*n
          if (cond(ii).gt.0) then
            if (img(ii).gt.brightnessmin) then
              if (if_peak(img,n,m,i,j)) then
                nclass=nclass+1
                call floodfill (i,j,-nclass,1)
              endif 
            endif
          endif
        enddo
      enddo

      do i=1,n*m
        if (cond(i).lt.0) then
          class(i) = -cond(i)-1
        endif
      enddo
      return
      end
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine set to zero when positive (x,n)
      implicit none
      integer n
      integer x(n)
      integer i
      
      do i=1,n
        if (x(i).gt.0) x(i)=0
      enddo
      return
      end
