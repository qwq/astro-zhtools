      real function klargest (x,n,k,reset)
      implicit none
      integer n
      real x(n)
      integer k
      logical reset
      integer nmax
      parameter (nmax=1024)
      real y(nmax)
      save y

      integer i
      
      if (k.gt.nmax) then
        write (0,*) 'maximum k=',nmax,' in klargest'
        call exit(0)
      endif

      if (reset) then
        do i=1,k
          y(i)=x(1)
        enddo
      endif
      
      do i=1,n
        if (x(i).gt.y(k)) call klargest_merge (y,k,x(i))
      enddo
      
      klargest = y(k)
      return
      end
      
      
      subroutine klargest_merge (y,k,x)
      implicit none
      integer k
      real y(k),x
      integer i,j

      do i=1,k
        if (x.gt.y(i)) then
          if (i.lt.k) then
            do j=k,i+1,-1
              y(j)=y(j-1)
            enddo
          endif
          y(i)=x
          return
        endif
      enddo
      
      return
      end
      
          
      
      


