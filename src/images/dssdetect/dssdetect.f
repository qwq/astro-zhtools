      implicit none
      character imgname*200
      integer nx,ny,unit,ny1
      logical defined

      integer nmax
      parameter (nmax=2*512*512)
      real img(nmax),work(nmax),work2(nmax)

      call get_command_line_par ("img",1,imgname)
      if (.not.defined(imgname)) then
        call usage
        call exit(1)
      endif
      
      call op_fits_img (imgname,unit,nx,ny)

      ny1=nmax/nx               ! this is how many lines one can store at once
      call do_dssdetect (imgname,unit,nx,ny,ny1,img,work,work2)
      call exit(0)
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_dssdetect (imgname,unit,nx,ny,ny1,img,work,work2)
      implicit none
      character imgname*(*)
      integer nx,ny,unit,ny1

      real img(nx,ny1), work(nx,ny1), work2(nx,ny1)
      
      character arg*80
      
      integer scale
      real threshold
      
      logical defined
      real w
      real detectthreshold
      logical if_peak,is_local_max
      
      integer jj
      integer iter,i,j
      double precision noise
      integer nsum
      real t,x
      real offset,area,lborder,inertia
      
      integer nstar,istar,kstar,nstarmax
      parameter (nstarmax=10000)
      integer xstar(nstarmax),ystar(nstarmax)
      real astar(nstarmax)
      logical star
      integer idistance
      
      real klargest,saturation
      integer nsaturation

      logical useoffset,useinertia,yespar,yesoffset,yesinertia,reset


      real noise0
      logical qnoise

      integer readoffset,readstep,jline,jstart,jmin,jmax,jdmin,jdmax,jref
      integer nyw
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

      call get_cl_par ('noise',arg)
      if (defined(arg)) then
        read (arg,*) noise
        qnoise = .true.
      else
        qnoise = .false.
      endif

      call get_pv_default ('noise0',noise0,1e10,'e')
      write (0,*) 'noise0=',noise0

      
      call get_command_line_par ('scale',1,arg)
      if (defined(arg)) then
        read(arg,*)w
        scale=nint(w)
      else
        scale=2
      endif

c
c  scale=1 offset=3
c  scale=2 offset=6
c  scale=3 offest=12
c
      readoffset = 3*2**(scale-1)+2
      if (3*readoffset.gt.ny1) then
        write (0,*) 'too big image in dssdetect'
        call exit(1)
      endif


      call get_command_line_par ('threshold',1,arg)
      if (defined(arg)) then
        read(arg,*) threshold
      else
        threshold=5.0
      endif
      
      
      useoffset  = .not. yespar ('nooffset')
      useinertia = .not. yespar ('noinertia')
      
c Find saturation
      call get_command_line_par ('satur',1,arg) 
      if (defined(arg)) then
        read (arg,*) saturation
      else
c Read image section-by-section and find the saturation limit
        call get_command_line_par ('nsatur',1,arg)
        if (defined(arg)) then
          read (arg,*) nsaturation
          if (nsaturation.gt.1024) then
            write (0,*) 'nsatur must be <= 1024'
            call exit (1)
          endif
        else
          nsaturation = 500
        endif
        do jline = 1,ny
          call read_fits_imageline_unit (unit,imgname,img,nx,jline,'e')
          reset = jline.eq.1
          saturation = klargest(img,nx,nsaturation,reset)
        enddo
        write(0,*) " saturation limit = ",saturation
        saturation = saturation - 500.0
      endif
      
c 
c - now, read section-by-section and do detection
c
      readstep = ny1-2*readoffset
      do jstart = 1, ny, readstep

c       Set up y-ranges.
        if (jstart.lt.readoffset) then
          jmin = 1
          jdmin = max(jstart,3)
          jref = 0
        else
          jmin = jstart - readoffset
          jdmin = readoffset + 1
          jref = jstart - jdmin
        endif
        
        jmax = min(ny,jmin+ny1-1)
        nyw = jmax-jmin+1
        
        if (jdmin.eq.3) then
          jdmax = min(1 + readstep-1,nyw-2)
        else
          jdmax = min(jdmin + readstep-1,nyw-2)
        endif

        write(0,*) '... lines ',jdmin+jref,jdmax+jref


c       Read the section.
        do j=1,nyw
          jline = jmin + j - 1
          call read_fits_imageline_unit (unit,imgname,img(1,j),nx,jline,'e')
        enddo

c       Set saturation mask,
        do i=1,nx
          do j=1,nyw
            if (img(i,j).gt.saturation) then
              work(i,j)=1.0
            else
              work(i,j)=0.0
            endif
          enddo
        enddo

c       and smooth it.
        do jj=1,max(scale-1,1)
          call atrousimg (work,nx,nyw,jj)
        enddo

        
c       Find saturated sources ("stars")        
        nstar=0
        do i=2,nx-1
          do j=jdmin,jdmax
            if (work(i,j).gt.5e-3) then
              if (if_peak (work,nx,ny,i,j) ) then
                kstar = 0
                do istar=1,nstar
                  if (idistance(xstar(istar),ystar(istar),i,j) .le. 5 ) then
                    kstar = istar
                  endif
                enddo
                if (kstar.eq.0) then
                  nstar=nstar+1
                  if (nstar.gt.nstarmax) then
                    write (0,*) 'too many saturated sources in lines',
     ~                  jmin+readoffset,'; maximum allowed is',nstarmax
                    call exit(1)
                  endif
                  xstar(nstar)=i
                  ystar(nstar)=j
                  astar(nstar)=work(i,j)
                else
                  if (work(i,j).ge.astar(kstar)) then
                    xstar(kstar)=i
                    ystar(kstar)=j
                    astar(kstar)=work(i,j)
                  endif
                endif
              endif
            endif
          enddo
        enddo
      
c       Now convolve the data with the wavelet
c       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        call mcopy (work,img,nx,nyw)
        do jj=1,scale
          call mcopy (work2,work,nx,nyw)
          call atrousimg (work,nx,nyw,jj)
        enddo
        call marith (work2,"=",work2,"-",work, nx*nyw)
c (and work2 contains the atrous-convolved image, work - background)
        
c       determine noise in the atrous image
        if (.not.qnoise) then
          noise = noise0
          do iter=1,3
            t=(2.5*noise)**2
            noise=0.0d0
            nsum=0
            do i=1,nx
              do j=1,nyw
                x = work2(i,j)**2
                if (x.le.t) then
                  noise=noise+x
                  nsum=nsum+1
                endif
              enddo
            enddo
            noise=sqrt(noise/nsum)
          enddo
        endif

        write(0,*)" image noise = ",sngl(noise)
      
        detectthreshold = noise*threshold

c -- find sources
        do i=3,nx-2
          do j=jdmin,jdmax
            if (work2(i,j).gt.detectthreshold) then
              if (if_peak(work2,nx,ny,i,j)) then
                if (is_local_max(img,nx,ny,i,j)) then
                  
                                ! is it in the star list ?
                  star = .false.
                  do istar = 1,nstar
                    if (idistance(xstar(istar),ystar(istar),i,j).lt.3) then
                      star = .true.
                    endif
                  enddo
                  
                  if (.not.star) then
                    if (useoffset.or.useinertia) then
                      call analyze shape (img,nx,ny,i,j,sngl(noise),
     ~                    offset,area,lborder,inertia)
                    endif
                    if ( useoffset ) then
                      yesoffset = offset.lt.2.00
                    else
                      yesoffset = .true.
                    endif
                    if ( useinertia ) then
                      yesinertia = (inertia/area).lt.3.0
                    else
                      yesinertia = .true.
                    endif
                    
                    if (yesoffset.and.yesinertia) then
                      print '(i4,1x,i4,1x,f10.3,1x,f10.3)',i,j+jref,
     ~                    area,inertia
                    endif
                    
                  endif
                endif
              endif
            endif
          enddo
        enddo
        
c -- print stars
        do i=1,nstar
          print '(i4,1x,i4)',xstar(i),ystar(i)+jref
        enddo

      enddo

      return
      end
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine analyze shape (img,nx,ny,isrc,jsrc,noise,
     ~    offset,area,lborder,inertia)
      implicit none 
      integer nx,ny,isrc,jsrc
      real img(nx,ny),noise,offset,area,lborder,inertia
      integer mask (-10:10,-10:10)
      logical class (-10:10,-10:10)
      real immax
      integer i,j,k,ii,jj
      integer ioff(4),joff(4)
      data ioff /-1,  0,  1,  0/
      data joff / 0,  1,  0, -1/
      integer npix
      real xmean,ymean
      logical border
      
      immax = 0.0
      npix  = 0
      do ii=isrc-1,isrc+1
        do jj=jsrc-1,jsrc+1
          immax = immax + img(ii,jj)
        enddo
      enddo
      immax = immax / 9.0
      
      do i=-10,10
        do j=-10,10
          mask(i,j) = 0
          class(i,j) = .false.
          if (
     ~        i+isrc.ge.1.and.i+isrc.le.nx.and.j+jsrc.ge.1.and.j+jsrc.le.ny
     ~        )
     ~        then
            if (img(i+isrc,j+jsrc).gt.immax-noise*5.0) then
              mask(i,j) = 1
            endif
          endif
        enddo
      enddo
      
      class(0,0)=.true.
      call classj (mask,class,21,21,1,1,11,11)

*      if (isrc.eq.266.and.jsrc.eq.327) then
*        call saoimage (mask,21,21,'j')
*        call saoimage (class,21,21,'j')
*        read*
*      endif
      
      
      xmean = 0
      ymean = 0
      npix  = 0
      area  = 0
      lborder = 0
      do i=-10,10
        do j=-10,10
          if (class(i,j)) then
            xmean = xmean + i
            ymean = ymean + j
            npix  = npix  + 1
            area  = area  + 1
            border = .false.
            do k = 1,4
              ii = i + ioff(k)
              jj = j + joff(k)
              if (ii.ge.-10.and.ii.le.10.and.jj.ge.-10.and.jj.le.10) then
                if (.not.class(ii,jj)) then
                  border = .true.
                endif
              endif
            enddo
            if (border) lborder = lborder + 1
          endif
        enddo
      enddo
      if (npix.gt.0) then
        xmean = xmean / npix
        ymean = ymean / npix
      endif
      
      inertia = 0.0
      do i=-10,10
        do j=-10,10
          if (class(i,j)) then
            inertia = inertia + (i-xmean)**2 + (j-ymean)**2
          endif
        enddo
      enddo


      offset = sqrt (xmean**2+ymean**2)
      
      return
      end
      
          
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine measure_ell (img,nx,ny,bg,i,j,ell)
      implicit none
      integer nx,ny
      real img(nx,ny)
      real bg
      integer i,j
      real ell
      real sxx,sxy,syy
      real r,val
      integer idistance
      real lmin,lmax
      integer ii,jj
      
      sxx=0.0
      syy=0.0
      sxy=0.0
      do ii=i-2,i+2
        do jj=j-2,j+2
          r = idistance (i,j,ii,jj)
          if (r.lt.4.1.and.r.gt.0.1) then
            val = img(ii,jj)-bg
            sxx = sxx + val*(i-ii)**2/r
            syy = syy + val*(j-jj)**2/r
            sxy = sxy + val*(i-ii)*(j-jj)/r
          endif
        enddo
      enddo
      
      lmax=0.5*(sxx+syy)+0.5*sqrt((sxx+syy)**2-4.0*(sxx*syy-sxy**2))
      lmin=0.5*(sxx+syy)-0.5*sqrt((sxx+syy)**2-4.0*(sxx*syy-sxy**2))
      
      ell = 1.0 - lmin/lmax
      
      return
      end


*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      logical function is_local_max (img,nx,ny,i,j)
      implicit none
      integer nx,ny,i,j
      real img(nx,ny)
      integer ii,jj
      integer imax,jmax
      real immax
      integer idistance
      
      imax=i
      jmax=j
      immax=img(i,j)
      do ii=i-1,i+1
        do jj=j-1,j+1
          if (idistance(ii,jj,i,j).le.4) then
            if (img(ii,jj).gt.immax) then
              immax=img(ii,jj)
              imax=ii
              jmax=jj
            endif
          endif
        enddo
      enddo
      is_local_max = ( idistance(i,j,imax,jmax) .le. 2 )
      return
      end
      


*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine find_med_bg (img,nx,ny,isrc,jsrc,noise,medbg)
      implicit none
      integer nx,ny
      real img(nx,ny)
      integer isrc,jsrc
      real noise,medbg
      
      real medwksp(200*200)
      integer nmed,kmed
      real select
      integer i,j,iter
      
      nmed=0
      do i=isrc-50,isrc+50
        do j=jsrc-50,jsrc+50
          if (i.ge.1.and.i.le.nx.and.j.ge.1.and.j.le.ny) then
            nmed = nmed + 1
            medwksp(nmed)=img(i,j)
          endif
        enddo
      enddo
      
      medbg = select (nmed/2,nmed,medwksp)
      
      do iter=1,3
        kmed = 0
        do i=1,nmed
          if (abs(medwksp(i)-medbg).lt.noise*2.5) then
            kmed=kmed+1
            medwksp(kmed)=medwksp(i)
          endif
        enddo
        if (kmed.le.10) then
          return
        endif
        nmed = kmed
        medbg = select (nmed/2,nmed,medwksp)
      enddo
          
      return
      end
      
      


      
      subroutine classj (img,classimg,nx,ny,classval,nclass,i,j)
      implicit none
      integer nx,ny
      integer img(nx,ny)
      integer classimg(nx,ny)
      integer classval
      integer nclass
      
      integer i,j,ii,jj,iclass,i1,i2,j1,j2

      logical qstep
      integer step
      integer nx1,ny1
      
      iclass=nclass
      classimg(i,j)=iclass
      
      step=1
      qstep=.true.
      
      nx1=nx-1
      ny1=ny-1
      
      do while (qstep)
        
        qstep=.false.
        i1=max(i-step,2)
        i2=min(i+step,nx1)
        j1=max(j-step,2)
        j2=min(j+step,ny1)
        
        do ii=i1,i2
          do jj=j1,j2
            if (classimg(ii,jj).eq.0.and.img(ii,jj).eq.classval) then
              if (
     ~            classimg(ii,jj-1).eq.iclass. or.
     ~            classimg(ii-1,jj).eq.iclass. or.
     ~            classimg(ii+1,jj).eq.iclass. or.
     ~            classimg(ii,jj+1).eq.iclass. or.
     ~            classimg(ii+1,jj+1).eq.iclass. or.
     ~            classimg(ii-1,jj+1).eq.iclass. or.
     ~            classimg(ii-1,jj-1).eq.iclass. or.
     ~            classimg(ii+1,jj-1).eq.iclass
     ~            ) then
                qstep=.true.
                if (img(ii,jj).eq.classval) then
                  classimg(ii,jj)=iclass
                endif
              endif
            endif
          enddo
        enddo

        step=step+1
        
      enddo
      
          
      return
      end





*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine usage
      write(0,*) "Usage: dssdetect img=image [scale=scale] [threshold=t]"
      return
      end
