c Assumes that the fits file is already opened and positioned to the desired
c image extenstion. The iunit indicates an input stream and filename is used
c only for error messages. Input file is not closed
c
c
c
      subroutine read_fits_imageline_unit
     ~    (iunit,filename,data,nx,jline,imgtype)
      implicit none
      character filename*(*)
      integer data(*)
      integer nx,jline
      character imgtype*(*)
      character nullb*1
      integer*2 nulli
      integer nullj
      real nulle
      double precision nulld
      
      integer iunit
      integer status
      integer lnblnk
      logical anyf
 
      integer fpixel
      
      status=0

      fpixel = 1 + nx*(jline-1)

      if      (imgtype(1:1).eq.'b'.or.imgtype(1:1).eq.'B') then
        nullb=char(0)
        call ftgpvb (iunit,0,fpixel,nx,nullb,data,anyf,status)
      else if (imgtype(1:1).eq.'i'.or.imgtype(1:1).eq.'I') then
        nulli=0
        call ftgpvi (iunit,0,fpixel,nx,nulli,data,anyf,status)
      else if (imgtype(1:1).eq.'j'.or.imgtype(1:1).eq.'J') then
        nullj=0
        call ftgpvj (iunit,0,fpixel,nx,nullj,data,anyf,status)
      else if (imgtype(1:1).eq.'e'.or.imgtype(1:1).eq.'E') then
        nulle=0
        call ftgpve (iunit,0,fpixel,nx,nulle,data,anyf,status)
      else if (imgtype(1:1).eq.'d'.or.imgtype(1:1).eq.'D') then
        nulld=0
        call ftgpvd (iunit,0,fpixel,nx,nulld,data,anyf,status)
      else
        write(0,*)'wrong image type: ',imgtype(1:lnblnk(imgtype))
     ~      ,' in read_fits_imageline_unit'
        call exit(1)
      endif
      
      if (status.ne.0) then
        call perror_fitsio (filename,status)
        call exit(1)
      endif
      
      return
      end
