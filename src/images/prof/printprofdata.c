#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printprofdata_ (char *format, double *b1, double *b2, double *signal, 
		     int *npix, double *s, double *e)
{
  printf (format,*b1,*b2,*signal,*npix,*s,*e);
  printf ("\n");
}

void printdeprojdata_ (double *r1, double *r2, double *c, double *ce, int *n)
{
  char file[200];
  char format[200];
  FILE *out;
  int i;

  get_command_line_par ("format",format);
  if (!defined(format)) {
    strcpy (format,"   %9.3e %9.3e  %10.3e %10.3e");
  }
  strcat (format,"\n");

  get_command_line_par ("out",file);
  if (defined(file)) {
    out = fopen(file,"w");
    if (out==NULL) {
      perror (file);
      exit(1);
    }
  } else {
    out = stdout;
  }

  for (i=0;i<(*n);i++) {
    fprintf (out,format,r1[i],r2[i],c[i],ce[i]);
  }
  if (defined(file)) fclose(out);
}
