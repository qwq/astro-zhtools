      subroutine ring0 (z,n,m,nx1,ny1,nx2,ny2,bound1,bound2,rz,nrz,nr,xc,yc)
c
c
c  nx,ny - phys. dimensions of z
c  nx1,ny1,nx2,ny2 - part to build ring
c
c
c
c
c
c     
      implicit none
      integer n,m,nx1,ny1,nx2,ny2,nr
      real z(n,m),rz(nr)
      integer nrz(nr)
      real bound1(nr),bound2(nr)
      real xc,yc
      

      integer n1,n2,m1,m2
      real r,rmax
      integer i,j,ir
      
      rmax=0.
      do i=1,nr
        rmax=max(rmax,bound2(i))
        bound1(i)=bound1(i)**2
        bound2(i)=bound2(i)**2
        rz(i)=0.
        nrz(i)=0
      enddo
      
      n1=xc-rmax
      n2=xc+rmax+1
      m1=yc-rmax
      m2=yc+rmax+1
      
      n1=max(nx1,n1)
      n2=min(nx2,n2)
      m1=max(ny1,m1)
      m2=min(ny2,m2)
      
      do i=n1,n2
        do j=m1,m2
          r=(i-xc)**2+(j-yc)**2
          do ir=1,nr
            if(r.ge.bound1(ir).and.r.lt.bound2(ir))then
              rz(ir)=rz(ir)+z(i,j)
              nrz(ir)=nrz(ir)+1
            endif
          enddo
        enddo
      enddo
      
      do i=1,nr
        bound1(i)=sqrt(bound1(i))
        bound2(i)=sqrt(bound2(i))
        if(nrz(i).gt.0)rz(i)=rz(i)/nrz(i)
      enddo
      
      return
      end
        
      
        
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine ring08 (z,n,m,nx1,ny1,nx2,ny2,bound1,bound2,rz,nrz,nr,xc,yc)
c
c
c  nx,ny - phys. dimensions of z
c  nx1,ny1,nx2,ny2 - part to build ring
c
c
c
c
c
c     
      implicit none
      integer n,m,nx1,ny1,nx2,ny2,nr
      real*8 z(n,m),rz(nr)
      integer nrz(nr)
      real*8 bound1(nr),bound2(nr)
      real*8 xc,yc
      

      integer n1,n2,m1,m2
      real*8 r,rmax
      integer i,j,ir
      
      rmax=0.
      do i=1,nr
        rmax=max(rmax,bound2(i))
        bound1(i)=bound1(i)**2
        bound2(i)=bound2(i)**2
        rz(i)=0.d0
        nrz(i)=0
      enddo
      
      n1=xc-rmax
      n2=xc+rmax+1
      m1=yc-rmax
      m2=yc+rmax+1
      
      n1=max(nx1,n1)
      n2=min(nx2,n2)
      m1=max(ny1,m1)
      m2=max(ny2,m2)
      do i=n1,n2
        do j=m1,m2
          r=(i-xc)**2+(j-yc)**2
          do ir=1,nr
            if(r.ge.bound1(ir).and.r.lt.bound2(ir))then
              rz(ir)=rz(ir)+z(i,j)
              nrz(ir)=nrz(ir)+1
            endif
          enddo
        enddo
      enddo
      
      do i=1,nr
        bound1(i)=dsqrt(bound1(i))
        bound2(i)=dsqrt(bound2(i))
        if(nrz(i).gt.0)rz(i)=rz(i)/nrz(i)
      enddo
      
      return
      end
      
      
      
      
      

      
