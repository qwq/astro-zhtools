      implicit none
c     
c
      integer nprofmax,nprof
      parameter (nprofmax=1024)
      double precision r1(nprofmax),r2(nprofmax),s(nprofmax),e(nprofmax)
      double precision c(nprofmax), ce(nprofmax)
      double precision w1,w2

      double precision bg

      logical defined
      character arg*80,file*80
      integer unit,status,newunit
      integer i

      call get_command_line_par ('prof',1,arg)
      if (.not.defined(arg)) call exiterror('prof=?')
      file=arg

      call get_command_line_par ('bg',1,arg)
      if (defined(arg)) then
        read (arg,*) bg
      else
        call get_command_line_par ('w',1,arg)
        if (.not.defined(arg)) write (0,*) 'warning: assume bg=0'
        bg=0
      endif
      

c Read profile
      if (file.eq.'-') then
        unit = 5
      else
        unit=newunit()
        open(unit,file=file,status='old')
      endif

      nprof=0
      status=0
      do while (status.eq.0)
        read (unit,*,iostat=status) 
     ~      r1(nprof+1),r2(nprof+1),w1,w2,s(nprof+1),e(nprof+1)
        if (status.eq.0) then
          nprof=nprof+1
        endif
      enddo
      close(unit)

c subtract background
      do i=1,nprof
        s(i)=s(i)-bg
      enddo

      call deproject (r1,r2,s,e,c,ce,nprof)

      call get_command_line_par ('out',1,arg)

      call printdeprojdata (r1,r2,c,ce,nprof)

      end
      



      subroutine deproject (r1,r2,s,se,d,de,nprof)
c
c
c
c
c
      implicit none
      integer nprof
      double precision r1(nprof), r2(nprof), s(nprof), se(nprof)
      double precision d(nprof), de(nprof)

      integer i,j,k
      double precision pi
      parameter (pi=3.1415926536)
      integer nprofmax
      parameter (nprofmax=1024)
      double precision a(nprofmax,nprofmax),a1(nprofmax,nprofmax), area_i
      double precision sumd,sumde

      if (nprof.gt.nprofmax) then
        write (0,*) 'Can deproject maximum',nprofmax,'-long profiles'
        call exit(1)
      endif
      
      do i=1,nprof
        do j=1,i-1
          a(i,j)=0
        enddo

        area_i=pi*(r2(i)**2-r1(i)**2)

        a(i,i)=(4*pi/3.0)*(r2(i)**2-r1(i)**2)**1.5
        do j=i+1,nprof
          a(i,j)=(4*pi/3.0) * (
     ~        (r1(j)**2-r2(i)**2)**1.5 -
     ~        (r1(j)**2-r1(i)**2)**1.5 -
     ~        (r2(j)**2-r2(i)**2)**1.5 +
     ~        (r2(j)**2-r1(i)**2)**1.5
     ~        )
        enddo

        do j=1,nprof
          a(i,j)=a(i,j)/area_i
        enddo
      enddo
      do i=1,nprof
        do j=1,nprof
          a1(i,j)=0
        enddo
        a1(i,i)=1
      enddo

      do k=1,nprof
! divide i-th raw of the input matrix by a(i,i)
        do i=1,k
          a1(i,k)=a1(i,k)/a(k,k)
        enddo
! subtract this raw
        do j=k+1,nprof
          do i=1,k
            a1(i,j)=a1(i,j)-a1(i,k)*a(k,j)
          enddo
        enddo
      enddo
          

      do i=1,nprof
        sumd = 0
        sumde = 0
*        if (i.lt.nprof-2) then
*          print*,a1(i,i),a1(i,i+1),a1(i,i+1)/a1(i,i)
*        endif
        do j=i,nprof
          sumd = sumd + a1(i,j)*s(j)
          sumde = sumde + (a1(i,j)*se(j))**2
        enddo
        d(i)=sumd
        de(i)=sqrt(sumde)
      enddo

      return
      end








