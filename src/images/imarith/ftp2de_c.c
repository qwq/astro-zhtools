#include <fitsio.h>
#include <zhtools.h>

/*
      call cftp2de(ounit,0,imax-imin+1,imax-imin+1,jmax-jmin+1,pout
     ~    ,status)

*/
void cftp2de_(int *ounit, int *v1, int *v2, int *v3, int *v4, ZHINT *pout,
	      int *status)
{
  ftp2de_(ounit, v1, v2, v3, v4, *pout, status);
  if( *pout ) free((void *)*pout);
}

