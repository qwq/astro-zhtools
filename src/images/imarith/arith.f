      implicit none
      character arg*80
      integer n,k, lnblnk
      
      call getarg(0,arg)

      n = lnblnk(arg)

      k = max(1,n-6)
      if (arg(k:n).eq.'imarith') then
        call imarith
        call exit(0)
      endif

      k = max(1,n-7)
      if (arg(k:n).eq.'imcarith') then
        call imcarith
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'mkcond') then
        call mkcond
        call exit(0)
      endif

      k = max(1,n-9)
      if (arg(k:n).eq.'projectimg') then
        call projectimg
        call exit(0)
      endif

      k = max(1,n-10)
      if (arg(k:n).eq.'stackimages') then
        call stackimages
        call exit(0)
      endif

      k = max(1,n-8)
      if (arg(k:n).eq.'addimages') then
        call addimages
        call exit(0)
      endif

      write(0,*)
     ~    'Use as imarith, imcarith, mkcond, projectimg, stackimages, or addimages'
      call exit(1)
      
      end
