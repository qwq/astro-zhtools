#include <zhtools.h>

      subroutine mkcond
      implicit none
      character data*200
      integer nx,ny,unit
      integer iargc,iarg
      ZHDECL(real,pimg)
      ZHDECL(integer,pclassimg)

      if (iargc().lt.3) call zhhelp ('mkcond')

      call getarg(2,data)
      if (data.eq.'=') then
        call getarg (3,data)
        iarg = 4
      else
        iarg = 3
      endif

      if (data.eq.'!'.or.data.eq.'.not.'.or.data.eq.'.NOT.') then
        call getarg (iarg,data)
        iarg = iarg - 1
      endif

      call op_fits_img (data,unit,nx,ny)

      ZHMALLOC(pimg,nx*ny,'e')
      ZHMALLOC(pclassimg,nx*ny,'j')

      call do_mkcond (data,unit,nx,ny,ZHVAL(pimg),ZHVAL(pclassimg),iarg)
      call exit(0)
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_mkcond  (data,unit,nx,ny,img,classimg,iarg)
      implicit none
      character data*(*)
      integer nx,ny,unit
      character class*200,arg*200, secondimg*200
      integer iarg
      
c image arrays
      real img(nx*ny)
      integer classimg(nx*ny)
      
      real value
      character condition*80
      
      logical isanum, bothimg, logicalop, logicaloperation
      external isanum,logicalop
      character typetoread*1
      
c-----------------------------------------------------------------      
      call getarg(1,class)

*      print*,iarg
      call getarg(iarg,arg)
      condition=arg
*      print*,'cond: ', condition
      call getarg(iarg+1,arg)
      bothimg = .not. isanum (arg)
*      print*,'bothimg: ',bothimg
      if (bothimg) then
        secondimg = arg
        logicaloperation = logicalop(condition)
        if (logicaloperation) then
          typetoread = 'j'
        else
          typetoread = 'e'
        endif
      else
        typetoread = 'e'
        read(arg,*)value
*        print*,value
      endif

c Read first image
      call read_fits_image_unit (unit,data,img,nx,ny,typetoread)
      if (bothimg) then
        if ( .not. (logicaloperation.and.(condition.eq.'!'.or.condition.eq
     ~      .'not.'.or.condition.eq.'.NOT.'))) then
          call read_fits_image (secondimg,classimg,nx,ny,typetoread)
        else
          call mcopyj (classimg,img,nx,ny)
        endif
        call makecond_2img (img,classimg,classimg,nx,ny,condition
     ~      ,logicaloperation)
      else
        call makecond (img,classimg,nx,ny,condition,value)
      endif

      call write_fits_image_header_unit (class,classimg,nx,ny,'j','b',data
     ~    ,unit)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      logical function logicalop (string)
      implicit none
      character string*(*)

      if (
     ~    string.eq.'.or.' .or.
     ~    string.eq.'||'   .or.
     ~    string.eq.'.and.'.or.
     ~    string.eq.'&&'   .or.
     ~    string.eq.'.not.'.or.
     ~    string.eq.'!'    .or.
     ~    string.eq.'.OR.' .or.
     ~    string.eq.'.AND.'.or.
     ~    string.eq.'.NOT' ) then
        logicalop = .true.
      else
        logicalop = .false.
      endif

      return
      end




