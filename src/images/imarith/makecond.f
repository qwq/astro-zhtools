      subroutine makecond (rimg,img,nx,ny,condition,value)
      implicit none
      integer nx,ny
      real rimg(nx*ny)
      integer img(nx*ny)
      character condition*(*)
      real value
      
      integer i
      
      if (condition.eq.'>' .or. condition.eq.'.gt.' .or. condition.eq.'.GT.')
     ~    then
        do i=1,nx*ny
          if (rimg(i).gt.value) then
            img(i)=1
          else
            img(i)=0
          endif
        enddo
      else if (condition.eq.'>=' .or. condition.eq.'.ge.' .or. condition.eq
     ~      .'.GE.') then
        do i=1,nx*ny
          if (rimg(i).ge.value) then
            img(i)=1
          else
            img(i)=0
          endif
        enddo

      else if (condition.eq.'==' .or. condition.eq.'.eq.' .or. condition.eq
     ~      .'.EQ.'.or. condition.eq.'=') then
        do i=1,nx*ny
          if (rimg(i).eq.value) then
            img(i)=1
          else
            img(i)=0
          endif
        enddo
        
      else if (condition.eq.'<' .or. condition.eq.'.lt.' .or. condition.eq
     ~      .'.LT.')then
        do i=1,nx*ny
          if (rimg(i).lt.value) then
            img(i)=1
          else
            img(i)=0
          endif
        enddo

      else if (condition.eq.'<=' .or. condition.eq.'.le.' .or. condition.eq
     ~      .'.LE.') then
        do i=1,nx*ny
          if (rimg(i).le.value) then
            img(i)=1
          else
            img(i)=0
          endif
        enddo
        
      else if (condition.eq.'!=' .or. condition.eq.'.ne.' .or. condition.eq
     ~      .'.NE.') then
        do i=1,nx*ny
          if (rimg(i).ne.value) then
            img(i)=1
          else
            img(i)=0
          endif
        enddo
      else
        call zhhelp ('mkcond')
      endif
      
      return
      end
      


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine makecond_2img (rimg1,rimg2,img,nx,ny,condition,logicalop)
      implicit none
      integer nx,ny
      real rimg1 (nx*ny), rimg2(nx*ny)
      integer img(nx*ny)
      character condition*(*)
      logical logicalop
      integer i

      if (logicalop) then
        call makecond_2img_l (rimg1,rimg2,img,nx,ny,condition)
      else
        if (condition.eq.'>' .or. condition.eq.'.gt.' .or. condition.eq.'.GT.')
     ~      then
          do i=1,nx*ny
            if (rimg1(i).gt.rimg2(i)) then
              img(i)=1
            else
              img(i)=0
            endif
          enddo
        else if (condition.eq.'>=' .or. condition.eq.'.ge.' .or. condition.eq
     ~        .'.GE.') then
          do i=1,nx*ny
            if (rimg1(i).ge.rimg2(i)) then
              img(i)=1
            else
              img(i)=0
            endif
          enddo
          
        else if (condition.eq.'==' .or. condition.eq.'.eq.' .or. condition
     ~        .eq.'.EQ.' .or. condition.eq.'=') then
          do i=1,nx*ny
            if (rimg1(i).eq.rimg2(i)) then
              img(i)=1
            else
              img(i)=0
            endif
          enddo
        
        else if (condition.eq.'<' .or. condition.eq.'.lt.' .or. condition.eq
     ~        .'.LT.')then
          do i=1,nx*ny
            if (rimg1(i).lt.rimg2(i)) then
              img(i)=1
            else
              img(i)=0
            endif
          enddo

        else if (condition.eq.'<=' .or. condition.eq.'.le.' .or. condition.eq
     ~        .'.LE.') then
          do i=1,nx*ny
            if (rimg1(i).le.rimg2(i)) then
              img(i)=1
            else
              img(i)=0
            endif
          enddo
        
        else if (condition.eq.'!=' .or. condition.eq.'.ne.' .or. condition.eq
     ~        .'.NE.') then
          do i=1,nx*ny
            if (rimg1(i).ne.rimg2(i)) then
              img(i)=1
            else
              img(i)=0
            endif
          enddo

        else
          call zhhelp ('mkcond')
        endif
        
      endif
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine makecond_2img_l (rimg1,rimg2,img,nx,ny,condition)
      implicit none
      integer nx,ny
      logical rimg1 (nx*ny), rimg2(nx*ny), img(nx*ny)
      character condition*(*)
      integer i
      
      if (condition .eq. '.or.' .or. condition .eq. '||'
     ~    .or. condition .eq. '.OR.' ) then
        do i=1,nx*ny
          img(i) = rimg1(i) .or. rimg2(i)
        enddo
      else if (condition .eq. '.and.' .or. condition .eq. '&&' .or. condition
     ~      .eq. '.AND.' ) then
        do i=1,nx*ny
          img(i) = rimg1(i) .and. rimg2(i)
        enddo
      else if (condition .eq. '.not.' .or. condition .eq. '!' .or. condition
     ~      .eq. '.NOR.' ) then
        do i=1,nx*ny
          img(i) = .not. rimg1(i)
        enddo
      endif

      return
      end

        
