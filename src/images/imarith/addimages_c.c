#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fitsio.h>
#include <math.h>
#include <zhtools.h>

#define nint(x) ((x)<0 ? ((int)(x-0.5)) :((int)(x+0.5)))

struct WorldCoor *wcsinit (char *header);
  
  
void do_addimages_exact_ (int *unit1, int *NX1, int *NY1,
			  int *unit2, int *NX2, int *NY2,
			  int *QERROR, int *qnoexpand,
			  int *SUBPIX,
			  float *img1, float *img2,
			  ZHINT *pout, 
			  int *IMIN, int *IMAX, int *JMIN, int *JMAX)
{
  fitsfile *Unit1, *Unit2;

  struct WorldCoor *wcs1, *wcs2;

  char *header1, *header2;

  int nx1=*NX1, nx2=*NX2, ny1=*NY1, ny2=*NY2, subpix=*SUBPIX, qerror=*QERROR;
  int iw1,iw2;
  int status;

  float *out;
  int imin,imax,jmin,jmax;
  double xpix,ypix,ra,dec;
  int offscale;
  int i,j;
  int ii,jj,is,js,io,jo;


  Unit1 = CUnit2FITS(*unit1);
  Unit2 = CUnit2FITS(*unit2);

  status = 0;
  ffgiwcs(Unit1,&header1,&status);
  ffgiwcs(Unit2,&header2,&status);
  if (status!=0) {
    ffrprt (stderr,status);
    exit(1);
  }

  wcs1 = wcsinit(header1);
  wcs2 = wcsinit(header2);

  imin = 1;
  imax = nx1;
  jmin = 1;
  jmax = ny1;

  if (!*qnoexpand) {
    
    xpix = 1.0;
    ypix = 1.0;
    pix2wcs (wcs2, xpix, ypix, &ra, &dec);
    wcs2pix (wcs1, ra, dec, &xpix, &ypix, &offscale);
    ii = nint(xpix);   jj = nint(ypix);
    printf ("%d %d\n",ii,jj);
    if (ii<imin) imin=ii;  if (ii>imax) imax=ii;
    if (jj<jmin) jmin=jj;  if (jj>jmax) jmax=jj;
    
    
    xpix = 1.0;
    ypix = ny2;
    pix2wcs (wcs2, xpix, ypix, &ra, &dec);
    wcs2pix (wcs1, ra, dec, &xpix, &ypix, &offscale);
    ii = nint(xpix);   jj = nint(ypix);
    printf ("%d %d\n",ii,jj);
    if (ii<imin) imin=ii;  if (ii>imax) imax=ii;
    if (jj<jmin) jmin=jj;  if (jj>jmax) jmax=jj;

    xpix = nx2;
    ypix = 1.0;
    pix2wcs (wcs2, xpix, ypix, &ra, &dec);
    wcs2pix (wcs1, ra, dec, &xpix, &ypix, &offscale);
    ii = nint(xpix);   jj = nint(ypix);
    printf ("%d %d\n",ii,jj);
    if (ii<imin) imin=ii;  if (ii>imax) imax=ii;
    if (jj<jmin) jmin=jj;  if (jj>jmax) jmax=jj;

    xpix = nx2;
    ypix = ny2;
    pix2wcs (wcs2, xpix, ypix, &ra, &dec);
    wcs2pix (wcs1, ra, dec, &xpix, &ypix, &offscale);
    ii = nint(xpix);   jj = nint(ypix);
    printf ("%d %d\n",ii,jj);
    if (ii<imin) imin=ii;  if (ii>imax) imax=ii;
    if (jj<jmin) jmin=jj;  if (jj>jmax) jmax=jj;
  }

  printf ("%d %d %d %d\n",imin,imax,jmin,jmax);
  *IMIN = imin; *JMIN = jmin; *IMAX = imax; *JMAX = jmax;

  out = (float *)malloc(sizeof(float)*(imax-imin+1)*(jmax-jmin+1));
  if (out==NULL) {
    fprintf (stderr,"Error: not enough memory in addimages_exact_\n");
    exit(1);
  }
  *pout = (ZHINT)out;

  for (i=imin;i<=imax;i++) {
    for (j=jmin; j<=jmax; j++) {
      ii = i-imin + (j-jmin)*(imax-imin+1);
      out[ii]=0;
    }
  }

  if (qerror) {
    fprintf (stdout,"Mode: error\n");
    for (i=1;i<=nx1;i++) {
      for (j=1;j<=ny1;j++) {
	if (i>=imin&&i<=imax&&j>=jmin&&j<=jmax) {
	  ii = i-imin + (j-jmin)*(imax-imin+1);
	  jj = i-1+(j-1)*nx1;
	  out[ii]=img1[jj]*img1[jj];
	}
      }
    }
  } else {
    for (i=1;i<=nx1;i++) {
      for (j=1;j<=ny1;j++) {
	if (i>=imin&&i<=imax&&j>=jmin&&j<=jmax) {
	  ii = i-imin + (j-jmin)*(imax-imin+1);
	  jj = i-1+(j-1)*nx1;
	  out[ii]=img1[jj];
	}
      }
    }
  }
  
  for (i=1;i<=nx2;i++) {
    for (j=1;j<=ny2;j++) {
      for (is=1;is<=subpix;is++) {
	for (js=1;js<=subpix;js++) {
	  xpix = i-0.5+(is-0.5)/subpix;
	  ypix = j-0.5+(js-0.5)/subpix;
	  pix2wcs (wcs2, xpix, ypix, &ra, &dec);
	  wcs2pix (wcs1, ra, dec, &xpix, &ypix, &offscale);
	  io = nint(xpix);   jo = nint(ypix);
	  
	  if (io>=imin&&io<=imax&&jo>=jmin&&jo<=jmax) {
	    ii = io-imin + (jo-jmin)*(imax-imin+1);
	    jj = i-1+(j-1)*nx2;
	    
	    if (qerror) {
	      out[ii] += (img2[jj]*img2[jj])/(subpix*subpix);
	    } else {
	      out[ii] += img2[jj]/(subpix*subpix);
	    }
	  }
	}
      }
    }
  }

  if (qerror) {
    for (i=imin;i<=imax;i++) {
      for (j=jmin; j<=jmax; j++) {
	ii = i-imin + (j-jmin)*(imax-imin+1);
	out[ii]=sqrt(out[ii]);
      }
    }
  }


  free (header1);
  free (header2);
  free (wcs1);
  free (wcs2);

  return;
}
