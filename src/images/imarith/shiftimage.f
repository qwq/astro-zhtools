      subroutine shift_image (img,oimg,nx,ny,shiftx,shifty)
      implicit none
      integer nx,ny
      real img(nx,ny)
      real oimg(nx,ny)
      integer shiftx,shifty
      integer i,j,ii,jj
      
      
      do i=1,nx
        do j=1,ny
          ii=i-shiftx
          jj=j-shifty
          if (ii.ge.1.and.ii.le.nx.and.jj.ge.1.and.jj.le.ny) then
            oimg(i,j)=img(ii,jj)
          else
            oimg(i,j)=0
          endif
        enddo
      enddo
      
      return
      end
      
      
      
      
