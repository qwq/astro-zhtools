#include <zhtools.h>

#define FILE_REGION     1
#define ZHTOOLS_REGION  2
#define FUNTOOLS_REGION 4

#if HAVE_FUNTOOLS

#include <strings.h>
#include <stdlib.h>
#include <fitsio.h>
#include <funtools.h>
#include <file.h>
#include <filter.h>

FITSHead ft_headsimple(char *buf, int len, int naxis, int *naxes, int bitpix);

void fregion_type_(char *rstr, int *itype, CHAR_INT nr)
{
  int i;
  int ip=0;
  int ndtab=0;
  int ntok=0;
  int ld=0;
  char *s=NULL, *t=NULL, *u=NULL;
  char tbuf[SZ_LINE];
  char *ds9tokens[] ={"global"
		      "physical",
		      "image",
		      "fk4",
		      "b1950",
		      "fk5",
		      "j2000",
		      "galactic",
		      "ecliptic",
		      "icrs",
		      "linear",
		      "amplifier",
		      "detector",
		      NULL};

  /* clear return result */
  *itype = 0;

  /* get c string copy */
  s = xmalloc(nr+1);
  strncpy(s, rstr, nr);
  /* remove white space */
  nowhite(s, s);

  /* quick check -- @foo is a ds9 file */
  if( *s == '@' ){
    *itype = FUNTOOLS_REGION|FILE_REGION;
    goto done;
  }

  /* look for a file and replace the string we process */
  if( t=FileContents(s, 0, NULL) ){
    xfree(s);
    s = t;
    *itype |= FILE_REGION;
  }

  /* if the FILTER_PAINT environment variable is set to true, use funtools */
  if( (u=(char *)getenv("FILTER_PAINT")) && istrue(u) ){
    *itype |= FUNTOOLS_REGION;
    goto done;
  }

  /* these are all valid for funtools but not for zhtools */
  newdtable("();,'\"");
  ndtab++;

  /* determine the number of zhtools tokens */
  for(ntok=0; ds9tokens[ntok]; ntok++) ;

  /* look at each word: the first one not on the zhtools list means funtools */
  while( word(s, tbuf, &ip) ){
    /* skip over comments to end of line */
    if( *tbuf == '#' ){
      while( word(s, tbuf, &ip) ){
	if( lastdelim() == '\n') break;
      }
      continue;
    }
    /* check for identifying ds9 token */
    for(i=0; i<ntok; i++){
      if( !strcasecmp(tbuf, ds9tokens[i]) ){
	*itype |= FUNTOOLS_REGION;
	goto done;
      }
    }
  }
  /* if we got this far, its still a zhtools-style region */
  *itype |= ZHTOOLS_REGION;

done:
  /* clean up and return */
  if( ndtab ) freedtable();
  if( s ) xfree(s);
}

void fregion_mask_(int *unit, int *itype, char *rstr, int *obuf, 
		   int *datatype,  int *nx, int *ny, int *rstat, 
		   CHAR_INT nr)
{
  int n;
  int x, y;
  int nkeys=0;
  int status=0;
  int hpad=0;
  int hlen=0;
  int len=0;
  int nmask=0;
  int pixsize=0;
  int rowsize=0;
  int dtype=0;
  int naxes[2];
  int bitpix=0;
  char *s=NULL;
  char *optr=NULL;
  char *headstr=NULL;
  char tail[SZ_LINE];
  char tbuf[SZ_LINE];
  fitsfile *fptr=NULL;
  FITSHead header=NULL;
  GIO gio=NULL;
  Filter filt=NULL;
  FilterMask masks=NULL;

  /* assume success */
  *rstat = 0;

  /* make sure we have a string */
  if( !rstr || !nr ){
    *rstat = -1;
    goto done;
  }

  /* data info */
  if( islower(*datatype) )
    dtype = toupper(*datatype);
  else
    dtype = *datatype;
  /* size of a single pixel in bytes */
  pixsize = ft_sizeof(dtype);
  /* size in bytes of a row */
  rowsize = *nx * pixsize;

  /* if we have a FITS file ... */
  if( *unit >0 ){
    /* convert the unit to a fitsion struct */
    if( !(fptr = (fitsfile *)CUnit2FITS(*unit)) ){
      *rstat = -1;
      goto done;
    }

    /* get the FITS header as a string */
    fits_hdr2str(fptr, 0, NULL, 0, &headstr, &nkeys, &status);
    if( !headstr ){
      *rstat = -1;
      goto done;
    }
    hlen = strlen(headstr);
    if( (hpad = FT_BLOCK - (hlen % FT_BLOCK)) == FT_BLOCK ) hpad = 0;
    if( hpad ){
      headstr = xrealloc(headstr, hlen+hpad+1);
      memset(headstr+hlen, ' ', hpad);
      headstr[hlen+hpad] = '\0';
    }

    /* get fitsy header, needed by filter */
    snprintf(tbuf, SZ_LINE, "buf:%#lx:%d", (long)headstr, strlen(headstr));
    if(!(gio=ft_fitsheadopen(tbuf, &(header), tail, SZ_LINE, "r")) ){
      *rstat = -1;
      goto done;
    }
  }
  else{
    /* create a fake header (no WCS) */
    naxes[0] = *nx;
    naxes[1] = *ny;
    switch(dtype){
    case 'B':
      bitpix=8;
      break;
    case 'I':
      bitpix=16;
      break;
    case 'J':
      bitpix=32;
      break;
    case 'E':
      bitpix=-32;
      break;
    case 'D':
      bitpix=-64;
      break;
    default:
      *rstat = -1;
      goto done;
    }
    header = (FITSHead)ft_headsimple(NULL, 0, 2, naxes, bitpix);
    ft_syncdata(header);
  }

  /* get c string copy */
  s = xcalloc(nr+2, sizeof(char));
  if( (*itype & 1) && (*rstr != '@') ){
    *s = '@';
    strncpy(s+1, rstr, nr);
  }
  else{
    strncpy(s, rstr, nr);
  }
  /* make sure we have a null */
  s[nr+1] = '\0';
  /* remove white space */
  nowhite(s, s);

  /* open the filter */
  strncpy(tbuf, "type=image,ptype=contained", SZ_LINE);
  if( !(filt = FilterOpen(header, s, tbuf)) ){
    *rstat = -1;
    goto done;
  }

  /* get the region masks */
  nmask = FilterImage(filt, 1, *nx, 1, *ny, 1, &masks, NULL);

  /* clear y rows before first row containing a mask segment */
  if( nmask ){
    for(y=1; y<=masks[0].y-1; y++){
      optr = (char *)obuf + ((y-1) * rowsize);
      memset(optr, 0, rowsize);
    }
  }

  /* process all mask segments */
  for(n=0; n<nmask; n++){
    /* blank out rows before the y row associated with this mask segment */
    for(; y<masks[n].y; y++){
      optr = (char *)obuf + ((y-1) * rowsize);
      memset(optr, 0, rowsize);
    }
    /* process each mask segment having this same y value */
    x = 1;
    while(1){
      /* clear columns before the start of this segment */
      optr = (char *)obuf + ((y-1) * rowsize) + ((x-1) * pixsize);
      if( (len = (masks[n].xstart - x) * pixsize) >0 )
	memset(optr, 0, len);
      x = masks[n].xstart;
      /* if flag is set, we set the pixel values to be the region id */
      optr = (char *)obuf + ((y-1) * rowsize) + ((x-1) * pixsize);
      for(x=masks[n].xstart; x<=masks[n].xstop; x++){
	switch(dtype){
	case 'B':
	  *((char *)optr) = (char)masks[n].region;
	  optr += pixsize;
	  break;
	case 'I':
	  *((short *)optr) = (short)masks[n].region;
	  optr += pixsize;
	  break;
	case 'J':
	  *((int *)optr) = (int)masks[n].region;
	  optr += pixsize;
	  break;
	case 'E':
	  *((float *)optr) = (float)masks[n].region;
	  optr += pixsize;
	  break;
	case 'D':
	  *((double *)optr) = (double)masks[n].region;
	  optr += pixsize;
	  break;
	default:
	  *rstat = -1;
	  goto done;
	}
      }
      /* position after end of segment */
      x = masks[n].xstop+1;
      /* if we have another mask segment with the same y,
	 we keep going */
      if( ((n+1) < nmask) && (masks[n+1].y == y) )
	n++;
      else
	break;
    }
    /* clear the rest of the columns for this y row */
    optr = (char *)obuf + ((y-1) * rowsize) + ((x-1) * pixsize);
    if( (len = (*nx - x + 1) * pixsize) >0 )
      memset(optr, 0, len);
    /* bump to next y row */
    y++;
  }

  /* clear y rows after last row containing a mask segment */
  for(y = masks[nmask-1].y+1; y<=*ny; y++){
    optr = (char *)obuf + ((y-1) * rowsize);
    memset(optr, 0, rowsize);
  }

  /* clean up */
done:
  if( s )       xfree(s);
  if( headstr ) xfree(headstr);
  if( masks )   xfree(masks);
  if( gio)      gclose(gio);
  if( header )  ft_headfree(header, 0);
  if( filt && (filt != NOFILTER) ) FilterClose(filt);
}

#else

void fregion_type_(char *rstr, int *itype, CHAR_INT nr)
{
  *itype = ZHTOOLS_REGION;
}

void fregion_mask_(int *unit, int *itype, char *rstr, int *obuf, 
		   int *datatype, int *nx, int *ny, int *rstat, 
		   CHAR_INT nr)
{
  /* always an error if we call this routine */
  *rstat = -1;
  return;
}

#endif
