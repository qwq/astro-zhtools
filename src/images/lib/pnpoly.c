/*
  The code below is from Wm. Randolph Franklin <wrf@ecse.rpi.edu>
  with some minor modifications for speed.  It returns 1 for strictly
  interior points, 0 for strictly exterior, and 0 or 1 for points on
  the boundary.  The boundary behavior is complex but determined;
  in particular, for a partition of a region into polygons, each point
  is "in" exactly one polygon.  See the references below for more detail.
*/
int pnpoly(int npol, float *xp, float *yp, float x, float y)
{
  int i, j, c = 0;
  for (i = 0, j = npol-1; i < npol; j = i++) {
    if ((((yp[i]<=y) && (y<yp[j])) ||
	 ((yp[j]<=y) && (y<yp[i]))) &&
	(x < (xp[j] - xp[i]) * (y - yp[i]) / (yp[j] - yp[i]) + xp[i]))
      
      c = !c;
  }
  return c;
}

int pnpoly_ (int *NPOL, float *xp, float *yp, float *X, float *Y)
{
  int i, j, c = 0, npol;
  float x,y;

  npol = *NPOL; x = *X; y = *Y;

  for (i = 0, j = npol-1; i < npol; j = i++) {
    if ((((yp[i]<=y) && (y<yp[j])) ||
	 ((yp[j]<=y) && (y<yp[i]))) &&
	(x < (xp[j] - xp[i]) * (y - yp[i]) / (yp[j] - yp[i]) + xp[i]))
      
      c = !c;
  }
  return c;
}
/*
  The code may be further accelerated, at some loss in clarity, by
  avoiding the central computation when the inequality can be deduced,
  and by replacing the division by a multiplication for those processors
  with slow divides.  For code that distinguishes strictly interior
  points from those on the boundary, see [O'Rourke (C)] pp. 239-245.

  References:
  [Gems IV]  pp. 24-46
  [O'Rourke (C)]
  [Glassner:RayTracing]
*/
