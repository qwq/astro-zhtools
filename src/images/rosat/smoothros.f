      subroutine smoothros 
      implicit none
      
      integer n
      parameter (n=512)
      real image(n,n)
      real oimage(n,n)
      real emean
      real resolution
      integer rmax
      character infile*200,ofile*200,arg*200
      integer status
      logical error
      
      call get_command_line_par ('img',1,infile)
      call get_command_line_par ('out',1,ofile)
      
      if (infile.eq.'UNDEFINED'.or.infile.eq.'undefined'.or.ofile.eq
     ~    .'UNDEFINED'.or.ofile.eq.'undefined') then
        write(0,*)
     ~'Usage: smoothros img=image out=out e=emean [resolution=res rmax=r error=yes]'
        call exit(1)
      endif
      
      call get_command_line_par ('e',1,arg)
      read (arg,*,iostat=status) emean
      if (status.ne.0) then
        write(0,*)
     ~'Usage: smoothros img=image out=out e=emean [resolution=res rmax=r error=yes]'
        call exit(1)
      endif
      
      call read_fits_image (infile,image,n,n,'e')
      
      call get_command_line_par ('rmax',1,arg)
      read (arg,*,iostat=status) rmax
      if (status.ne.0) then
        status=0
        rmax=200
      endif
     
      call get_command_line_par ('resolution',1,arg)
      read (arg,*,iostat=status) resolution
      if (status.ne.0) then
        status=0
        resolution=2
      endif
     
      call get_command_line_par ('error',1,arg)
      if (arg(1:1).eq.'y'.or.arg(1:1).eq.'Y') then
        error=.true.
      else
        error=.false.
      endif
     
      call av_convolve (image,oimage,emean,rmax,error,resolution)
      
      call write_fits_image (ofile,oimage,n,n,'e','e')
      
      call exit(0)
      end


*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine av_convolve (image,oimage,emean,rmax,error,resolution)
c
c  real image (512,512)
c  real oimage (512,512)
c  integer rmax   - max radius (pixles) to convolve
c  logical error  - rate or error image
c  real resfunc   - effective resolution (arcmin) as a function of 
c                   offaxis angle (arcmin).
c
      implicit none
      integer n
      parameter (n=512)
      real image(n,n)
      real oimage(n,n)
      integer nf
      integer nf1
      parameter (nf=15)
      real field (-nf:nf,-nf:nf)
      
      real resolution
      integer i,j,ii,jj,i1,j1,i2,j2,rr,idistance,ir,ir1,ir2
      integer rmax,dr
      real offaxis,sigmapsf,sigmagauss,pspcsigma
      real sum
      real emean
      real value
      logical error
      
      
      do i=1,n
        do j=1,n
          oimage(i,j)=0
        enddo
      enddo

      dr=4

      do ir=0,rmax,dr
        print*,ir,'   of',rmax
        ir1=ir**2
        ir2=(ir+dr)**2
        offaxis=0.25*(ir+dr/2)
        
        sigmapsf=pspcsigma(emean,offaxis)
        sigmagauss=((resolution*60)**2-sigmapsf**2)/15.0**2
        if (sigmagauss.gt.0.0) then
          sigmagauss=sqrt(sigmagauss)
          sum=0
          nf1=min(nint(3.0*sigmagauss),nf)
          do i=-nf1,nf1
            do j=-nf1,nf1
              field(i,j)=exp(-0.5*(i**2+j**2)/sigmagauss**2)
              sum=sum+field(i,j)
            enddo
          enddo
          
          do i=-nf1,nf1
            do j=-nf1,nf1
              field(i,j)=field(i,j)/sum
            enddo
          enddo
          
          if (error) then
            do i=-nf1,nf1
              do j=-nf1,nf1
                field(i,j)=field(i,j)**2
              enddo
            enddo
          endif
          
          do i=1,n
            do j=1,n
              rr=idistance(i,j,256,256)
              if (rr.ge.ir1.and.rr.lt.ir2) then
                i1=i-nf1
                i2=i+nf1
                j1=j-nf1
                j2=j+nf1
                if (error) then
                  value=image(i,j)**2
                else
                  value=image(i,j)
                endif
                do ii=i1,i2
                  do jj=j1,j2
                    oimage(ii,jj)=oimage(ii,jj)+value*field(ii-i,jj-j)
                  enddo
                enddo
              endif
            enddo
          enddo
        else
          do i=1,n
            do j=1,n
              rr=idistance(i,j,256,256)
              if (rr.ge.ir1.and.rr.lt.ir2) then
                if (error) then
                  value=image(i,j)**2
                else
                  value=image(i,j)
                endif
                oimage(i,j)=oimage(i,j)+value
              endif
            enddo
          enddo
        endif
        
      enddo

      if (error) then
        do i=1,n
          do j=1,n
            oimage(i,j)=sqrt(oimage(i,j))
          enddo
        enddo
      endif
      
      
      return
      end


*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      function pspcsigma (E,offaxis)
      fpspc=sqrt(409.65/E + 69.28*E**2.88 + 66.29)
      fmirror=0.29*OFFAXIS**1.74
      faspect=2.355*1.5
      fwhm=(fpspc**2+fmirror**2+faspect**2)
      pspcsigma=sqrt(fwhm)/2.355
      return
      end

