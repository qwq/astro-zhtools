      subroutine phalign
      implicit none
      character data*200
      
      integer lnblnk
      
c image arrays
      real img1(512,512)
      real img2(512,512)
      real oimg(512,512)
      integer*2 im1(512,512)
      integer*2 im2(512,512)
      integer*2 im0(512,512)
      
c photon arrays
      integer nphotmax
      parameter (nphotmax=2000000)
      integer*2 xpix(nphotmax),ypix(nphotmax)
      character pi (nphotmax)*1
      character igti (nphotmax)*1
      
c gti arrays      
      integer ngti
      double precision tstart(1000),tend(1000),tspan(1000)
      integer wgti(1000)
      integer dx(1000),dy(1000)

c fitsio staff
      integer iunit
      parameter (iunit=10)
      integer status
      
      integer pcount,gcount
      logical anyf
      
      integer edge,npatches
      logical sortgti
      
      character arg*80
      integer blocksize
      character comment*80
      integer colnum,length,nrows,tfields
      character ttype(300)*80,tform(300)*80,tunit(300)*80,extname*80
      integer varidat
      integer fx,fy,fpi,ftime
      integer ifield,irow,i,j,ii,jj,i1,j1,i0,j0
      
      integer*2 x,y
      integer*2 pi0
      double precision time
      character sx*2,sy*2
      character spi*2
      character stime*8
      equivalence (pi0,spi)
      equivalence (x,sx)
      equivalence (y,sy)
      equivalence (stime,time)
      character string*100
      integer iigti,iigtilast,iigtifirst
      integer idistance
      real immax
      integer totcounts,mincounts
      logical qnew
      
c-----------------------------------------------------------------      
      edge=180
      npatches=20
      sortgti=.true.
      mincounts=300
      
      call getarg(2,data)
      
      call get_command_line_par  ('edge',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)edge
      endif
      
      call get_command_line_par  ('npacthes',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)npatches
      endif
      
      call get_command_line_par  ('sortgti',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        if (sortgti.eq.'NO'.or.sortgti.eq.'no') sortgti=.false.
      endif
      
      call get_command_line_par  ('mincounts',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)mincounts
      endif
      

      pcount=0
      gcount=1
      
      
      edge=edge**2
      
      
      
c read FITS file
      status=0
      call ftopen(iunit,data,1,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',data(1:lnblnk(data))
        call exit(1)
      endif
      
c move to the GTI extension
      print*,'read GTI'
      call move_ext (iunit,'GTI',status)
      if (status.ne.0) then
        write(0,*) 'Cannot find GTI extension; exit'
        call exit(1)
      endif

      call ftgkyj(iunit,'NAXIS2',ngti,comment,status) ! NGTI
      if (status.ne.0) then
        status=3
        return
      endif

      call ftgcno (iunit,.false.,'START',colnum,status)
      call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tstart,anyf,status)
      
      call ftgcno (iunit,.false.,'STOP',colnum,status)
      call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tend,anyf,status)
      
      if (status.ne.0) then
        write(0,*)'error reading GTIs'
        call exit(1)
      endif
       
      
c read EVENTS
      print*,'read events'
      call move_ext (iunit,'EVENTS',status)
      if (status.ne.0) then
        write(0,*) 'Cannot find EVENTS extension; exit'
        call exit(1)
      endif
      
      call ftgkyj (iunit,'NAXIS1',length,comment,status)
      call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~    ,status)
*      
*      print*,'allocate memory'
*      pxpix=malloc(2*nrows)
*      print*,'xpix'
*      pypix=malloc(2*nrows)
*      print*,'ypix'
*      ppi=malloc(nrows)
*      print*,'pi'
*      pigti=malloc(nrows)
*      print*,'igiti'
*      
*      if (pxpix.eq.0.or.pypix.eq.0.or.ppi.eq.0.or.pigti.eq.0) then
*        write(0,*)'not enough memory'
*        call exit(1)
*      endif
*      print*,'ok'
      
c necessary byte positions
      ftime=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'time'.and.ttype(ifield).ne.'TIME') then
          if (tform(ifield).eq.'1I') then
            ftime=ftime+2
          else if (tform(ifield).eq.'1J') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1E') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1D') then
            ftime=ftime+8
          else
            write(0,*) 'unknown format:',tform(ifield)(1:lnblnk(tform(ifield
     ~          )))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1D') then
            write (0,*) 'TIME field format: assume 1D, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 20
        endif
      enddo
       call exiterror('TIME column not found')
 20   continue
        
      fx=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'x'.and.ttype(ifield).ne.'X') then
          if (tform(ifield).eq.'1I') then
            fx=fx+2
          else if (tform(ifield).eq.'1J') then
            fx=fx+4
          else if (tform(ifield).eq.'1E') then
            fx=fx+4
          else if (tform(ifield).eq.'1D') then
            fx=fx+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'X field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 21
        endif
      enddo
       call exiterror('X column not found')
 21   continue
      
      fy=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'y'.and.ttype(ifield).ne.'Y') then
          if (tform(ifield).eq.'1I') then
            fy=fy+2
          else if (tform(ifield).eq.'1J') then
            fy=fy+4
          else if (tform(ifield).eq.'1E') then
            fy=fy+4
          else if (tform(ifield).eq.'1D') then
            fy=fy+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'Y field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 22
        endif
      enddo
       call exiterror('Y column not found')
 22   continue
      
      fpi=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'pi'.and.ttype(ifield).ne.'PI') then
          if (tform(ifield).eq.'1I') then
            fpi=fpi+2
          else if (tform(ifield).eq.'1J') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1E') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1D') then
            fpi=fpi+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'PI field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 23
        endif
      enddo
       call exiterror('PI column not found')
 23   continue

c read EVENTS
      print*,'read EVENTS'
      do irow=1,nrows
        call ftgtbs (iunit,irow,1,length,string,status)
c        decode bytes
        sx=string(fx:fx+1)
        sy=string(fy:fy+1)
        spi=string(fpi:fpi+1)
        stime=string(ftime:ftime+7)
          
        igti(irow)=char(0)
        do i=1,ngti
          if (time.ge.tstart(i).and.time.le.tend(i)) then
            igti(irow)=char(i)
            goto 30
          endif
        enddo
 30     continue
        
        xpix(irow)=x
        ypix(irow)=y
        pi(irow)=char(pi0)
        
        if ((irow/20000)*20000.eq.irow) then
          print '(i7,''  of   '',i7)',irow,nrows
        endif
      enddo

      print*,'events read'
      
      if (sortgti) then
        do i=1,ngti
          tspan(i)=tend(i)-tstart(i)
        enddo
        call indexx8 (ngti,tspan,wgti)
      else
        do i=ngti,1,-1
          wgti(i)=i
        enddo
      endif
      
      do i=1,512
        do j=1,512
          img1(i,j)=0
          img2(i,j)=0
          oimg(i,j)=0
          im1(i,j)=0
          im2(i,j)=0
          im0(i,j)=0
        enddo
      enddo
      
c form first image
      iigti=ngti
 700  continue
      dx(wgti(iigti))=0
      dy(wgti(iigti))=0
      
      totcounts=0
      do i=1,nrows
        if (ichar(igti(i)).eq.wgti(iigti)) then
          if (ichar(pi(i)).ge.40.and.ichar(pi(i)).le.200) then
            ii=int(float(xpix(i)-1)/10.)-510
            jj=int(float(ypix(i)-1)/10.)-510
            if(ii.ge.1.and.ii.le.512.and.jj.ge.1.and.jj.le.512)then
              im1(ii,jj)=im1(ii,jj)+1
              totcounts=totcounts+1
            endif
          endif
        endif
      enddo
      
      if (totcounts.lt.mincounts) then
        iigti=iigti-1
        goto 700
      endif
      
      do i=1,512
        do j=1,512
          im0(i,j)=im1(i,j)
        enddo
      enddo
      
      qnew=.true.
      iigtilast=iigti
      iigtifirst=iigti-1
      do iigti=iigtifirst,1,-1
        do i=1,512
          do j=1,512
            im1(i,j)=im0(i,j)
          enddo
        enddo
        print*,'GTI:',iigti
        if (qnew) then
          totcounts=0
          do i=1,512
            do j=1,512
              im2(i,j)=0
            enddo
          enddo
        endif
        do i=1,nrows
          if (ichar(igti(i)).eq.wgti(iigti)) then
            if (ichar(pi(i)).ge.40.and.ichar(pi(i)).le.200) then
              ii=int(float(xpix(i)-1)/10.)-510
              jj=int(float(ypix(i)-1)/10.)-510
              if(ii.ge.1.and.ii.le.512.and.jj.ge.1.and.jj.le.512)then
                im2(ii,jj)=im2(ii,jj)+1
                totcounts=totcounts+1
              endif
            endif
          endif
        enddo
        
        if (totcounts.gt.mincounts) then
          do i=1,512
            do j=1,512
              if (idistance(i,j,256,256).gt.edge) then
                img1(i,j)=0.0
                img2(i,j)=0.0
              else
                img1(i,j)=im1(i,j)
                img2(i,j)=im2(i,j)
              endif
            enddo
          enddo
          
          
          call ccorr2d (img1,img2,oimg,512,512)
          call crude_cgauss(oimg,512,512,5.0)
          
          immax=-1000.0
          i0=256+1
          j0=256+1
          do i=256-20,256+20
            do j=256-20,256+20
              if (oimg(i,j).gt.immax) then
                i0=i
                j0=j
                immax=oimg(i,j)
              endif
            enddo
          enddo
          
          dx(wgti(iigti))=-(256+1-i0)
          dy(wgti(iigti))=-(256+1-j0)
          
          print*,dx(wgti(iigti)),dy(wgti(iigti))
          
          if (dx(wgti(iigti))**2+dy(wgti(iigti))**2.gt.2*10**2) then
            
*            call show_r4 (oimg,512,512)
*            call show_i2 (im1,512,512)
*            call show_i2 (im2,512,512)
*            call show_i2 (im0,512,512)
            
            dx(wgti(iigti))=0
            dy(wgti(iigti))=0
            
          endif
          do i=1,512
            do j=1,512
              i1=i+dx(wgti(iigti))
              j1=j+dy(wgti(iigti))
              if (i1.ge.1.and.i1.le.512.and.j1.ge.1.and.j1.le.512) then
                im0(i,j)=im0(i,j)+im2(i,j)
              endif
            enddo
          enddo
          
          do i=iigtilast-1,iigti,-1
            dx(wgti(i))=dx(wgti(iigti))
            dy(wgti(i))=dy(wgti(iigti))
          enddo
          
          qnew=.true.
          iigtilast=iigti
          
        else
          print*,'!!! Too few counts:',totcounts
         dx(wgti(iigti))=0
         dy(wgti(iigti))=0
         qnew=.false.
       endif
      enddo
      
c write back EVENTS
      call ftrdef (iunit,status)
      print*,'write EVENTS'
      do irow=1,nrows
*        print*,irow,status
        if ((irow/20000)*20000.eq.irow) print*,irow,'  of',nrows
        call ftgtbs (iunit,irow,1,length,string,status)
c        decode bytes
        iigti=ichar(igti(irow))
        x=xpix(irow)+dx(iigti)*10
        y=ypix(irow)+dy(iigti)*10
        
        string(fx:fx+1)=sx(1:2)
        string(fy:fy+1)=sy(1:2)
        call ftptbs (iunit,irow,1,length,string,status)
      enddo
      
      call ftclos(iunit,status)
      if (status.ne.0) then
        call fitsio_write_error (status,0)
      endif
      
      end
      
      
*      
*      subroutine crude_cgauss (z,nx,ny,fwhm)
*c
*      implicit undefined (a-z)
*      integer nx,ny
*      real z(nx,ny)
*      real fwhm
*      
*      real sigma
*      integer w1,w2,w3
*      
*      
*      
*      
*      sigma=fwhm/(2.0*sqrt(2.0*alog(2.0)))
*      
*      sigma=nint(sigma*4.0)/4.0
*      
*      if (sigma.lt.1.0) then
*         call exiterror('too small FWHM in crude_cgauss')
*      endif
*      
*      if (abs(sigma-1.0).lt.0.01) then
*        w1=3
*        w2=3
*        w3=1
*      else if (abs(sigma-1.25).lt.0.01) then
*        w1=1
*        w2=3
*        w3=3
*      else if (abs(sigma-1.50).lt.0.01) then
*        w1=3
*        w2=3
*        w3=3
*      else if (abs(sigma-1.75).lt.0.01) then
*        w1=1
*        w2=5
*        w3=3
*      else if (abs(sigma-2.00).lt.0.01) then
*        w1=3
*        w2=5
*        w3=3
*      else if (abs(sigma-2.25).lt.0.01) then
*        w1=3
*        w2=5
*        w3=5
*      else if (abs(sigma-2.50).lt.0.01) then
*        w1=5
*        w2=5
*        w3=5
*      else if (abs(sigma-2.75).lt.0.01) then
*        w1=5
*        w2=7
*        w3=3
*      else if (abs(sigma-3.00).lt.0.01) then
*        w1=5
*        w2=5
*        w3=7
*      else if (abs(sigma-3.25).lt.0.01) then
*        w1=5
*        w2=7
*        w3=7
*      else if (abs(sigma-3.50).lt.0.01) then
*        w1=7
*        w2=7
*        w3=7
*      else if (abs(sigma-3.75).lt.0.01) then
*        w1=5
*        w2=7
*        w3=9
*      else if (abs(sigma-4.00).lt.0.01) then
*        w1=9
*        w2=7
*        w3=7
*      else if (abs(sigma-4.25).lt.0.01) then
*        w1=9
*        w2=9
*        w3=7
*      else if (abs(sigma-4.50).lt.0.01) then
*        w1=9
*        w2=9
*        w3=9
*      else if (abs(sigma-4.75).lt.0.01) then
*        w1=11
*        w2=9
*        w3=7
*      else if (abs(sigma-5.00).lt.0.01) then
*        w1=9
*        w2=11
*        w3=9
*      else if (abs(sigma-5.25).lt.0.01) then
*        w1=11
*        w2=9
*        w3=11
*      else if (abs(sigma-5.50).lt.0.01) then
*        w1=11
*        w2=11
*        w3=9
*      else if (abs(sigma-5.75).lt.0.01) then
*        w1=11
*        w2=11
*        w3=11
*      else if (abs(sigma-6.00).lt.0.01) then
*        w1=13
*        w2=11
*        w3=11
*      else if (abs(sigma-6.25).lt.0.01) then
*        w1=13
*        w2=13
*        w3=9
*      else if (abs(sigma-6.50).lt.0.01) then
*        w1=13
*        w2=11
*        w3=13
*      else if (abs(sigma-6.75).lt.0.01) then
*        w1=13
*        w2=13
*        w3=13
*      else if (abs(sigma-7.00).lt.0.01) then
*        w1=15
*        w2=13
*        w3=13
*      else if (abs(sigma-7.00).lt.0.01) then
*        w1=15
*        w2=13
*        w3=13
*      else if (abs(sigma-7.25).lt.0.01) then
*        w1=15
*        w2=15
*        w3=11
*      else if (abs(sigma-7.50).lt.0.01) then
*        w1=15
*        w2=13
*        w3=15
*      else if (abs(sigma-7.75).lt.0.01) then
*        w1=15
*        w2=15
*        w3=15
*      else if (abs(sigma-8.00).lt.0.01) then
*        w1=15
*        w2=17
*        w3=15
*      else if (abs(sigma-8.25).lt.0.01) then
*        w1=17
*        w2=17
*        w3=13
*      else if (abs(sigma-8.50).lt.0.01) then
*        w1=17
*        w2=17
*        w3=15
*      else if (abs(sigma-8.75).lt.0.01) then
*        w1=17
*        w2=17
*        w3=17
*      else if (abs(sigma-9.00).lt.0.01) then
*        w1=13
*        w2=19
*        w3=19
*      else if (abs(sigma-9.25).lt.0.01) then
*        w1=19
*        w2=15
*        w3=19
*      else if (abs(sigma-9.50).lt.0.01) then
*        w1=19
*        w2=19
*        w3=17
*      else if (abs(sigma-9.75).lt.0.01) then
*        w1=19
*        w2=19
*        w3=19
*      else if (abs(sigma-10.00).lt.0.01) then
*        w1=19
*        w2=21
*        w3=17
*      else if (abs(sigma-10.25).lt.0.01) then
*        w1=21
*        w2=19
*        w3=19
*      else if (abs(sigma-10.50).lt.0.01) then
*        w1=21
*        w2=21
*        w3=19
*      else if (abs(sigma-10.75).lt.0.01) then
*        w1=21
*        w2=17
*        w3=23
*      else if (abs(sigma-11.00).lt.0.01) then
*        w1=21
*        w2=19
*        w3=23
*      else if (abs(sigma-11.25).lt.0.01) then
*        w1=21
*        w2=21
*        w3=23
*      else if (abs(sigma-11.50).lt.0.01) then
*        w1=21
*        w2=23
*        w3=23
*      else if (abs(sigma-11.75).lt.0.01) then
*        w1=23
*        w2=19
*        w3=25
*      else if (abs(sigma-12.00).lt.0.01) then
*        w1=25
*        w2=23
*        w3=21
*      endif
*      
*      if (sigma.gt.12.0) then
*        w2=(23.0/12.0)*sigma
*        w2=(2*w2)/2+1
*        w1=w2+2
*        w3=w2-2
*      endif
*      
*      
*      
*      call conv_rect1(z,nx,ny,w1,w1)
*      call conv_rect1(z,nx,ny,w2,w2)
*      call conv_rect1(z,nx,ny,w3,w3)
*      
*      return
*      end
*      
