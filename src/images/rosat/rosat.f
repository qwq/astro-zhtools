      character arg*80
      integer lnblnk
      integer n,k

      call getarg (0,arg)
      n = lnblnk(arg)
      
      k = max(1,n-12)
      if ( arg(k:n) .eq. 'extrrosatspec' ) then
        call extrspec
        call exit(0)
      endif

      k = max(1,n-6)
      if ( arg(k:n) .eq. 'makeimg' ) then
        call makeimg
        call exit(0)
      endif

      k = max(1,n-5)
      if ( arg(k:n) .eq. 'select' ) then
        call select
        call exit(0)
      endif

      k = max(1,n-11)
      if ( arg(k:n) .eq. 'makerosatpsf' ) then
        call getarg (1,arg)
        if (arg.eq.'pspc') then
          call makerosatpspcpsf
          call exit(0)
        else
          write (0,*) 'Only ROSAT PSPC is currently supported, use as'
          call exiterror ('makerosatpsf pspc ... other args ...')
        endif
      endif
      
      k = max(1,n-11)
      if ( arg(k:n) .eq. 'rosatpsffwhm' ) then
        if (iargc().lt.1) call zhhelp ('rosatpsffwhm')
        call getarg (1,arg)
        if (arg.eq.'pspc') then
          call pspcfwhm
          call exit(0)
        else
          write (0,*) 'Only ROSAT PSPC is currently supported, use as'
          call exiterror ('rosatpsffwhm pspc ... other args ...')
        endif
      endif
      

      
      
      call getarg (1,arg)
      
      if (arg.eq.'-splitgti') then
        call splitgti
      else if (arg.eq.'-crosscorr') then
        call crosscorr
*      else if (arg.eq.'-phalign') then
*        call phalign
      else if (arg.eq.'-cleandata') then
        call cleandata
      else if (arg.eq.'-specmap') then
        call specmap
      else if (arg.eq.'-smoothros') then
        call smoothros
      endif
      
      call exit(0)
      end
      
