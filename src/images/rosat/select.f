      subroutine select
      implicit none
      character data*200,out*200
      
      integer lnblnk
      
c fitsio staff
      integer iunit,ounit
      parameter (iunit=10)
      parameter (ounit=11)
      integer status
      
      integer pcount,gcount
      
      character arg*80
      integer blocksize
      character comment*80
      integer length,nrows,tfields
      character ttype(300)*80,tform(300)*80,tunit(300)*80,extname*80
      integer varidat
      integer fx,fy,fpi,ftime
      integer ifield,irow
      
      integer*2 x,y,xmin,xmax,ymin,ymax
      integer*2 pi
      double precision time
      integer pimin,pimax
      
      integer keysexist,keysadd,ikey
      character hrec*200
      
      logical qtime,qgood,qx,qy,qpi
      double precision tmin,tmax
      character string*300
      character buff*8192
      
      integer nhdu,hdutype
      integer nrow,nbuff
      integer nbuffmax
      logical copyall
      logical defined
      integer iargc
c-----------------------------------------------------------------   
      if (iargc().lt.2) call zhhelp ('select')

      call getarg(1,data)
      call getarg(2,out)
      
      pimin=10
      pimax=256
      qpi=.false.
      xmin=1
      xmax=15360
      ymin=1
      ymax=15360
      tmin=0
      tmax=1.0e10
      copyall=.true.
      
      call get_command_line_par  ('pimin',1,arg)
      if (defined(arg)) then
        read (arg,*)pimin
        qpi=.true.
      endif

      call get_command_line_par  ('pimax',1,arg)
      if (defined(arg)) then
        read (arg,*)pimax
        qpi=.true.
      endif
      
      qtime=.false.
      call get_command_line_par  ('tmin',1,arg)
      if (defined(arg)) then
        qtime=.true.
        read (arg,*)tmin
      endif
      
      call get_command_line_par  ('tmax',1,arg)
      if (defined(arg)) then
        qtime=.true.
        read (arg,*)tmax
      endif
      
      call get_command_line_par ('xmin',1,arg)
      if (defined(arg)) then
        read (arg,*) xmin
        qx=.true.
      endif
      
      call get_command_line_par ('xmax',1,arg)
      if (defined(arg)) then
        read (arg,*) xmax
        qx=.true.
      endif
      
      call get_command_line_par ('ymin',1,arg)
      if (defined(arg)) then
        read (arg,*) ymin
        qy=.true.
      endif
      
      call get_command_line_par ('ymax',1,arg)
      if (defined(arg)) then
        read (arg,*) ymax
        qy=.true.
      endif

      call get_command_line_par ('copyall',1,arg)
      if (arg(1:1).eq.'n') then
        copyall=.false.
      endif
      
      
      pcount=0
      gcount=1
      
      
c open output
      call unlink(out)
      call ftinit(ounit,out,0,status)


      
c read FITS file
      status=0
      call ftopen(iunit,data,0,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',data(1:lnblnk(data))
        call exit(1)
      endif
      

c copy other extensions until we're at the EVENTS
      nhdu=1
 10   continue
      call ftmahd (iunit,nhdu,hdutype,status)
      if (hdutype.ne.0) then
        call ftgkys (iunit,'EXTNAME',extname,comment,status)
      else
        extname='prime'
      endif
      
      if (copyall.or.extname.eq.'prime') then
        if (extname.ne.'EVENTS'.and.
     ~      extname.ne.'events'.and.
     ~      extname.ne.'STDEVT') then
          if (nhdu.gt.1) then
            call ftcrhd(ounit,status)
          endif
          
          call ftcopy(iunit,ounit,0,status)
          
          nhdu = nhdu + 1
          goto 10
        endif
      endif
        
      
c read EVENTS
      call move_ext (iunit,'EVENTS',status)
      if (status.ne.0) then
                                ! TRY RDF DATA FORMAT
        status=0
        call move_ext (iunit,'STDEVT',status)
        if (status.ne.0) then
          status=0              ! TRY RDS DATA
          call move_ext (iunit,'PHOTLIST',status)
          if (status.ne.0) then
            write(0,*) 'Cannot find EVENTS extension; exit'
            call exit(1)
          endif
        endif
      endif

c copy events header
      call ftcrhd(ounit,status)
      call ftghsp (iunit,keysexist,keysadd,status)
        
      print*,'create eventsheader:',status
      
      do ikey=1,keysexist
        call ftgrec(iunit,ikey,hrec,status) 
        call ftprec(ounit,hrec,status)
      enddo
      
      if (qpi) then
        hrec=' '
        write(hrec,'(''HISTORY    selection:  PI '',i3,'' - '',i3)')pimin
     ~      ,pimax
        call ftprec(ounit,hrec,status)
      endif
        
      if (qx) then
        hrec=' '
        write(hrec,'(''HISTORY    selection:  X '',i5,'' - '',i5)')xmin
     ~      ,xmax
        call ftprec(ounit,hrec,status)
      endif
        
      if (qy) then
        hrec=' '
        write(hrec,'(''HISTORY    selection:  Y '',i5,'' - '',i5)')ymin
     ~      ,ymax
        call ftprec(ounit,hrec,status)
      endif
        
      if (qtime) then
        hrec=' '
        write(hrec,'(''HISTORY    selection:  TSTART '',1pe20.10)')tmin
        call ftprec(ounit,hrec,status)
        hrec=' '
        write(hrec,'(''HISTORY    selection:  TSTOP  '',1pe20.10)')tmax
        call ftprec(ounit,hrec,status)
      endif
        

        
      call ftgkyj (iunit,'NAXIS1',length,comment,status)
      call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~    ,status)
      
      call fthdef (ounit,0,status)
      call ftbdef (ounit,tfields,tform,varidat,nrows,status)
      
      nbuffmax=8192/length

      
c necessary byte positions
      ftime=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'time'.and.ttype(ifield).ne.'TIME') then
          if (tform(ifield).eq.'1I') then
            ftime=ftime+2
          else if (tform(ifield).eq.'1J') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1E') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1D') then
            ftime=ftime+8
          else
            write(0,*) 'unknown format:',tform(ifield)(1:lnblnk(tform(ifield
     ~          )))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1D') then
            write (0,*) 'TIME field format: assume 1D, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 20
        endif
      enddo
       call exiterror('TIME column not found')
 20   continue
        
      fx=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'x'.and.ttype(ifield).ne.'X') then
          if (tform(ifield).eq.'1I') then
            fx=fx+2
          else if (tform(ifield).eq.'1J') then
            fx=fx+4
          else if (tform(ifield).eq.'1E') then
            fx=fx+4
          else if (tform(ifield).eq.'1D') then
            fx=fx+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'X field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 21
        endif
      enddo
       call exiterror('X column not found')
 21   continue
      
      fy=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'y'.and.ttype(ifield).ne.'Y') then
          if (tform(ifield).eq.'1I') then
            fy=fy+2
          else if (tform(ifield).eq.'1J') then
            fy=fy+4
          else if (tform(ifield).eq.'1E') then
            fy=fy+4
          else if (tform(ifield).eq.'1D') then
            fy=fy+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'Y field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 22
        endif
      enddo
       call exiterror('Y column not found')
 22   continue
      
      fpi=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'pi'.and.ttype(ifield).ne.'PI') then
          if (tform(ifield).eq.'1I') then
            fpi=fpi+2
          else if (tform(ifield).eq.'1J') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1E') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1D') then
            fpi=fpi+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'PI field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 23
        endif
      enddo
       call exiterror('PI column not found')
 23   continue
      
      
      call fthdef (ounit,0,status)
      call ftbdef (ounit,tfields,tform,varidat,nrows,status)
      nrow=0
      nbuff=0

c read EVENTS
      print*,'read EVENTS'
      do irow=1,nrows
        qgood=.true.
        if (qpi) then
          call ftgtbb (iunit,irow,fpi,2,pi,status)
          qgood=pi.ge.pimin.and.pi.le.pimax
        endif
        
        if (qgood) then
          if (qtime) then
            call ftgtbb (iunit,irow,ftime,8,time,status)
            qgood=time.ge.tmin.and.time.le.tmax
          endif
        endif
        
        if (qgood) then
          if (qx) then
            call ftgtbb (iunit,irow,fx,2,x,status)
            qgood=x.ge.xmin.and.x.le.xmax
          endif
        endif
        
        if (qgood) then
          if (qy) then
            call ftgtbb (iunit,irow,fy,2,y,status)
            qgood=y.ge.ymin.and.y.le.ymax
          endif
        endif
        
        if (qgood) then
          call ftgtbs (iunit,irow,1,length,string,status)
          buff(nbuff*length+1:nbuff*length+length)=string(1:length)
          nbuff=nbuff+1
          if (nbuff.eq.nbuffmax) then
            call ftptbs (ounit,nrow+1,1,length*nbuffmax,buff,status)
            nrow=nrow+nbuffmax
            nbuff=0
          endif
        endif

        
        if ((irow/50000)*50000.eq.irow) then
          print '(i8,''  of   '',i8)',irow,nrows
        endif
      enddo
      
      call ftptbs (ounit,nrow+1,1,length*nbuff,buff,status)
      nrow=nrow+nbuff
      
      call ftmkyj(ounit,'NAXIS2',nrow,'the number of entries',status)
      call ftrdef(ounit,status)

      if (copyall) then
c copy other extensions until we're at the EVENTS
        nhdu=nhdu+1
 100    continue
        call ftmahd (iunit,nhdu,hdutype,status)
        if (status.ne.0) then
          status=0
          goto 110
        endif
        if (hdutype.ne.0) then
          call ftgkys (iunit,'EXTNAME',extname,comment,status)
        else
          extname='prime'
        endif
        
        if (extname.ne.'EVENTS'.and.extname.ne.'events') then
          if (nhdu.gt.1) then
            call ftcrhd(ounit,status)
          endif
          
          call ftcopy(iunit,ounit,0,status)
          
          nhdu = nhdu + 1
          goto 100
          
        endif
        
      endif
      
 110  continue
      call ftclos(ounit,status)
      if (status.ne.0) then
        write(0,*)'Close file:'
        call fitsio_write_error (status,0)
         call exiterror('')
      endif
        
      
      end
      







