      program makespec
      implicit none
      
      character parname*200,parvalue*200

      character bgtable*200
      character datadir*200,imgname*200
      character bandname*20,datatypename*20
      integer nbands
      parameter (nbands=7)
      real snowdenbg(nbands)
      real flux(nbands)
      real error(nbands)
      real bgerr(nbands), bgerrnorm(nbands), bgnorm
      integer channel(nbands)
      integer npix_s
      real backscale, areascale, corrscale, specarea, specscale, exposure_0
      character hist(100)*80,comment(100)*80
      real syserr(34)
      integer qualty,grping,ierr
      character voltage*1
      
      integer ounit,newunit
      integer bitpix, pcount, gcount, naxis, naxes(10)
      character phafile*200,backfil*80,none*80, ancrfil*200, none1*80
      character respfil*200,orespfile*200
      character detector*80
      logical defined


      integer n
      parameter (n=512)
      real rate(n,n,nbands)
      real err(n,n,nbands)
      real exposure(n,n,nbands)
      real totimg(n,n)
      integer region(n,n)
      logical include(n,n)
      logical onaxis
      
      integer bandmin,bandmax
      
      integer unit,status
      integer i,j,iband
      real w,sum
      
      integer nchan
      parameter (nchan=729)
      integer noff
      parameter (noff=40)
      real area (nchan,noff)
      real elow(nchan),ehigh(nchan)
      real warf(noff), arf(nchan)
      integer itheta,idistance,ioff
      
      character out(100)*80,outw*80
      character ireg(100)*80,iregw*80
      character words(100)*80
      integer nwords
      character regname*200
      integer jreg,kreg
      integer nout,nreg
      character gain*80
      integer nw,lnblnk
      
c ///   R E A D    P A R A M E T E R S

c --- BG table
      parname="bgtable"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call usage_makespec
        call exit(1)
      endif
      bgtable=parvalue

c --- OUT keyword
      parname="out"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call usage_makespec
        call exit(1)
      endif
      outw=parvalue
      nw=lnblnk(outw)
      do i=1,nw
        if (outw(i:i).eq.',') outw(i:i)=' '
      enddo
      call splitwords(outw,out,nout)

c --- exclude region
      parname="exclreg"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        do i=1,n
          do j=1,n
            include(i,j)=.true.
          enddo
        enddo
      else
        call read_fits_image (parvalue,include,n,n,'j')
        do i=1,n
          do j=1,n
            include(i,j)=.not.include(i,j)
          enddo
        enddo
      endif
      
c --- exclude point sources
      parname="exclpoint"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        continue
      else
        call read_fits_image (parvalue,region,n,n,'j')
        do i=1,n
          do j=1,n
            include(i,j)=include(i,j).and.(region(i,j).eq.0)
          enddo
        enddo
*        call saoimage (include,n,n,'j')
      endif
      
c --- Use on-axis resonce?
      parname="onaxis"
      call get_command_line_par (parname,1,parvalue)
      if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
        onaxis=.true.
      else
        onaxis=.false.
      endif
      

c --- region#
      parname="reg_todo"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        iregw='1'
      else
        iregw=parvalue
      endif
      nw=lnblnk(iregw)
      do i=1,nw
        if (iregw(i:i).eq.',') iregw(i:i)=' '
      enddo
      call splitwords(iregw,ireg,nreg)

      if (iregw.ne.'all') then
        if (nreg.ne.nout) then
          write(0,*)'Different number of out keywords and regions'
          write(0,*)'stop'
          call exit(1)
        endif
      endif

c --- Gain
      parname="gain"
      call get_command_line_par (parname,1,parvalue)
      if (parvalue.ne.'early'.and.parvalue.ne.'late') then
        write (0,*) 'You maust set gain to early or late'
        call exit(1)
      endif
      gain=parvalue

      if (gain.eq.'late') then
        voltage = 'l'
      else
        voltage = ' '
      endif

c --- detector
      parname="detector"
      call get_command_line_par (parname,1,parvalue)
      if (
     ~    parvalue.eq.'pspcc'.or.
     ~    parvalue.eq.'PSPCC'.or.
     ~    parvalue.eq.'PSPC-C'.or.
     ~    parvalue.eq.'pspc-c'.or.
     ~    parvalue.eq.'c'.or.
     ~    parvalue.eq.'C'
     ~    ) then
        detector='PSPCC'
      else
        detector='PSPCB'
      endif
      write (0,*) 'Use ROSAT/PSPC detector: ',detector(5:5)
      
c --- BAND min
      parname="bandmin"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        bandmin=2
      else
        read (parvalue,*) bandmin
      endif

c --- BAND max
      parname="bandmax"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        bandmax=7
      else
        read (parvalue,*) bandmax
      endif

      
c --- BG error
      parname="bgerr"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        do i=1,nbands
          bgerrnorm(i)=0.0
        enddo
      else
        call replace_characters (parvalue,',',' ')
        call splitwords(parvalue,words,nwords)
        if (nwords.eq.1) then
          read (parvalue,*) bgerrnorm(1)
          do i=2,nbands
            bgerrnorm(i)=bgerrnorm(1)
          enddo
        else
          if (nwords.ne.7) then
            write(0,*) 'set either 1 or 7 values for bgerrnorm'
            call exit (1)
          endif
          do i=1,nbands
            read (words(i),*) bgerrnorm(i)
          enddo
        endif
      endif
      
c --- BG error
      parname="bgnorm"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        bgnorm=1.0
      else
        read (parvalue,*) bgnorm
      endif
      

c --- DATAdir
      parname="datadir"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        datadir="./"
      else
        datadir=parvalue
      endif

c /// R E A D    D A T A
      
c --- BG table
      do i=1,nbands
        snowdenbg(i)=0
      enddo
      unit=newunit()
      open(unit,file=bgtable,status="old")
      status=0
      print*,'Backround intensities:'
      do while (status.eq.0)
        read (unit,*,iostat=status) iband,w
        if (status.eq.0) then
          snowdenbg(iband)=w
          print*,iband,snowdenbg(iband)
        endif
      enddo
      close(unit)
      print*,'--------------------------'
      
c --- Regions
      
      call get_command_line_par ('reg',1,regname)
      if (regname.eq.'undefined'.or.regname.eq.'UNDEFINED') then
        call usage_makespec
        call exit(1)
      endif
      
      call read_fits_image (regname,region,n,n,'j')
      
c --- calibration data
      call get_areas(area,nchan,noff,elow,ehigh,detector)

c --- Images
      write(0,*)'Reading images ...'
      do iband=bandmin,bandmax
        write(0,*)'band ',iband

        if (iband.gt.1) then
          write (bandname,'(i1)') iband
        else
          write (bandname,'(i1,a)') iband,voltage
        endif
        
        datatypename="rate"
        imgname=datadir
        call strcat(imgname,'/')
        call strcat(imgname,datatypename)
        call strcat(imgname,'_')
        call strcat(imgname,bandname)
        call strcat(imgname,'.fits')
        call read_fits_image (imgname,rate(1,1,iband),n,n,'e')
        
        datatypename="sig"
        imgname=datadir
        call strcat(imgname,"/")
        call strcat(imgname,datatypename)
        call strcat(imgname,"_")
        call strcat(imgname,bandname)
        call strcat(imgname,".fits")
        call read_fits_image (imgname,err(1,1,iband),n,n,'e')
        
        if (.not.onaxis) then
          datatypename="expose"
          imgname=datadir
          call strcat(imgname,"/")
          call strcat(imgname,datatypename)
          call strcat(imgname,"_")
          call strcat(imgname,bandname)
          call strcat(imgname,".fits")
          call read_fits_image (imgname,exposure(1,1,iband),n,n,'e')
        endif
      enddo


c /// make total image
      if (.not.onaxis) then
        do i=1,n
          do j=1,n
            sum=0
            do iband=2,7
              sum=sum+
     ~            (rate(i,j,iband)-bgnorm*snowdenbg(iband))*exposure(i,j,iband
     ~            )
            enddo
            totimg(i,j)=sum
          enddo
        enddo
      endif

      if (iregw.eq.'all') then
        nreg = 0
        do i=1,n
          do j=1,n
            nreg = max(nreg,region(i,j))
          enddo
        enddo
      endif

      do jreg=1,nreg
        if (iregw.eq.'all') then
          kreg = jreg
        else
          read(ireg(jreg),*) kreg
        endif
        print*,"region #",kreg
c /// calculate arf
        print*,'    get arf'
        do i=1,noff
          warf(i)=0
        enddo

        if (.not.onaxis) then
          do i=1,n
            do j=1,n
              if (include(i,j)) then
                if (region(i,j).eq.kreg) then
                  itheta=sqrt(idistance(i,j,n/2,n/2)/16.0)+1
                  warf(itheta)=warf(itheta)+totimg(i,j)
                endif
              endif
            enddo
          enddo
      
          sum=0
          do i=1,noff
            sum=sum+warf(i)
          enddo
          if (sum.gt.0.0) then
            do i=1,noff
              warf(i)=warf(i)/sum
            enddo
          endif
        else
          warf(1)=1.0
        endif
        
        do i=1,nchan
          sum=0.0
          do ioff=1,noff
            sum=sum+warf(ioff)*area(i,ioff)
          enddo
          arf(i)=sum
        enddo
      
c /// extract spectrum
        print*,'    get spec'
        do iband=1,7
          flux(iband)=0
          error(iband)=0
          bgerr(iband)=0
        enddo
        npix_s=0
        
        exposure_0=0
        do i=1,n
          do j=1,n
            if (include(i,j)) then
              if (region(i,j).eq.kreg) then
                npix_s=npix_s+1
                exposure_0=exposure_0+exposure(i,j,6)
                do iband=bandmin,bandmax
                  if (onaxis) then
                    flux(iband)=flux(iband)+
     ~                  rate(i,j,iband)-bgnorm*snowdenbg(iband)
                    error(iband)=error(iband)+
     ~                  err(i,j,iband)**2
                    bgerr(iband)=bgerr(iband)+
     ~                  (bgerrnorm(iband)*snowdenbg(iband))
                  else
                    flux(iband)=flux(iband)+
     ~                  (rate(i,j,iband)-bgnorm*snowdenbg(iband))*
     ~                  exposure(i,j,iband)
                    error(iband)=error(iband)+
     ~                  (err(i,j,iband)*exposure(i,j,iband))**2
                    bgerr(iband)=bgerr(iband)+
     ~                  (bgerrnorm(iband)*snowdenbg(iband)*exposure(i,j,iband))
                  endif
                enddo
              endif
            endif
          enddo
        enddo
        
        if (npix_s.gt.0) then
          if (.not.onaxis) then
            exposure_0=exposure_0/npix_s
          else
            exposure_0=1
          endif
          
          do iband=bandmin,bandmax
            error(iband)=sqrt(error(iband)+bgerr(iband)**2)
            flux(iband)=flux(iband)/exposure_0
            error(iband)=error(iband)/exposure_0
          enddo
        else
          if (iregw.eq.'all') goto 782
          exposure_0=0
          do iband=bandmin,bandmax
            error(iband)=1.0
            flux(iband)=0.0
          enddo
        endif
 
c /// save output
        print *,'   write pha'
        bitpix=-32
        pcount=0
        gcount=1
        naxis=0

        if (iregw.eq.'all') then
          write (phafile,'(a,''_'',i4,''.pha'')') out(1),kreg
          write (ancrfil,'(a,''_'',i4,''.arf'')') out(1),kreg
          write (orespfile,'(a,''_'',i4,''.rmf'')') out(1),kreg
          call rmblanks(phafile)
          call rmblanks(ancrfil)
          call rmblanks(orespfile)
        else
          phafile=out(jreg)
          call strcat(phafile,".pha")
          ancrfil=out(jreg)
          call strcat(ancrfil,".arf")
          orespfile=out(jreg)
          call strcat (orespfile,".rmf")
        endif

        status=0
        call ftgiou (ounit,status)
        call unlink(phafile)
        call ftinit(ounit,phafile,0,status)
        call ftphpr(ounit,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .false.,status)
        call ftpdef(ounit,bitpix,naxis,naxes,pcount,gcount,status)
        if (status.ne.0) then
          print*,phafile(1:lnblnk(phafile)),' ',status
          call fitsio_write_error (status,0)
          call exit(1)
        endif
        backfil='none'
        if (gain.eq.'early') then
          if (detector.eq.'PSPCC') then
            respfil="caldata/rosat/ogip/pspcc_gain1_7.rmf"
          else
            respfil="caldata/rosat/ogip/pspcb_gain1_7.rmf"
          endif
        else
          if (detector.eq.'PSPCC') then
            respfil="caldata/rosat/ogip/pspcc_gain1_7.rmf"
          else
            respfil="caldata/rosat/ogip/pspcb_gain2_7.rmf"
          endif
        endif
        call dataenv(respfil,"ZHTOOLS")
        
        print*,'   ... link resp'
        print*,respfil,orespfile
        call unlink (orespfile)
        call symlnk (respfil,orespfile)
        respfil=orespfile
        print*,'   ... ok'
        
        
        do i=1,nbands
          channel(i)=i
        enddo
        
        areascale=1.0
        backscale=1.0
        corrscale=0.0
        
c My keywords                               
        specarea=(npix_s*(15.0)**2)/3600.0 ! To arcmin^2
        specscale=1.0           ! No additional corrections
        
        none='none'
        none1='none'
        call wtpha1(ounit,0,0,hist,0,
     &      comment,'ROSAT',detector,' ',NONE1,
     &      '1.1.0','TOTAL',1,
     &      exposure_0,areascale,backfil,backscale,NONE1, 
     &      corrscale,respfil,ancrfil,7,'PHA',
     &      channel,flux,2,.true.,error,.false.,
     &      syserr,.false.,qualty,.false.,grping,7,
     &      specarea,specscale,
     &      ierr)
        
        call ftclos(ounit,status)
        call ftfiou(ounit,status)
        if (status.gt.0) then
          write(0,*)'Writing PHA file:'
          call fitsio_write_error(status,0)
        endif
        
c  ARF
        print*,'   write arf'
        call ftgiou(ounit,status)
        call unlink(ancrfil)
        call ftinit(ounit,ancrfil,0,status)
        call ftphpr(ounit,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .false.,status)
        call ftpdef(ounit,bitpix,naxis,naxes,pcount,gcount,status)
        call wtarf1(ounit, 0,0, hist,0, comment,
     ~      '1.1.0',phafile,
     &      'ROSAT', detector, ' ', NONE1,
     &      nchan, nchan, elow, ehigh,arf, ierr)
        call ftclos(ounit,status)
        call ftfiou(ounit,status)
        if (status.gt.0) then
          write(0,*)'Writing ARF file:'
          call fitsio_write_error(status,0)
        endif

 782    continue
c -- end of region loop
      enddo
      end

      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine get_areas (area,nchan,noff,elow,ehigh,detector)
      implicit none
      integer nchan,noff
      real area(nchan,noff)
      real area0(729,14)
      real theta0(14)
      real elow(nchan),ehigh(nchan)
      integer iunit,status,rwmode,blocksize,hdutype,colnum
      logical anyf
      integer i,itheta,ioff
      real w1,w2,theta
      character detector*(*)
  
      character filename*200
      
      if (detector.eq.'PSPCC') then
        filename="caldata/rosat/ogip/pspcc_v1.spec_resp"
      else
        filename="caldata/rosat/ogip/pspcb_v2.spec_resp"
      endif
      call dataenv(filename,"ZHTOOLS")
      
      status=0
      call ftgiou(iunit,status)
      rwmode=0
      call ftopen(iunit,filename,rwmode,blocksize,status)
      call ftmahd(iunit,2,hdutype,status)
      
      call ftgcno(iunit,.false.,'ENERG_LO',colnum,status)
      call ftgcve(iunit,colnum,1,1,729,0.0,elow,anyf,status)
      call ftgcno(iunit,.false.,'ENERG_HI',colnum,status)
      call ftgcve(iunit,colnum,1,1,729,0.0,ehigh,anyf,status)
      call ftgcno(iunit,.false.,'THETA',colnum,status)
      call ftgcve(iunit,colnum,1,1,14,0.0,theta0,anyf,status)
      call ftgcno(iunit,.false.,'SPECRESP',colnum,status)
      call ftgcve(iunit,colnum,1,1,10206,0.0,area0,anyf,status)
      call ftclos(iunit,status)
      call ftfiou(iunit,status)
      if (status.ne.0) then
        call fitsio_write_error(status,0)
      endif

      do itheta=1,noff
        theta=itheta-1
        ioff=0
        do i=1,14
          if(theta.ge.theta0(i).and.theta.lt.theta0(i+1)) then
            ioff=i
            goto 100
          endif
        enddo
         call exiterror(' could not find offaxis')
 100    continue
        
        w1=(theta0(ioff+1)-theta)/(theta0(ioff+1)-theta0(ioff))
        w2=1.0-w1
        do i=1,729
          area(i,itheta)=w1*area0(i,ioff)+w2*area0(i,ioff+1)
        enddo
      enddo
       
      return
      end
      
       

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine usage_makespec
      call zhhelp ('makesnowdenspec')
      return
      end
      
