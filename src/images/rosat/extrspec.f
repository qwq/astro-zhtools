      subroutine extrspec
      implicit none
      character data*200,out*200
      
      integer lnblnk
      
c GTI 
      integer ngti
      double precision tstart(1000),tend(1000)
      real exposure_0
      
c image arrays
      integer region (512,512),img(512,512),bimg(512,512)
      character bkgdreg*200, srcreg*200
      
c fitsio staff
      integer iunit,ounit
      parameter (iunit=10)
      parameter (ounit=11)
      integer status
      
      integer pcount,gcount,colnum
      logical anyf
      
      character arg*200
      integer blocksize
      integer length,nrows,tfields
      character ttype(300)*80,tform(300)*80,tunit(300)*80,extname*80
      integer varidat
      integer fx,fy,fpi,ftime
      integer ifield,irow,i,j,ii,jj
      
      integer*2 x,y
      integer*2 pi
      integer ipi
      double precision time
      integer pimin,pimax
      
      integer bitpix
      integer naxis,naxes(10)
      
      logical qtime,qgood
      double precision tmin,tmax
      
      integer channel(34)
      real srcspec(34),bkgdspec(34)
      
      real offaxis_s(100),offaxis_b(100)
      
      integer ch
      real sum
      integer pi_to_sass
      integer pixel
      
      logical rdf
      
      integer npix_s, npix_b
      integer itheta,idistance,itheta_s
      
      character phafile*200,backfil*200,respfil*200,ancrfil*200
      character hist(100)*80,comment(100)*80
      real serr(34),syserr(34)
      integer qualty,grping,ierr
      
      character XNAME*20, YNAME*20
      character eventsname*80,gtiname*80
      
      real nh
      real floor
      
      character bpi*1
      logical qbpi
      integer xmin,xmax,ymin,ymax
      
      real areascale, backscale, corrscale
      
      real specarea, specscale
      
      character chartest*80
      character NONE1*4,none*4
      
      
c-----------------------------------------------------------------  
      NONE1='NONE'
      none='none'
      
      data = ' '
      out  = ' '
      srcreg = none
      bkgdreg = none
      
      nh=0
      qbpi=.false.
      
      call get_command_line_par  ('data',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        data=arg
      endif
      
      call get_command_line_par  ('out',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        out=arg
      endif
      
      call get_command_line_par  ('src',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        srcreg=arg
      endif
      
      call get_command_line_par  ('bkgd',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        bkgdreg=arg
      endif

      
      
      if (data.eq.' '.or.out.eq.' '.or.(srcreg.eq.none.and.bkgdreg.eq.none
     ~    )) then
        write(0,*
     ~      )'Usage: extrrosatspec data=events out=keyword src=source_region\\'
        write(0,*
     ~      )'               bkgd=background_region\\'
        write(0,*
     ~      )'       [tmin=tmin] [tmax=tmax] \\'
        write(0,*
     ~      )'       [pixel=pixelsz (arcsec)] '
        call exit(1)
      endif
      
      pimin=1
      pimax=256
      rdf=.false.
      pixel=15
      
      tmin=0
      tmax=1.0e10
      qtime=.false.
      call get_command_line_par  ('tmin',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        qtime=.true.
        read (arg,*)tmin
      endif
      
      call get_command_line_par  ('tmax',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        qtime=.true.
        read (arg,*)tmax
      endif
      
      call get_command_line_par  ('pixel',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)pixel
      endif
      
      
      call get_command_line_par  ('nh',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)nh
      endif
      
      
      call get_command_line_par  ('xrange',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        do i=1,lnblnk(arg)
          if (arg(i:i).eq.',') arg(i:i)=' '
        enddo
        read (arg,*)xmin,xmax
      else
        xmin=1
        xmax=15360
      endif
      
      call get_command_line_par  ('yrange',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        do i=1,lnblnk(arg)
          if (arg(i:i).eq.',') arg(i:i)=' '
        enddo
        read (arg,*)ymin,ymax
      else
        ymin=1
        ymax=15360
      endif

      call get_command_line_par  ('xname',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        xname=arg
      else
        xname='X'
      endif
      
      call get_command_line_par  ('yname',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        yname=arg
      else
        yname='Y'
      endif
      
      call get_command_line_par  ('events',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        eventsname=arg
      else
        eventsname='EVENTS'
      endif
      
      call get_command_line_par  ('gti',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        gtiname=arg
      else
        gtiname='GTI'
      endif
      
      pixel=pixel*2

      
      if (bkgdreg.eq.'standard'.and.nh.lt.1.0) then
        write(0,*)'you must specify nH'
        call exit(1)
      endif
      
c make region image
      call make_region_image (region,srcreg,lnblnk(srcreg),bkgdreg
     ~    ,lnblnk(bkgdreg))
      pcount=0
      gcount=1

*****      call show_i4(region,512,512)
      
      
c read FITS file
      status=0
      call ftopen(iunit,data,0,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',data(1:lnblnk(data))
        call exit(1)
      endif


c read GTIs
      if (gtiname.ne.none) then
        call move_ext (iunit,gtiname,status)
        if (status.ne.0) then
                                ! TRY RDF DATA FORMAT
          status=0
          call move_ext (iunit,'STDGTI',status)
          if (status.ne.0) then
            status=0
            call move_ext (iunit,'GTIINFO',status)
            if (status.ne.0) then
              write(0,*) 'Cannot find GTI extension; exit'
              call exit(1)
            endif
          endif
          rdf=.true.
        endif
        
        
        call ftgkyj(iunit,'NAXIS2',ngti,comment,status) ! NGTI
        if (status.ne.0) then
          call fitsio_write_error(status,0)
          call exit(1)
        endif
        
        call ftgcno (iunit,.false.,'START',colnum,status)
        call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tstart,anyf,status)
        
        call ftgcno (iunit,.false.,'STOP',colnum,status)
        if (status.ne.0) then   ! TRY RDS DATA FORMAT
          status=0
          call ftgcno (iunit,.false.,'FINISH',colnum,status)
        endif
        call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tend,anyf,status)
        
        if (status.ne.0) then
          write(0,*)'error reading GTIs'
          call exit(1)
        endif
        
        exposure_0=0
        do i=1,ngti
          exposure_0=exposure_0+tend(i)-tstart(i)
        enddo
        
        
      else
        exposure_0=1.0
      endif
        
      
c read EVENTS
      call move_ext (iunit,eventsname,status)
      if (status.ne.0) then
        write(0,*) 'Cannot find EVENTS extension; exit'
        print*,eventsname
        call exit(1)
      endif
      
      call ftgkyj (iunit,'NAXIS1',length,comment,status)
      call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~    ,status)
      
c necessary byte positions
      if (qtime) then
        ftime=1
        do ifield=1,tfields
          if (ttype(ifield).ne.'time'.and.ttype(ifield).ne.'TIME') then
            if (tform(ifield).eq.'1I'.or.tform(ifield).eq.'I') then
              ftime=ftime+2
            else if (tform(ifield).eq.'1J'.or.tform(ifield).eq.'J') then
              ftime=ftime+4
            else if (tform(ifield).eq.'1E'.or.tform(ifield).eq.'E') then
              ftime=ftime+4
            else if (tform(ifield).eq.'1D'.or.tform(ifield).eq.'D') then
              ftime=ftime+8
            else
              write(0,*) 'unknown format:',tform(ifield)(1:lnblnk(tform(ifield
     ~            )))
               call exiterror('')
            endif
          else
            if (tform(ifield).ne.'1D'.and.tform(ifield).ne.'D') then
              write (0,*) 'TIME field format: assume 1D, got ',tform(ifield
     ~            )(1:lnblnk(tform(ifield)))
               call exiterror('')
            endif
            goto 20
          endif
        enddo
         call exiterror('TIME column not found')
 20     continue
      endif
      
      fx=1
      do ifield=1,tfields
        if (ttype(ifield).ne.xname) then
          if (tform(ifield).eq.'1I'.or.tform(ifield).eq.'I') then
            fx=fx+2
          else if (tform(ifield).eq.'1J'.or.tform(ifield).eq.'J') then
            fx=fx+4
          else if (tform(ifield).eq.'1E'.or.tform(ifield).eq.'E') then
            fx=fx+4
          else if (tform(ifield).eq.'1D'.or.tform(ifield).eq.'D') then
            fx=fx+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I'.and.tform(ifield).ne.'I') then
            write (0,*) 'X field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 21
        endif
      enddo
       call exiterror('X column not found')
 21   continue
      
      fy=1
      do ifield=1,tfields
        if (ttype(ifield).ne.yname) then
          if (tform(ifield).eq.'1I'.or.tform(ifield).eq.'I') then
            fy=fy+2
          else if (tform(ifield).eq.'1J'.or.tform(ifield).eq.'J') then
            fy=fy+4
          else if (tform(ifield).eq.'1E'.or.tform(ifield).eq.'E') then
            fy=fy+4
          else if (tform(ifield).eq.'1D'.or.tform(ifield).eq.'D') then
            fy=fy+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I'.and.tform(ifield).ne.'I') then
            write (0,*) 'Y field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 22
        endif
      enddo
       call exiterror('Y column not found')
 22   continue
      
      qbpi=.false.
      fpi=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'pi'.and.ttype(ifield).ne.'PI') then
          if (tform(ifield).eq.'1I'.or.tform(ifield).eq.'I') then
            fpi=fpi+2
          else if (tform(ifield).eq.'1J'.or.tform(ifield).eq.'J') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1E'.or.tform(ifield).eq.'E') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1D'.or.tform(ifield).eq.'D') then
            fpi=fpi+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I'.and.tform(ifield).ne.'I') then
            if (tform(ifield).eq.'B') then
              qbpi=.true.
            else
              write (0,*) 'PI field format: assume 1I or B, got ',tform(ifield
     ~            )(1:lnblnk(tform(ifield)))
               call exiterror('')
            endif
          endif
          goto 23
        endif
      enddo
       call exiterror('PI column not found')
 23   continue
      
      do i=1,512
        do j=1,512
          img(i,j)=0
          bimg(i,j)=0
        enddo
      enddo
          
      do i=1,34
        srcspec(i)=0
        bkgdspec(i)=0
      enddo

c read EVENTS
      do irow=1,nrows
        if (qbpi) then
          call ftgtbb (iunit,irow,fpi,1,bpi,status)
          pi=ichar(bpi)
        else
          call ftgtbb (iunit,irow,fpi,2,pi,status)
        endif
        ipi=pi
        if (pi.ge.pimin.and.pi.le.pimax) then
          if (qtime) then
            call ftgtbb (iunit,irow,ftime,8,time,status)
            qgood=time.ge.tmin.and.time.le.tmax
          else
            qgood=.true.
          endif
          if (qgood) then
            call ftgtbb (iunit,irow,fx,2,x,status)
            call ftgtbb (iunit,irow,fy,2,y,status)
            ii=floor(float(x-xmin)/pixel)+1
            jj=floor(float(y-ymin)/pixel)+1
            if(ii.ge.1.and.ii.le.512.and.jj.ge.1.and.jj.le.512)then
              if (region(ii,jj).eq.1) then
                ch=pi_to_sass(ipi)
                if (ch.ne.0) then
                  img(ii,jj)=img(ii,jj)+1
                  srcspec(ch)=srcspec(ch)+1
                endif
              else if (region(ii,jj).eq.2) then
                ch=pi_to_sass(ipi)
                if (ch.ne.0) then
                  bimg(ii,jj)=bimg(ii,jj)+1
                  bkgdspec(ch)=bkgdspec(ch)+1
                endif
              endif
            endif
          endif
        endif
      enddo

*      call saoimage (img,512,512,'j')
*      call saoimage (bimg,512,512,'j')
c off-axis histograms
      do i=1,100
        offaxis_s(i)=0
        offaxis_b(i)=0
      enddo
      
      
      naxes(1)=nint(((xmax-xmin)/15360.0)*512.0*30.0/pixel)
      naxes(2)=nint(((ymax-ymin)/15360.0)*512.0*30.0/pixel)
      
      print*, naxes(1), naxes(2)

      npix_s=0
      npix_b=0
      do i=1,512
        do j=1,512
          itheta=(sqrt(float(idistance(i,j,naxes(1)/2,naxes(2)/2))))/(60.0
     ~        /(pixel/2))+1
          offaxis_s(itheta)=offaxis_s(itheta)+img(i,j)
          offaxis_b(itheta)=offaxis_b(itheta)+bimg(i,j)
          if (region(i,j).eq.1) npix_s=npix_s+1
          if (region(i,j).eq.2) npix_b=npix_b+1
        enddo
      enddo
      
      
      
      sum=0.0
      do i=1,100
        sum=sum+offaxis_s(i)
      enddo
      if (sum.gt.0.0) then
        sum=sum/100.0
        do i=1,100
          offaxis_s(i)=offaxis_s(i)/100.0
        enddo
      endif
      
      
      sum=0.0
      do i=1,100
        sum=sum+offaxis_b(i)
      enddo
      if (sum.gt.0.0) then
        sum=sum/100.0
        do i=1,100
          offaxis_b(i)=offaxis_b(i)/100.0
        enddo
      endif
      

c  !!!! attention -- instead of weighting the off-axis angles, 
c      use max. weight angle
      
      sum=0.0
      do i=1,100
        if (sum.lt.offaxis_s(i)) then
          itheta_s=i-1
          sum=offaxis_s(i)
        endif
      enddo
      
      bitpix=-32
      
      pcount=0
      gcount=1
      naxis=0
      
c  write up the SOURCE spectrum
      if (srcreg(1:4).ne.none) then
        phafile=out(1:lnblnk(out))//'.pha'
        
      
c   Primary header      
        call unlink(phafile)
        call ftinit(ounit,phafile,0,status)
        call ftphpr(ounit,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .false.,status)
        call ftpdef(ounit,bitpix,naxis,naxes,pcount,gcount,status)
        
c   SPECTRUM extension
        if (bkgdreg(1:4).ne.none) then
          backfil=out(1:lnblnk(out))//'.bcg'
        else
          backfil=none
        endif
        
        respfil='caldata/rosat/pspcb_93jan12.rmf.34'
        call dataenv(respfil,'ZHTOOLS')
        
        write(ancrfil,'(a,i2.2)')'caldata/rosat/pspcb_area.arf.',itheta_s
        call dataenv(ancrfil,'ZHTOOLS')
        
        do i=1,34
          channel(i)=i
        enddo
        
        areascale=1.0
        corrscale=0.0
        
c My keywords                               
        specarea=(npix_s*(pixel/2)**2)/3600.0 ! To arcmin^2
        specscale=1.0                         ! No additional corrections
        backscale=specarea
        
        call wtpha1(ounit,0,0,hist,0,
     &			comment,'ROSAT','PSPC',' ',NONE1,
     &			'1.1.0','TOTAL',1,
     &                  exposure_0,areascale,backfil,backscale,NONE1, 
     &			corrscale,respfil,ancrfil,34,'PHA',
     &			channel,srcspec,1,.false.,serr,.false.,
     &			syserr,.false.,qualty,.false.,grping,34,
     &                  specarea,specscale,
     &			ierr)
        
        call ftclos(ounit,status)
        if (status.ne.0) then
          write(0,*)'Writing PHA file:'
          call fitsio_write_error(status,0)
        endif
      endif

      chartest=bkgdreg(1:4)
      if (chartest.ne.none) then
        
        phafile=out(1:lnblnk(out))//'.bcg'
        
      
c   Primary header      
        call unlink(phafile)
        call ftinit(ounit,phafile,0,status)
        call ftphpr(ounit,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .false.,status)
        call ftpdef(ounit,bitpix,naxis,naxes,pcount,gcount,status)
        
c   SPECTRUM extension
        backfil=none

        respfil='caldata/rosat/pspcb_93jan12.rmf.34'
        call dataenv(respfil,'ZHTOOLS')
        
        write(ancrfil,'(a,i2.2)')'caldata/rosat/pspcb_area.arf.',itheta_s
        call dataenv(ancrfil,'ZHTOOLS')
        
        do i=1,34
          channel(i)=i
        enddo

        if (bkgdreg(1:8).ne.'standard') then
          
          areascale=1.0
          corrscale=0.0
        
          specarea=npix_b*(pixel/2)**2/3600.0 ! To arcmin^2
          specscale=1.0                       ! no additional corrections
          backscale=specarea

          call wtpha1(ounit,0,0,hist,0,
     &        comment,'ROSAT','PSPC',' ',NONE1,
     &        '1.1.0','BKG',1,
     &        exposure_0,areascale,backfil,backscale,NONE1, 
     &        corrscale,respfil,ancrfil,34,'PHA',
     &        channel,bkgdspec,1,.false.,serr,.false.,
     &        syserr,.false.,qualty,.false.,grping,34,
     &        specarea, specscale,
     &        ierr)
        else
          call st_bkgd (float(itheta_s),nh,bkgdspec,status)
          if (status.ne.0) then
            write(0,*)'Error reading standard ROSAT background spectrum:',
     ~          status
            call exit(1)
          endif
          print*,'Standard specrum'
          do i=1,34
            bkgdspec(i)=bkgdspec(i)*(pixel/2)**2*npix_s
            serr(i)=0.0
            print*,i,bkgdspec(i),serr(i)
          enddo
          
                       ! calculated over the source are
          specarea=npix_s*(pixel/2)**2/3600.0 ! To arcmin^2
          specscale=1.0                       ! No additional corrections

          call wtpha1(ounit,0,0,hist,1,
     &        'standard ROSAT background','ROSAT','PSPC',' ',NONE1,
     &        '1.1.0','BKG',1,
     &        exposure_0,1.0,backfil,1.0,NONE1, 
     &        0.0,respfil,ancrfil,34,'PHA',
     &        channel,bkgdspec,2,.true.,serr,.false.,
     &        syserr,.false.,qualty,.false.,grping,34,
     &        specarea,specscale,
     &        ierr)
        endif
          
          
        call ftclos(ounit,status)
        if (status.ne.0) then
          write(0,*)'Writing PHA file:'
          call fitsio_write_error(status,0)
        endif
      endif
      
      
      return
      end
      







