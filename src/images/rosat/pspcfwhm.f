      subroutine pspcfwhm
      implicit none
      character arg*80
      logical sigma
      integer iargc
      real theta,e,fpspc,fmirror
*     real faspect
      real fwhm
      
      if (iargc().lt.2)  call zhhelp ('rosatpsffwhm')
      
      call getarg(2,arg)
      read (arg,*) theta
      
      call get_command_line_par ('e',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read(arg,*)E
      else
        E=1.0
      endif
      
      call get_command_line_par ('sigma',1,arg)
      if (arg(1:1).eq.'y'.or.arg(1:1).eq.'Y') then
        sigma=.true.
      else
        sigma=.false.
      endif
      
      
      
*      fpspc=sqrt(409.65/E + 69.28*E**2.88 + 66.29)
*      fmirror=0.29*THETA**1.74
*      faspect=2.355*1.5
*        
*      fwhm=sqrt((fpspc**2+fmirror**2+faspect**2))

      fpspc=sqrt(108.7*E**(-0.888) + 1.121*E**6)
      fmirror=sqrt(0.219*THETA**2.848)
      
      fwhm=sqrt(fpspc**2+fmirror**2)*2.355
      
      if (sigma) fwhm=fwhm/2.355
      
      print*,fwhm,fwhm/5

      end
      
        
