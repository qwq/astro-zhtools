      subroutine splitgti
      implicit none
      integer iunit
      integer ounit(12),igti(12),nrow(12)
      
      character rootname*80,arg*80
      character outname(11)*80
      
      integer ngti
      double precision tstart(1000),tend(1000)
      
      integer status
      integer blocksize
      
      character comment*80
      
      integer ngtifirst,ngtilast
      integer i
      integer lnblnk
      integer colnum
      logical anyf
      integer nhdu,hdutype
      character extname*80
      
      integer keysexist,keysadd,ikey
      character hrec*200
      
      integer length,nrows
      character ttype(300)*80,tform(300)*80,tunit(300)*80
      integer tfields
      integer varidat
      integer firstbyte
      integer ifield,irow
      character string*300
      character stime*8
      double precision time
      equivalence (stime,time)
      
      character buff(12)*8192
      integer nbuff(12),nbuffmax
      integer*2 pimin,pimax,pi
      character spi*2,sx*2,sy*2
      equivalence (pi,spi)
      integer*2 xmin,xmax,x
      integer*2 ymin,ymax,y
      equivalence (x,sx)
      equivalence (y,sy)
      
      logical query,good
      
      integer fx,fy,fpi
c---------------------------------------------------------------      
      call getarg(2,rootname)
      
      query=.false.
      pimin=1
      pimax=256
      xmin=1
      xmax=15360
      ymin=1
      ymax=15360
      
      call get_command_line_par ('pimin',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*) pimin
        query=.true.
      endif
      
      call get_command_line_par ('pimax',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*) pimax
        query=.true.
      endif
      
      call get_command_line_par ('xmin',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*) xmin
        query=.true.
      endif
      
      call get_command_line_par ('xmax',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*) xmax
        query=.true.
      endif
      
      call get_command_line_par ('ymin',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*) ymin
        query=.true.
      endif
      
      call get_command_line_par ('ymax',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*) ymax
        query=.true.
      endif
      
      
      iunit=1
      
      do i=1,11
        ounit(i)=20+i
      enddo
      
      status=0
      call ftopen(iunit,rootname,0,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',rootname(1:lnblnk(rootname))
        call exit(1)
      endif
      
c move to the GTI extension
      call move_ext (iunit,'GTI',status)
      if (status.ne.0) then
        write(0,*) 'Cannot find GTI extension; exit'
        call exit(1)
      endif

      call ftgkyj(iunit,'NAXIS2',ngti,comment,status) ! NGTI
      if (status.ne.0) then
        status=3
        return
      endif

      call ftgcno (iunit,.false.,'START',colnum,status)
      call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tstart,anyf,status)
      
      call ftgcno (iunit,.false.,'STOP',colnum,status)
      call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tend,anyf,status)
      
      if (status.ne.0) then
        write(0,*)'error reading GTIs'
        call exit(1)
      endif
        
      

      ngtifirst=1
      ngtilast=ngtifirst-1+11
      ngtilast=min(ngtilast,ngti)
      
      do while (ngtifirst.le.ngti)
        
        ngtilast=ngtifirst-1+11
        ngtilast=min(ngtilast,ngti)
        
        do i=ngtifirst,ngtilast
          igti(i-ngtifirst+1)=i
          outname(i-ngtifirst+1)=' '
          write(outname(i-ngtifirst+1),'(a,''.gti.'',i4)'
     ~        )rootname(1:lnblnk(rootname)),igti(i-ngtifirst+1)
          call rmblanks(outname(i-ngtifirst+1))
          call unlink(outname(i-ngtifirst+1))
          call ftinit(ounit(i-ngtifirst+1),outname(i-ngtifirst+1),0,status)
        enddo
        
        if (status.ne.0) then
          call fitsio_print_error(status)
           call exiterror('')
        endif
        
c copy other extensions until we're at the EVENTS
        nhdu=1
 10     continue
        call ftmahd (iunit,nhdu,hdutype,status)
        if (hdutype.ne.0) then
          call ftgkys (iunit,'EXTNAME',extname,comment,status)
        else
          extname='prime'
        endif
        
        if (extname.ne.'EVENTS'.and.extname.ne.'events') then
          if (nhdu.gt.1) then
            do i=1,ngtilast-ngtifirst+1
              call ftcrhd(ounit(i),status)
            enddo
          endif
        
          do i=1,ngtilast-ngtifirst+1
            call ftcopy(iunit,ounit(i),0,status)
          enddo
          
          nhdu = nhdu + 1
          goto 10
          
        endif


c copy events header
        do i=1,ngtilast-ngtifirst+1
          call ftcrhd(ounit(i),status)
        enddo
        call ftghsp (iunit,keysexist,keysadd,status)
        
        print*,'create eventsheader:',status
        
        do ikey=1,keysexist
          call ftgrec(iunit,ikey,hrec,status) 
          do i=1,ngtilast-ngtifirst+1
            call ftprec(ounit(i),hrec,status)
          enddo
        enddo

        
c copy events data
        call ftgkyj (iunit,'NAXIS1',length,comment,status)
        call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~      ,status)
        
        do i=1,ngtilast-ngtifirst+1
          call fthdef (ounit(i),0,status)
          call ftbdef (ounit(i),tfields,tform,varidat,nrows,status)
          nrow(i)=0
          nbuff(i)=0
        enddo

        print*,'copy events header:',status

        
        nbuffmax=8192/length
        
c determine the first byte of the time
        firstbyte=1
        do ifield=1,tfields
          if (ttype(ifield).ne.'time'.and.ttype(ifield).ne.'TIME') then
            if (tform(ifield).eq.'1I') then
              firstbyte=firstbyte+2
            else if (tform(ifield).eq.'1J') then
              firstbyte=firstbyte+4
            else if (tform(ifield).eq.'1E') then
              firstbyte=firstbyte+4
            else if (tform(ifield).eq.'1D') then
              firstbyte=firstbyte+8
            else
              write(0,*) 'unknown format:',tform(ifield)(1:lnblnk(tform(ifield
     ~            )))
               call exiterror('')
            endif
          else
            if (tform(ifield).ne.'1D') then
              write (0,*) 'TIME field format: assume 1D, got ',tform(ifield
     ~            )(1:lnblnk(tform(ifield)))
               call exiterror('')
            endif
            goto 20
          endif
        enddo
         call exiterror('TIME column not found')
 20     continue
        
        if (query) then
          fx=1
          do ifield=1,tfields
            if (ttype(ifield).ne.'x'.and.ttype(ifield).ne.'X') then
              if (tform(ifield).eq.'1I') then
                fx=fx+2
              else if (tform(ifield).eq.'1J') then
                fx=fx+4
              else if (tform(ifield).eq.'1E') then
                fx=fx+4
              else if (tform(ifield).eq.'1D') then
                fx=fx+8
              else
                write(0,*) 'unknown format:',tform(ifield
     ~              )(1:lnblnk(tform(ifield)))
                 call exiterror('')
              endif
            else
              if (tform(ifield).ne.'1I') then
                write (0,*) 'X field format: assume 1I, got ',tform(ifield
     ~              )(1:lnblnk(tform(ifield)))
                 call exiterror('')
              endif
              goto 21
            endif
          enddo
           call exiterror('X column not found')
 21       continue

          fy=1
          do ifield=1,tfields
            if (ttype(ifield).ne.'y'.and.ttype(ifield).ne.'Y') then
              if (tform(ifield).eq.'1I') then
                fy=fy+2
              else if (tform(ifield).eq.'1J') then
                fy=fy+4
              else if (tform(ifield).eq.'1E') then
                fy=fy+4
              else if (tform(ifield).eq.'1D') then
                fy=fy+8
              else
                write(0,*) 'unknown format:',tform(ifield
     ~              )(1:lnblnk(tform(ifield)))
                 call exiterror('')
              endif
            else
              if (tform(ifield).ne.'1I') then
                write (0,*) 'Y field format: assume 1I, got ',tform(ifield
     ~              )(1:lnblnk(tform(ifield)))
                 call exiterror('')
              endif
              goto 22
            endif
          enddo
           call exiterror('Y column not found')
 22       continue

          fpi=1
          do ifield=1,tfields
            if (ttype(ifield).ne.'pi'.and.ttype(ifield).ne.'PI') then
              if (tform(ifield).eq.'1I') then
                fpi=fpi+2
              else if (tform(ifield).eq.'1J') then
                fpi=fpi+4
              else if (tform(ifield).eq.'1E') then
                fpi=fpi+4
              else if (tform(ifield).eq.'1D') then
                fpi=fpi+8
              else
                write(0,*) 'unknown format:',tform(ifield
     ~              )(1:lnblnk(tform(ifield)))
                 call exiterror('')
              endif
            else
              if (tform(ifield).ne.'1I') then
                write (0,*) 'PI field format: assume 1I, got ',tform(ifield
     ~              )(1:lnblnk(tform(ifield)))
                 call exiterror('')
              endif
              goto 23
            endif
          enddo
           call exiterror('PI column not found')
 23       continue
        endif
        
        do irow=1,nrows
          if ((irow/20000)*20000.eq.irow) print*,irow,'  of',nrows
          call ftgtbs (iunit,irow,1,length,string,status)
c        decode bytes
          good=.true.
          if (query) then
            sx=string(fx:fx+1)
            sy=string(fy:fy+1)
            spi=string(fpi:fpi+1)
            good=(pi.ge.pimin.and.pi.le.pimax).and.(x.ge.xmin.and.y.le.xmax)
     ~          .and.(y.ge.ymin.and.y.le.ymax)
          endif
          
          if (good) then
            stime=string(firstbyte:firstbyte+7)
          
            do i=1,ngtilast-ngtifirst+1
              if (time.ge.tstart(igti(i)).and.time.le.tend(igti(i))) then
                
                buff(i)(nbuff(i)*length+1:nbuff(i)*length+length)
     ~              =string(1:length)
                nbuff(i)=nbuff(i)+1
                
                if (nbuff(i).eq.nbuffmax) then
                  call ftptbs (ounit(i),nrow(i)+1,1,length*nbuffmax,buff(i)
     ~              ,status)
                  nrow(i)=nrow(i)+nbuffmax
                  nbuff(i)=0
                endif
                goto 30
              endif
            enddo
 30         continue
          endif
        enddo
        
        do i=1,ngtilast-ngtifirst+1
          call ftptbs (ounit(i),nrow(i)+1,1,length*nbuff(i),buff(i)
     ~        ,status)
          nrow(i)=nrow(i)+nbuff(i)
        enddo
          
          
        
        
        do i=1,ngtilast-ngtifirst+1
          call ftmkyj(ounit(i),'NAXIS2',nrow(i),'the number of entries',status
     ~        )
          call ftrdef(ounit(i),status)
        enddo
          


c copy other extensions until we're at the EVENTS
        nhdu=nhdu+1
 100    continue
        call ftmahd (iunit,nhdu,hdutype,status)
        if (status.ne.0) then
          status=0
          goto 110
        endif
        if (hdutype.ne.0) then
          call ftgkys (iunit,'EXTNAME',extname,comment,status)
        else
          extname='prime'
        endif
        
        if (extname.ne.'EVENTS'.and.extname.ne.'events') then
          if (nhdu.gt.1) then
            do i=1,ngtilast-ngtifirst+1
              call ftcrhd(ounit(i),status)
            enddo
          endif
        
          do i=1,ngtilast-ngtifirst+1
            call ftcopy(iunit,ounit(i),0,status)
          enddo
          
          nhdu = nhdu + 1
          goto 100
          
        endif


 110    continue
        do i=1,ngtilast-ngtifirst+1
          call ftclos(ounit(i),status)
          if (status.ne.0) then
            write(0,*)'Close file:',igti(i)
            call fitsio_write_error (status,0)
             call exiterror('')
            status=0
          endif
        enddo
        print*,'close files:',status
        
        ngtifirst=ngtilast+1
      enddo

      

      return
      end
      
      




