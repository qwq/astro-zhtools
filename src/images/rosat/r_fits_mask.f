      subroutine read_fits_mask (filename,n,region)
      implicit none
      character filename*(*)
      character file*200
      integer lnblnk
      integer region(512,512)
      integer n
      integer status
      
      integer naxis,naxes(10)
      logical anyf
      integer hdtype
      character comment*80
      integer nx,ny
      
      integer iunit
      parameter (iunit=50)
      
      file=filename(6:n)
      
      status=0

      call open_fits (iunit, file, status)
      if(status.ne.0)then
        write(0,*)'>>> Error opening MASK file: ',file(1:lnblnk(file))
        call exit (1)
      endif
      
      call ftmahd (iunit, 1, hdtype, status) ! Goto the primary array  
      

*      call ftghpr(iunit,2,simple,bitpix,naxis,naxes,pcount,gcount,extend
*     ~    ,status)              ! Get dimensions
      call ftgkyj(iunit,'NAXIS',naxis,comment,status)
      call ftgkyj(iunit,'NAXIS1',naxes(1),comment,status)
      call ftgkyj(iunit,'NAXIS2',naxes(2),comment,status)
      
      
c . check that dimensions are OK
      if (naxis.ne.2)then
        write(0,*)'Not 2-D image in the primary header'
        call exit (1)
      endif
      nx=naxes(1)
      ny=naxes(2)
      
      
c . get image      
      call ftg2dj(iunit,0,0,512,nx,ny,region,anyf,status)
      
      call ftclos(iunit,status)
      
      if (status.ne.0) then
        call fitsio_print_error (status)
        write(0,*)'Error reading input image'
        call exit (1)
      endif
      
      
      
      
      return
      end
      
