#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

main (int argc, char *argv[])
{
  FILE *file;
  char buff[200];
  int n;
  float w,p1,p2;
  float s1[34], s2[34];
  int i;
  int boundband[8][3];
  float spec[8],spec1[8],spec2[8];
  int iband;
  float sum,sum1,sum2,disp;
  int iarg;
  int bandmin,bandmax;
  char affix[80];
  int qnorm,qsignal;
  
  if ( argc < 3 )
    {
      fprintf(stderr,"Usage: specweight spec.grid.file obs.control\n");
      exit(1);
    }

  strcpy(affix," ");
  bandmin=2;
  bandmax=7;
  qnorm=1;
  qsignal=1;
  for (iarg=2;iarg<argc;iarg++)
    {
      if (strncmp(argv[iarg],"bandmin=",8)==0)
	sscanf(argv[iarg],"bandmin=%d",&bandmin);
      if (strncmp(argv[iarg],"bandmax=",8)==0)
	sscanf(argv[iarg],"bandmax=%d",&bandmax);
      if (strncmp(argv[iarg],"affix=",6)==0)
	strcpy(affix,argv[iarg]+6);
      if (strncmp(argv[iarg],"norm=no",7)==0)
	qnorm=0;
      if (strcmp(argv[iarg],"sigma=yes")==0)
	qsignal=0;
    }
  
  
  file=fopen(argv[1],"r");
  if (file==NULL)
    {
      fprintf(stderr,"Failed to open file: %s\n",argv[1]);
      exit(1);
    }

  n=0;
  while (n<3)
    {
      fgets(buff,100,file);
      if (strncmp(buff,"----------",10) == 0)
	n++;
    }

  fscanf(file,"%s %e %e",buff,&p1,&p2);
  fgets(buff,100,file);
  fgets(buff,100,file);
  
  for(i=0;i<34;i++)
    {
      fgets(buff,100,file);
      sscanf(buff,"%e %e %e",&w,&s1[i],&s2[i]);
    }

  fclose(file);

  boundband[2][1]=6;
  boundband[2][2]=10;

  boundband[3][1]=11;
  boundband[3][2]=12;

  boundband[4][1]=13;
  boundband[4][2]=15;

  boundband[5][1]=16;
  boundband[5][2]=18;

  boundband[6][1]=19;
  boundband[6][2]=23;

  boundband[7][1]=24;
  boundband[7][2]=30;



  for(iband=bandmin;iband<bandmax+1;iband++)
    {
      spec1[iband]=0;
      spec2[iband]=0;
      for (i=boundband[iband][1]-1;i<=boundband[iband][2]-1;i++)
	{
	  spec1[iband]+=s1[i];
	  spec2[iband]+=s2[i];
	}
    }

  sum1=0.;
  sum2=0.;
  for(iband=bandmin;iband<bandmax+1;iband++)
    {
      sum1+=spec1[iband];
      sum2+=spec2[iband];
    }

  for(iband=bandmin;iband<bandmax+1;iband++)
    {
      spec[iband]=(spec1[iband]-spec2[iband]*sum1/sum2)/(p1-p2);
    }

  
  disp=0;
  for(iband=bandmin;iband<bandmax+1;iband++)
    {
      disp+=spec[iband]*spec[iband];
    }

  disp/=3;
  disp=sqrt(disp);

  for(iband=bandmin;iband<bandmax+1;iband++)
    {
      spec[iband]/=disp;
    }

  printf(" ( ");
  for (iband=bandmin;iband<bandmax+1;iband++)
    {
      printf ("%s.%d%s * %e ",argv[2],iband,affix,spec[iband]);
      if (iband<7)
	printf (" + ");
    }
  printf(" ) ");
  if (qnorm==1)
    {
      
      printf(" / amax1 ( ");
      
      for (iband=bandmin;iband<bandmax+1;iband++)
	{
	  printf ("%s.%d%s ",argv[2],iband,affix);
	  if (iband<7)
	    printf (" + ");
	}
      printf(" , 0.0001 ) ");
    }
  
  printf("\n");
}
  








