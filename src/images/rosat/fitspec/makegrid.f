      implicit none
      real a,z
      character amode*80
      
      character parname*80,parvalue*80
      logical defined
      integer lnblnk
      
      parname="amode"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'You need to set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      amode=parvalue
      
      
      parname="a"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'You need to set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      read(parvalue,*) a
      
      
      parname="z"
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'You need to set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      read(parvalue,*) z
      
      
      call make_rs_grid (amode,a,z)
      
      end

       
      
