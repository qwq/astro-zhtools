      subroutine read_data (name,data,error,effarea,matrix,nchan,elow,ehigh
     ~    ,nechan,nspchanmax,exposure)
      implicit none
      character name*(*)
      real data(*)
      real error(*)
      real elow(*), ehigh(*)
      integer nechan,nspchanmax
      real effarea(nechan)
      real matrix(nspchanmax,nechan)
      integer nchan
      integer unitpha,unitarf,unitrmf
      real exposure
      
      character rootname*200,filename*200,rmf*200,arf*200
      integer lnblnk
      integer k,junk
      character comment*80,colname*80
      integer colnum
      integer status
      logical anyf
c for rdrmf1:
      integer maxgrp,nwork,nwork1
      parameter (maxgrp=10, nwork=7290, nwork1=729)
      character telescop*128, instrume*128, detnam*128, filter*128, matext*128
     ~    ,rmfversn*128, hduclas3*128
      real areascal, lo_thresh
      integer chatter, ichan, ienerg, imaxgrp, ngrp(nwork1),
     ~    f_chan(nwork), n_chan(nwork), ierr
      integer i
      
      
      k=lnblnk(name)
      if (name(k-3:k).eq.'.pha') then
        rootname=name(1:k-4)
      else
        rootname=name
      endif
      
      write(0,*)'Reading dataset: ',rootname(1:lnblnk(rootname))

c // read PHA
      filename=name
      status=0
      call ftgiou (unitpha,status)
      call ftopen(unitpha,filename,0,junk,status)
      if (status.ne.0) then
        filename=rootname
        call strcat (filename,".pha")
        close(unitpha)
        status=0
        call ftopen(unitpha,filename,0,junk,status)
        if (status.ne.0) then
          call showerror (0,status,'!!! Trouble in reading',filename)
          call exit(1)
        endif
      endif
      
      call ftmahd(unitpha,2,junk,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! Trouble in reading ',filename)
        call exit(1)
      endif
      
      call ftgkyj(unitpha,'NAXIS2',nchan,comment,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot get NAXIS2 from ',filename)
        call exit(1)
      endif 

      call ftgkys(unitpha,'RESPFILE',rmf,comment,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot get RESPFILE from ',filename)
        call exit(1)
      endif 

      call ftgkys(unitpha,'ANCRFILE',arf,comment,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot get RESPFILE from ',filename)
        call exit(1)
      endif 

      call ftgkye(unitpha,'EXPOSURE',exposure,comment,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot get EXPOSURE from ',filename)
        call exit(1)
      endif 

      colname='RATE'
      call ftgcno (unitpha,.false.,colname,colnum,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot find RATE in ',filename)
        call exit(1)
      endif 
      
      call ftgcve(unitpha,colnum,1,1,nchan,0.0,data,anyf,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! trouble reading RATE from ',filename)
        call exit(1)
      endif 
      
      colname='STAT_ERR'
      call ftgcno (unitpha,.false.,colname,colnum,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot find STAT_ERR in ',filename)
        call exit(1)
      endif 
      
      call ftgcve(unitpha,colnum,1,1,nchan,0.0,error,anyf,status)
      if (status.ne.0) then
        call showerror(0,status,
     ~      '!!! trouble reading STAT_ERR from ',filename)
        call exit(1)
      endif 
      
      call ftclos(unitpha,status)
      call ftfiou(unitpha,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! general problems with ',filename)
        call exit(1)
      endif 
      

      
c /// read responce matrix
      call ftgiou (unitrmf,status)
      filename=rmf
      call ftopen(unitrmf,filename,0,junk,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! cannot open ',filename)
        call exit(1)
      endif 
      
      call ftmahd (unitrmf,2,junk,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! trouble moving to the second ext in '
     ~      ,filename)
        call exit(1)
      endif 
      
      chatter=5
      call rdrmf1(unitrmf, chatter,matext,
     ~    telescop, instrume, detnam, filter, areascal,
     ~    ichan, ienerg, elow, ehigh,
     ~    imaxgrp, ngrp, f_chan, n_chan,
     ~    matrix, lo_thresh, nspchanmax,nechan,
     ~    rmfversn,hduclas3,ierr)

      call ftclos (unitrmf,status)
      call ftfiou (unitrmf,status)
      if (status.ne.0) then
        call showerror(0,status,'!!! trouble reading matrix from ',filename)
        call exit(1)
      endif 
       
c /// read arf
      if (arf.ne.'none'.and.arf.ne.'NONE') then
        call ftgiou (unitarf,status)
        filename=arf
        call ftopen(unitarf,filename,0,junk,status)
        if (status.ne.0) then
          call showerror(0,status,'!!! cannot open ',filename)
          call exit(1)
        endif 
        
        call ftmahd (unitarf,2,junk,status)
        if (status.ne.0) then
          call showerror(0,status,'!!! trouble moving to the second ext in '
     ~        ,filename)
          call exit(1)
        endif 
        
        colname='SPECRESP'
        call ftgcno (unitarf,.false.,colname,colnum,status)
        if (status.ne.0) then
          call showerror(0,status,'!!! cannot find SPECRESP in ',filename)
          call exit(1)
        endif 
        
        call ftgcve(unitarf,colnum,1,1,nechan,0.0,effarea,anyf,status)
        if (status.ne.0) then
          call showerror(0,status,'!!! trouble reading SPECRESP from '
     ~        ,filename)
          call exit(1)
        endif

        call ftclos(unitarf,status)
        call ftfiou(unitarf,status)
        if (status.ne.0) then
          call perror_fitsio (filename,status)
          call exit(1)
        endif
      else
        do i=1,nechan
          effarea(i)=1
        enddo
      endif
      
 

      return
      end



