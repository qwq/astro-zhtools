      subroutine write_rs_grid (amode,a,z,rsspec,ntgrid)
      implicit none
      character amode*(*)
      real a,z
      integer ntgrid
      real rsspec(0:729,-1:ntgrid)
      integer lnblnk

      character filename*200
      
      write(filename,'(a,a,a,f4.2,a,f5.3)')
     ~    'caldata/rosat/R-S.grid/',
     ~    amode(1:lnblnk(amode)),
     ~    ",a=",a,
     ~    ",z=",z
      
      call dataenv(filename,"ZHTOOLS")
      write(0,*)filename
      call write_fits_image (filename,rsspec,730,ntgrid+2,'e','e')
      return
      end
      
