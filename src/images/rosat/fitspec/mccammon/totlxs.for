From:	47413::MCCAMMON     11-MAY-1992 16:32:36.15
To:	NSSDC::STPMODELS
CC:	
Subj:	totlxs.for

C----------------------------------------------------------------------
C
      FUNCTION TOTLXS( E, AB )
C
C     Reference:
C      Monika Balucinska-Church and Dan McCammon
C      "Photoelectric Absorption Cross Sections with Variable Abundances"
C      Ap.J. xxx, pyyy (1992)
C
C
C     Description: 
C     Calculates the effective absorption cross section TOTLXS in units of 
C     cm**2/hydrogen atom at energy E in eV for the abundances of the elements 
C     specified in vector AB
C
C     Method:
C     Calls seventeen functions that calculate the mass absorption coeffs
C     in cm**2/g for the following elements: H, He, C, N, O, Ne, Na, Mg,
C     Al, Si, S, Cl, Ar, Ca, Cr, Fe, Ni.
C
C     Deficiencies:
C     Works only in the range of energy from 30 eV to 10,000 eV.
C
C     Bugs:
C     None known -- please report any problems to authors
C  
C     Authors:
C     Dan McCammon               (47413::MCCAMMON)
C     Monika Balucinska-Church   (BHVAD::MBC)
C
C     History:
C     8.8.91  - original
C     8.1.92  - modified to remove VAX fortran77 extensions
C
C     Parameters:
C     E - energy in eV
C     AB - vector of length 17 of Log10 abundances relative to 
C          hydrogen = AB(1).  N.B. The order of the elements in AB
C          is as follows: H,He,C,N,O,Ne,Na,Mg,Al,Si,S,Cl,Ar,Ca,Cr,Fe,Ni.
C     TOTLXS - effective cross section in cm**2/H atom for assumed
C             abundances of cosmic elements.  Note that this will be per
C             hydrogen atom for the relative abundances given in AB.
C
C     Type Definitions:
C      IMPLICIT NONE
C
C     Local variables:
      INTEGER K          
C          ( index through elements)
      REAL A(17)         
C          ( mass absorption cross sections in cm**2/g 
C           for the cosmic elements -- in the same order as for AB)
C     Import:
      REAL E             
C          ( energy in eV)
      REAL AB(17)        
C          ( log10 abundances relative to hydrogen)

C     Export:
      REAL TOTLXS         
C          ( effective cross section in cm**2/H atom)
C     External functions: ( mass absorption coefficients for the elements)
      REAL HYDRO         
      REAL HELIUM
      REAL CARBON
      REAL NITRO
      REAL OXYGEN
      REAL NEON
      REAL SODIUM
      REAL MAGNES
      REAL ALUM
      REAL SILICN
      REAL SULFUR
      REAL CHLORN
      REAL ARGON
      REAL CALC
      REAL CHROM
      REAL IRON
      REAL NICKEL
    

C     Local constants:
      REAL AW(17)        
C          ( atomic weights of the elements)
      REAL AV            
C          ( Avogadro's number)
      INTEGER NUMB       
C          ( number of elements)


      DATA AW /1.00797, 4.0026, 12.01115, 14.0067, 15.9994, 20.183,
     &         22.9898, 24.312, 26.9815, 28.086, 32.064, 35.453, 
     &         39.94, 40.08, 51.996, 55.847, 58.71/

      DATA AV /6.022045E23/

      DATA NUMB /17/


C     Start:

      A(1)=HYDRO(E)
      A(2)=HELIUM(E)
      A(3)=CARBON(E)
      A(4)=NITRO(E)
      A(5)=OXYGEN(E)
      A(6)=NEON(E)
      A(7)=SODIUM(E)
      A(8)=MAGNES(E)
      A(9)=ALUM(E)
      A(10)=SILICN(E)
      A(11)=SULFUR(E)
      A(12)=CHLORN(E)
      A(13)=ARGON(E)
      A(14)=CALC(E)
      A(15)=CHROM(E)
      A(16)=IRON(E)
      A(17)=NICKEL(E) 

      TOTLXS=0.

C     loop through elements
      DO 10 K=1,NUMB
         TOTLXS=TOTLXS + AW(K)*A(K)/AV*(10.**(AB(K)-AB(1)))
10    CONTINUE

      RETURN
      END 
