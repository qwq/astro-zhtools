      subroutine make_rs_grid (amode,a,z)
      implicit none
      real a,z
      character amode*80

      real ebin(0:729)
      integer ntgrid
      parameter (ntgrid=100)
      real Tgrid(ntgrid)
      real rsspec(0:729,-1:ntgrid)
      real testspec(729)
      double precision norm
      character filename*200
      integer unit,newunit
      integer i,j
      real e1,e2,w1,tmin,tmax


      call set_abunds (amode)
      filename="caldata/rosat/energy.grid"
      call dataenv(filename,"ZHTOOLS")
      unit=newunit()
      open(unit,file=filename,status="old")
      do i=1,729
        read (unit,*) w1,e1,e2
        ebin(i-1)=e1
        ebin(i)=e2
      enddo
      close(1)
      
      tmin=0.1
      tmax=25.0
      
      do i=1,ntgrid
        Tgrid(i)=alog(tmin)+(i-1)*(alog(tmax/tmin)/(ntgrid-1))
        Tgrid(i)=exp(Tgrid(i))
        write(0,*)i,Tgrid(i)
        rsspec(0,i)=Tgrid(i)
        call raym (Tgrid(i),a,z,729,ebin,rsspec(1,i))
        call raym (Tgrid(i),0.0,z,729,ebin,testspec)
        norm=0.0d0
        do j=1,729
          norm=norm+testspec(j)/(ebin(j)-ebin(j-1))
        enddo
        norm=1000.0/norm
        do j=1,729
          rsspec(j,i)=norm*rsspec(j,i)/(ebin(j)-ebin(j-1))
        enddo
      enddo
      
      do j=1,729
        rsspec(j,-1)=ebin(j-1)
        rsspec(j,0)=ebin(j)
      enddo
      
      call write_rs_grid (amode,a,z,rsspec,ntgrid)
      return
      end
