      subroutine read_rs_grid (amode,a,z,rsspec,ntgrid,status)
      implicit none
      character amode*(*)
      real a,z
      integer ntgrid
      real rsspec(0:729,-1:ntgrid)
      integer lnblnk
      integer status
      logical exist

      character filename*200
      
      write(filename,'(a,a,a,f4.2,a,f5.3)')
     ~    'caldata/rosat/R-S.grid/',
     ~    amode(1:lnblnk(amode)),
     ~    ",a=",a,
     ~    ",z=",z
      
      call dataenv(filename,"ZHTOOLS")
      inquire (file=filename,exist=exist)
      if (.not.exist) then
        status=1
        return
      endif
      call read_fits_image (filename,rsspec,730,ntgrid+2,'e')
      status=0
      return
      end
      
