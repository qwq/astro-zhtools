*+RDRMF1
c       ---------------------------------------------------------
        subroutine rdrmf1(iunit, chatter,matext,
     &          telescop, instrume, detnam, filter, areascal,
     &          ichan, ienerg, energ_lo, energ_hi,
     &          imaxgrp, ngrp, F_chan, N_chan,
     &          fmatrix, lo_thresh, maxchan,maxen,
     &          rmfversn,hduclas3,ierr)
c       ---------------------------------------------------------
c --- DESCRIPTION -----------------------------------------------------
c
c Reads the RMF extension for an RMFVERSN = 1992a RMF file
c The file is assumed to conform to the HDUVERS2='1.*.*' family.
c Currently the OGIP formats supported are
c HDUVERS2 = '1.0.0'
c HDUVERS2 = '1.1.0'
c see OGIP/92-002a
c The HDU CLASS keywords have only been currently introduced thus DO NOT
c have to be present to use this reader.
c
c Assumes the FITS file is open.
c !!! NOTE !!! File is left open at end
c     ... close file using FTCLOS, or
c     ... read another extension
c
c Columns read are ...
c
c ENERG_LO      : Low energy bound for row
c ENERG_HI      : High energy bound for row
c N_GRP         : Number of channel subsets for row
c F_CHAN        : Firstchannel in each subset for row
c N_CHAN        : Number of channels for each subset for row
c MATRIX        : (non-zero) Matrix element for row
c
c Keywords read ...
c
c TELESCOP      : Mission/Telescop name , if not present set to UNKNOWN
c INSTRUME      : Instrument/Detector name, if not present set to UNKNOWN 
c DETNAME       : Specific detector name, if not present set to NONE 
c FILTER        : Filter in use, if not present set to NONE
c EFFAREA       : Effective area, if not present,set to 1
c LO_THRESH     : Threshold used to construct the MATRIX, if not present,
c                 set to 0
c HDUCLAS3      : Values describing data, OGIP approved - REDIST,FULL,
c		  DETECTOR
c RMFVERSN      : OGIP RMF version
c
c --- VARIABLES ------------------------------------------------------
c          
	IMPLICIT NONE
	integer chatter, ierr, maxchan,maxen
	integer iunit
	integer ichan, ienerg, imaxgrp
	integer ngrp(maxen), F_chan(maxen,*)
	integer N_chan(maxen,*)
	real areascal, lo_thresh
	real energ_lo(maxen), energ_hi(maxen)
	real fmatrix(maxchan,maxen)
        character rmfversn*(*)
        character hduclas3*(*)
	character telescop*(*), instrume*(*), detnam*(*), filter*(*)
        character matext*(*)
c 
c --- VARIABLE DIRECTORY ----------------------------------------------
c 
c Passed parameters
c  IUNIT         i   : FORTRAN unit number of open RMF file
c  CHATTER       i   : chattiness flag for o/p (5 quite,10 normal,>20 silly)
c  MATEXT        o   : extension name SPECRESP/MATRIX
c  TELESCOP      o   : String listing telescope/mission
c  INSTRUME      o   : String listing instrument/detector
c  DETNAM        o   : String listing specific detector name   
c  FILTER        o   : String listing instrument filter in use
c  AREASCAL      o   : Area scaling factor
c  RMFVERSN      o   : RMF version
c  HDUCLAS3      o   : Hduclas3 keyword value
c  ICHAN         o   : No. channels in the full array
c  IENERG        o   : No. energy bins
c  ENERG_LO      o   : Array containing lower bound to each energy bin
c  ENERG_HI      o   : Array containing upper bound to each energy bin
c  NGRP          o   : Array containing no. channel subsets at each energy
c  IMAXGRP       o   : Max no. grps in any given row
c  F_CHAN        o   : Array containing 1st chan of each subset at each energy
c  N_CHAN        o   : Array containing no. chans within each subset 
c                           at each energy
c  FMATRIX       o   : Array containing the full matrix
c  LO_THRESH     o   : The lower threshold used to construct the matrix
c  IERR            o : Error flag (0 = OK)
c
c User i/ps required (prompted for):
c  None
c
c Include files
c  None
c
c Called Routines:
c  subroutine FCECHO     : (FTOOLS) Writes to standard o/p device
c  subroutine WT_FERRMSG : (CALLIB) Writes FITSIO error message etc
c
c Compilation & Linking
c  link with FITSIO & CALLIB & FTOOLS
c
c --- AUTHORS/MODIFICATION HISTORY ----------------------------------------
c
c Rehana Yusaf (1993 July 26 : 1.0.0; WT_RMF1992A.F (CALLIB) used as basis
c Rehana Yusaf (1993 Oct 27 ) 1.0.1; added arguments for rmfversn and 
c				     hduclas3, also the name has been 
c				     changed from rd_rmf1992a.
c				     In addition if extname is not found 
c	                             HDUCLAS1='RESPONSE' is searched for.
c				     and HDUCLAS2='RSP_MATRIX'
c Rehana Yusaf (1993 Nov 10) 1.0.2;  HDUVERS2 is read to obtain rmfversn
c				     if HDUVERSN not present then RMFVERSN
c				     is read. Prev' only RMFVERSN read
c Ian M George (93 Nov 17) 1.1.0     Took out searching for correct xtens
c					(this is now responsibilty of main)
c Rehana Yusaf (94 Jan 11) 1.1.1;    Remove mvalues array and read matrix
c				     matrix values straight into fmatrix
c
c Rehana Yusaf (1994 June 24)1.1.1;  Make the routine less verbose, that
c                                    is only print warnings at chatter>30 
	character version*7
	parameter (version = '1.1.1')
*-
c ------------------------------------------------------------------------- 
c
c Internals
c
	integer status
	integer i, k, siz_mat, siz_ngrp
	integer ie,j, ic, frow,felem,colnum,inull
        character desc*70
	character message*80
        character errstr*27, wrnstr*27
        character comm*30
	integer ivalues(10)
	real enull
        logical anyflg
c
c --- INITIALISE ---
c
      ierr = 0
      status = 0
      errstr = ' ERROR:RDRMF1 Ver '//version//':'
      wrnstr = ' WARNING :RDRMF1 Ver '//version//':'
	
c
c --- GIVE USER INFO IF REQUESTED ---
c
      IF (chatter.GE.15) THEN
        message = ' ... using RDRMF1 '// version
        call fcecho(message)
      ENDIF

c
c --- READING KEYWORD VALUES ---
c
c     NAXIS2 ...
c
      status = 0
      call ftgkyj(iunit,'NAXIS2',ienerg,comm,status)
      message = errstr//' reading NAXIS2 value '
      IF (status.NE.0) THEN
        ierr = 4
        call wt_ferrmsg(status,message)
        return
      ENDIF
      IF (ienerg.GT.maxen) THEN
        ierr = 4
        message = errstr//' Energy Array dimension is too small !'
        call fcecho(message)
        return
      ENDIF

c HDUCLAS3 ...

      hduclas3 = '  '
      status = 0
      call ftgkys(iunit,'HDUCLAS3',hduclas3,comm,status)
      message = wrnstr//' reading HDUCLAS3 '
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,message)
      ENDIF

c RMFVERSN ...

      rmfversn = '  '
      status = 0
      call ftgkys(iunit,'HDUVERS2',rmfversn,comm,status)
      message = wrnstr//' reading HDUVERS2/RMFVERSN'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,message)
      ENDIF         
      IF (rmfversn.EQ.'  ') THEN
        status = 0
        call ftgkys(iunit,'RMFVERSN',rmfversn,comm,status) 
        message = wrnstr//' reading RMFVERSN'
        IF (chatter.GE.30) THEN
          call wt_ferrmsg(status,message)
        ENDIF
      ENDIF

c     TELESCOP ...

      status = 0
      call ftgkys(iunit,'TELESCOP',telescop,comm,status)
      IF (chatter.GE.20) THEN
        message = wrnstr//' reading TELESCOP '
        call wt_ferrmsg(status,message)      
      ENDIF
      IF (status.EQ.202) THEN
        telescop = 'UNKNOWN'
      ENDIF 	

c     INSTRUME ...

      status = 0
      call ftgkys(iunit,'INSTRUME',instrume,comm,status)
      IF (chatter.GE.20) THEN
        message = wrnstr//' reading INSTRUME '
        call wt_ferrmsg(status,message)
      ENDIF
      IF (status.EQ.202) THEN
        instrume = 'UNKNOWN'
      ENDIF         

c     FILTER ...

      status = 0
      call ftgkys(iunit,'FILTER',filter,comm,status)
      IF (chatter.GE.30) THEN
        message = wrnstr//' reading FILTER '
        call wt_ferrmsg(status,message)
      ENDIF
      IF (status.EQ.202) THEN
        filter = 'NONE'
      ENDIF         

c     DETNAM ...

      status = 0
      call ftgkys(iunit,'DETNAM',detnam,comm,status)
      IF (chatter.GE.30) THEN
        message = wrnstr//' reading DETNAM '
        call wt_ferrmsg(status,message)
      ENDIF
      IF (status.EQ.202) THEN
        detnam = 'NONE'
      ENDIF

c     EFFAREA ...

      status = 0
      call ftgkye(iunit,'EFFAREA',areascal,comm,status)
      IF (chatter.GE.30) THEN
        message = wrnstr//' reading EFFAREA '
        call wt_ferrmsg(status,message)
      ENDIF
      IF (status.NE.0) THEN
        areascal = 1
      ENDIF

c     LO_THRESH ...

      status = 0
      call ftgkye(iunit,'LO_THRESH',lo_thresh,comm,status)
      IF (chatter.GE.30) THEN
        message = wrnstr//' reading LO_THRESH '
        call wt_ferrmsg(status,message)
      ENDIF
      IF (status.NE.0) THEN
        lo_thresh = 0
      ENDIF

c     DETCHANS ...

      status = 0
      call ftgkyj(iunit,'DETCHANS',ichan,comm,status)
      message = errstr//' reading DETCHANS '
      call wt_ferrmsg(status,message)   
      IF (status.NE.0) THEN
        ierr = 1
        return
      ENDIF
      IF (ichan.GT.maxchan) THEN
        ierr = 4
        message = errstr//' Channel Array dimension is too small !'
        call fcecho(message)
        return
      ENDIF

      IF (chatter.GE.20) THEN
        desc = '     ... read KEYWORD values '
        call fcecho(desc)
      ENDIF
c
c --- READ DATA ---
c
      
c     ENERG_LO ...

      frow = 1
      felem = 1
      status = 0
      call ftgcno(iunit,.false.,'ENERG_LO',colnum,status)
      IF (status.NE.0) THEN
         message = errstr//' ENERG_LO column not present !'
         call fcecho(message)
         ierr = 4
         return
      ENDIF
      enull = 0
      call ftgcve(iunit,colnum,frow,felem,ienerg,enull,energ_lo,
     &            anyflg,status)      
      IF (status.NE.0) THEN
        message = errstr//' reading ENERG_LO column'
        call fcecho(message)
        ierr = 1
        return
      ENDIF

c     ENERG_HI ...
         
      status = 0
      call ftgcno(iunit,.false.,'ENERG_HI',colnum,status)
      If (status.NE.0) THEN
         message = errstr//' ENERG_HI column not present !'
         call fcecho(message)
         ierr = 4
         return
      ENDIF
      enull = 0
      call ftgcve(iunit,colnum,frow,felem,ienerg,enull,energ_hi,
     &            anyflg,status)
      IF (status.NE.0) THEN
        message = errstr//' reading ENERG_HI column'
        call fcecho(message)
        ierr = 1
        return
      ENDIF      

c     NGRP ...

      status = 0
      call ftgcno(iunit,.false.,'N_GRP',colnum,status)
      If (status.NE.0) THEN
         message = errstr//' N_GRP column not present !'
         call fcecho(message)
         ierr = 4
         return
      ENDIF
      inull = 0
      call ftgcvj(iunit,colnum,frow,felem,ienerg,inull,ngrp,
     &            anyflg,status)
      IF (status.NE.0) THEN
        message = errstr//' reading N_GRP column'
        call fcecho(message)
        ierr = 1
        return
      ENDIF    

c     F_CHAN ...

      status = 0
      call ftgcno(iunit,.false.,'F_CHAN',colnum,status)
      If (status.NE.0) THEN
         message = errstr//' F_CHAN column not present !'
         call fcecho(message)
         ierr = 4
         return
      ENDIF
      do i=1,ienerg
        inull = 0
        call ftgcvj(iunit,colnum,i,felem,ngrp(i),inull,
     &            ivalues,anyflg,status)
        IF (status.NE.0) THEN
          message = errstr//' reading F_CHAN column'
          call fcecho(message)
          ierr = 1
          return
        ENDIF 
        do j=1,ngrp(i)
           F_chan(i,j) = ivalues(j)
        enddo
      enddo

c     N_CHAN ...

      status = 0
      call ftgcno(iunit,.false.,'N_CHAN',colnum,status)
      If (status.NE.0) THEN
         message = errstr//' N_CHAN column not present !'
         call fcecho(message)
         ierr = 4
         return
      ENDIF
      do i=1,ienerg
        inull = 0
        call ftgcvj(iunit,colnum,i,felem,ngrp(i),inull,
     &              ivalues,anyflg,status)
        IF (status.NE.0) THEN
          message = errstr//' reading N_CHAN column'
          call fcecho(message)
          ierr = 1
          return
        ENDIF  
        do j=1,ngrp(i)
          N_chan(i,j) = ivalues(j)
        enddo
      enddo

c     MATRIX ...


c     initialise matrix array ...

      do i=1,ichan
        do j=1,ienerg
          fmatrix(i,j) = 0.0
        enddo
      enddo

c     imaxgrp ...

      siz_ngrp = 0
      do i=1,ienerg
        siz_ngrp = MAX(siz_ngrp,ngrp(i))         
      enddo
      imaxgrp = siz_ngrp

c     read matrix ...

      status = 0
      call ftgcno(iunit,.false.,'MATRIX',colnum,status)
      If (status.NE.0) THEN
         message = errstr//' MATRIX column not present !'
         call fcecho(message)
         ierr = 4
         return
      ENDIF
      do ie=1,ienerg
        siz_mat = 0
        do j=1,ngrp(ie)
          siz_mat = siz_mat + N_chan(ie,j)
        enddo
        k = 0
        do j=1,ngrp(ie)
          do ic=F_chan(ie,j),F_chan(ie,j)+N_chan(ie,j) - 1
            k=k+1
            enull = 0
            call ftgcve(iunit,colnum,ie,k,1,enull,
     &              fmatrix(ic,ie),anyflg,status)
            IF (status.NE.0) THEN
              message = errstr//' reading MATRIX column'
              call wt_ferrmsg(status,message)
              ierr = 1
              return
            ENDIF
          enddo
        enddo
      enddo
      IF (chatter.GE.20) THEN
        desc = '     ... read RSP_MATRIX data'
        call fcecho(desc)
      ENDIF  
      return
      end
c ----------------------------------------------------------------------
c     END OF RDRMF1
c ----------------------------------------------------------------------

  
*+WTEBD1
	subroutine wtebd1(ounit, chatter, 
     &		nk_hist, hist, 
     & 		nk_comm, comment,rmfversn,
     &		telescop, instrume, detnam, filter, areascal, 
     &		iebound, e_min, e_max, ierr) 

	IMPLICIT NONE
	integer chatter, ierr
	integer ounit, nk_hist, nk_comm
	integer iebound
	real areascal
	real e_min(*), e_max(*)
	character rmfversn*5
	character telescop*16, instrume*16, detnam*16, filter*16
	character hist(*)*70, comment(*)*70
c 
c Description:
c  Creates and Writes the EBOUNDS extension for an RMF file one of the formats
c  conforming to the HDUVERS2='1.*.*' family.
c Currently the following formats are supported (see OGIP/92-002a)
c   HDUVERS2 = '1.0.0'
c   HDUCERS2 = '1.1.0'
c The requested format is checked, and if belonging to the '1.*.*' family,
c but not included above, the extension is written in the last format listed.
c  Assumes the FITS is open and has had the Primary Header written
c  !!! Note !!!! File is left open at the end  
c      and  MUST BE CLOSED               by FTCLOS 
c      or   ANOTHER EXTENSION ADDED      by FTCRHD
c  in order to (automatically) write the mandatory END header keyword.
c
c Passed parameters
c  OUNIT         i   : FORTRAN unit number of open RMF file
c  CHATTER       i   : chattiness flag for o/p (5 quite,10 normal,>20 silly)
c  NK_HIST       i   : No. records to be written as HISTORY records
c  HIST          i   : Array of history strings to be written
c  NK_COMM       i   : No. records to be written as COMMENT records
c  COMMENT       i   : Array of comment strings to be written
c  RMFVERSN      i   : String denoting OGIP HDUVERS2 family  
c  TELESCOP      i   : String listing telescope/mission
c  INSTRUME      i   : String listing instrument/detector
c  DETNAM        i   : String listing specific detector name
c  FILTER        i   : String listing instrument filter in use
c  AREA          i   : Area scaling factor
c  IEBOUND       i   : No. channels in the full array
c  E_MIN         i   : Array containing min nominal energy bound to each chan
c  E_MAX         i   : Array containing max nominal energy bound to each chan
c  IERR            o : Error Flag (0=OK)
c
c User i/ps required (prompted for):
c  None
c
c Include files
c  None
c
c Called Routines:
c  subroutine FCECHO     : (FTOOLS) writes to standard o/p unit
c  subroutine FTBDEF     : (FITSIO) Defines the BINTABLE data structure
c  subroutine FTCRHD     : (FITSIO) Creates a new FITS extension file
c  subroutine FTPHBN     : (FITSIO) Writes the required header keywords
c  subroutine FTPCOM     : (FITSIO) Writes a FITS comment keyword
c  subroutine FTPCLx     : (FITSIO) Writes the data 
c  subroutine FTPHIS     : (FITSIO) Writes a FITS history keyword
c  subroutine FTPKYS     : (FITSIO) Writes a keyword
c  subroutine WT_FERRMSG : (CALLIB) Dumps FITSIO Error message etc
c
c Compilation & Linking
c  link with FITSIO & CALLIB & FTOOLS
c
c Origin:
c  Code mostly hacked from within the BBRSP program
c
c Authors/Modification History:
c  Alan Smale       (1992 Sept/Oct), original BBRSP version
c  Ian M George     (1.0.0; 1992 Dec 31), tidied-up version
c  Ian M George     (1.0.1; 1993 Feb 16), replaced write(5,*) with FCECHO calls
c  Ian M George     (1.0.2: 1993 Feb 28), minor debug of History Record problem
c  Ian M George     (1.0.3; 1993 May 19), hist & comment made chara*70 
c  Rehana Yusaf     (1.0.4; 1993 July 26), e_min,e_max array dimension 
c                                          changed to *
c  Ian M George     (2.0.0: 1993 Oct 14), renamed from wt_ebd1992a & major
c                                       overhaul of HDUCLAS/VERS stuff
c  Rehana Yusaf     (2.0.1; 1993 Nov 10), only wqarn about rmfversn not
c                                         being 1.0.0 or 1.1.0 at high chatter
	character version*7
	parameter (version = '2.0.1')
*- 

c Internals
	integer status, decimals, itemp
        integer npar, nfields
	integer ie, i
        parameter (npar = 6, nfields=3, decimals=6)
        character hduvers2*5
	character ttype(nfields)*16, tform(nfields)*16, tunits(nfields)*16
        character string*70
	character message*80
        character errstr*30, wrnstr*30
c Initialization
	ierr = 0
	status = 0 
        errstr = '** WTEBD1 ERROR: '
        wrnstr = '** WTEBD1 WARNING: '

c Give user info if requested
        if(chatter.GE.15) then
	  message = ' ... using WTEBD1 ' // version
	  call fcecho(message)
        endif

        print*,'WRITE E. BOUND'
c Check for sillies
	if(rmfversn(1:1).NE.'1') then
	   message = wrnstr // ' Format/subroutine mismatch'
	   call fcecho(message)
	   message = 
     &		' ...... This routine writes only the 1.*.* family' //
     &		' of formats'
	   call fcecho(message)
	   message = 
     &		' ...... requested format: '// rmfversn
	   call fcecho(message)
	   ierr = 15
	   goto 998
	endif
c Check that we know the format
	if((rmfversn.EQ.'1.0.0').OR.(rmfversn.EQ.'1.1.0')) then
	   hduvers2 = rmfversn
	else
	   hduvers2 = '1.1.0'
           IF (chatter.GE.20) THEN
	    message = wrnstr // ' Unknown format: '// rmfversn
	    call fcecho(message)
	    message = 
     &	      ' ...... Resetting format (HDUVERS2) to '//hduvers2
	    call fcecho(message)
           ENDIF
	endif

c Create a new extension
	call FTCRHD(ounit,status)
        message = errstr
        call wt_ferrmsg(status, message)
        if(chatter.GE.20) then
          message = ' ... new extension created'
          call fcecho(message)
        endif
        if(status.NE.0) then
		ierr = 1
		goto 998
	endif		
 

c Set up the columns n stuff
	ttype(1)   = 'CHANNEL'
	tform(1)   = 'J'
	tunits(1)  = ' '
	ttype(2)   = 'E_MIN'
	tform(2)   = 'E'
	tunits(2)  = 'keV'
	ttype(3)   = 'E_MAX'
	tform(3)   = 'E'
	tunits(3)  = 'keV'
        
        print*,'column names'

c Write the required header keywords
	call FTPBNH(ounit,iebound,nfields,ttype,tform,tunits,
     &		'EBOUNDS',0,status)
        message = errstr
        call wt_ferrmsg(status, message)
        if(status.NE.0) then
		ierr = 1
		goto 998
	endif		
        if(chatter.GE.20) then
          message = ' ... written the extension header keywords'
          call fcecho(message)
        endif


c
c --- WRITE THE HDUCLASn & HDUVERSn keywords
c
	call FTPKYS(ounit,'HDUCLASS ',
     &		'OGIP',
     & 		'format conforms to OGIP standard',
     &		status)
	message = wrnstr // ' Problem putting HDUCLASS keyword '
	call wt_ferrmsg(status, message)
	status = 0

	call FTPKYS(ounit,'HDUCLAS1 ',
     &		'RESPONSE',
     & 		'dataset relates to spectral response',
     &		status)
	message = wrnstr // ' Problem putting HDUCLAS1 keyword '
	call wt_ferrmsg(status, message)
	status = 0

	call FTPKYS(ounit,'HDUVERS1 ',
     &		'1.0.0',
     & 		'Version of family of formats',
     &		status)
	message = wrnstr // ' Problem putting HDUVERS1 keyword '
	call wt_ferrmsg(status, message)
	status = 0

	call FTPKYS(ounit,'HDUCLAS2 ',
     &		'EBOUNDS',
     & 		'nominal energies of PHA chan boundaries',
     &		status)
	message = wrnstr // ' Problem putting HDUCLAS2 keyword '
	call wt_ferrmsg(status, message)
	status = 0

	call FTPKYS(ounit,'HDUVERS2 ',
     &		hduvers2,
     &          'Version of format (OGIP memo CAL/GEN/92-002a)',
     &		status)
	message = wrnstr // ' Problem putting HDUVERS2 keyword '
	call wt_ferrmsg(status, message)
	status = 0

c Add the other (passed) OGIP required keywords
 	call FTPKYS(ounit,'TELESCOP ',
     &		telescop,
     &   	'mission/satellite name',
     &		status)
        message = wrnstr // ' Putting TELESCOP keyword '
        call wt_ferrmsg(status, message)
        status = 0
	
	call FTPKYS(ounit,'INSTRUME ',
     &		instrume,
     &   	'instrument/detector name',
     &		status)
        message = wrnstr // ' Putting INSTRUME keyword '
        call wt_ferrmsg(status, message)
        status = 0

        if(detnam.NE.' ') then
        call FTPKYS(ounit,'DETNAM ',
     &          detnam,
     &          'specific detector name in use',
     &          status)
        message = wrnstr // ' Putting DETNAM keyword '
        call wt_ferrmsg(status, message)
        status = 0
        endif

	call FTPKYS(ounit,'FILTER   ',
     &		filter,
     &   	'filter in use',
     &		status)
        message = wrnstr // ' Putting FILTER keyword '
        call wt_ferrmsg(status, message)
        status = 0

	call FTPKYJ(ounit,'DETCHANS ',
     &		iebound,
     &   	'total number of detector channels',
     &		status)
        message = wrnstr // ' Putting DETCHANS keyword '
        call wt_ferrmsg(status, message)
        status = 0

        call FTPKYF(ounit,'EFFAREA ',
     &          areascal, decimals,
     &          'Area scaling factor',
     &          status)
        message = wrnstr // ' Putting EFFAREA keyword '
        call wt_ferrmsg(status, message)
        status = 0

c Add other advised keywords

	call FTPKYS(ounit,'RMFVERSN ',
     &		'1992a',
     &   	'OGIP classification of FITS format',
     &		status)
        message = wrnstr // ' Putting RMFVERSN keyword '
        call wt_ferrmsg(status, message)
        status = 0

        if(chatter.GE.20) then
          message = ' ... written the OGIP required keywords'
          call fcecho(message)
        endif


c Add the (passed) history cards, adding one related to this programme
	itemp = 0
        do i = 1, nk_hist
                call FTPHIS(ounit, hist(i), status)
                if(status.NE.0) then
                        itemp = status
                        status = 0
                        call FTPHIS(ounit,
     &          ' - (missing record) fitsio illegal character ?',
     &           status)
                 endif
        enddo
        write(string,'(2a)')
     &		' FITS EBOUNDS extension written by WTEBD1 ',
     &                          version
        call FTPHIS(ounit,string,status)
        message = wrnstr // ' Putting at least one History record'
        call wt_ferrmsg(itemp, message)
        if(chatter.GE.20) then
          message = ' ... written the history keywords'
          call fcecho(message)
        endif
        status = 0

c Add the (passed) comment cards
	itemp = 0
        do i = 1, nk_comm
                call FTPCOM(ounit, comment(i), status)
                if(status.NE.0) then
                        itemp = status
                        status = 0
                        call FTPCOM(ounit,
     &          ' - (missing record) fitsio illegal character ?',
     &           status)
		endif
        enddo
        message = wrnstr // ' Putting at least one Comment record'
        call wt_ferrmsg(itemp, message)
        status = 0
        if(chatter.GE.20) then
          message = ' ... written the comment header keywords'
          call fcecho(message)
        endif

c Define the extension data structure
	call FTBDEF(ounit,nfields,tform,0,iebound,status)
        message = errstr // ' Defining Data Structure '
        call wt_ferrmsg(status, message)
        if(status.NE.0) then
                ierr = 1
                goto 998
        endif
        if(chatter.GE.20) then
          message = ' ... defined the extension data structure'
          call fcecho(message)
        endif


c Write the data
	do ie = 1, iebound
		call FTPCLJ(ounit, 1, ie, 1, 1, ie,status)
		call FTPCLE(ounit, 2, ie, 1, 1, e_min(ie),status)
		call FTPCLE(ounit, 3, ie, 1, 1, e_max(ie),status)
	enddo

        if(chatter.GE.20) then
          message = ' ... written the data'
          call fcecho(message)
        endif

c Final check for errors
        message = wrnstr // ' Writing Data '
        call wt_ferrmsg(status, message)

998     if(ierr.NE.0) then
           message = errstr // ' FATAL: Extension not written'
           call fcecho(message)
        endif
 

	return
	end

	

*+WT_FERRMSG
      subroutine wt_ferrmsg(status,errstr)
c     ------------------------------------
c --- DESCRIPTION -------------------------------------------------
c
c This routine checks the error flag obtained from FITSIO routines
c and if appropriate uses FTGERR to get the relevant error text.
c Fcecho is used to write to the screen to ensure that the program
c can be used in differant environments.
c
c --- VARIABLES ---------------------------------------------------
c
      character errstr*80
      character errmess*80
      integer status
c
c --- LINKING AND COMPILATION ---
c
c FTOOLS and FITSIO
c 
c --- CALLED ROUTINES ---
c
c subroutine FTGERR     : (FITSIO) Obtains appropriate error text
c subroutine FCECHO     : (FTOOLS) Writes to screen
c
*-Version 1.0
  
       IF (status.NE.0) THEN
         call ftgerr(status,errmess)
         call fcecho(errmess)
         call fcecho(errstr)
       ENDIF
       return
       end
c
c      <<<--- END OF SUBROUTINE WT_FERRMSG --->>>
c
                                               
 
C******************************************************************************
C SUBROUTINE:
C      fcpars
C
C DESCRIPTION:
C      This subroutine will PARSE the INFILE into a FILENAME and
C      an EXTENSION NUMBER using the template ...
C      INFILE = FILENAME[EXTENSION NUMBER]
C
C AUTHOR/DATE:
C      Kent Blackburn  11/5/91      
C
C MODIFICATION HISTORY:
C      W Pence 7/22/92 - if the extension number is given as '*', then
C         this subroutine will return extnumb=-1 as a flag indicating
C         that ALL extensions are to be used
C       EAG    1/22/93 - Added BN to format statments for VAX
C       EAG    6/16/93 - deal with enclosing ' or "
C       EAG    8/24/93 - allow + instead of [, return -99 for no extension
C
C NOTES:
C
C USAGE:
C      call fcpars(infile,filename,extnumb,status)
C
C ARGUMENTS:
C      infile   - input FITS file and extension number
C      filename - FITS file name
C      extnumb  - FITS file extension number
C      status   - fitsio returned error status code
C
C PRIMARY LOCAL VARIABLES:
C      inlen  - length of infile string
C      ilbrk  - position in infile string of right bracket
C      irbrk  - position in infile string of right bracket
C      digits - number of digits in extension number
C
C CALLED ROUTINES:
C
C******************************************************************************
      subroutine fcpars(infile,filename,extnumb,status)

      character infile*(*),filename*(*)
      integer       extnumb,status, fcstln
      integer       inlen,i,ilbrk,irbrk,digits

C ---   Find the last non blank character ---

      if ( status.ne.0 ) goto 999

      inlen = fcstln(infile)

C ---   Remove enclosing " or ' 
      if ((ichar(infile(1:1)) .eq. 34) .or. 
     &     (ichar(infile(1:1)) .eq. 39) .or. 
     &     (ichar(infile(1:1)) .eq. 44)) then
         do 10 i = 2, inlen
            infile(i-1:i-1) = infile(i:i)
 10      continue
         inlen = inlen - 1
      endif
      if ((ichar(infile(inlen:inlen)) .eq. 34) .or. 
     &     (ichar(infile(inlen:inlen)) .eq. 39) .or.
     &     (ichar(infile(inlen:inlen)) .eq. 44)) inlen = inlen - 1

C ---   Determine if any extension number is attached ---

      irbrk = inlen
      if ((infile(inlen:inlen) .eq. ']') .or. 
     &     (infile(inlen:inlen) .eq. '+')) irbrk = inlen - 1
      do 15 i = inlen-1,1,-1
         ilbrk = i
         if ( infile(i:i) .eq. '[' ) goto 20
         if ( infile(i:i) .eq. '+' ) goto 20
 15   continue

C no extension specified, return with -99
      goto 900

 20   continue

C ---     extract the filename and extension number ---

      filename = infile(1:(ilbrk - 1))
      digits = irbrk - ilbrk
      if ( digits .eq. 4 ) then
         read(infile((ilbrk+1):irbrk),1004,err=900) extnumb
      else if ( digits .eq. 3 ) then
         read(infile((ilbrk+1):irbrk),1003,err=900) extnumb
      else if ( digits .eq. 2 ) then
         read(infile((ilbrk+1):irbrk),1002,err=900) extnumb
      else if ( digits .eq. 1 ) then
         if (infile((ilbrk+1):(ilbrk+1)) .eq. '*')then
C  ---         return extnumb = -1 as special flag indicating ALL extensions
            extnumb=-1
         else
            read(infile((ilbrk+1):irbrk),1001,err=900) extnumb
         end if
      else
         goto 900
      end if

      go to 999

C ---   come here on internal read error
 900  continue

C ---     No extension number attached so take defaults ---
      filename = infile(1:inlen)
      extnumb=-99

 999  continue
 1001 format( BN,i1 )
 1002 format( BN,i2 )
 1003 format( BN,i3 )
 1004 format( BN,i4 )
      return
      end 




C******************************************************************************
C SUBROUTINE:
C      fcgcls
C
C DESCRIPTION:
C      gets the list and number of columns names or filenames 
C
C AUTHOR/DATE:
C      Janice Tarrant  12/23/91
C
C MODIFICATION HISTORY:
C      Tarrant  1/8/92  changed column parser to accept column names
C        with embedded spaces
C       Greene 8/25/92  changed to allow for blank lines in input file
C       Greene 8/4/93   do not break on , enclosed in []
C       4/12/94 EAG     Stop after 999 values
C      10/20/94 EAG 3.1a - allow for extra spaces between items
C
C NOTES:
C
C USAGE:
C      call fcgcls(columns,colist,numcols,negflag)
C
C ARGUMENTS:
C      columns - string list of column names to translate
C      colist  - array of separate column names
C      numcols - number of column names in list
C      negflag - exclude name flag
C
C PRIMARY LOCAL VARIABLES:
C      col_fname   - name of file containing column names
C      context     - error message
C      colen       - length of column string
C      cname_index - position of name in column string
C      funit       - unit number associated with input file
C      ios         - I/O status
C
C CALLED ROUTINES:
C      subroutine fcecho - echo message to terminal
C      function   fcstln - returns length of character string (integer)
C
C******************************************************************************
      subroutine fcgcls(columns,colist,numcols,negflag)

      character columns*(*)
      character colist(999)*(*)
      integer numcols
      logical negflag
      character col_fname*160
      character context*80
      integer i, j, fcstln, colen, cname_index, funit, ios

C  find position of first nonwhite character
      colen = fcstln(columns)
      do 10 i = 1, colen
         if (columns(i:i) .ne. ' ') goto 11
 10   continue
C  (JCI) escape if the input string is blank, otherwise it core dumps.
C  If you get here, the string is empty
      numcols = 0
      goto 999
 11   cname_index = i

C  if first character is @ then the column names are in a file
      funit = 17
      if (columns(cname_index:cname_index) .eq. '@') then
         col_fname = columns(cname_index+1:)
         open(unit=funit,file=col_fname,iostat=ios,status='old')
         if (ios .ne. 0) then
            context = 'FCGCLS 3.1a: file containing column ' // 
     &           'list not found'
            call fcerr(context)
            close(funit)
            goto 999
         else
            i = 1
 20         read(funit,1000,end=30) colist(i)
 1000       format(A)
C       deal with blank lines (EAG 8/25/92)
            if (fcstln(colist(i)) .le. 0) go to 20
            i = i + 1
            if (i .gt. 999) then
               call fcecho
     &              (' WARNING:FCGCLS 3.1a: truncating at 999 items')
               goto 30
            endif
            goto 20
 30         close(funit)
            numcols = i - 1
         endif

C  copy column names into column list
      else
         i = cname_index
         j = 1
 40      if (i .eq. colen) goto 42
         if (columns(i:i) .eq. '"') then
            i = i + 1
            cname_index = i
 43         if (columns(i:i) .eq. '"') goto 44
            i = i + 1
            goto 43
 44         colist(j) = columns(cname_index:i-1)
            i = i + 2
            cname_index = i
            if ( i .ge. colen ) goto 45
            j = j + 1
            goto 40
         endif

C check for column name seperators
C space is always a seperator
         if (columns(i:i) .eq. ' ') goto 41

C comma is sometimes a seperator
         if (columns(i:i) .eq. ',') then

C check if there is a previous open bracket
            if (index(columns(cname_index:i-1),'[') .le. 0) goto 41

C if so, this is column has a vector specification
C check for close bracket, too
            if (index(columns(cname_index:i-1),']') .gt. 0) goto 41
         endif
         i = i + 1
         goto 40
 41      colist(j) = columns(cname_index:i-1)
 100     i = i + 1

C deal with multiple spaces between items
         if (columns(i:i) .eq. ' ') goto 100

         cname_index = i
         j = j + 1
         goto 40
 42      colist(j) = columns(cname_index:i)
 45      continue
         numcols = j
      endif

C  check for exclude column flag
      negflag = .false.
      if (colist(1)(1:1) .eq. '-') then
         negflag = .true.
         colen = fcstln(colist(1))
         do 50 i = 1, colen
            colist(1)(i:i) = colist(1)(i+1:i+1)
 50      continue
      endif

 999  continue

      return
      end  

*+COPYPHD
      subroutine copyphd(infile,outfile,killit,errflg,chatter)
c     -------------------------------------------------------
c --- DESCRIPTION ---------------------------------------------------
c
c This subroutine copys the primary header from infile to output file
c
c --- VARIABLES -----------------------------------------------------
c
      IMPLICIT NONE
      character infile*(*),outfile*(*)
      integer chatter, errflg
      logical killit
c
c --- VARIABLE DIRECTORY ---------------------------------------------
c
c Arguments ...
c
c infile     char   : Input filename
c outfile    char   : Output filename
c chatter    int    : Chattiness flag
c
c --- CALLED ROUTINES ------------------------------------------------
c
c FTOPEN            : (FITSIO) Open FITS file
c FTINIT            : (FITSIO) Open/Create new FITS file
c FTCOPY            : (FITSIO) Copy header and data
c FTCLOS            : (FITSIO) Close FITS file
c
c --- AUTHORS/MODIFICATION HISTORY -----------------------------------
c
c Rehana Yusaf (1993 April 2) 
c Rehana Yusaf (1993 July 20) : use cgetlun to get unit numbers
c Rehana Yusaf (1993 Aug 5) 1.0.2: USE FTCOPY, instead of calling
c                           copying header and data using seperate routines 
c Rehana Yusaf (1994 July 20) 1.0.3; replace cgetlun with ftgiou
c Rehana Yusaf (1994 Sept 13) 1.0.4; additional argument killit, and 
c                                    use opfits instead of ftinit
      character version*5
      parameter (version = '1.0.4')
*-
c --------------------------------------------------------------------
c
c --- INTERNALS -----------------------------------------------------
c
      character subinfo*70
      character errstr*32
      integer block,iunit,ounit,status,nmore
c
c --- USER INFO ---
c
      IF (chatter.GE.20) THEN
         subinfo = ' ... using COPYPHD Ver '//version
         call fcecho(subinfo)
      ENDIF
      errstr = ' ERROR : COPYPHD Ver '//version
c
c --- OPENING INPUT AND OUTPUT FILES ---
c
      status = 0
      call ftgiou(iunit,status)
      IF (status.NE.0) THEN
        subinfo = errstr//' obtaining free lun'
        call fcecho(subinfo)
        errflg = status
        return
      ENDIF
      call ftopen(iunit,infile,0,block,status)
      subinfo = errstr// ' opening infile !'
      call wt_ferrmsg(status,subinfo)
      IF (status.NE.0) THEN
        errflg = 1
        return
      ENDIF
      status = 0
      block = 2880
      call ftgiou(ounit,status)
      IF (status.NE.0) THEN
        subinfo = errstr//' obtaining free lun'
        call fcecho(subinfo)
        errflg = status
        return
      ENDIF
      call opfits(ounit,outfile,killit,chatter,status)
      subinfo = errstr//' opening outfile !'
      call wt_ferrmsg(status,subinfo)
      IF (status.NE.0) THEN
        errflg = 1
        return
      ENDIF
c
c --- USE FTCOPY TO COPY HEADER AND DATA
c
      nmore = 0 
      call ftcopy(iunit,ounit,nmore,status)  
      subinfo = errstr//' writing primary header and array'
      call wt_ferrmsg(status,subinfo)
      IF (status.NE.0) THEN
        errflg = 1
        return
      ENDIF
c
c --- CLOSE FILES ---
c
      status = 0
      call ftclos(iunit,status)
      status = 0
      call ftclos(ounit,status)
      status = 0
      call ftfiou(iunit,status)
      call ftfiou(iunit,status)
      return
      end
c -----------------------------------------------------------------
c     END OF COPYPHD
c -----------------------------------------------------------------

c ------------------------------------------------------------------
*+ FNDHDU
	subroutine fndhdu(chatter, iunit, ninstr, instr,
     &		nsearch, nfound, next, outhdu, outver, extnam, ierr)

	IMPLICIT NONE
	integer iunit, chatter, ninstr, ierr, nsearch, nfound
	integer next(*)
	character instr(*)*(*)
	character outhdu(9,*)*(*), extnam(*)*(*), outver(9,*)*(*)

c Description
c  Searches through the current, plus a specified number of additional 
c  extensions, of a FITS file looking for extensions which contain HDUCLASn 
c  values which match passed i/p strings. Returned are the number of extensions 
c  safifying the criteria, the extension number (relative to the current 
c  extension on input), the full HDUCLASn hierarchy, the EXTNAME, and the full 
c  HDUVERSn hierarchy for each of those extensions.
c  The FITS file is assumed to be open, and is rewound back to the 
c  Header unit (extension) it was at on entry.
c  !!! NOTE !!! Backwards searches are not yet supported.
c	        The i/p strings must be in ordered is the same way as the 
c		  expected HDUCLASn hierarchy, with instr(1) corresponding 
c		  to the highest level (HDUCLAS1)
c               The special wild-card character "*" is allowed in any/all 
c 		  of the i/p strings to indicate that all HDUCLASn values 
c		  corresponding to this string to deemed acceptable.
c
c Passed Parameters
c  CHATTER		i   : The chattiness flag (>9 increasing verbose)
c  IUNIT 		i   : The FORTRAN unit of the open FITS file
c  NINSTR		i   : The number of strings to be searched for
c  INSTR		i   : Array of strings to be searched for
c  NSEARCH		i   : The number extensions to be searched 
c				 (in addition to current extension)
c  NFOUND		  o : The number of extensions found with HDUCLASn
c				 matching INSTR(n)
c  NEXT			  o : Array of the numbers of each extension found 
c				(relative to the extension on input)
c  OUTHDU		  o : 2-d array of HDUCLASn values for each xtens found
c  OUTVER		  o : 2-d array of HDUVERSn values for each xtens found
c  EXTNAM		  o : Array of EXTNAME value for each xtens found
c  IERR			  o : Error flag (zero = OK)
c 
c Origin
c   Original
c
c Called Routines
c  {incomplete}
c
c Author/Modification History
c  Ian M George    (1.0.0: 93 Nov 17) Original
c  Ian M George    (1.1.0: 94 Jun 27) make passed arrays (*)
	character version*7
	parameter (version='1.1.0')
*-
c Internals 
	integer status, CLENACT
	integer htype, ngot
	integer i,jj, imove, iextno
	logical qyep
	character comm*20
	character hduclas(9)*20
	character extname*40
        character message*80
        character errstr*40, wrnstr*40
c Initialize
	nfound = 0
	ierr = 0
	do i = 1, nsearch
	  next(i) = 0
	enddo
        errstr = '** FNDHDU '//version//' ERROR: '
        wrnstr = '** FNDHDU '//version//' WARNING: '
	status = 0

c User info, if requested
        if(chatter.GE.20) then
             message = ' ... using FNDHDU '// version
             call fcecho(message)
        endif
 
c Check for sillies
	if(ninstr.LE.0) then
           message = errstr // ' Illegal no. i/p strings requested'
           call fcecho(message)
	   ierr = 1
	   goto 482
	endif

c Prepare the input strings, leading removing blanks etc
	do jj = 1, ninstr
	   call crmvlbk(instr(jj))
	enddo

c Loop through the extensions, noting those which we want
      	do iextno = 0, nsearch
	    if(chatter.GT.25) then
	       write(message,'(a,i3)') '... searching extension: ', iextno
	       call fcecho(message)
	    endif 
c	  ... Read the EXTNAME (usually present)
          status = 0
          call ftgkys(iunit,'EXTNAME',extname,comm,status)
	  IF(status.NE.0) then
	    extname = 'Undefined'
	    status = 0
	  ENDIF
	    if(chatter.GT.25) then
	       message = '...... EXTNAME = '// extname
	       call fcecho(message)
	    endif 
c	  ... Reset the hduclas array, then attempt to fill from CHU
	  do jj = 1, 9
	    hduclas(jj) = 'Undefined'
	  enddo
          status = 0
   	  call ftgkns(iunit,'HDUCLAS',1,9,hduclas,ngot,status)
c	  ... Go on to next extension if too few values recovered 
	  if(ngot.LT.ninstr) goto 321
	  do jj = 1, ngot
	   call crmvlbk(hduclas(jj))
	  enddo
c	  ... Now compare with (i/p) requested strings
	  qyep = .true.
	  do jj = 1, ninstr
	     if(instr(jj).NE.hduclas(jj)(:clenact(instr(jj))) .AND.
     &		instr(jj)(:1).NE.'*')
     &		qyep = .false.
	  enddo
c	  ... Fill in the necessary if we've found a match
	  IF(qyep) then
	        if(chatter.GT.25) then
	          message = '... Located acceptable extension'
	          call fcecho(message)
	        endif 
      		nfound = nfound + 1
		next(nfound) = iextno
		extnam(nfound) = extname
		do jj = 1, 9
		  outhdu(jj,nfound) = hduclas(jj)
	          if(chatter.GT.30) then
	            write(message,'(a,i1,a,a)') '...... HDUCLAS',jj,
     &				' = ', hduclas(jj)
		    call fcecho(message)
		  endif
		enddo
	  ENDIF
c	  ... Move on the the next extension
321       status = 0
	  call ftmrhd(iunit,1,htype,status)
c	  ... Stop if hit the end of the file
          IF ((status.EQ.107)) THEN
	    if(chatter.GT.25) then
               message = '... end-of-file found'
               call fcecho(message)
	    endif 
	    goto 123
	  endif
	enddo

c Move the pointer back to the original place in i/p file
123	if(iextno.GT.0) then
	   imove = -1*iextno
	   status = 0
           call ftmrhd(iunit,imove,htype,status)
	   if(status.NE.0) then
             message = wrnstr // ' Problem moving back to orig xtens'
             call wt_ferrmsg(status, message)
	     ierr = -1
	   endif
	endif

482     if(ierr.GT.0) then
           message = errstr // ' Incomplete Execution'
           call fcecho(message)
        endif
 


	return
	end
c ------------------------------------------------------------------


C******************************************************************************
C SUBROUTINE:
C      fcecho
C
C DESCRIPTION:
C      This subroutine provides a single point to send text to the
C      terminal. This routine should be modified when a new host
C      environment is used.
C
C AUTHOR/DATE:
C      Kent Blackburn  11/5/91 
C
C MODIFICATION HISTORY:
C
C NOTES:
C      fcecho uses F77/VOS like calls for terminal I/O
C
C USAGE:
C      call fcecho(string)
C
C ARGUMENTS:
C      string - text string sent to terminal
C
C PRIMARY LOCAL VARIABLES:
C
C CALLED ROUTINES:
C      subroutine umsput - put message
C
C******************************************************************************
      subroutine fcecho(string)

      character string*(*)
      integer dest,prio,irafsts

      dest = 1
      prio = 0
      irafsts = 0
*      call logstr(string)
      call umsput(string,dest,prio,irafsts)
      return
      end


      
C******************************************************************************
C SUBROUTINE:
C      fcerrm
C
C DESCRIPTION:
C      This subroutine provides an echo of the error status to
C      terminal environment in use.
C
C AUTHOR/DATE:
C      Kent Blackburn  12/5/91      
C
C MODIFICATION HISTORY:
C
C NOTES:
C
C USAGE:
C      call fcerrm(stat)
C
C ARGUMENTS:
C      stat - fitsio returned error status code
C
C PRIMARY LOCAL VARIABLES:
C      context - error message
C      message - error message and code
C
C CALLED ROUTINES:
C      subroutine fcecho - echo message to terminal
C
C******************************************************************************
      subroutine fcerrm(stat)

      integer stat
      character context*40,message*40

      if ((stat .lt. 1).or.(stat .gt.411)) goto 99
      context = 'Error Status Returned : '
      write(message,1000) context,stat
 1000 format(A24,I4)
      call fcerr(message)
      message = ' '
      call ftgerr(stat,message)
      call fcerr(message)
      call fitsdstk()
 99   return
      end

*+RDEBD1
c     --------------------------------------------------------
	subroutine rdebd1(iunit,chatter,maxchan, 
     &		telescop,instrume,detnam,filter,areascal, 
     &		iebound,channel,e_min,e_max,rmfversn,ierr)
c     --------------------------------------------------------
c --- DESCRIPTION ------------------------------------------------------
c
c  Reads the EBOUNDS extension for an RMFVERSN=1992a RMF file
c Assumes the extension conforms to HDUVERS2='1.*.*' family
c Currently the following formats are supported -
c HDUVERS2='1.0.0'
c HDUVERS2='1.1.0'
c see OGIP/92-002a
c The HDU CLASS keywords have only been currently introduced thus DO NOT
c have to be present to use this reader.  
c
c  Assumes the FITS is open.
c  !!! Note !!!! File is left open at the end
c      ... close file using FTCLOS, or
c      ... read another extension
c
c  Columns read are ...
c
c  CHANNEL     : Channel numbers
c  E_MIN       : Nominal lower energy bound
c  E_MAX       : Nominal upper energy bound
c
c  Keywords read ...
c
c  TELESCOP : Mission/Telescope name, NOTE: If not present set to UNKNOWN
c  INSTRUME : Instrument/Detector name NOTE: If not present set to UNKNOWN
c  DETNAM   : Specific detector name NOTE: If not present set to NONE
c  FILTER   : Filter in use, if not present set to NONE
c  EFFAREA  : Areascaling factor, if not present set to 1
c  RMFVERSN : RMF version
c  
c --- VARIABLES -------------------------------------------------------
c                                                      
	IMPLICIT NONE
	integer chatter, ierr, maxchan
	integer iunit
	integer iebound, channel(maxchan)
	real areascal
	real e_min(maxchan), e_max(maxchan)
        character rmfversn*(*)
	character telescop*(*), instrume*(*), detnam*(*), filter*(*)
c
c --- VARIABLE DIRECTORY ----------------------------------------------
c
c Passed parameters
c
c  IUNIT         i   : FORTRAN unit number of open RMF file
c  CHATTER       i   : chattiness flag for o/p (0 quite,10 normal,>20 silly)
c  TELESCOP      o   : String listing telescope/mission
c  INSTRUME      o   : String listing instrument/detector
c  DETNAM        o   : String listing specific detector name
c  FILTER        o   : String listing instrument filter in use
c  AREA          o   : Area scaling factor
c  IEBOUND       o   : No. channels in the full array
c  CHANNEL       o   : Channel array
c  E_MIN         o   : Array containing min nominal energy bound to each chan
c  E_MAX         o   : Array containing max nominal energy bound to each chan
c  RMFVERSN      o   : RMF version
c  IERR          o   : Error Flag, ierr = 0 okay
c                                  ierr = 1 error finding extension
c                                  ierr = 2 error finding Column number
c                                  ierr = 3 error reading data
c                                  ierr = 4 NAXIS2 not found
c				   ierr = 5 maxchan, array size NOT large enough
c
c --- CALLED ROUTINES --------------------------------------------------
c
c  subroutine FTMRHD     : (FITSIO) Move to extension
c  subroutine FTGKYS     : (FITSIO) Read FITS extension header keyword 
c  subroutine FTGCNO     : (FITSIO) Get column number
c  subroutine FTGCVx     : (FITSIO) Read data in x format
c  subroutine WT_FERRMSG : (CALLIB) Dumps FITSIO Error message etc
c
c --- Compilation & Linking --------------------------------------------
c
c  link with FITSIO & CALLIB & FTOOLS
c
c --- Authors/Modification History: ------------------------------------
c
c Rehana Yusaf (1993 July 15)
c Rehana Yusaf (1993 Oct 27) 1.0.1; Rename from RD_EBD1992a, and
c				    additional argument passed, rmfversn
c 				    Also HDUCLASS is now used to find
c				    extension if not present then extname
c                                   searched for.
c Rehana Yusaf (1993 Nov 10) 1.0.2; Read HDUVERS2 keyword to get rmfversn
c 				    if HDUVERSN not present the read RMFVERSN
c				    previously RMFVERSN was read
c Ian M George (93 Nov 17) 1.1.0; Took out extension searching stuff
c Rehana Yusaf (1994 Jan 11) 1.1.1; Additional argument, maxchan - array
c                                   dimension, compared to Naxis2 and error out
c                                   if too small
c Rehana Yusaf (1994 June 24) 1.1.2;Less verbose;Only printer warnings at
c                                   high chatter. Also add fcecho call
c                                   after channel error check

	character version*7
	parameter (version = '1.1.2')
*- 
c ----------------------------------------------------------------------
c
c --- INTERNALS ---
c
        character errstr*25, wrnstr*25
        character comm*30
        character desc*70, errinfo*70
        integer status,  inull, felem, frow, colnum
        real enull
        logical anyflg, foundcol
c
c --- INITIALISATION ---
c
      ierr = 0
      status = 0 
      errstr = ' ERROR : RDEBD1 Ver '//version//':'
      wrnstr = ' WARNING : RDEBD1 Ver '//version//':'
c
c --- USER INFO ---
c
      IF (chatter.GE.15) THEN
        desc = ' ... using RDEBD1 ' // version
	call fcecho(desc)
      ENDIF
c
c --- READING DATA ---
c

c NAXIS2 ...

      status = 0
      call ftgkyj(iunit,'NAXIS2',iebound,comm,status)
      errinfo = errstr//' reading NAXIS2 value '
      call wt_ferrmsg(status,errinfo)
      IF (status.NE.0) THEN
        ierr = 4
        return
      ENDIF
      IF (iebound.GT.maxchan) THEN
        errinfo = errstr//' Channel array dimension exceeded !'
        call fcecho(errinfo)
        ierr=5
        return
      ENDIF

c RMFVERSN ...

      status = 0
      rmfversn = '  '
      call ftgkys(iunit,'HDUVERS2',rmfversn,comm,status)
      errinfo = wrnstr//' reading HDUVERS2/RMFVERSN'
      IF (rmfversn.EQ.'   ') THEN
        status = 0
        call ftgkys(iunit,'RMFVERSN',rmfversn,comm,status)
        errinfo = wrnstr//' reading RMFVERSN'
      ENDIF
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF

c TELESCOP ...

      status = 0
      call ftgkys(iunit,'TELESCOP',telescop,comm,status)
      errinfo = wrnstr//' reading TELESCOP'
      IF (chatter.GE.20) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      IF (status.EQ.202) THEN
        telescop = 'UNKNOWN'
      ENDIF 

c INSTRUME ...

      status = 0
      call ftgkys(iunit,'INSTRUME',instrume,comm,status)
      errinfo = wrnstr//' reading INSTRUME'
      IF (chatter.GE.20) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      IF (status.EQ.202) THEN
        instrume = 'UNKNOWN'
      ENDIF 

c FILTER ...

      status = 0
      call ftgkys(iunit,'FILTER',filter,comm,status)
      errinfo = wrnstr//' reading FILTER'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      IF (status.EQ.202) THEN
        filter = 'NONE'
      ENDIF

c DETNAM ...

      status = 0
      call ftgkys(iunit,'DETNAM',detnam,comm,status)
      errinfo = wrnstr//' reading DETNAM'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      IF (status.EQ.202) THEN
        detnam = 'NONE'
      ENDIF   

c EFFAREA ...

      status = 0
      call ftgkye(iunit,'EFFAREA',areascal,comm,status)
      errinfo = wrnstr//' reading EFFAREA'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      IF (status.NE.0) THEN
        areascal = 1
      ENDIF

c CHANNEL COLUMN NUMBER ...
 
      status = 0
      foundcol = .true.
      call ftgcno(iunit,.false.,'CHANNEL',colnum,status)
      IF (status.NE.0) THEN
        foundcol = .false.
      ENDIF
      IF (.NOT.foundcol) THEN
        errinfo = errstr//' CHANNEL COLUMN not present'
        call fcecho(errinfo)
        ierr = 2
        return
      ENDIF

c READ CHANNEL COLUMN ...

      status = 0
      frow = 1
      felem = 1
      inull = 0
      call ftgcvj(iunit,colnum,frow,felem,iebound,inull,channel,
     &            anyflg,status)
      IF (status.NE.0) THEN
        errinfo = errstr//' reading CHANNEL column !'
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF

c E_MIN COLUMN NUMBER ...

      status = 0
      foundcol = .true.
      call ftgcno(iunit,.false.,'E_MIN',colnum,status)
      IF (status.NE.0) THEN
        foundcol = .false.
      ENDIF
      IF (.NOT.foundcol) THEN
        errinfo = errstr//' E_MIN COLUMN not present'
        call fcecho(errinfo)
        ierr = 2
        return
      ENDIF
    
c READ E_MIN ...

      status = 0 
      enull = 0
      call ftgcve(iunit,colnum,frow,felem,iebound,enull,e_min,
     &            anyflg,status) 
      IF (status.NE.0) THEN
        errinfo = errstr//' reading E_MIN column !'
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF      
      
c E_MAX COLUMN NUMBER ...

      status = 0
      foundcol = .true.
      call ftgcno(iunit,.false.,'E_MAX',colnum,status)
      IF (status.NE.0) THEN
        foundcol = .false.
      ENDIF
      IF (.NOT.foundcol) THEN
        errinfo = errstr//' E_MAX COLUMN not present'
        call fcecho(errinfo)
        ierr = 2
        return
      ENDIF    

c READ E_MAX ...

      status = 0
      enull = 0
      call ftgcve(iunit,colnum,frow,felem,iebound,enull,e_max,
     &            anyflg,status)
      IF (status.NE.0) THEN
        errinfo = errstr//' reading E_MAX column !'
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF
      IF (chatter.GE.20) THEN
        desc = '      ... read EBOUNDS data '
        call fcecho(desc)
      ENDIF
      return
      end
c --------------------------------------------------------------------
c     END OF RDEBD1
c --------------------------------------------------------------------   


*+GETKEYS
c     ------------------------------------------------- 
      subroutine getkeys(iunit,nkeys,keys,chatter,ierr)
c     -------------------------------------------------
c --- DESCRIPTION -------------------------------------------------------
c This subroutine reads all the keywords in an extension  and returns
c an array of keywords, this is usefull for other routines as the array
c can be used to ensure no duplication of keywords.
c NOTE !!! HISTORY AND COMMENT keyword names are NOT included
c --- VARIABLES ---------------------------------------------------------
c
      IMPLICIT NONE
      integer iunit,nkeys,chatter,ierr
      character keys(*)*8
c
c --- VARIABLE DIRECTORY ------------------------------------------------
c
c iunit      int  i : file unit number
c chatter    int  i : Verboseness flag
c ierr       int  i : error flag, 0 is okay
c keys       char o : array of keyword names
c
c --- CALLED ROUTINES ---------------------------------------------------
c
c FTGHSP     : (FTOOLS/FITSIO) Gets numbers of keywords
c FTGREC     : (FTOOLS/FITSIO) Reads record
c WT_FERRMSG : (FTOOLS/CALLIB) Writes approriate error message
c
c --- COMPILATION/LINKING -----------------------------------------------
c
c FTOOLS/FITSIO, FTOOLS/CALLIB
c
c --- AUTHORS/MODIFICATION HISTORY --------------------------------------
c
c Rehana Yusaf (1993 Oct 27) 1.0.0;
c
      character version*6
      parameter (version='1.0.0')
*-
c -----------------------------------------------------------------------
c
c Internals
c
      character errstr*24
      character desc*70,errinfo*70
      character rec*80
      integer nmore,i,tot_keys,status
c
c --- USER INFO ---
c
      errstr =' ERROR:GETKEYS Ver '//version//':'
      IF (chatter.GE.15) THEN
        desc = ' ... using GETKEYS Ver '//version
        call fcecho(desc)
      ENDIF

c --- Get counter for number of keywords ---

      status = 0
      call ftghsp(iunit,tot_keys,nmore,status)

c --- Read each record and store keyword names ---

      nkeys = 0
      do i=1, tot_keys
        status = 0
        call ftgrec(iunit,i,rec,status)
        errinfo = errstr//' reading Record'
        call wt_ferrmsg(status,errinfo)
        errinfo = 'Rec :'//rec
        IF (status.NE.0) THEN
          call fcecho(errinfo)
        ENDIF
        IF ((rec(1:8).NE.'HISTORY').AND.
     &    (rec(1:8).NE.'COMMENT')) THEN
          nkeys = nkeys + 1
          keys(nkeys) = rec(1:8)
        ENDIF
      enddo
      return
      end
c -----------------------------------------------------------------
c     END OF GETKEYS
c -----------------------------------------------------------------
      



*+ CRMVBLK
      	subroutine crmvblk(name)
	
	IMPLICIT NONE
        character Name*(*)
c
c Description:
c  Removes all blanks from a string
c
c Passed Parameters:
c  NAME		i/o: String to be de-blanked
c
c Called Routines:
c  None
c 
c Origin:
c  Direct copy of the XANADU routine RMVBLK (copied 1993 March 31), 
c 
c Authors/Modification History:
c  Linda Osborne 	(1989 Apr 12), original 
c  Nick White		(1992 Jun 08), removed c*400 dummy array
c  Ian M George		(1993 Mar 31), direct copy to caldb library
	character version*7
	parameter (version = '1.0.0')
*-

c Internals
      INTEGER*4 jj , i , k , FCSTLN , out , newlen
c Main
      jj = FCSTLN(Name)
      i = 1
      out = 0
      DO 100 k = 1 , jj
         IF ( Name(k:k).NE.' ' ) THEN
            Name(i:i) = Name(k:k)
            i = i + 1
         ELSE
            out = out + 1
         ENDIF
 100  CONTINUE
c
      newlen = jj - out
      Name(newlen+1:jj) = ' '

      RETURN
      END

      INTEGER FUNCTION CLENACT(CBUF)
      CHARACTER CBUF*(*)
C---
C --- RY, Taken routine from Xanadu for CALLIB
C NOTE : Routine should be used with care, as null characters
C at the end of a string can give a larger active length than
C expected. This can be avoided by initialising the string before
C using it.
C
C Function to return the active length of a character string, not
C counting any trailing blanks.  N.B. an all blank string will
C return ZERO as the length.
C---
C CBUF    I    String whose length is to be measured.
C---
C 1988-Jun-13 - Standard Fortran version [AFT]
C---
      INTEGER   I
C---
      DO 190 I=LEN(CBUF),1,-1
         IF(CBUF(I:I).NE.' ') THEN
            CLENACT=I
            RETURN
         END IF
  190 CONTINUE
      CLENACT=0
      RETURN
      END


C******************************************************************************
C SUBROUTINE:
C      fcerr
C
C DESCRIPTION:
C      This subroutine provides a single point to send text to the
C      stderr. This routine should be modified when a new host
C      environment is used.
C
C AUTHOR/DATE:
C      Kent Blackburn  2/18/93 
C
C MODIFICATION HISTORY:
C
C NOTES:
C      fcerr uses F77/VOS like calls for terminal I/O
C
C USAGE:
C      call fcerr(string)
C
C ARGUMENTS:
C      string - text string sent to terminal
C
C PRIMARY LOCAL VARIABLES:
C
C CALLED ROUTINES:
C      subroutine umsput - put message
C
C******************************************************************************
      subroutine fcerr(string)

      character string*(*)
      integer dest,prio,irafsts
      character taskname*40
      common /task/ taskname
      character buffer1*120,buffer2*120
      integer i, fcstln

      dest = 2
      prio = 0
      irafsts = 0
      buffer1 = taskname
      i = fcstln(buffer1)
      buffer2 = buffer1(1:i)//' : '//string
*      call logstr(buffer2)
      call umsput(buffer2,dest,prio,irafsts)
      return
      end

      SUBROUTINE CRMVLBK(String)
c
      IMPLICIT NONE
c
c  kills leading blanks in a character string
c      string can be up to 400 bytes long
c
c   pads end of string with ascii blanks
c
c
      CHARACTER String*(*)
      INTEGER i
      INTEGER length
      INTEGER FCSTLN
      length = FCSTLN(String)
      IF ( length.NE.0 ) THEN
         i = 1
         DO WHILE ( String(i:i).EQ.' ' )
            i = i + 1
         ENDDO
         IF ( i.GT.1 ) String = String(i:)
      ENDIF
      RETURN
      END


*+OPFITS
c     ------------------------------------------------------
      subroutine opfits(unit,filename,killit,chatter,status)
c     ------------------------------------------------------
c --- DESCRIPTION --------------------------------------------------------
c Open a new FITS file, deleting an old one of the same name if requested
c --- VARIABLES ----------------------------------------------------------
c
      IMPLICIT NONE
      integer unit, status,chatter
      character filename*(*)
      logical killit
c
c --- ARGUMENTS ---
c
c      unit     - input  - integer - unit number to open
c      filename - input  - string  - name of file to open ! to overwrite
c      status   - output - integer - status of operation
c      chatter  - input  - integer - user info printed at hight chatter
c      killit   - input  - logical - if true then filename is clobbered
c
c --- MODIFICATION HISTORY ----------------------------------------------
c
c Rehana Yusaf (1.0.0: Sep 12 1994), Emily Greene's FFINIT used as a
c                                    basis, but with additional killit 
c                                    argument
      character version*5
      parameter (version='1.0.0')
*-
c -----------------------------------------------------------------------
c
c --- INTERNALS ---
c
      character errstr*30
      character subinfo*70

      if (status .ne. 0) return
      
      errstr = ' ERROR : OPFITS Ver '//version

      if (chatter.GE.15) then
        subinfo = ' ... using OPFITS Ver '//version
        call fcecho(subinfo)
      endif        

      if (filename(1:1) .eq. '!') then
         filename = filename(2:)
         killit = .true.
      endif

      if (killit) call clobber (filename, status)
      if (status .ne. 0) then
         subinfo = errstr//' clobbering existing file'
         call fcecho(subinfo)
         goto 999
      endif

      call ftinit (unit, filename, 2880, status)

 999  return
      end
c -------------------------------------------------------------------
c     END OF OPFITS
c -------------------------------------------------------------------


*+ CGETLUN  (UNX version)
      	subroutine CGETLUN(LUN)
	IMPLICIT NONE
      	INTEGER   LUN
c
c Description 
C  SYSTEM DEPENDENT routine to return a free Logical Unit Number.  
c Note, units 1-9 should be avoided, since many programs use these units 
c without warning.
c
c Passed Parameters
c  LUN        o  : An unopened logical unit number.
c
c Called Routines
c  subroutine FCERR      : (FTOOLS) Writes to standard Error
c
c Origin
c  Swiped from XANLIB for inclusion in CALLIB
c
C Authors/Modification History:
c anon (unknown), Original XANADU version
c Ron Zellar (1993, Feb 3) Modified for Calibration Library
c Ian M George (1993 Jul 19), added version number & error checks
c
	character version *9
	parameter(version = '1.0.2.UNX')
*-
c Internals/Initialization
      INTEGER   I
      CHARACTER CONTXT*80

c Main
      lun = 0
      i = 0
      call ftgiou(lun,i)
      if (i .ne. 0) then
         LUN = -99
         CONTXT = ' ** CGTLUN ' // version // 
     &        ' ERROR :Out of free units'
         CALL FCERR(CONTXT)
      end if

      RETURN
      END

C******************************************************************************
C SUBROUTINE:
C     clobber
C
C DESCRIPTION:
C     This routine clears the way to use the file named filenam.
C     It deletes the file named filenam (under VMS, it deletes 
C     the highest numbered version of filenam.) Thus, it leaves
C     filenam available for an OPEN(...,STATUS='NEW') statement.
C
C AUTHOR/DATE:
C     Lawrence Brown 7/12/94
C
C MODIFICATION HISTORY:
C
C NOTES:
C     To add clobber (overwrite) capability to an ftool, put lines like
C     the following in the parameter fetching routine:
C
C      LOGICAL DELEXIST
C      CHARACTER OUTFILE*160
C      INTEGER STATUS
C      CALL UCLGSB('CLOBBER', DELEXIST, STATUS)
C      IF (STATUS .NE. 0) THEN
CC     Probably means there wasn't a clobber field in the .par file
C         STATUS=0
C      ELSE IF(DELEXIST) THEN
C         CALL CLOBBER(OUTFILE,STATUS)
C         IF(STATUS.NE.0) THEN
CC     Do something appropriate. outfile is probably read only.
C      ENDIF
C
C    Then add:
C     
C    clobber,b,h,no,,,"Overwrite existing output file? (CAUTION)"
C
C    to the par file.
C
C USAGE:
C     call clobber(filenam,status)
C
C ARGUMENTS:
C     filenam - the file to be "clobbered"
C     status  - returned error status 
C
C PRIMARY LOCAL VARIABLES:
C     exists - logical for inquire statements
C     lun - logical unit number for clobbering
C
C CALLED ROUTINES:
C
C******************************************************************************
      subroutine clobber(filenam,status)
      character filenam*(*)
      integer status
C
      logical exists,opened
      integer lun

      if(status.ne.0) return
      
      inquire(file=filenam,exist=exists)
      if(exists) then
C     get rid of it
C     first look for a free logical unit number to use to commit the act with
         do 10 lun=99,10,-1
            inquire(lun,opened=opened)
            if(.not.opened) goto 20
 10      continue
C     failed to find free lun 
         status=1
         goto 999
 20      continue
         open(lun,file=filenam,status='old',err=30)
         close(lun,status='delete',err=40)
C     we're done
         goto 999
 30      continue
C     error opening file
         status=2
         goto 999
 40      continue
C     error closing and deleting file (This could really mess up the main 
C     code, so check for it
         status=3
      endif
 999  continue
      return
      end

**==umsput.spg  processed by SPAG 4.50F  at 15:18 on 26 Aug 1994
      SUBROUTINE UMSPUT(Text,Dest,Priority,Status)

C
C EAG 11/28/94 changed to be called by fxwrite
C
 
      CHARACTER Text*(*)
      INTEGER Dest , Priority , Status
      integer lstr, lnblnk

c chattiness dealt with in xwrite 
c      CALL XCHATY(10,10)

      lstr = max(1, lnblnk(Text))
 
      IF ( Dest.EQ.2 ) THEN
         write(0,'(a)')Text
       ELSE
         print*,Text(1:lstr)
      ENDIF
c
      Status = 0
      RETURN
      END
C******************************************************************************
C SUBROUTINE:
C      fitsdstk
C
C DESCRIPTION:
C      This subroutine dumps the FITSIO Error Stack
C
C AUTHOR/DATE:
C      Kent Blackburn  6/21/94      
C
C MODIFICATION HISTORY:
C
C NOTES:
C
C USAGE:
C      call fitsdstk()
C
C ARGUMENTS:
C
C PRIMARY LOCAL VARIABLES:
C      msgstk - error message string
C
C CALLED ROUTINES:
C      subroutine umsput - echo message to terminal
C
C******************************************************************************
      subroutine fitsdstk()

      integer dest,prio,irafsts
      character msgstk*80

C     Report contents of the FITSIO error message stack
      msgstk = ' '
      dest = 2
      prio = 0
      irafsts = 0
      call umsput(msgstk,dest,prio,irafsts)
      msgstk = ' ***** FITSIO Error Stack Dump ***** '
      call fcerr(msgstk)
      msgstk = ' '

 10   call ftgmsg(msgstk)
      call umsput(msgstk,dest,prio,irafsts)
      if (msgstk .eq. ' ') goto 99
      goto 10

 99   return
      end


 
C******************************************************************************
C FUNCTION:
C      fcstln
C
C DESCRIPTION:
C      Finds length of character string throwing out end spaces
C      
C AUTHOR/DATE:
C      Janice Tarrant  12/12/91 
C
C MODIFICATION HISTORY:
C       
C NOTES:
C
C USAGE:
C      x = fcstln(string)
C
C ARGUMENTS:
C      string - text string
C
C PRIMARY LOCAL VARIABLES:
C      length - length of text string, including end spaces
C
C CALLED ROUTINES:
C
C******************************************************************************
      integer function fcstln(string)

      character string*(*)
      integer  length, i

      length = len(string)
      do 10 i = length,1,-1
         if ( string(i:i) .ne. ' ' ) goto 20
 10   continue
 20   fcstln = i
      return
      end 


