c MM 16 Feb 94: parameter (NSMAX=2000); changed common /PARAMS/
C*** CNTRL VER 1.0  SAO/JPH Sept 24, 1991

      SUBROUTINE BREMS(T,N)

      parameter (NSMAX=2000)
      REAL KT
      COMMON/RESULT/CONCE(30),GNDREC(30),POWER(220),RHY,HENEUT,HEPLUS,
     1  DNE,PCOOL,POU,POT,RE,TU,PM(4)
C*** JPH Change 7/29/92, MM 16/2/94
      COMMON/PARAMS/NJ(12),ABUNJ(0:12),ABUND,NBIN
C      COMMON/PARAMS/NJ(12),ABUNJ(12),ABUND,BINMIN,BINSYZ,NBIN
      COMMON/CONTIN/BRMEV(NSMAX),RECEV(NSMAX),TUF(NSMAX)
      COMMON/ENER/ENIN(0:NSMAX)
      DIMENSION X(30)
      HC = 12399.
      J2 = N+1
      J1 = 2
      T6 = T / 1.E6
      KT = 86.17*T6
      Q = 10.**(ABUND-12.) * 86.17 * SQRT(T6) * 20.4 / HC
      DO 10 J = J1,J2
      X(J) = 0.
      IF (CONCE(J) .GT. 1.E-5) X(J)=CONCE(J) * Q * (J-1)**2
   10 CONTINUE
      I2 = MIN0( NBIN, IENBN(ENIN(0)+46.*KT) )
      DO 40 I = 1,I2
      Y = (ENIN(I)+ENIN(I-1)) * .5 / KT
      CHRGSM = 0.
      DO 30 J = J1,J2
      IF (X(J) .EQ. 0.) GO TO 30
      Z = (J-1)**2 * .158 / T6
      CHRGSM = CHRGSM + X(J)*FBG(Y,Z)
   30 CONTINUE
   40 BRMEV(I) = BRMEV(I) + CHRGSM * EXPROD(I-1,I,KT)
      RETURN
      END
