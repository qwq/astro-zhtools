c MM 16 Feb 94: changed common /PARAMS/
C*** Ver 2.0 jph@cfa   July 27, 1992
      SUBROUTINE HYSEQ(N,J,T,DENE,IX)
C  HYDROGENIC IONS : ADDS RECOMBINATION, DENSITY DEPENDENCE
C  HAYES AND SEATON 2S EXCITATION,  HYDROGENIC RECOMBINATION

      COMMON/DAT/E(30),S(30),C(30),WAVE(220),E3(220),F(220),LL(30),
     1  SIG(30),ALF(30),ES(30)
      COMMON/RESULT/CONCE(30),GNDREC(30),POWER(220),RHY,HENEUT,HEPLUS,
     1  DNE,PCOOL,POU,POT,RE,TU,PM(4)
C*** JPH Change 7/29/92, MM 16/2/94
	COMMON/PARAMS/NJ(12),ABUNJ(0:12),ABUND,NBIN
C	COMMON/PARAMS/NJ(12),ABUNJ(12),ABUND,BINMIN,BINSYZ,NBIN
	IF (CONCE(J) .LT. .00001) GO TO 1000
	ABU = 10. ** (ABUND-12.)
	EN = N
	CN = CONCE(J) * ABU
	Y = E3(IX+1) * 11590. / T
	CC = ALOG((Y+1)/Y) - 0.4/(Y+1)**2
        gbar = 0.047
c     GBAR = 0.20 * Y * CC + 0.276 * CC	
	IF (N .EQ. 1) GBAR = .017 + .1*CC
C  1.2 FOR CASCADES : TU IS DIRECT EXCITATION TWO PHOTON
	OM = 1.2 * 14.5 * .416 * GBAR * 13.6 / E3(IX+1)
	P2PH = (8.63E-6 * OM / SQRT(T))*EXP(-Y)*E3(IX+1)*1.6E11*CN
	TU = TU + P2PH

C  RADIATIVE RECOMBINATION : CASE A : exponent revised Aug. '86
        TP = .0001 * T / N**2
	CN = CONCE(J+1) * ABU
        RC = N * 16.7E-14 * TP ** (-0.91)
	POWER(IX+1) = POWER(IX+1) + RC * CN * E3(IX+1) * 1.6E11
        RC = N * 3.61E-14 * TP ** (-0.72)
        POWER(IX+2) = POWER(IX+2) + RC * CN * E3(IX+2) * 1.6E11
        RC = N * 1.40E-14 * TP ** (-0.757)
        POWER(IX+3) = POWER(IX+3) + RC * CN * E3(IX+3) * 1.6E11
        RC = N * .693E-14 * TP ** (-.750)
        POWER(IX+4) = POWER(IX+4) + RC * CN * E3(IX+4) * 1.6E11
        RC = N * 7.84E-14*TP**(-1.04)
        IF (N .EQ. 2) RC=2*11.8E-14*TP**(-1.02)
C  CASE B FOR HELIUM PLUS
	POWER(IX+5) = POWER(IX+5) +  RC * CN *1.6E11 *
     1  12399. / WAVE(IX+5)
        RC = N * 3.18E-14 * TP ** (-.610)
        P2PH = P2PH + RC * CN * 1.6E11 * E3(IX+1)

C  DENSITY DEPENDENCE OF METASTABLE LEVEL MEWE AND GRONENSCHILD
	CRIT = 100. * EN ** 8 * SQRT(T)
	POWER(IX+1) = POWER(IX+1) + P2PH*DENE/(DENE+CRIT)
	P2PH = P2PH * CRIT / (DENE+CRIT)
	WAV2 = WAVE(IX+1)
	IF (NBIN .NE. 0) CALL TWOPH(WAV2,P2PH,0)
	POWER(IX+5)=POWER(IX+5)+POWER(IX+2)*.11*WAVE(IX+2)/WAVE(IX+5)
	POWER(IX+2) = .89*POWER(IX+2)
	POWER(IX+3) = .86*POWER(IX+3)
	POWER(IX+4) = .78*POWER(IX+4)
 1000 CONTINUE
	RETURN
	END
