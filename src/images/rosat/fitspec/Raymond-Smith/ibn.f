c MM 16 Feb 94: added parameter (NSMAX=2000), changed common /PARAMS/
C*** CNTRL VER 1.0  SAO/JPH Sept 24, 1991


      FUNCTION IBNREC(E)

C*** FOR USE WITH RECEMS. WANT ECEN > E.

      parameter (NSMAX=2000)
      COMMON/ENER/ENIN(0:NSMAX)
c  MM 16/2/94:
      common /PARAMS/ nj(12), abunj(0:12), abund, nbin
c      COMMON/PARAMS/NJ(12),ABUNJ(0:12),ABUND,BINMIN,BINSYZ,NBIN

      DO 1 I=1,NBIN
      IF((ENIN(I)+ENIN(I-1))/2. .GT. E) GOTO 2
    1 CONTINUE
      I = NBIN + 1
    2 IBNREC = I

      END


      FUNCTION IENBN(E)

      parameter (NSMAX=2000)
      COMMON/ENER/ENIN(0:NSMAX)
c  MM 16/2/94:
      common /PARAMS/ nj(12), abunj(0:12), abund, nbin
c      COMMON/PARAMS/NJ(12),ABUNJ(0:12),ABUND,BINMIN,BINSYZ,NBIN

      DO 1 I=0,NBIN
      IF(ENIN(I) .GT. E) GOTO 2
    1 CONTINUE
      I = NBIN + 1
    2 IENBN = I

      END
