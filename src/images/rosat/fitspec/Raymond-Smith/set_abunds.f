c sets abunds to either allen or xspec (anders & grevesse)
c which="allen", "xspec" or "default". user calls this function with either
c "allen" or "xspec"; function raym always calls it with "default", in which
c case, if user has called it already, no changes are made to the abunds.


      subroutine set_abunds (which)

      implicit none
      real abunds(0:12), allen(0:12), xspec(0:12)
      character which*(*)
      character default*128, which1*128
      data default /'allen'/
      integer lnblnk,i
      logical user_set
      data user_set /.false./
      common /absv/ abunds

c These are Allen 1973 abundances (JPH has 6.90 for Ar):
      data allen/12.0, 10.93, 8.52, 7.96, 8.82, 7.92, 7.42, 7.52, 7.20,
c                 H     He     C     N     O     Ne    Mg    Si    S 
     ~    6.80, 6.30, 7.60, 6.30/
c         Ar    Ca    Fe    Ni
c These are newer abundances (recently adopted by xspec, 16 Feb 94) from
c Anders & Grevesse 1989, Geochimica et Cosmochimica Acta 53, 197:
      data xspec/12.0, 10.99, 8.56, 8.05, 8.93, 8.09, 7.58, 7.55, 7.21,
c                 H     He     C     N     O     Ne    Mg    Si    S 
     ~    6.56, 6.36, 7.67,  6.25/
c         Ar    Ca    Fe    Ni
      
      
      which1=which(1:lnblnk(which))

      if (which1.ne."default".and.which1.ne."allen".and.which1.ne."xspec") then
        which1="default"
        print'(" set_abunds: either allen or xspec, using ",a)',
     ~      default(1:lnblnk(default))
      end if


      if (which1.eq."allen") then
c this is call from user:
        do i=0,12
          abunds(i)=allen(i)
        end do
      end if
      if (which1.eq."xspec") then
c this is call from user:
        do i=0,12
          abunds(i)=xspec(i)
        end do
      end if

      if (.not.user_set) user_set=which1.ne."default"

      if (which1.eq."default") then
        if (user_set) then
c this is call from raym, but user has set abunds already:
          return
        else
c this is call from raym, user doesnt care, set default:
          if (default.eq."allen") then
            do i=0,12
              abunds(i)=allen(i)
            end do
          else
            do i=0,12
              abunds(i)=xspec(i)
            end do
          end if
        end if
      end if

      end
