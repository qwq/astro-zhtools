C*** Ver 2.0 jph@cfa   July 27, 1992
      FUNCTION ADH(L,N,T)
C  DIELECTRONIC RECOMBINATION TO HE-LIKE EXCITED STATES
C  USING MEWE&SCHRIJVER
      P = 33 * (N-1.) ** 0.6 * T ** (-0.3)
	Z = N
	Z2 = Z*Z
	Z3 = Z*Z2
	Z4 = Z * Z3
	ZPT = (Z+.5)*(Z+.5)
	A = 6.46E-8*Z4*T**(-1.5)
	EX1 = EXP(-78900.*ZPT/T)
	EX2 = EXP(-101800.*Z2/T)
	EX3 = EXP(-118400.*Z2/T)
	GO TO (1,2,3,4) , L
    1 ADH = A * (12.*EX1/(1.+6.E-6*Z4) + 18.*EX2/(1.+3.0E-5*Z4) +
     1  69.*EX3/(1.+5.0E-3*Z3))
	RETURN
    2   ADH = A*(9.*EX1/(1.+7.E-5*Z4) + 27.*EX2/(1.+8.0E-5*Z4) +
     1  380.*EX3/((1.+P)*(1.+5.0E-3*Z3))  )
	RETURN
    3   ADH = A * (18.*EX1/9.5 + 54*EX2/(1.+1.9E-4*Z4) +
     1  380. * EX3 * P / ((1.+P)*(1.+5.0E-3*Z3)) )
	RETURN
    4  ADH = A*(3.*EX1/(1.+3.0E-6*Z4) + 0.5*EX2/(1.+2.2E-5*Z4) +
     1  6.3 * EX3 / (1.+5.0E-3*Z3) )
	RETURN
	END
