c MM 16 Feb 94: added parameter (NSMAX=2000); changed common /COM/,/PARAMS/
C Ver 2.0 jph@cfa   July 27, 1992
C
C xspsub.f
C  
C  This routine drives the Raymond code calculation for
C    EQI ionization fractions in the low density limit.
C
C   Input parameters
C        KT  Plasma temperature in keV
C        IPR, JPR print switches
C        JCONT  Ion. frac calculation (1=don't calculate EQI fract,
C                  other=EQI frac)
C        IPHOT  Switch for photo- or Auger ionization, code commented out,
C                 (see nquil.f)
C        IDENS  Switch for inclusion of metastable levels in ion. rates
C                 (0=no, other=yes) (see nquil.f)
C        ICX    Switch for inclusion of charge transfer processes in rates
C                 (0=no, other=yes) (see nquil.f)
C   Output parameters
C         BIN (in common block COM) X-ray spectrum in units of
C                 1.e-23 photon cm**3/s
C

      subroutine XSPSUB (kT, ipr, jpr, jcont, iphot, idens, icx)

      parameter (NSMAX=2000)
      real*4 kT
      common /RESULT/ conce(30), gndrec(30), power(220), rhy, heneut, heplus,
     ~  dne, pcool, pou, pot, re, tu, pm(4)
c MM 16/2/94:
      common /COM/    cnc(12,30), bin(NSMAX)
      common /PARAMS/ nj(12), abunj(0:12), abund, nbin
c      common /COM/    cnc(12,30), ptot(12,220), abin(NSMAX), bin(NSMAX)
c      common /PARAMS/ nj(12), abunj(0:12), abund, binmin, binsyz, nbin
      common /ENER/   enin(0:NSMAX)


      bolt=8.617e-8
      T=kT/bolt

c low density, modify if necessary
      dene=1.

c compute ratio of ionized to neutral hydrogen
      cionh=CION(1,1,13.6,T)

      be=157890./T
      crec=2.06e-11*(.4288+.5*alog(be)+.469*be**(-.33333))/sqrt(T)
      rhy=cionh/crec

      do i=1,NSMAX
        bin(i)=0.
      enddo

      call FELINE (T,dene,12,ipr,jpr,jcont,iphot,idens,icx)

c convert units of bin from 1.e23 erg cm**3/s to 1.e23 photon cm**3/s:
      do i=1,nbin
        E=(Enin(i-1)+Enin(i))/2.
        if (e.gt.0.) bin(i)=bin(i)/1.6018e-12/E
      enddo

      end
