c MM 16 Feb 94: parameter (NSMAX=2000), changed common /PARAMS/
C*** Ver 2.0 jph@cfa   July 27, 1992


      SUBROUTINE RECEMS(T,N,IPRINT,JPRINT)                              

      parameter (NSMAX=2000)
      REAL*4 KT                                                         
      LOGICAL*4 NOTGRC(25),DONE(25),SWITCH                              
      DIMENSION DRAT(20),LION(2,20),ZETA(2,20),IEDGE(26),EDGE(25),      
     1  CHI(25),ESMIN3(25),ALFX(25),JEDGE(25),IMAP(25)                  
      DIMENSION S2X(25),S3X(25),S4X(25),S5X(25)

C*** JPH Change 7/29/92
      COMMON/ENER/ENIN(0:NSMAX)
	COMMON/DAT/E(30),EA(30),S2(30),WAVE(220),E3(220),F(220),
     1  LL(30),S3(30),S4(30),S5(30)
C*** JPH Change 7/29/92, MM 16/2/94
	COMMON/PARAMS/NJ(12),ABUNJ(0:12),ABUND,NBIN
C	COMMON/PARAMS/NJ(12),ABUNJ(12),ABUND,BINMIN,BINSYZ,NBIN
	COMMON/RATES/C1(30),C2(30),CTH
	COMMON/RESULT/CONCE(30),GNDREC(30),POWER(220),RHY,HENEUT,
     1  HEPLUS,DNE,PCOOL,POU,POT,RE,TU,PM(4)
	COMMON/CONTIN/BRMEV(NSMAX),RECEV(NSMAX),TUF(NSMAX)
C*** JPH Change 7/29/92
      ECEN(I) = 0.5*(ENIN(I) + ENIN(I-1))
C      IBN(Z) = INT((Z - BINMIN)/BINSYZ + 0.5) + 1
C      ECEN(I) = BINMIN + (I - 0.5)*BINSYZ
      GFUNC(X,Y) = 1. + 0.1728*(Y*X)**(-2./3.)*(X-2.)                   
     1  - 0.0496*(Y*X)**(-4./3.)*(X*X - X*2./3. + 2./3.)                

      DATA DRAT/2.,.5,2.,.5,6.,2.5,2.22,2.25,.667,.167,2.,.5,           
     1                      6.,2.5,2.22,2.25,.667,.167,2.,.5/           
      DATA LION/0,0,3,7,1,5,2,4,4,0,4,0,5,0,2,0,2,0,3,0,1,5,1,4,3,5,    
     1          1,3,1,4,1,3,1,3,1,2,4,0,6,0/                            
      DATA ZETA/0.,0.,2.,3.,2.,3.,1.75,3.,3.,0.,3.,0.,3.,0.,3.,0.,      
     1  3.,0.,3.,0.,3.,4.,2.83,4.,2.67,4.,2.5,4.,2.33,4.,2.17,4.,       
     1  2.,4.,1.83,4.,4.,0.,4.,0./                                      

      T6 = T/1.E6                                                       
      KT = 86.17*T6                                                     
C UPPER LIMIT TO BINNING IS CHOSEN WHERE EXP = 1.E-20                  
C*** JPH Change 7/29/92
      IMAX = MIN0(IBNREC(46.*KT),NBIN)
C      IMAX = NBIN                                     
      IF(IMAX .LT. 1) RETURN                                            
      AB = 10.**(ABUND - 12.)                                           
C*** JPH Change 7/29/92
      Q = AB * KT / T6**1.5
C      EBK = EXP(BINSYZ/KT)
C      Q = AB * KT * (EBK - 1.)*EXP(-BINMIN/KT)/T6**1.5
      qprime = ab * kt / t6 ** 1.5
      QGREC = Q * 1.31E-8
      QTK = Q * 6.52/12399.
      Z = N
      QHYD = Q * 5.28E-4*(Z**4)
      qgrecpr = qprime * 1.31e-8
      qtkpr = qprime * 6.52 / 12399.
      qhydpr = qprime * 5.28e-4*(z**4)                                      
C*** JPH Change 7/29/92
C      EBK = 1./EBK                                                      
C LIMIT TO SIGNIFICANT CONTRIB FROM AN EDGE; ASSUMES #IONS PRESENT     
C  GOES LIKE SQRT(Z); COMPARE THRESH RECEMS TO EDGLIM*(CURRENT CONTIN) 
      EDGLIM = 0.01/SQRT(Z)                                             
C FIRST CONSTRUCT TABLE OF SIGNIFICANT EDGES                           
      IX = 0                                                            
      J = 1                                                             
      ISO = N                                                           
    4 IF (ISO .LE. 20) GO TO 5                                          
      IX = IX + 3*LL(J)                                                 
      ISO = ISO -1                                                      
      J = J + 1                                                         
      GO TO 4                                                           
    5 IEMAX = 0                                                         
   10 IF(ISO .EQ. 1) GOTO 200                                           
      IF(CONCE(J+1) .LT. 1.E-4) GO TO 190                               
      CHITRY = E(J)
C*** JPH Change 7/29/92
      IGND = IBNREC(CHITRY)
C      IGND = IBN(CHITRY)                                                
      IF(IGND .GE. nbin) GO TO 100                                      
      IF(IGND .LT. 1)IGND = 1                                           
	THRESH = S2(J) + S3(J) + S4(J) + S5(J)
c
      if (chitry/kt .gt. 46.) recev(ignd) = recev(ignd) +
     1  qgrecpr*conce(j+1)*drat(iso)*(chitry**3)*thresh
      if (chitry/kt .gt. 46.) go to 100
c
      EDGTRY = QGREC*CONCE(J+1)*DRAT(ISO)*(CHITRY**3)*                  
     1 THRESH *  EXP(CHITRY/KT)
      EN = ECEN(IGND)                                                   
      X = CHITRY/EN                                                     
      X2 = X*X
C*** JPH Change 7/29/92
      EDG=EDGTRY * EXPROD(IGND-1,IGND,KT) * (S2(J)/X+S3(J)+S4(J)*X
     1  +S5(J)*X2) / THRESH
C      EDG=EDGTRY *(EBK**  IGND )* (S2(J)/X+S3(J)+S4(J)*X
C     1  +S5(J)*X2) / THRESH
      BINNOW = BRMEV(IGND) + RECEV(IGND)                                
      IF(EDG .LT. EDGLIM*BINNOW) GO TO 190                              
      IEMAX = IEMAX + 1                                                 
      IEDGE(IEMAX) = IGND                                               
      EDGE(IEMAX) = EDGTRY                                              
      CHI(IEMAX) = CHITRY                                               
      JEDGE(IEMAX) = J                                                  
      NOTGRC(IEMAX) = .FALSE.                                           
	S2X(IEMAX) = S2(J) / THRESH
	S3X(IEMAX) = S3(J) / THRESH
	S4X(IEMAX) = S4(J) / THRESH
	S5X(IEMAX) = S5(J) / THRESH
   99 IF(IEMAX .EQ. 25) GO TO 395                                       
  100 LN = LL(J)*3                                                      
      IF(LN .EQ. 0) GO TO 190                                           
      DO 110 K = 1,2                                                    
      L = LION(K,ISO)                                                   
      IF(L .EQ. 0 .OR. L .GT. LN) GO TO 110                             
      E3X = E3(IX + L)                                                  
      IF(E3X .LT. 1.) GO TO 110                                         
      IXL = IX + L                                                      
      CHITRY = E(J) - E3(IXL)                                           
C*** JPH Change 7/29/92
      IGND = IBNREC(CHITRY)
C      IGND = IBN(CHITRY)
      IF(IGND .GE. nbin) GO TO 110                                      
      IF(IGND .LT. 1) IGND = 1                                          
c
      if (chitry/kt .gt. 46.) recev(ignd) = recev(ignd) +
     1  qtkpr*conce(j+1) * zeta(k,iso) * ((chitry/13.6)**2)
      if (chitry/kt .gt. 46.) go to 110
c
      EDGTRY = QTK* CONCE(J+1)*ZETA(K,ISO)*((CHITRY/13.6)**2)           
     1   * EXP(CHITRY/KT)                                               
      EN = ECEN(IGND)                                                   
C*** JPH Change 7/29/92
      EDG = EDGTRY * EXPROD(IGND-1,IGND,KT)
C      EDG = EDGTRY * EBK**  IGND                                        
      BINNOW = BRMEV(IGND) + RECEV(IGND)                                
      IF(EDG .LT. EDGLIM*BINNOW) GO TO 110                              
      IEMAX = IEMAX + 1                                                 
      IEDGE(IEMAX) = IGND                                               
      EDGE(IEMAX) = EDGTRY                                              
      CHI(IEMAX) = CHITRY                                               
      JEDGE(IEMAX) = J                                                  
      NOTGRC(IEMAX) = .TRUE.                                            
      DONE(IEMAX) = .TRUE.                                              
      IF(IEMAX .EQ. 25) GO TO 395                                       
  110 CONTINUE                                                          
  190 IX = IX + 3*LL(J)                                                 
      J = J + 1                                                         
      ISO = ISO -1                                                      
      GO TO 10                                                          
  200 IF(CONCE(N+1) .LT. 1.E-6) GO TO 400
      IF(CONCE(N+1) .LT. 1.E-4 .AND. N .NE. 2) GO TO 400
      DO 250 K = 1,2
      Y = K
      CHITRY = 13.6 * (Z/Y)**2
C*** JPH Change 7/29/92
      IGND = IBNREC(CHITRY)
C      IGND = IBN(CHITRY)
      IF(IGND .GE. nbin) GOTO 250
      IF(IGND .LT. 1) IGND = 1
      EN = ECEN(IGND)
      X = EN/CHITRY
      GFTR = GFUNC(X,Y)                                                 
      IF(X .GT. 100.) GFTR = 7./SQRT(X)                                 
      GMXTRY = 1.1 - 1./(10.* Y**1.5)                                   
      IF(GFTR .GT. GMXTRY) GFTR = GMXTRY   
c
      if (chitry/kt .gt. 46.) recev(ignd) = recev(ignd) +
     1  qhydpr * conce(n+1) * gftr / y ** 3
      if (chitry/kt .gt. 46.) go to 250
c                             
      EDGTRY = QHYD*CONCE(N+1)*EXP(CHITRY/KT)/Y**3
C*** JPH Change 7/29/92
      EDG = EDGTRY * GFTR * EXPROD(IGND-1,IGND,KT)
C      EDG = EDGTRY * GFTR * EBK**  IGND                                 
      BINNOW = BRMEV(IGND) + RECEV(IGND)                                
      IF(EDG .LT. EDGLIM*BINNOW) GO TO 250                              
      IEMAX = IEMAX + 1                                                 
      IEDGE(IEMAX) = IGND                                               
      EDGE(IEMAX) = EDGTRY                                              
      CHI(IEMAX) = CHITRY                                               
      JEDGE(IEMAX) = J                                                  
      ESMIN3(IEMAX) = GMXTRY                                            
      ALFX(IEMAX) = Y                                                   
      NOTGRC(IEMAX) = .TRUE.                                            
      DONE(IEMAX) = .FALSE.                                             
      IF(IEMAX .EQ. 25) GO TO 395                                       
  250 CONTINUE                                                          
      GO TO 400                                                         
  395 PRINT 396, N                                                      
  396 FORMAT(1X//' IEMAX OVERRUN ON ELEMENT N =',I3//)                  
  400 IF(IPRINT .GT. 0) PRINT 401, N,T,IEMAX                            
  401 FORMAT(1X//' RECEMS FOR ELEMENT N =',I3,'  AT T =',1PE10.3,       
     1  ',   WITH',I3,'  SIGNIF. EDGES'/)                               
      IF ( IEMAX .EQ. 0 ) RETURN                                        
C  BUBBLE SORT TO PUT IEDGE IN INCREASING ORDER; PARALLEL SORT OF IMAP 
      DO 410 IE = 1,25                                                  
  410 IMAP(IE) = IE                                                     
  415 SWITCH = .FALSE.                                                  
      IEDGE(IEMAX + 1) = 0                                              
      IF(IEMAX .LT. 25) IEDGE(IEMAX + 2) = 0                            
      IF(IEMAX .EQ. 1) GOTO 430                                         
      IE =1                                                             
  420 IESAVE = IEDGE(IE)                                                
      IF(IEDGE(IE + 1) .GE. IESAVE) GO TO 425                           
      IMSAVE = IMAP(IE)                                                 
      IEDGE(IE) = IEDGE(IE + 1)                                         
      IMAP(IE) = IMAP(IE + 1)                                           
      IEDGE(IE + 1) = IESAVE                                            
      IMAP(IE + 1) = IMSAVE                                             
      IF(IE .GT. 1) SWITCH = .TRUE.                                     
  425 IE = IE + 1                                                       
      IF(IE .LT. IEMAX) GO TO 420                                       
      IF( SWITCH ) GO TO 415                                            
C  END SORT, PRINT TABLE OF EDGES SIGNIFICANT ENOUGH TO BE INCLUDED    
  430 IF(JPRINT .EQ. 0) GO TO 500                                       
      PRINT 441                                                         
  441 FORMAT(2(11X,'J     CHI',8X,'EDGE',7X,'N GRC   SHRT')/)           
      IEP = IEMAX/2  + 1                                                
      DO 450 IE = 1,IEP                                                 
      JB = IE*2                                                         
      IB = JB - 1                                                       
      JP = IMAP(JB)                                                     
      IP = IMAP(IB)                                                     
      IF(IEDGE(IB) .EQ. 0) GO TO 450                                    
      IF(IEDGE(JB) .EQ. 0) GO TO 444                                    
      PRINT 445,JEDGE(IP),CHI(IP),EDGE(IP),NOTGRC(IP),DONE(IP),         
     1          JEDGE(JP),CHI(JP),EDGE(JP),NOTGRC(JP),DONE(JP)          
      GO TO 450                                                         
  444 PRINT 445, JEDGE(IP),CHI(IP),EDGE(IP),NOTGRC(IP),DONE(IP)         
  445 FORMAT(2(10X,I2,0PF10.1,1PE12.2,2L8))                             
  450 CONTINUE                                                          
C  BEGIN BINNING, USING LIST IEDGE TO GOVERN NUMBER OF EDGES INCLUDED  
  500 IMIN = IEDGE(1)                                                   
      IEMAX = 1                                                         
C*** JPH Change 7/29/92
C      FIMIN = IMIN - 1
C      EXPROD = EBK ** FIMIN
      DO 550 I = IMIN,nbin                                              
  510 IF(I .NE. IEDGE(IEMAX+1) ) GO TO 520                              
      IEMAX = IEMAX + 1                                                 
      GO TO 510                                                         
  520 EN = ECEN(I)                                                      
      BINSUM = 0.                                                       
      DO 540 IE = 1,IEMAX                                               
      IPNT = IMAP(IE)                                                   
      BINN = EDGE(IPNT)                                                 
      IF(NOTGRC(IPNT)) GOTO 530                                         
      X = CHI(IPNT)/EN                                                  
      BINN = BINN * (S2X(IPNT)/X+S3X(IPNT)+S4X(IPNT)*X+S5X(IPNT)*X*X)
      GO TO 540
  530 IF(DONE(IPNT)) GO TO 540
      Y = ALFX(IPNT)
      X = EN/CHI(IPNT)
      GFTR = 7./SQRT(X)
      IF(X .LT. 100.) GFTR = GFUNC(X,Y)
      GMAX = ESMIN3(IPNT)
      IF(GFTR .GT. GMAX) GFTR = GMAX
      BINN = BINN * GFTR
  540 BINSUM = BINSUM + BINN
C*** JPH Change 7/29/92
      RECEV(I) = RECEV(I) + BINSUM * EXPROD(I-1,I,KT)
C      EXPROD = EXPROD*EBK
C      RECEV(I) = RECEV(I) + BINSUM*EXPROD
  550 CONTINUE

      END
