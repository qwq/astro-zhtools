C*** Ver 2.0 jph@cfa   July 27, 1992
      FUNCTION EFFN(I,ZEFF,T)
  100 T3 = T/(ZEFF*ZEFF*1000.0)
      XX = 0.4342 * LOG(T3)
      IF (T3-1.0)101,101,102
  101 F1 = 0.266
      F2 = 0.13
      F3 = 0.13
      GO TO 105
  102 IF(T3-10.**5)103,104,104
  103 F1 = 0.266 + 0.1068 * XX - 0.074 * SIN(1.2566*XX)
      F2 = 0.130 + 0.1160 * XX - 0.074 * SIN(1.2566*XX)
      F3=0.130-0.0120*XX+0.050*EXP(-(XX-2.)*(XX-2.))
      GO TO 105
  104 F1 = 0.80
      F2 = 0.71
      F3 = 0.07
  105 CONTINUE
      IF (I .EQ. 0) GO TO 1
      IF(I-18) 110,110,18
  110 GO TO (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18),I
    1 EYE = I
      EFFN = 2. - EYE
      GO TO 1000
    2 EFFN = 8.0
      GO TO 1000
    3 EFFN = 8. - (4.*F1)
      GO TO 1000
    4 EFFN = 8.*(1.-F1)
      GO TO 1000
    5 EFFN = 6.6667*(1.-F1)
      GO TO 1000
    6 EFFN = 5.33333*(1.-F1)
      GO TO 1000
    7 EFFN = 4. * (1.-F1)
      GO TO 1000
    8 EFFN =  2.6667*(1.-F1)
      GO TO 1000
    9 EFFN =  1.33333*(1.-F1)
      GO TO 1000
   10 EFFN = 18.
      GO TO 1000
   11 EFFN =  18.-(9.*F2)
      GO TO 1000
   12 EFFN = 18.0 * (1.-F2)
      GO TO 1000
   13 EFFN = 18.*(1.-F2) -1.*(9.*F3)
      GO TO 1000
   14 EFFN = 18.*(1.-F2) -2.*(9.*F3)
      GO TO 1000
   15 EFFN = 18.*(1.-F2) -3.*(9.*F3)
      GO TO 1000
   16 EFFN = 18.*(1.-F2) - 4.*(9.*F3)
      GO TO 1000
   17 EFFN = 18.*(1.-F2) -45.0*F3
      GO TO 1000
   18 NO = 28 -I
      EFFN = NO * 1.8 * (1.-3.*F3-F2)
      IF (EFFN)106,1000,1000
  106 EYE = I
      EFFN = 60. -EYE
C     GUARD PACKAGE FOR I = 17
C     PROBABLY UNNECESSARY
      IF(EFFN .LE. 0) EFFN = 1.0
 1000 CONTINUE
      RETURN
      END
