c MM 4 Apr 96: fixed division by zero (below label 306)
c MM 16 Feb 94: changed common /PARAMS/
C*** Ver 2.0 jph@cfa   July 27, 1992

      SUBROUTINE NQUIL(T,N,DENE,JCONT,IPHOT,IDENS,ICX)
C   ************************  METASTABLE POPULATION DATA

      DIMENSION EMETJ(30,4),CD1(28),CD2(28)
      DIMENSION RAT(30),PROD(30),C5(30),C6(30),OVER(30),IMETA(30)
      DIMENSION SEC(30)

      COMMON/DAT/E(30),EA(30),S2(30),WAVE(220),E3(220),F(220),LL(30),
     1  S3(30),S4(30),S5(30)
C*** JPH Change 7/29/92, MM 16/2/94
      COMMON/PARAMS/NJ(12),ABUNJ(0:12),ABUND,NBIN
C      COMMON/PARAMS/NJ(12),ABUNJ(12),ABUND,BINMIN,BINSYZ,NBIN
      COMMON/RATES/C1(30),C2(30),CTH
      COMMON/RESULT/CONCE(30),CA(30),POWER(220),RHY,HENEUT,HEPLUS,
     1  DNE,PCOOL,POU,POT,RE,TU,PM(4)

      DATA EMETJ/5*0.,6.5,8.35,10.2,0.,14.,0.,17.6,0.,21.6,0.,25.3,0.,
     1  29.0,0.,32.7,5*0.,47.0,0.,51.8,0.,0.,
     2  5*0.,5.34,7.10,8.87,0.,12.5,0.,16.0,0.,19.8,0.,24.0,0.,28.6,0.,
     3  34.2,5*0.,57.1,0.,64.8,0.,0.,
     4  5*0.,4.18,5.78,7.48,0.,11.0,0.,14.7,0.,18.6,0.,23.0,0.,28.0,0.,
     5  33.9,5*0.,60.4,0.,69.2,0.,0.,  11*0.,
     6  2.72,0.,6.55,0.,10.4,0.,14.2,0.,18.1,5*0.,29.7,0.,38.9,2*0./
C
	DATA IMETA/0,0,0,1,2,3,5*0,4,18*0/
      DATA CD1/.0024,-.0005,.00,.061,.027,.011,.005,.005,0.,.0107,
     1  .09,.13,.11,.081,.075,.066,.051,11*0./
      DATA CD2/0.,.01485,.300,.108,.107,.024,.075,.051,.054,.0167,.36,
     1  17*0./
      DATA SEC/4*1.,.9,.9,6*1.,.6,.5,1.,.7,.9,13*1./
      IX = 0
      ABHE = 10. ** (ABUNJ(1) - 12.)
      DO 678 I=1,4
  678 PM(I) = 0.
      ORANGE = 1.
      C5(1) = 0.
      C5(N+1) = 0.
      JLOW=1
      JMAX=N+1
      JTOP = N + 1
      C6(1)=0
      T6=T*1.0E-6
      ST = SQRT(T)
      DO 301 J=1,N
C  IONIZATION RATE FROM YOUNGER
	EION = E(J)
	C1(J) = CION(N,J,EION,T)
	IF (N-J .LE. 1) GO TO 104
C  IONIZATION FROM METASTABLE LEVEL : BE,B,C,MG SEQUENCES
      IF (IDENS .EQ. 0) GO TO 105
	IMET = IMETA(N-J+1)
	IF (IMET .EQ. 0) GO TO 105
	EMET = EMETJ(N,IMET)
	FMET =  POPMET(N,IMET,EMET,DENE,T)
	PM(IMET) = FMET
	EM = E(J) - EMET
	C1(J) = C1(J)*(1.-FMET) + FMET * CION(N,J,EM,T)
  105 CONTINUE
C  INNERSHELL IONIZATION
	IF (N-J .LE. 1) GO TO 104
	EION = EA(J)
	JP = N-1
	IF (N-J .GE. 10) JP = N-9
	C1(J) = C1(J) + CION(N,JP,EION,T)
  104 CONTINUE
C  IONIZATION FOLLOWING INNERSHELL EXCITATION  :  COWAN AND MANN
C  INCREASED FOR FE
	C1(J) = C1(J) + AUTOIN(N,J,T)
C  ****************      PHOTOIONIZATION
c      IF (IPHOT .NE. 0) C1(J) = C1(J) + PHOT(N,J,E(J),T,DENE)/DENE
C  ****************      RADIATIVE RECOMBINATION
      ZEFF=J
      BETA=ZEFF*ZEFF/(6.34*T6)
    2 C2(J+1)=2.07E-11*ZEFF*ZEFF*(0.4288 + 0.5*ALOG(BETA)
     1  + 0.469 * (BETA**(-1./3.))) / SQRT(T)
      APPLE=C2(J+1)
      PLUM=0
      RECN = 0.
      RGND = 0.
      IF (N .EQ. J) GO TO 50
      NVAL=GND(N-J) + 0.1
      STARN = SQRT(13.6/E(J))*ZEFF
      DO 5000 NQM= 1,NVAL
      QM=NQM
      BE = BETA / (QM*QM)
 5000 C2(J+1) = C2(J+1) - 5.197E-14 * J * SQRT(BE) * SEATON(BE,QM)
      IF (C2(J+1) .LT. 0)  C2(J+1) =0
      I = N-J
C  RECOMBINATION TO GROUND STATE
      IF (I .LE. 17) RGND = GREC(N,J,E(J),T)
      IF (I .LE. 1) GO TO 50
      IF (I .GE. 4 .AND. I .LE. 9) GO TO 50
      EI = E(J)
      IF (I .GE. 17) GO TO 40
C  RECOMBINATION TO OTHER STATES IN SAME SHELL
      LION = 1
      IF (I .EQ. 12) LION = 3
      EI = EI - E3(IX+LION)
   40 STARN = SQRT(13.595/EI)*ZEFF
      BE = BETA / (STARN*STARN)
      RECN = EFFNEW(N-J,T,ZEFF,N) * SEATON(BE,STARN) * 2.599E-14 * J *
     1  SQRT(EI*11590./T) / (STARN*STARN)
   50 C2(J+1) = C2(J+1) + RECN + RGND
      IF (BETA .GE. 0.5) GO TO 62
      CHI=1.2*BETA + ( 0.55 + 1.04*ALOG(BETA))*BETA*BETA
     1  + (-0.43 + 1.01*ALOG(BETA))*BETA**3
      GO TO 63
   62 CHI=0.5 * (0.735 + ALOG(BETA) + 1./(3.*BETA))
   63 CONTINUE
      DO 64 NQM=1,NVAL
      QM=NQM
   64 CHI = CHI - P(BETA,QM)
      CHI = CHI + EFFN(N-J,ZEFF,T) * P(BETA,STARN)/(2.*STARN*STARN)
    6 C6(J+1)=E(J)*1.6027E-12*(C2(J+1)+4.22E-22*SQRT(T/1.037E-11)*CHI)
C  DIELECTRONIC RECOMBINATION ***** DENSITY DEPENDENCE AND SECONDARY
C                             ***** AUTOIONIZATION
      IY = LL(J) * 3
      IF (J .EQ. 1) GO TO 370
      ZEFF = J-1
      RHO = (DENE / ZEFF ** 7.) ** .2
      DD = 1.
      IF (RHO .GE. 3) DD = 1./(1.+(CD1(N-J+1)+CD2(N-J+1)/ZEFF)*RHO)
      DO 320 L=1,IY
      LN=IX+L
      PLUM = PLUM + ALPHADI(N,J,L,LN,T) * AMIN1(DD,SEC(N-J+1))
  320 CONTINUE
C  DIELECTRONIC RECOMBINATION FOR METASTABLE LEVELS OF BE,B,C,MG ISO
	IMET = IMETA(N-J+1)
	IF (IMET .EQ. 0) GO TO 321
	ZF = J-1
	B = SQRT(ZF)*(ZF+1.)**2.5/SQRT(ZF*ZF+13.4)
      PLUM = PLUM * (1.-FMET)
      PLUM = PLUM + FMET * DIMET(N,J,T,B,DD)
  321 CONTINUE
C   *************** ADD DIELECTRONIC RECOMB AND STOREY'S LOW T DIELEC REC
      C2(J) = C2(J) + PLUM + ALFLO(N,J,T)
  370 OVER(J) = PLUM/ORANGE
      IF (J .NE. 1) C5(J) = PLUM * E(J-1) * 10. ** (ABUND-1.) * 1.6027
      IX = IX + IY
  301 ORANGE = APPLE
      if (ICX .EQ. 0) go to 306
C  CHARGE TRANSFER CONTRIBUTION
      FPLUS = RHY / (1.+RHY)
      DENH = 1./(.003 +FPLUS+ABHE*HEPLUS+ABHE*2.*(1.-HENEUT-HEPLUS))
      C1(N+1) = 0.
      C2(1) = 0.
      NN = N + 1
C  **********************************  S. Butler Charge Transfer Rates
      DO 305 J = 1,N
      C1(J) = C1(J)+CTHI(N,J,T)*DENH*FPLUS+
     1  CTHEI(N,J,T,DENE)*HEPLUS*ABHE
      C2(J+1)=C2(J+1)+CTHR(N,J+1,T)*DENH*(1.-FPLUS)  +
     1  CTHER(N,J+1,T)*HENEUT*ABHE		
  305 CONTINUE
  306 continue
C  AUGER IONIZATION
c      IF (IPHOT .EQ. 2) CALL APHOT(N,DENE,JCONT)
	IF (JCONT .EQ. 1) GO TO 725
      DO 400 J=1,N
      C2(J+1)=ABS(C2(J+1))

c MM changed this 4.4.96 (this div is nonsense and caused div by zero):
c      IF (C1(J) / C2(J+1) .LE. 0.) GO TO 31
      IF (C1(J) .LE. 0.) GO TO 31

      RAT(J)=C2(J+1)/C1(J)
      IF(J .EQ. 1 .AND. T .GE. 1.0E8) RAT(J)=AMIN1(RAT(J),1.0E4)
      GO TO 32
   31 RAT(J)=1.0E+6
   32 CONTINUE
      IF (RAT(J) .GE. 1.0E-5) GO TO 34
      JLOW=J+1
   34 IF (RAT(J) .LT. 1.0E+5) GO TO 400
      JMAX=J
      GO TO 41
  400 CONTINUE
      IF (JLOW .EQ. JMAX) GO TO 525
   41 JUMP=JMAX - 1
      PROD(JUMP)=RAT(JUMP)
      KTOP=JUMP - JLOW
      SUM = 1.00000 + PROD(JUMP)
      IF (KTOP .EQ. 0)  GO TO 550
      DO 500 K=1,KTOP
      PROD(JUMP-K)=RAT(JUMP-K)*PROD(JMAX-K)
      PROD(JUMP-K) = AMIN1(PROD(JUMP-K),1.0e30)
  500 SUM = SUM + PROD(JUMP-K)
      GO TO 550
  525 SUM = 1.0000
  550 CONTINUE
      DO 600 J=1,JTOP
      CONCE(J)=0
  600 C5(J) = 0.
      CONCE(JMAX) = 1.000/SUM
      KMAX=JMAX-JLOW
      IF (KMAX .EQ. 0)  GO TO 725
      DO 700 K=1,KMAX
  700 CONCE(JMAX-K)=PROD(JMAX-K)*CONCE(JMAX)
  725 CONTINUE
      IF (N .EQ. 2)  DNE = 0.
      WORL = 0.
      DEI = 0.
      DO 901 J=1,JTOP
      DNE = (J-1)*CONCE(J)*10. ** (ABUND-12.) + DNE
    8 C6(J)=C6(J)*CONCE(J)*10.0**(11. + ABUND)
      RE = RE + C6(J)
      C5(J) = C5(J) * CONCE(J)
      PCOOL = PCOOL + C5(J)
      PCOOL=PCOOL + C6(J)
  901 CONTINUE
      RETURN
      END
