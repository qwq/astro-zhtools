C*** Ver 2.0 jph@cfa   July 27, 1992
      FUNCTION EFFNEW(I,T,Z,N)
      T3 = .001 * T / Z**2
      XX = .4342 * LOG(T3)
      IF (T3-1.) 101,101,102
  101 F1 = .266
      F2 = .13
      F3 = .13
      GO TO 105
  102 IF ( (T3 - 1.E5) .GE. 0.) GO TO 104
      F1 = .266 + .1068*XX - .074*SIN(1.2566*XX)
      F2 = .130 + .1160*XX - .074*SIN(1.2566*XX)
      F3 = .130 - .012 * XX + .05*EXP(-(XX-2.)*(XX-2.))
      GO TO 105
  104 F1 = .80
      F2 = .71
      F3 = .07
  105 CONTINUE
      IF (I .EQ. 2 .OR. I .EQ. 3) EFFNEW = 8. * (1.-F1)
      IF (I .EQ. 10 .OR. I .EQ. 11) EFFNEW = 18. * (1.-F2)
      IF (I .GE. 12 .AND. I .LE. 17) EFFNEW = 18. * (1.-3.*F3 - F2)
      IF (I .GE. 18) EFFNEW = (28. - N + Z) * 1.8 * (1.-3.*F3 - F2)
      RETURN
      END
