C*** Ver 2.0 jph@cfa   July 27, 1992
      FUNCTION POPMET(N,IJ,EMET,DENE,T)
C  RETURNS POPULATION OF METASTABLE LEVEL OF BE,B,C, OR MG-LIKE ION;  CARBON-NICKEL
C  RESONANCE CONTRIBUTIONS CALC FOR HIGH Z WITH SMITH ET AL METHOD
C  PASSES REDUC BACK TO GAUNT FOR INTERCOMBINATION LINES

      REAL MGMM,MGM1,MGM5,MG21,MG5,MG1

      DIMENSION RM5(12),EBE(12),pops(4)
      DIMENSION MGMM(3,7),MGM1(7),MG5(7),MG21(2,7),EMG(7)
      DIMENSION NJ(30),A21(2,12),B21(3,12),C21(3,12),RM1(12)
      DIMENSION RMM(3,12),BM1(12),BMM(3,12),CM1(3,12),CMM(2,12)
      DIMENSION ECM(2,12),R(3,3),X(3),C(3),CPR(3)
      DIMENSION RB5(10),RC5(10),RRES(4,10),ERES(4,10)
      DIMENSION RBE(10),BEE(10)

      COMMON/RESULT/CONCE(30),GNDREC(30),POWER(220),RHY,HENEUT,HEPLUS,
     1  DNE,PCOOL,POU,POT,RE,TU,PM(4)
      COMMON/INTERC/REDUC(4)
      COMMON/METLN/PLN(4,3,8),TLN(4,3,8)

      DATA RM5/0.,18.6,11.7,6.09,2.79,1.54,.93,.61,.41,.31,.13,.10/
      DATA EBE/0.,6.20,7.87,9.51,12.81,16.1,19.5,23.11,26.9,28.9,55.5,
     1  63.1/
      DATA MG21/430.,5.8E-4,22000.,.008,1.63E+5,.000,9.E+5,.12,3.3E+6,
     1  .26,4.5E+7,1.00,8.E+7,2./
      DATA MG5/0.2,0.5,.26,.035,.026,.0058,.0039/

C  GILES QUOTED BY MENDOZA FOR S V, BALUJA FOR SI III

      DATA MGM1/.045,.35,.072,.026,.016,.0069,.0047/
      DATA MGMM/.5,.38,1.5,1.4,1.06,4.1,.25,.39,1.32,
     1  .29,.21,.82,.17,.13,.51,.033,.25,.43,.02,.13,.30/
      DATA EMG/1.63,3.72,5.50,7.13,8.72,13.9,17./
      DATA NJ/0,1,0,0,0,2,3,4,0,5,0,6,0,7,0,8,0,9,0,10,5*0,11,0,12,0,0/
      DATA A21/2*0.,77.2,.00282,471.,.00629,1880.,.0118,16200.,.0306,
     1  84500.,.0629,320000.,.112,972600.,.1823,2.546E+6,.2781,
     2  5.914E+6,.4029,7.3E+7,1.21,1.4E+8,1.8/
      DATA B21/3*0.,332.,130.,82.,  1183.,130.,347.,4210.,499.,1470.,
     1 28400.,3170.,8910.,152000.,19800.,56200.,705000.,102000.,288000.,
     2 43790.,11613.,27300.,4.28E+06,7.08E+05,2.42E+06,
     3  1.02E+07,1.55E+06,5.62E+06,8.E+7,1.E+7,5.E+7,2.E+8,2.E+7,1.E+8/
      DATA C21/3*0.,.00031,.0026,32.,.0041,.0342,190.,.022,.16,385.,
     1  .599,4.64,6910.,4.67,37.1,45200.,26.1,200.,197000.,115.,856.,
     2 680000.,424.,3000.,2.0E+06,1370.,9090.,5.32E+06,
     3  29400.,128000.,6.7E+7,95000.,380000.,1.7E+8/

C  REDUCED COLLISION RATES * 10. ** 7

      DATA RM1/0.,9.384,4.944,2.814,1.340,.7265,.4718,.3164,.2243,
     1  .1732,.08,.062/

C  INCLUDES PROTON RATES FROM MUNRO THROUGH SI, EXT F OR S, NONE ABOVE A

      DATA RMM/3*0.,29.9,13.4,50.8,17.7,8.41,32.2,11.2,6.00,21.8,
     1  6.48,4.19,13.9,3.96,3.04,9.68,2.91,2.67,7.96,2.19,2.42,6.83,
     1  1.25,0.40,1.79,.533,.317,1.40,.046,.16,.67,.020,.12,.52/
      DATA BM1/0.,14.,8.1,5.3,4.1,3.01,2.0,1.5,1.1,.95,.67,.58/
      DATA BMM/3*0.,23.,7.9,31.,17.1,5.73,20.8,12.3,4.17,14.1,
     1  6.52,2.39,7.91,3.84,1.47,3.01,2.65,1.01,3.16,18.6,13.9,38.,
     2  1.68,.57,1.9,1.34,.43,1.45,.69,.18,.69,.55,.14,.54/
      DATA CM1/3*0.,12.1,12.9,10.,51.,29.5,23.,12.0,8.6,4.18,
     1 9.1,6.4,2.92,6.75,4.72,2.1,5.39,3.86,1.55,4.17,3.04,1.19,
     2  2.84,2.02,1.00,2.43,1.57,0.90,1.1,.75,.60,1.0,.56,.54/
      DATA CMM/2*0.,12.9,.0005,32.5,.001,29.,.0015,17.5,.00205,11.2,
     1  .00173,7.42,.00173,5.44,.00518,4.1,.0069,3.52,.0138,1.7,.03,1.5,
     1  .05/
      DATA ECM/2*0.,1.26,2.68,1.90,4.05,2.42,5.34,3.76,7.93,5.09,10.6,
     1 6.58,13.4,8.34,16.5,10.6,20.1,13.5,24.5,30.1,45.,51.,62./
      DATA RB5/0.,25.,21.6,16.9,8.80,5.23,3.64,15.5,2*0./
      DATA RC5/10*0./
      DATA BEE/0.,10.5,13.4,16.3,22.1,28.,34.,40.5,2*0./
      DATA RBE/0.,115.,95.4,78.4,47.7,32.1,24.6,18.2,2*0./
      DATA ERES/4*0.,12.7,9.25,7.94,0.,16.3,12.5,11.5,0.,
     1  19.7,15.8,14.9,0.,27.0,22.3,21.9,0.,33.8,28.8,28.9,4.35,
     2  40.9,35.8,36.3,10.3,48.4,11.6,44.1,15.8,  8*0./
      DATA RRES/4*0.,327.,95.,5.9,0.,306.,111.,74.9,0.,
     1  229.,97.8,139.,0.,141.,60.7,111.,0.,96.,41.3,70.3,68.0,
     2  71.7,30.3,37.1,956.,52.5,22.0,26.5,64.3,8*0./

      DO 9 I = 1,3
      DO 8 J=1,3
    8 R(I,J) = 0.
    9 CONTINUE
c  avoid overflow for low t

      REDUC(IJ) = 0.
      POPMET = 0.
      IF (EMET*11590./T .GE. 30.) RETURN
      ST = SQRT(T)
      NO = NJ(N)
      V = DENE * 1.E-7 / ST
      GO TO (1,2,3,4) , IJ
    1 CONTINUE
      CN = CONCE(N-3)
      R(1,1) = -(RM1(NO)+RMM(1,NO)*3.+RMM(2,NO)*5.) * V
      R(2,2) = -A21(1,NO) - V*(RM1(NO)+RMM(1,NO)+RMM(3,NO)*1.6666667)
      R(3,3) = -A21(2,NO) - V * (RM1(NO)+RMM(2,NO)+RMM(3,NO))
      R5 = RM5(NO)*V*EXP(-11590.*EBE(NO)/T)
      R(1,1) = R(1,1) - R5
      R(2,2) = R(2,2) - R5
      R(3,3) = R(3,3) - R5
      R(2,1) = V * RMM(1,NO) * 3.
      R(3,1) = V * RMM(2,NO) * 5.
      R(1,2) = V * RMM(1,NO)
      R(1,3) = V * RMM(2,NO)
      R(3,2) = V * 1.666667 * RMM(3,NO)
      R(2,3) = V * RMM(3,NO)
      C(1) =-RM1(NO) * EXP(-EMET*11590./T) * V
      C(2) = C(1) * 3.
      C(3) = C(1) * 5.
      GO TO 10
    2 CONTINUE
C  B-LIKE IONS
      CN = CONCE(N-4)
      R5 = RB5(NO)*V*EXP(-11590.*(ERES(2,NO)-EMET)/T)
      R(1,1) = -(BM1(NO)+BMM(1,NO)*2.+BMM(2,NO)*3.)*V - B21(1,NO)
      R(2,2) = -(BM1(NO)+BMM(1,NO)+BMM(3,NO)*1.5)*V - B21(2,NO)
      R(3,3) = -(BM1(NO)+BMM(2,NO)+BMM(3,NO))*V - B21(3,NO)
      R(2,1) = V * BMM(1,NO) * 2.
      R(3,1) = V * BMM(2,NO) * 3.
      R(1,2) = V * BMM(1,NO)
      R(1,3) = V * BMM(2,NO)
      R(3,2) = V * 1.5 * BMM(3,NO)
      R(2,3) = V * BMM(3,NO)
      C(1) = -BM1(NO) * EXP(-EMET*11590./T) * V / 3.
      C(2) = C(1) * 2.
      C(3) = C(1) * 3.
      GO TO 10
    3 CONTINUE
C  C-LIKE IONS
      CN = CONCE(N-5)
      R5 = RC5(NO) * V * EXP(-11590.*(ERES(3,NO)-EMET)/T)
      E1 = EXP(-ECM(1,NO)*11590./T)
      E2 = EXP(-ECM(2,NO)*11590./T)
      E3 = EXP(-EMET*11590./T)
      R(1,1) = -(CM1(1,NO)+CMM(1,NO)*.2*E2/E1+CMM(2,NO)*E3/E1)*V-C21(1,
     1 NO)
      R(2,2) = -(CM1(2,NO)+CMM(1,NO))*V- C21(2,NO)
      R(3,3) = -(CM1(3,NO) + CMM(2,NO)) * V - C21(3,NO)
      R(2,1) = V * CMM(1,NO) * .2 * E2/E1
      R(1,2) = V * CMM(1,NO)
      R(3,1) = V * CMM(2,NO) * E3/E1
      R(1,3) = V * CMM(2,NO)
      R(3,2) = 0.
      R(2,3) = 0.
      C(1) = -CM1(1,NO) * V * 5. * E1 / 9.
      C(2) = -CM1(2,NO) * V * E2 / 9.
      C(3) = -CM1(3,NO) * V * E3 * 5. / 9.
      GO TO 10
    4 CONTINUE
      CN = CONCE(N-11)
      V = 8.63E-6 * DENE / ST
      NP = NO - 5
      MG1 = V * MGM1(NP)
      MGM5 = V * MG5(NP) * EXP(-11590.*EMG(NP)/T)
      IF (N .EQ. 14) MGM5 = MGM5 / (1.+8.6E-6*T)
      IF (N .EQ. 14) MG1 = MG1 / (1.+T*3.5E-6)
      R(1,1) = -MG1 - MGM5 - MGMM(1,NP) * V - MGMM(2,NP) * V
      R(2,2) = -MG1-MGM5-MGMM(1,NP)*V/3-MGMM(3,NP)*V/3-MG21(1,NP)
      R(3,3) = -MG1-MGM5-MG21(2,NP)-MGMM(2,NP)*V/5.-MGMM(3,NP)*V/5.
      R(2,1) = V*MGMM(1,NP)
      R(3,1) = V*MGMM(2,NP)
      R(1,2) = V*MGMM(1,NP) / 3.
      R(1,3) = V*MGMM(2,NP) / 5.
      R(3,2) = V*MGMM(3,NP) / 3.
      R(2,3) = V * MGMM(3,NP) / 5.
      C(1) = -MG1 * EXP(-11590.*EMET/T)
      C(2) = C(1) * 3.
      C(3) = C(1) * 5.
   10 CONTINUE
      NN = NOSON(R,X,3,3)
      IF (NN .EQ. 0) PRINT 99
   99 FORMAT (' BAD INVERSION')
      DO 20 I = 1,3
      CPR(I) = 0.
      DO 15 J = 1,3
   15 CPR(I) = CPR(I) + R(I,J) * C(J)
   20 CONTINUE
      SUM = 1. + CPR(1) + CPR(2) + CPR(3)
      POPS(1) = 1./SUM
      POPS(2) = CPR(1) / SUM
      POPS(3) = CPR(2) / SUM
      POPS(4) = CPR(3) / SUM
   30 CONTINUE
      POPMET = (CPR(1) + CPR(2) + CPR(3)) / SUM
      IF (IJ .EQ. 3) POPMET = CPR(3) / SUM
      FC = 1.E23 * 1.6E-12 * CN / DENE
      GO TO (51,52,53,54) , IJ
C  ERGS * 10-23 / S / ATOM
   51 PLN(1,1,NO) = (POPS(1)*RRES(1,NO)*V*EXP(-11590.*ERES(1,NO)/T) +
     1 POPMET * R5) * FC * ERES(1,NO)
      PLN(1,2,NO) = POPMET * V * RBE(NO) * EXP(-11590.*BEE(NO)/T)*FC *
     1  BEE(NO)
      PLN(1,3,NO) = POPS(3) * A21(1,NO) * EMET * FC
      SUMC = -C(1) - C(2) - C(3)
      REDUC(1) = (POPS(3)*A21(1,NO)+POPS(4)*A21(2,NO))/SUMC
      RETURN
   52 PLN(2,1,NO) = (POPS(1)*RRES(2,NO)*V*EXP(-11590.*ERES(2,NO)/T) +
     1  POPMET * R5) * FC * ERES(2,NO)
      PLN(2,3,NO) = (POPS(2)*B21(1,NO) + POPS(3)*B21(2,NO) + POPS(4) *
     1  B21(3,NO)) * FC * EMET
      SUMC = -C(1) - C(2) - C(3)
      REDUC(2)=(POPS(2)*B21(1,NO)+POPS(3)*B21(2,NO)+POPS(4)*B21(3,NO))/
     1  SUMC
      RETURN
   53 PLN(3,1,NO) = (POPS(1) *RRES(3,NO)*V*EXP(-11590.*ERES(3,NO)/T)
     1  + POPMET*R5) * FC * ERES(3,NO)
      PLN(3,3,NO) = POPS(4) * C21(3,NO) * FC * EMET
      REDUC(3) = -POPS(4)*C21(3,NO) / C(3)
      RETURN
   54 PLN(4,1,NO) = (POPS(1)*RRES(4,NO)*V*EXP(-11590.*ERES(4,NO)/T)
     1  + POPMET*MGM5)*FC * ERES(4,NO)
      PLN(4,3,NO) = POPS(3)*MG21(1,NP) * FC * EMET
      SUMC = -C(1) - C(2) - C(3)
      REDUC(4) = (POPS(3)*MG21(1,NP)+POPS(4)*MG21(2,NP))/SUMC
      RETURN
      END

      FUNCTION NOSON (A,X,L,LMAX)
C     RUDOLF LOESER, 28 JULY 1965
C     NOSON IS A MATRIX INVERSION ROUTINE, USING THE METHOD OUTLINED ON
C     P 434 OF HILDEBRAND, INTRODUCTION TO NUMERICAL ANALYSIS,
C     (NEW YORK,1956).
C     A IS A MATRIX OF ORDER L, WITH COLUMN DIMENSION LMAX, ITS ELEMENTS
C     ARE ASSUMED TO BE STORED COLUMNWISE, IN THE USUAL FORTRAN MANNER,
C     X IS WORKING STORAGE OF LENGTH L.
C     THE INVERSE OF A WILL REPLACE A.
C     UPON RETURN, NOSON=1 IF INVERSION WENT OK, =0 IF A DIVISOR IS
C     ZERO (IN WHICH CASE A MAY CONTAIN GARBAGE UPON RETURN).
      DIMENSION A(1),X(1)
      N = L - 1
      MAX = N * LMAX + L
      DO 100 I = 1,L
        X(I) = 1.
 100  CONTINUE
      K1 = -LMAX
      DO 110 K=1,L
        K1 = K1 + LMAX
        K2 = K1 + K
        IF (A(K2) )101,115,101
 101  DO 104 I = 1,L
          J1 = K1 + I
           IF ( A(J1) )102,104,102
 102       F = 1. / A(J1)
           X(I) = X(I) * F
          DO 103 J1 = I,MAX,LMAX
               A(J1) = A(J1) * F
 103           CONTINUE
 104      CONTINUE
        A(K2) = X(K)
        X(K) = 1.
        DO 108 I=1,L
           KI = K-I
           IF ( KI )105,108,105
 105       J1 = K1 + I
           IF ( A(J1) )106,108,106
 106       A(J1) = 0.
           DO 107 J2 = I, MAX, LMAX
              J1 = J2 + KI
              A(J2) = A(J2) - A(J1)
 107          CONTINUE
 108       CONTINUE
 110    CONTINUE
      DO 113 I = 1,N
        IF ( X(I) )111,115,111
 111    F = 1. / X(I)
        DO 112 J1 = I, MAX, LMAX
           A(J1) = A(J1) * F
 112       CONTINUE
 113    CONTINUE
      NOSON = 1
 114  RETURN
 115  NOSON = 0
      RETURN
      END
