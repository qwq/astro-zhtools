c MM 13 Jun 94: commented out adding lines to output
c MM 16 Feb 94: added parameter (NSMAX=2000); removed common /BLN/ and
c call BLND; changed common /COM/,/PARAMS/

C*** Ver 2.0 jph@cfa   July 27, 1992


	subroutine FELINE_CONT (T,dene,num,ipr,jpr,jcont,iphot,idens,icx)


	parameter (NSMAX=2000)
        common /ENER/   enin(0:NSMAX)
	common /FEL/    wj(12,220),fj(12,220),e3j(12,220),pwr(12,220),
     1	    c1j(12,30),c2j(12,30),ej(12,30),eaj(12,30),s2j(12,30),
     2	    llj(12,30),s3j(12,30),s4j(12,30),s5j(12,30)
	common /DAT/    e(30),ea(30),s2(30),wave(220),e3(220),f(220),ll(30),
     1	    s3(30),s4(30),s5(30)
c MM 16/2/94:
	common /COM/    cnc(12,30), bin(NSMAX)
	common /PARAMS/ nj(12),abunj(0:12),abund,nbin
c	common /COM/    cnc(12,30), ptot(12,220), abin(NSMAX), bin(NSMAX)
c	common /PARAMS/ nj(12),abunj(0:12),abund,binmin,binsyz,nbin
	common /RESULT/ conce(30),ca(30),power(220),rhy,heneut,heplus,
     1	    dne,pcool,pou,pot,re,tu,pm(4)
	common /AUG/    caj(12,30)
	common /RATES/  c1(30),c2(30),cth


	common /CONTIN/ brmev(NSMAX), recev(NSMAX), tufev(NSMAX)
	dimension pc(12)


	do i=1,nbin
	  recev(i)=0.
	  tufev(i)=0.
	  brmev(i)=0.
 	  bin(i)=0.
	end do 

	nlines=220
	st=sqrt(T)
	pcool=0.
        abund=abunj(0)
	conce(2)=rhy/(1.+rhy)

	if (nbin.ne.0) call BREMS (T,1)
	if (nbin.ne.0) call RECEMS (T,1,0,0)
	
c element loop:
	do no=1,num
	  re=0.
	  tu=0.
	  abund=abunj(no)

	  do l=1,nlines
	    power(l)=0.
	    wave(l)=wj(no,l)
	    f(l)=fj(no,l)
	    e3(l)=e3j(no,l)
	  end do

	  n=nj(no)
	  nn=n+1
	  do j=1,n
	    e(j)=ej(no,j)
	    ea(j)=eaj(no,j)
	    s2(j)=s2j(no,j)
	    conce(j)=cnc(no,j)
	    ll(j)=llj(no,j)
	    s3(j)=s3j(no,j)
	    s4(j)=s4j(no,j)
	    s5(j)=s5j(no,j)
	  end do 
	  
	  e(n+1)=0.
	  ll(n+1)=0.
	  conce(n+1)=cnc(no,n+1)
	  
	  call SINGLE (T,dene,no,io,ipr,jpr,jcont,iphot,idens,icx)
	  
	  if (no.eq.1) heneut=conce(1)
	  if (no.eq.1) heplus=conce(2)
	  do j=1,nn
	    if (jcont.eq.0) cnc(no,j)=conce(j)
	    caj(no,j)=ca(j)
	    c1j(no,j)=c1(j)
	    c2j(no,j)=c2(j)
	  end do
	  
	  if (n.ne.18.and.n.ne.20.and.n.ne.28) then
	    if (nbin.ne.0) call BREMS (T,n)
	    if (nbin.ne.0) call RECEMS (T,n,0,0)
	  end if 
	  
	  do l=1,220
	    pwr(no,l)=power(l)
	  end do

*c MM: here is where it adds lines: 
*	  if (nbin.ne.0) call BUND (no)

	  pc(no)=pcool+tu
	end do


	pcool=0.
	do no=1,num
 	  pcool=pcool+pc(no)
	end do 

c MM: here is where it adds continuum:
	do i=1,nbin
	  bin(i)=bin(i)+recev(i)+brmev(i)+tufev(i)
	end do


	end
