C*** Ver 2.0 jph@cfa   July 27, 1992
	FUNCTION DELT(N,J,L)
C  OUR VERY OWN MODIFICATIONS TO JACOBS
C  DENSITY DEPENDENCE EXTERNAL
C  TAKE 0.5 FOR MERTS ET AL   DELTA N .NE. 0 EXCEPT S
C  ASSUME 3D OF N,O,F INTERP BETWEEN C AND NE ISOSEQUENCES
C  THIS USES YOUNGER'S CLAIM THAT DELT=1 FOR HE-LIKE RESONANCE LINE;
C  TRY BELY-DUBAU??????
	DELT = 0.
	IF (J .EQ. 1) RETURN
	I = N-J+1
	IF (L .EQ. 12 .AND. I .NE. 10) GO TO 20
	IF (L .NE. 1 .OR. I .LE. 2) GO TO 50
	IF (I .EQ. 13) GO TO 50
   20   DELT = 1.
	GO TO 200
   50   CONTINUE
	IF (I .GE. 19) GO TO 19
	IF (I .EQ. 18) GO TO 18
	IF (I .GE. 15) GO TO 14
	GO TO (1,2,3,4,5,6,7,8,9,10,11,12,13,14) , I
    1   CONTINUE
C   BURGESS & TWORKOWSKI ARE IN ALPHADI
	IF (L .EQ. 1) DELT = 1.
	IF (L .GT. 1) DELT = 0.1
	IF (L .EQ. 5) DELT = 0.
	GO TO 200
    2   IF (L .EQ. 4 .OR. L .EQ. 5) DELT = 0.1
	IF (L .EQ. 9) DELT = 1.0
	GO TO 200
    3  IF (L .GE. 2 .AND. L .LE. 4) DELT = .5 * (-.35+1.26*N/(N+11.))
	GO TO 200
    4   IF (L .EQ. 7) DELT = AMAX1(0.5 * (-.55+1.50*N/(N+11.)), 0.05)
	GO TO 200
    5   IF (L .LE. 6) DELT = 1.
	IF (L .EQ. 4 .OR. L .EQ. 5) DELT = 0.5 * (-.54 + 2.2*N/(N+11.))
	GO TO 200
    6   IF (L .LE. 5) DELT = 1.
	IF (L .EQ. 4) DELT = AMAX1(0.5 * (-1.28+3.2*N/(N+11)), 0.05)
        GO TO 200
    7   IF (L .EQ. 4) DELT = 1.
	IF (L .EQ. 5) DELT = AMAX1(0.5*(-1.44+3.4*N/(N+11.)),0.05)
	IF (L .EQ. 7) DELT = AMAX1(0.25*(-1.44+3.4*N/(N+11.)), 0.025)
	GO TO 200
    8   IF (L .LE. 3) DELT = 0.5*(-1.60 + 3.6*N/(N+11.))
	IF (L .EQ. 4 .OR. L .EQ. 5) DELT = 1.
	IF (L .EQ. 7) DELT = .25 * (-1.6+3.6*N/(N+11.))
        IF (L .EQ. 15) DELT = 1.
	GO TO 200
    9   IF (L .LE. 3) DELT = 0.5 * (-1.75 + 3.8*N/(N+11.))
	IF (L .EQ. 5) DELT = 1.
	IF (L .EQ. 6) DELT = 0.25* (-1.75 + 3.8*N/(N+11.))
	GO TO 200
   10   IF (L .LE. 2) DELT = 1.
	IF (L .GE. 3 .AND. L .LE. 5) DELT = 0.5 * (-1.9+4.0*N/(N+11.))
	IF (L .GE. 6 .AND. L .LT. 8) DELT = .25*(-1.9+4.*N/(N+11.))
        IF (L .EQ. 14) DELT = 1.
	GO TO 200
   11   CONTINUE
	IF (L .EQ. 2 .OR. L .EQ. 3 .OR. L .EQ. 6) DELT = .25
	GO TO 200
   12   IF (L .EQ. 2) DELT = .25
	GO TO 200
   13   IF (L .EQ. 2 .OR. L .GE. 9) DELT = 1.
	IF (L .EQ. 3 .OR. L .EQ. 5) DELT = .5
	IF (L .EQ. 7) DELT = 0.25
        GO TO 200
   14   IF (L .LE. 2) DELT = 1.
	IF (L .EQ. 3 .OR. L .EQ. 4) DELT = .25
	GO TO 200
   18   DELT = 0.25
        IF (L .LE. 3) DELT = 1.
	IF (N .NE. 20) RETURN
	DELT = 0.
	IF (L .EQ. 6) DELT = 0.05
	GO TO 200
   19   DELT = 0.25
  200 CONTINUE
	RETURN
	END
