C*** Ver 2.0 jph@cfa   July 27, 1992
      FUNCTION ALPHADI(N,J,L,LN,T)
C  DIELECTRONIC RECOMBINATION : BURGESS AND TWORKOWSKI FOR H-LIKE
	COMMON/DAT/E(30),S(30),C(30),WAVE(220),E3(220),F(220),LL(30),
     1  SIG(30),ALF(30),ES(30)

C  BURGESS AND TWORKOWSKI
	Z12 = J-12.
	TWORK = .84 + .5/J**2 + .03*Z12/(1.+4.5E-5*Z12**3)
	IF (N .NE. J) TWORK = 1.
	ALPHADI = 0.
	DL = DELT(N,J,L)
	IF (DL .LE. 0.) RETURN
	ZF = J-1.
	B = SQRT(ZF)*(ZF+1.)**2.5/SQRT(ZF*ZF+13.4)
	X = E3(LN) / ((ZF+1.)*13.6)
	A = SQRT(X) / (1.+.105*X + .015*X*X)
	EBAR = E3(LN) / (1.+.015*ZF**3/(ZF+1.)**2)
      if (n-j .ne. 1) go to 25
      b = (0.5 * zf/sqrt(zf)) * b
      x = .75 * j
      a = sqrt(x) * 0.5 / (1.+0.21*x+0.03*x*x)
c  younger jqsrt 29, 67 for he-like ions
   25 continue

	ALPHADI = .0030 * T ** (-1.5) * A * B * F(LN) * TWORK * DL *
     1  EXP(-EBAR*11590./T)
	RETURN
	END
