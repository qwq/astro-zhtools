C*** Ver 2.0 jph@cfa   July 27, 1992
      FUNCTION GAMMA2(A,X)
      REAL*8 CAN1,CAN2,CBN1,CBN2,AN,BN,CAN,CBN
C     EXP(-X) * X ** A   TAKEN OUTSIDE
      CAN2 = 1.
      CAN1 = 0.
      CBN2 = 0.
      CBN1 = 1.
      AN = 1.
      BN = X
      N = 1
      CAN = BN * CAN1 + AN * CAN2
      CBN = BN * CBN1 + AN * CBN2
      FN1 = CAN/CBN
      DO 100 N = 2,1000
      IF (CAN .LE. 1.E30) GO TO 50
      CAN1 = CAN1 * 1.E-30
      CAN = CAN * 1.E-30
      CBN1 = CBN1 * 1.E-30
      CBN = CBN * 1.E-30
   50 CONTINUE
      IN = N
      CAN2 = CAN1
      CBN2 = CBN1
      CAN1 = CAN
      CBN1 = CBN
      MN = MOD(N,2)
      BN = MN * X + (1.-MN)
      NN = N/2
      AN = NN - (1.-MN) * A
      CAN = BN*CAN1 + AN*CAN2
      CBN = BN*CBN1 + AN*CBN2
      FN = CAN/CBN
      IF (ABS((FN-FN1)/FN1) .LE. .0001 .AND. N .GE. 20) GO TO 200
      FN2 = FN1
  100 FN1 = FN
      GAMMA2 = (FN+FN2) / 2.
      RETURN
  200 GAMMA2 = FN
      RETURN
      END
