C*** Ver 2.0 jph@cfa   July 27, 1992
	FUNCTION GNCRCH(R,L,Y,CC,N)
	DIMENSION R(1)
	IL = 8 * (L-1)
	A = R(IL+1) + R(IL+2) * N
	B = R(IL+3) + R(IL+4) * N
	C = R(IL+5) + R(IL+6) * N
	D = R(IL+7) + R(IL+8) * N
	GNCRCH = A + B*Y*CC + C*(Y-Y*Y*CC) + D*CC
	RETURN
	END
      FUNCTION GND(NUM)
      GND = 4.
      IF (NUM .LT. 28) GND = 3.
      IF (NUM .LT. 10) GND = 2.
      IF (NUM .LT. 2) GND = 1.
      RETURN
      END
      FUNCTION GREC(N,J,E,T)
      COMMON/DAT/V(30),EA(30),S2(30),WAVE(220),E3(220),F(220),
     1  LL(30),S3(30),S4(30),S5(30)
      DIMENSION DRAT(30)
      DATA DRAT/2.,.5,2.,.5,6.,2.5,2.22,2.25,.67,.167,2.,.5,6.,2.5,
     1  2.22,2.25,.67,.167,12*0./
      DEG = DRAT(N-J + 1)
      X1 = E*11590. / T
      X12 = X1*X1
      X14 = X12*X12
      G = 5.238E-23 * T ** 1.5 * DEG * (S2(J)*X12 + S4(J)*X12*X1 +
     1  X12*X1*S5(J)*(0.5-X1/2.)
     1 +EXINT1(X1,2) * (S3(J)*X12*X1-X14*S4(J)+X1*X14*S5(J)/2.))
      GREC = G
      if (X1 .ge. 300.) grec = 5.238e-23 * t ** 1.5 * deg * (s2(j)
     1  + s3(j) + s4(j) + s5(j)) * x12
      RETURN
      END
