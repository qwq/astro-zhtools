c MM 16 Feb 94: parameter (NSMAX=2000)
C*** Ver 2.0 jph@cfa   July 27, 1992


      FUNCTION ETWO(X)

      ETWO = EXP(-X) - X * EXINT1(X,1)
      END


      FUNCTION EXINT1(X,JUMP)

C     R. MOORE OCTOBER 1976
C   JUMP=1    EXINT1=E1(X)
C   JUMP=2    EXINT1=EXP(X)*E1(X)
C   JUMP=3    EXINT1=X*EXP(X)*E1(X)
      IF(X.GE.1.) GO TO   30
      EXINT1 =((((((((7.122452D-7*X-1.766345D-6)*X+2.928433D-5)*X
     1  -.0002335379D0)*X+.001664156D0)*X-.01041576D0)*X+.05555682D0)*X
     2  -.2500001D0)*X+.9999999D0)*X-LOG(X)-.57721566490153D0
      GO TO (9999,  10,  20),JUMP
   10 EXINT1=EXP(X)*EXINT1
      RETURN
   20 EXINT1=X*EXP(X)*EXINT1
      RETURN
   30 X2=X*X
      X3=X2*X
      X4=X3*X
      EXINT1=(X4+8.5733287401D0*X3+18.059016973D0*X2+8.6347608925D0*X
     1  +.2677737343D0) /(X4+9.5733223454D0*X3+25.6329561486D0*X2
     2  +21.0996530827D0*X+3.9584969228D0)
      GO TO (  40,  50,9999),JUMP
   40 EXINT1=EXINT1*EXP(-X)/X
      RETURN
   50 EXINT1=EXINT1/X

 9999 RETURN
      END


      FUNCTION EXPROD(I1,I2,KT)

      parameter (NSMAX=2000)
      REAL*4 KT
      DOUBLE PRECISION PROD,DEXP2
      COMMON/ENER/ENIN(0:NSMAX)

      E1 = ENIN(I1)
      E2 = ENIN(I2)
C      PROD=(1.D0-EXP((E1-E2)/KT))
      PROD = 1.D0-DEXP2(DBLE((E1-E2)/KT))
      IF((E2-E1)/KT .LT. 1.E-5) PROD=(E2-E1)/KT-.5*((E2-E1)/KT)**2
C      EXPROD=PROD*EXP(-E1/KT)
      EXTEMP = EXP2(-E1/KT)
      EXPROD = 0.0
      IF(EXTEMP .GT. 0.)THEN
        IF ( PROD .GE. 1.E-35/EXTEMP ) EXPROD = PROD*EXTEMP
C        IF ( PROD .GE. 1.E-30/EXTEMP ) EXPROD = PROD*EXTEMP
      ENDIF

      END


      FUNCTION EXP2(Y)

C*** Exponential function to avoid underflows
      EXP2 = 0.
      IF(Y .GT. -69.0776)EXP2 = EXP(Y)
C      IF(Y .GT. -46.0517)EXP2 = EXP(Y)

      END


      FUNCTION DEXP2(Y)

      DOUBLE PRECISION DEXP2,Y
C*** Exponential function to avoid underflows
      DEXP2 = 0.D0
      IF(Y .GT. -69.0776D0)DEXP2 = DEXP(Y)

      END

