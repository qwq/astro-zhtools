c MM 4.4.96: abundances are now set by call to set_abunds
c M.Markevitch, 16 Feb 1994

      subroutine RAYM (T, abun, z, ns, e_bin, spect)

c Interface to the Raymond-Smith/Jack Hughes code. The original Jack Hughes'
c code is in the subdirectory JPH.  
c 
c Abundances are set in set_abunds, which can be called prior to this
c function with either "allen" or "xspec" arg to override default (one of
c these).
c 
c Atomic data filename is set in this subroutine (parameter datafile).
c 
c Input:
c 
c real T -- temperature, keV;
c real abun -- relative heavy element abundance (see note above);
c real z -- redshift;
c integer ns -- number of the spectrum bins, must be Ns.le.NSMAX;
c real e_bin(0:ns) -- energy bin boundaries (note the dimension), receiver
c frame, keV
c
c Output:
c 
c real spect(ns) -- spectrum in units of photon*cm**3/s (into 4pi angle) in a
c given e_bin interval (energy boundaries are in the receiver frame). Flux is
c given in emitter frame. It should be multiplied by EM=n_e*n_p*V  to get the
c luminosity.               NOTE THE DEFINITION OF EM~~~~~~~~~~~~


      character*128 datafile
      data datafile / "/data/alexey3/zhtools-2.1/caldata/atomic.dat" /
      parameter (NSMAX=2000)
      real absave(0:12), e_bin(0:NSMAX), spect(NSMAX)
      logical firstime
      common /COM/    cnc(12,30), bin(NSMAX)
      common /PARAMS/ nj(12), abunj(0:12), abund, nbin
      common /ENER/   enin(0:NSMAX)
      common /absv/ absave
      data firstime /.true./


      if (ns.le.0.or.ns.gt.NSMAX) then
        print'(" raym: spectrum dimension <1 or >",i5)',NSMAX
        return
      end if

c read atomic data:
      if (firstime) then
        ifi=newunit()
        open(ifi,file=datafile,status="old")

        call ATREAD (ifi,12)

        close(ifi)
        firstime=.false.
      end if 


      call set_abunds ("default")
c (always call it with "default" from here; user may call with other args from
c outside. change default setting in that subroutine.)

c set up abundances, H/He fixed (note that JPH also fixes C):
      abunj(0)=absave(0)
      abunj(1)=absave(1)
      do i=2,12
        abunj(i)=alog10(max(abun,1e-12))+absave(i)
      end do


c set up energy bins for raymond code:
      nbin=ns
      do i=0,nbin
        enin(i)=e_bin(i)*1000.*(1.+z)
      enddo


c run raymond code:
      call XSPSUB (T,0,0,0,0,0,0)


c rewrite output:
      do i=1,nbin
        spect(i)=bin(i)/1.e23
      end do        


      end
