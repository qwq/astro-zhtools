      subroutine showerror (unit,status,message,file)
      implicit none
      integer unit,status
      character message*(*),file*(*)
      character errormessage*60
      integer lnblnk

      write(unit,*)message(1:lnblnk(message)),' ',file(1:lnblnk(file)),':'
      call ftgerr(status,errormessage)
      write(0,*)'   ',errormessage
 
      return
      end
      
