      implicit none
      
      integer nechan
      parameter (nechan=729)
      real ebin(0:nechan)
      real elow(nechan), ehigh(nechan)

      integer nspchanmax
      parameter (nspchanmax=34)
      
      integer ndatasetmax,ndataset
      parameter (ndatasetmax=20)
      
      real matrix  (nspchanmax,nechan,ndatasetmax)
      real effarea (nechan,ndatasetmax)
      
      real data(nspchanmax,ndatasetmax)
      real error(nspchanmax,ndatasetmax)
      logical usechan(nspchanmax,ndatasetmax)
      real syserr(nspchanmax,ndatasetmax)
      real sys
      integer nspchan(ndatasetmax)
      real exposure

      integer ignore(1000),nignore
      character cignore(1000)*10
      
      common /cdata/
     ~    ebin, matrix, effarea, data, error, usechan, syserr, nspchan,
     ~    ndataset
      
      character parfile*80
      logical defined
      character parname*80,parvalue*80
      integer lnblnk
      character dataname(100)*80
      integer ndata
      
      double precision p(200), pp(200)
      integer pfit(200)
      double precision punit(200)
      common /cpar/ p,punit,pfit
      
      double precision ftol,dp
      integer iret,maxit,npar
      
      character amode*80
      common /camode/ amode
      

      real nh,T,abund,z
      integer fitnh,fitT,fitabund,fitz
      double precision ch2_T,ch2_nH,ch2_T_err, ch2_nh_err, ch2_a
      external ch2_T,ch2_nH,ch2_T_err, ch2_nh_err, ch2_a
      real chi2thresh
      common /cchi2/ chi2thresh
      double precision zeroin
      real tpos,tneg,nhpos,nhneg

      
      real deltaT,deltanh,told,nhold
      double precision tmin,tmax,nhmin,nhmax
*     double precision fmin
      double precision stepT,stepnh
*     real x
      real chi2min
      integer ndof
      
      integer i,j

c parameter file name
      call get_command_line_par ('pfile',1,parfile)
      if (.not.defined(parfile)) then
        parfile='fitspec.par'
      endif
      write(0,*)'parameter file:  ',parfile(1:lnblnk(parfile))
c FIT parameters
      parname = 'nh'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          write(0,*)'Please set ',parname(1:lnblnk(parname))
          call exit(1)
        endif
      endif
      read (parvalue,*) nh
      
*      if (nh.gt.1e10) then      ! it is in absolute units, convert to 1e20
*        nh=nh/1.0e20
*      endif
      
      parname = 'fitnh'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          fitnh=1
        else
          if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
            fitnh=1
          else
            fitnh=0
          endif
        endif
      else
        if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
          fitnh=1
        else
          fitnh=0
        endif
      endif
      
      parname = 'T'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          write(0,*)'Please set ',parname(1:lnblnk(parname))
          call exit(1)
        endif
      endif
      read (parvalue,*) T
      
      parname = 'fitT'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          fitT=1
        else
          if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
            fitT=1
          else
            fitT=0
          endif
        endif
      else
        if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
          fitT=1
        else
          fitT=0
        endif
      endif
      
      parname = 'abund'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          write(0,*)'Please set ',parname(1:lnblnk(parname))
          call exit(1)
        endif
      endif
      read (parvalue,*) abund
      
      parname = 'fitabund'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          fitabund=0
        else
          if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
            fitabund=1
          else
            fitabund=0
          endif
        endif
      endif
 
      
      parname = 'z'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          write(0,*)'Please set ',parname(1:lnblnk(parname))
          call exit(1)
        endif
      endif
      read (parvalue,*) z

      
      parname = 'fitz'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          fitz=0
        else
          if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
            fitz=1
          else
            fitz=0
          endif
        endif
      endif
 
      write(0,*) ' Parameter      start value        fit it'
      write(0,'("   Nh            ",f7.2,14x,i1)') nh,fitnh
      write(0,'("   T             ",f7.2,14x,i1)') T,fitT
      write(0,'("   abund         ",f7.2,14x,i1)') abund,fitabund
      write(0,'("   z             ",f7.3,14x,i1)') z,fitz


c Abund mode
      parname = 'amode'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          amode="allen"
        else
          amode=parvalue
        endif
      else
        amode=parvalue
      endif
      
c Sys error
      parname = 'syserr'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
        if (.not.defined(parvalue)) then
          sys=0.0
        else
          read (parvalue,*) sys
        endif
      else
        read (parvalue,*)sys
      endif
      



c DATA
      parname='data'
      call g_seq_par (parname,parfile,dataname,ndata)
      if (ndata.eq.0) then
        write(0,*) 'You must set datasets'
        call exit(1)
      else
        ndataset=ndata
      endif


      write(0,*)'--------------------------------------'
      write(0,*)'Datasets'
      do i=1,ndataset
        write(0,'(i2,3x,a)')i,dataname(i)(1:lnblnk(dataname(i)))
      enddo

c IGNORE
      parname='ignore'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        call get_parameter_file_par (parfile,parname,1,parvalue)
      endif
      if (.not.defined(parvalue)) then
        nignore=0
      else
        write(0,*)'will ignore channels:'
        nignore=lnblnk(parvalue)
        do i=1,nignore
          if (parvalue(i:i).eq.',') parvalue(i:i)=' '
        enddo
        call splitwords(parvalue,cignore,nignore)
        do i=1,nignore
          read (cignore(i),*)ignore(i)
          write(0,*)ignore(i)
        enddo
        write(0,*)
        write(0,*)
      endif
      
c read data
      do i=1,ndataset
        call read_data (dataname(i),data(1,i),error(1,i),effarea(1,i)
     ~      ,matrix(1,1,i),nspchan(i),elow,ehigh,nechan,nspchanmax,exposure)
        if (exposure.gt.0.0) then
          do j=1,nspchan(i)
            usechan(j,i)=.true.
            syserr(j,i)=sys
          enddo
        else
          do j=1,nspchan(i)
            usechan(j,i)=.false.
            syserr(j,i)=sys
          enddo
        endif
        
        do j=1,nignore
          usechan(ignore(j),i)=.false.
        enddo
      enddo
      
      ebin(0)=elow(1)
      do i=1,nechan
        ebin(i)=ehigh(i)
      enddo



c fit it
      punit(1)=1
      p(1)=alog10(nh)
      pfit(1)=fitnh

      punit(2)=1
      p(2)=T
      pfit(2)=fitT

      punit(3)=abund
      p(3)=1.0
      pfit(3)=fitabund
      
      punit(4)=z
      p(4)=1.0
      pfit(4)=fitz

      dp=0.5
*      call set_abunds("xspec")

      deltat=1.0
      deltanh=1.0
      
      stepT=1.5
      stepnh=0.5
      told=t
      nhold=p(1)
      ftol=1.0e-6
      tmin=T/stepT
      tmax=T*stepT
      nhmin=p(1)-stepnh
      nhmax=p(1)+stepnh
      Tmin=max(0.1d0,Tmin)
      Tmax=min(25.0d0,Tmax)
      nhmin=max(-1.0d0,nhmin)
      nhmax=min(2.0d0,nhmax)

* 1    continue
*        if (fitnh.eq.1) then
*          x=fmin(nhmin,nhmax,ch2_nh,1.0d-5)
*          p(1)=x
*        endif
*        if (fitT.eq.1) then
*          x=fmin(Tmin,Tmax,ch2_T,1.0d-5)
*          p(2)=x
*        endif
*        
*        deltanh=p(1)-nhold
*        deltaT=p(2)-Told
*        
*        nhold=p(1)
*        Told=p(2)
*
*        if (fitnh.eq.1.and.fitT.eq.1) then
*                                ! we fit both parameters, need repeated steps
*          if (sqrt((deltanh)**2+deltat**2).gt.ftol) then
*            stepnh=2.5*abs(deltanh)
*            stepT=2.5*(max(Told/(Told-deltaT),(Told-deltaT)/Told)-1)+1
*            nhmin=nhold-stepnh
*            nhmax=nhold+stepnh
*            nhmin=max(-1.0d0,nhmin)
*            nhmax=min(2.0d0,nhmax)
*            Tmin=Told/stepT
*            Tmax=Told*stepT
*            Tmin=max(0.1d0,Tmin)
*            Tmax=min(25.0d0,Tmax)
*            goto 1
*          endif
*        endif
*        
*      continue
      npar=4
      maxit=5000
      do i=1,npar
        pp(i)=p(i)
      enddo
      call amoebf8(pp,pfit,dp,npar,ftol,maxit,ch2_a,iret)
      do i=1,npar
        p(i)=pp(i)
      enddo
      
      chi2min=ch2_a(pp,npar)
      
      Nh=10.0**(p(1)*punit(1))
      T=p(2)*punit(2)
      
      write(0,*) 'OK, best fit T, Nh:',T,Nh,chi2min

      chi2thresh=chi2min+2.706
      
      if (fitt.eq.1) then
        p(1)=alog10(nh)/punit(1)
        p(2)=T/punit(2)
        Tmin=T
        Tmax=25.0
        Tpos=zeroin(Tmin,Tmax,ch2_T_err,1.0d-2)
        write(0,*) '     Tpos:',Tpos
        Tmin=0.1
        Tmax=T
        Tneg=zeroin(Tmin,Tmax,ch2_T_err,1.0d-2)
        write(0,*) '     Tneg:',Tneg
      endif

      if (fitnh.eq.1) then
        p(1)=alog10(nh)/punit(1)
        p(2)=T/punit(2)
        nhmin=alog10(nh)/punit(2)
        nhmax=2.90
        nhpos=zeroin(nhmin,nhmax,ch2_nh_err,1.0d-2)
        nhpos=10.0**(nhpos*punit(1))
        write(0,*) '     Nhpos:',Nhpos
        nhmin=-1
        nhmax=alog10(nh)/punit(2)
        nhneg=zeroin(nhmin,nhmax,ch2_nh_err,1.0d-4)
        nhneg=10.0**(nhneg*punit(1))
        write(0,*) '     Nhneg:',Nhneg
      endif

      ndof=0
      do i=1,ndataset
        do j=1,nspchanmax
          if (usechan(j,i)) ndof=ndof+1
        enddo
        ndof=ndof-1
      enddo
      if (fitnh.eq.1) then
        ndof=ndof-1
      endif
      if (fitT.eq.1) then
        ndof=ndof-1
      endif
      

      write(0,*)' Nh    Nhmin Nhmax         T    Tmin  Tmax        chi^2'


      Nh = max(Nh,0.0)
      print '(f6.2,2x,f6.2,1x,f6.2,7x,f5.2,2x,f5.2,1x,f5.2,6x,f7.2,1x,f7.2)'
     ~    ,Nh,Nhneg,Nhpos,T,Tneg,Tpos,chi2min,chi2min/ndof

      call exit(0)
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_T_err (x)
      implicit none
      double precision ch2_T_err,x,ch2
      double precision p(200)
      double precision punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      real chi2thresh
      common /cchi2/ chi2thresh
      double precision ch2_nh,fmin
      external ch2_nh
       
      p(2)=x
      if (pfit(1).eq.1) then
        p(1)=fmin(-1.0d0,2.0d0,ch2_nh,1.0d-5)
      endif
      ch2_T_err=ch2()-chi2thresh
      return
      end
      
      function ch2_nh_err (x)
      implicit none
      double precision ch2_nh_err,x,ch2
      double precision p(200)
      double precision punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      real chi2thresh
      common /cchi2/ chi2thresh
      double precision ch2_T,fmin
      external ch2_T
       
      p(1)=x
      if (pfit(2).eq.1) then
        p(2)=fmin(0.1d0,25.0d0,ch2_T,1.0d-5)
      endif
      ch2_nh_err=ch2()-chi2thresh
      return
      end
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_T (x)
      implicit none
      double precision x
      double precision ch2_T,ch2
      double precision p(200)
      double precision punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
       
      p(2)=x
      ch2_T=ch2()
      return
      end
      
      function ch2_nh (x)
      implicit none
      double precision x
      double precision ch2_nh,ch2
      double precision p(200)
      double precision punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
       
      p(1)=x
      ch2_nh=ch2()
      return
      end
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_a (pp,n)
      implicit none
      double precision ch2_a,pp(*)
      integer n
      double precision ch2
      double precision p(200)
      double precision punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      integer i

      do i=1,n
        p(i)=pp(i)
      enddo
      ch2_a=ch2()
      return
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      function ch2()
      implicit none
      double precision ch2
      double precision p(200)
      double precision punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      character amode*80
      common /camode/ amode

      integer nechan
      parameter (nechan=729)
      real ebin(0:nechan)
      integer nspchanmax
      parameter (nspchanmax=34)
      
      integer ndatasetmax,ndataset
      parameter (ndatasetmax=20)
      
      real matrix  (nspchanmax,nechan,ndatasetmax)
      real effarea (nechan,ndatasetmax)
      
      real data(nspchanmax,ndatasetmax)
      real error(nspchanmax,ndatasetmax)
      logical usechan(nspchanmax,ndatasetmax)
      real syserr(nspchanmax,ndatasetmax)
      integer nspchan(ndatasetmax)

      common /cdata/
     ~    ebin, matrix, effarea, data, error, usechan, syserr, nspchan,
     ~    ndataset
 

      real nH, T, a, z
      real modelspec(nechan)
      real model(nspchanmax)
      real Told,aold,zold
      real rsmodelspec(nechan)
      data Told/-1.0/
      real ee,absorp,sigism
      real sum,ch2_1,www
      logical firstcall
      data firstcall /.true./
      real rsspec(0:729,-1:100)
      real Tgrid(100)
      real w1,w2
      integer ibin
      integer i,idata
      integer lnblnk,status

      save firstcall,aold,zold,Told,Tgrid,rsspec,ibin,rsmodelspec
      
      nH=10.0**(p(1)*punit(1))
      T=p(2)*punit(2)
      a=p(3)*punit(3)
      z=p(4)*punit(4)
      
      if (nH.lt.0.0) then
        ch2=1.0e10
        return
      endif
      
      if (nH.lt.0.0) then
        ch2=1.0e10
        return
      endif
      
      
      if (T.gt.25.0.or.T.lt.0.2) then
        ch2=1.0e10
        return
      endif
       
      if (a.lt.0.0) then
        ch2=1.0e10
        return
      endif
        
      if (z.lt.0.0) then
        ch2=1.0e10
        return
      endif

      if (firstcall) then
        write(0,*)'initialise RS grid for ',amode(1:lnblnk(amode)),a,z
        call read_rs_grid (amode,a,z,rsspec,100,status)
        if (status.ne.0) then
          write(0,*)' RS grid does not exist for ',amode(1:lnblnk(amode)),a,z
          write(0,*)' I''ll try to generate it'
          call make_rs_grid(amode,a,z)
          call read_rs_grid (amode,a,z,rsspec,100,status)
          if (status.ne.0) then
            write(0,*)'General problems with RS grid for '
     ~          ,amode(1:lnblnk(amode)),a,z
            call exit(1)
          endif
        endif
        write(0,*)'  ... ok'
        firstcall=.false.
        aold=a
        zold=z
        do i=1,100
          Tgrid(i)=rsspec(0,i)
        enddo
        write(0,*)'done'
      endif
        
      if ( (a.ne.aold) .or. (z.ne.zold)) then
        call exiterror('only allow fixed a and z')
      endif

      if ( (T.ne.Told) ) then
        Told=T
        call locate (Tgrid,100,T,ibin)
        if (ibin.lt.1.or.ibin.ge.100) then
          ch2=1.0e10
          return
        endif
        w1=(Tgrid(ibin+1)-T)/(Tgrid(ibin+1)-Tgrid(ibin))
        w2=1.0-w2
        call veclinint(w1,w2,rsspec(1,ibin),rsspec(1,ibin+1),rsmodelspec,729,
     ~      rsspec(1,-1),rsspec(1,0))
      endif
      
      do i=1,nechan
        ee=500.0*(ebin(i-1)+ebin(i))
        absorp=sigism(ee)*nH*1.0e20
        if (absorp.gt.-50.0) then
          absorp=exp(-absorp)
        else
          absorp=0.0
        endif
        modelspec(i)=rsmodelspec(i)*absorp
      enddo
      
      sum=0
      do idata=1,ndataset
        
        call conv_matr (model,modelspec,effarea(1,idata),matrix(1,1,idata),
     ~      nspchan(idata),nechan)
        
        www = ch2_1(model,data(1,idata),error(1,idata),syserr(1,idata)
     ~      ,usechan(1,idata),nspchan)
        sum = sum + www
      enddo
      
      ch2=sum
      
      
      return
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine veclinint(w1,w2,v1,v2,r,n,e1,e2)
      implicit none
      real w1,w2
      integer n
      real v1(n),v2(n),r(n),e1(n),e2(n)
      integer i
      
      do i=1,n
        r(i)=(w1*v1(i)+w2*v2(i))*(e2(i)-e1(i))
      enddo
      return
      end
       

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_1 (model,data,err,syserr,usechan,n)
      implicit none
      real ch2_1
      real model(*),data(*),err(*),syserr(*)
      logical usechan(*)
      integer n
      
      real rr,ss,norm,sum
      integer i
      
      rr=0
      ss=0
      do i=1,n
        if (usechan(i)) then
*          print*,model(i),data(i),err(i),syserr(i)
          ss=ss+model(i)*data(i)/(err(i)**2+(data(i)*syserr(i))**2)
          rr=rr+model(i)**2/(err(i)**2+(data(i)*syserr(i))**2)
        endif
      enddo
      
      norm=ss/rr
      
      sum=0
      do i=1,n
        if (usechan(i)) then
          sum=sum+
     ~        (norm*model(i)-data(i))**2/(err(i)**2+(data(i)*syserr(i))**2)
        endif
      enddo
       
      ch2_1=sum
      return
      end


*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine conv_matr (model,modelspec,effarea,matrix,nspchan,nechan)
      implicit none
      integer nspchan,nechan
      real model(nspchan)
      real modelspec (nechan)
      real effarea(nechan)
      integer nspchanmax
      parameter (nspchanmax=34)
      real matrix(nspchanmax,nechan)
      
      real flux
      integer ich,ie
      real sum

      do ich=1,nspchan
        model(ich)=0.0
      enddo
      
      do ie=1,nechan
        flux=modelspec(ie)*effarea(ie)
        do ich=1,nspchan
*          print*,ie,ich,modelspec(ie),effarea(ie),matrix(ich,ie)
          model(ich)=model(ich)+flux*matrix(ich,ie)
          sum=sum+matrix(ich,ie)
        enddo
      enddo
      return
      end
      




