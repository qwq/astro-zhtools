      function rosat_pspc_psf (offsource,offaxis,e,we,ne)
      implicit none
c=====================================================================
c
c Rosat PSPC PSF at 1 keV.
c
c rosat_pspc_psf  - real - PSF intensity (1/arcsec**2)
c offsource       - real - off-source distance (arcsec)
c offaxis         - real - off-axis angele (arcmin)
c e(ne)           - real - energy array
c we(ne)          - real - relative weights of energy bins
c      
      real rosat_pspc_psf
      real offsource,offaxis
      integer ne
      real e(ne),we(ne)

      real weight,sum,rosat_pspc_psf_0
      integer i
      
      
      weight=0.0
      sum=0.0
      do i=1,ne
        sum=sum+we(i)*rosat_pspc_psf_0 (offsource,offaxis,e(i))
        weight=weight+we(i)
      enddo
      
      rosat_pspc_psf=sum/weight
      return
      end
      
      
      
      
      function rosat_pspc_psf_0 (offsource, offaxis,e)
c=====================================================================
c
c Rosat PSPC PSF at 1 keV.
c
c pspcpsf1  - real - PSF intensity (1/arcsec**2)
c offsource - real - off-source distance (arcsec)
c offaxis   - real - off-axis angle (arcmin)
c e         - real - energy (keV)
c
c
c
c
      implicit none
      
      real rosat_pspc_psf_0, offsource, offaxis,e
      
      integer ntheta
      parameter (ntheta=64)
      integer npsf,itheta
      parameter (npsf=100)
      real offsrc(npsf)
      integer ne,ie
      parameter (ne=34)
      real e0(34)
      
      real psf0(npsf,0:ntheta,ne)
      
      logical firstcall
      data firstcall /.true./
      character psffile*200
      real offsrcold
      data offsrcold /-1.0/

      save firstcall,offsrcold,offsrc,e0,psf0,ipsf
      
      integer i,ipsf
      real www
      integer unit,newunit
      
      if (firstcall) then
        firstcall=.false.
        psffile='caldata/rosat/psfdata/pspcpsf1.data'
        call dataenv(psffile,'ZHTOOLS')
        unit = newunit()
        open(unit,file=psffile,form='unformatted',status='old')
        read(unit)offsrc
        read(unit)e0
        read(unit)psf0
        close(unit)
      endif
      
      
      
      itheta=anint(offaxis)
      if(itheta.gt.64) itheta=64
      
      
      if (offsource.ne.offsrcold) then
        offsrcold=offsource
        if (offsource.le.0.) then
          ipsf=1
        else
          call locate(offsrc,npsf,offsource,ipsf)
          if (ipsf.le.0.or.ipsf.ge.npsf) then
            rosat_pspc_psf_0=0.0
            return
          endif
        endif
      endif
      
      ie=0
      www=1.0e10
      do i=1,ne
        if (abs(e-e0(i)).lt.www) then
          ie=i
          www=abs(e-e0(i))
        endif
      enddo
      
      rosat_pspc_psf_0=psf0(ipsf,itheta,ie)
      
      return
      end











