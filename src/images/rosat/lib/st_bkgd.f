      subroutine st_bkgd (theta,nh,spectrum,status)
      implicit none
      real theta,nh,spectrum(34)
      
      integer status
      
      integer ntheta,itheta
      parameter (ntheta=25)
      integer counts(34,ntheta)
      double precision exposure(ntheta)
      
      double precision texp
      integer cnt(34)
      
      
      integer seq(1000),n,nn
      real nh0(1000)
      
      integer lnblnk
      character filename*200 
      character path*200
      logical firstcall
      data firstcall /.true./

      integer i,j, unit,newunit

      save firstcall,n,seq,nh0

      
C      call getenv('ZHTOOLS',path)
C      path=path(1:lnblnk(path))//'/caldata/rosat/bkgd.'
      path='/caldata/rosat/bkgd.'
      call dataenv(path,'ZHTOOLS')

      if (firstcall) then
        filename=path(1:lnblnk(path))//'list.bin'
        print*,filename
        unit = newunit()
        open(unit,file=filename,status='old',form='unformatted',iostat=status)
        if (status.ne.0) then
          status=10
          close(unit)
          return
        endif
        
        read(unit)n
        call read_i4(seq,n,unit)
        call read_r4(nh0,n,unit)
        close(unit)
        firstcall=.false.
      endif
        
      nn=0
      itheta=nint(theta)+1
      if (itheta.gt.25) then
        status=8
        return
      endif
      
      if (itheta.le.3) then
        itheta=4
      endif
      
      filename=path(1:lnblnk(path))//'dat'
      open(unit,file=filename,status='old',form='unformatted',iostat=status)
      if (status.ne.0) then
        status=9
        close(unit)
        return
      endif
      
      do i=1,34
        cnt(i)=0
      enddo
      texp=0.0d0
      
      do i=1,n
        if (nh/nh0(i).gt.0.75.and.nh/nh0(i).lt.1.25) then
          nn=nn+1
          read(unit)counts
          read(unit)exposure
          do j=1,34
            cnt(j)=cnt(j)+counts(j,itheta)
          enddo
          texp=texp+exposure(itheta)
        else
          read(unit)
          read(unit)
        endif
      enddo
      
      if (nn.eq.0) then
        status=1
        return
      endif
      
      do i=1,34
        spectrum(i)=cnt(i)/texp
      enddo
      
      status=0
      return
      end
      




