      function pi_to_sass (pi)
      implicit none
      integer pi_to_sass,pi
      
      integer pi10(34),pi20(34)
      data pi10 /7, 9, 11, 14, 17, 20, 24, 28, 32, 37, 42, 47, 52, 58, 64, 70,
     ~    77, 84, 91, 99, 107, 115, 123, 132, 141, 150, 160, 170, 180, 191,
     ~    202, 213, 224, 236/
      
      data pi20 /8, 10, 13, 16, 19, 23, 27, 31, 36, 41, 46, 51, 57, 63, 69, 76
     ~    , 83, 90, 98, 106, 114, 122, 131, 140, 149, 159, 169, 180, 190, 201,
     ~    212, 223, 235, 247/


      save pi10,pi20
      integer i
      
      
      do i=1,34
        if (pi.ge.pi10(i).and.pi.le.pi20(i)) then
          pi_to_sass=i
          return
        endif
      enddo
      
      pi_to_sass=0
      return
      end
      
