      subroutine cleandata
      implicit none
      integer lnblnk
      character arg*80
      integer rseq
      character data*200,root*200,out*200
      character events*200,mex*200
      
      
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2
       

      
c image arrays
      integer nd
      parameter (nd=512)
      real texp(nd,nd)
      integer*2 img(nd,nd)
      
      integer nn
      parameter (nn=128)
      real im0(nn,nn)
      real im1(nn,nn)
      real im2(nn,nn)
      real im20(nn,nn)
      real ccf(nn,nn)
      
      
c photon arrays
      integer nphotmax,nphot
      parameter (nphotmax=2000000)
      integer*2 xpix(nphotmax),ypix(nphotmax)
      character pi(nphotmax)*1,igti(nphotmax)*1
      integer*2 pi0
      double precision time
      integer xmin,xmax,ymin,ymax
      parameter (
     ~    xmin=3840,
     ~    xmax=11520,
     ~    ymin=3840,
     ~    ymax=11520
     ~    )
      
      
c GTI 
      integer ngti
      double precision tstart(1000),tend(1000)
      integer gtiqual(1000)
      double precision bcgrate(1000)
      integer ugti(1000)
      
      
c units
      integer iunit,ounit,unitmex
      parameter (iunit=1,ounit=2,unitmex=3)

c FITSIO
      integer colnum
      logical anyf
      character extname*80
      
      integer length,nrows
      character ttype(300)*80,tform(300)*80,tunit(300)*80
      integer tfields
      integer varidat
      integer ftime,fx,fy,fpi
      integer ifield,irow
      integer blocksize
      character comment*80
      integer pcount,gcount,bitpix
      integer naxis,naxes(2)
      
      
      
c various
      integer status,i,j,ii,jj,ig,iph,iter,i0,j0
      real immax
      integer iphot
c-----------------------------------------------------------

c 
c
c    set file names
c
c   
      call getarg(2,arg)

      data = arg
      events=data(1:lnblnk(data))//'.fits'
      mex=data(1:lnblnk(data))//'_mex.fits'
      
      root = arg
      out=root(1:lnblnk(root))//'.events.fits'
      
      
      
c
c
c    open events file; 
c    read GTI extension
c    form xpix,ypix,pi,and igti arrays
c
c
c
      status=0
      call ftopen(iunit,events,0,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',events(1:lnblnk(data))
        call exit(1)
      endif
      
      
c Read GTI
      call move_ext (iunit,'GTI',status)
      if (status.ne.0) then
        write(0,*) 'Cannot find GTI extension; exit'
        call exit(1)
      endif

      call ftgkyj(iunit,'NAXIS2',ngti,comment,status) ! NGTI
      if (status.ne.0) then
        call fitsio_write_error(status,0)
        call exit(1)
      endif

      call ftgcno (iunit,.false.,'START',colnum,status)
      call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tstart,anyf,status)
      
      call ftgcno (iunit,.false.,'STOP',colnum,status)
      call ftgcvd (iunit,colnum,1,1,ngti,0.0d0,tend,anyf,status)
      
      if (status.ne.0) then
        write(0,*)'error reading GTIs'
        call exit(1)
      endif
      
      do i=1,ngti
        if (tend(i)-tstart(i).gt.200.0) then
          gtiqual(i)=0
        else
          gtiqual(i)=1
        endif
      enddo
        
      
c read EVENTS
      call move_ext (iunit,'EVENTS',status)
      if (status.ne.0) then
        write(0,*) 'Cannot find EVENTS extension; exit'
        call exit(1)
      endif
      
      call ftgkyj (iunit,'NAXIS1',length,comment,status)
      call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~    ,status)
      

c necessary byte positions
      ftime=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'time'.and.ttype(ifield).ne.'TIME') then
          if (tform(ifield).eq.'1I') then
            ftime=ftime+2
          else if (tform(ifield).eq.'1J') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1E') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1D') then
            ftime=ftime+8
          else
            write(0,*) 'unknown format:',tform(ifield)(1:lnblnk(tform(ifield
     ~          )))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1D') then
            write (0,*) 'TIME field format: assume 1D, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 20
        endif
      enddo
       call exiterror('TIME column not found')
 20   continue
        
      fx=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'x'.and.ttype(ifield).ne.'X') then
          if (tform(ifield).eq.'1I') then
            fx=fx+2
          else if (tform(ifield).eq.'1J') then
            fx=fx+4
          else if (tform(ifield).eq.'1E') then
            fx=fx+4
          else if (tform(ifield).eq.'1D') then
            fx=fx+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'X field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 21
        endif
      enddo
       call exiterror('X column not found')
 21   continue
      
      fy=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'y'.and.ttype(ifield).ne.'Y') then
          if (tform(ifield).eq.'1I') then
            fy=fy+2
          else if (tform(ifield).eq.'1J') then
            fy=fy+4
          else if (tform(ifield).eq.'1E') then
            fy=fy+4
          else if (tform(ifield).eq.'1D') then
            fy=fy+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'Y field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 22
        endif
      enddo
       call exiterror('Y column not found')
 22   continue
      
      fpi=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'pi'.and.ttype(ifield).ne.'PI') then
          if (tform(ifield).eq.'1I') then
            fpi=fpi+2
          else if (tform(ifield).eq.'1J') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1E') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1D') then
            fpi=fpi+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'PI field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 23
        endif
      enddo
       call exiterror('PI column not found')
 23   continue
      
      print*,'read EVENTS'
      do irow=1,nrows
        call ftgtbb (iunit,irow,fx,2,xpix(irow),status)
        call ftgtbb (iunit,irow,fy,2,ypix(irow),status)
        call ftgtbb (iunit,irow,fpi,2,pi0,status)
        call ftgtbb (iunit,irow,ftime,8,time,status)
        pi(irow)=char(pi0)
        do i=1,ngti
          if (time.ge.tstart(i).and.time.le.tend(i)) then
            igti(irow)=char(i)
            goto 30
          endif
        enddo
 30     continue
        if ((irow/50000)*50000.eq.irow) print '(i7,'' of '',i7)',irow,nrows
      enddo
      
      nphot=nrows
      
c
c
c read exposure map
c convolve it with the square window to avoid edge effects
c
c      
      call open_fits (unitmex,mex,status)
      if (status.ne.0) then
         call exiterror('error opening exposure map')
      endif
      call g_image_r(unitmex, 'PRIMARY', texp, nd, nd, status)
      if (status.ne.0) then
         call exiterror('error reading exposure map')
      endif
      
      call ftclos(unitmex,status)
      
      call conv_rect1 (texp,nd,nd,5,5)
      
c
c      
c look over GTIs;       
c
c 1) find GTI with unreliable aspect solution: image != 0 while exposure = 0
c
c
      
      do ig=1,ngti
        
        do i=1,nd
          do j=1,nd
            img(i,j)=0
          enddo
        enddo
        
        do iph=1,nphot
          if (ichar(igti(iph)).eq.ig) then
            ii=xpix(iph)/30+1
            jj=ypix(iph)/30+1
            if (ii.ge.1.and.ii.le.nd.and.jj.ge.1.and.jj.le.nd) then
              img(ii,jj)=img(ii,jj)+1
            endif
          endif
        enddo
        
        do i=1,nd
          do j=1,nd
            if (texp(i,j).lt.0.001) then
              if (img(i,j).ne.0) then
                gtiqual(ig)=10000
*                call show_r4 (texp,nd,nd)
*                call show_i2 (img,nd,nd)
                print*,'GTI:',ig,' is bad !!! -- img!=0 && exposure==0'
                goto 40
              endif
            endif
          enddo
        enddo
        
 40     continue
        
      enddo
      
c      
c check CCF      
c
c form total image
c look over GTIs
c
c     
      
      do iter=1,2
        
        do i=1,nn
          do j=1,nn
            im0(i,j)=0
          enddo
        enddo
        
        print*,'form total image'
        do ig=1,ngti
          print*,ig,ngti
          if (gtiqual(ig).eq.0) then
            
            do iph=1,nphot
              if (ichar(igti(iph)).eq.ig) then
                pi0=ichar(pi(iph))
                if (pi0.ge.20) then
                  ii=xpix(iph)/30-192
                  jj=ypix(iph)/30-192
                  if (ii.ge.1.and.ii.le.nn.and.jj.ge.1.and.jj.le.nn) then
                    im0(ii,jj)=im0(ii,jj)+1
                  endif
                endif
              endif
            enddo
          endif
        enddo
          
        
        print*,'compute CCF'
        do ig=1,ngti
          
          print*,ig,ngti
          
          do i=1,nn
            do j=1,nn
              im1(i,j)=im0(i,j)
              im20(i,j)=0
            enddo
          enddo
          
          if (gtiqual(ig).eq.0) then
            
            do iph=1,nphot
              if (ichar(igti(iph)).eq.ig) then
                pi0=ichar(pi(iph))
                if (pi0.ge.20) then
                  ii=xpix(iph)/30-192
                  jj=ypix(iph)/30-192
                  if (ii.ge.1.and.ii.le.nn.and.jj.ge.1.and.jj.le.nn) then
                    im20(ii,jj)=im20(ii,jj)+1
                  endif
                endif
              endif
            enddo
            
            do i=1,nn
              do j=1,nn
                im2(i,j)=im20(i,j)
              enddo
            enddo
            
            call ccorr2d (im1,im2,ccf,nn,nn)
            
            immax=0
            do i=1,nn
              do j=1,nn
                if (ccf(i,j).gt.immax) then
                  i0=i
                  j0=j
                  immax=ccf(i,j)
                endif
              enddo
            enddo
            
            if ((i0-nn/2)**2+(j0-nn/2)**2.gt.2*5**2) then
              gtiqual(ig)=1000
*              call show_r4 (ccf,nn,nn)
*              call show_r4 (im0,nn,nn)
*              call show_r4 (im20,nn,nn)
              print*,'GTI :',ig,'  is bad !!!!'
              do i=1,nn
                do j=1,nn
                  im0(i,j)=im0(i,j)-im20(i,j)
                enddo
              enddo
            
            endif
          endif
        enddo
        
      enddo

*c      
*c check Background     
*c
*c
*      print*,'compute background'
*      do ig=1,ngti
*        print*,ig,ngti
*        bcgrate(ig)=0.0
*        do iph=1,nphot
*          if (ichar(igti(iph)).eq.ig) then
*            pi0=ichar(pi(iph))
*            if (pi0.ge.20) then
*              ii=xpix(iph)/30
*              jj=ypix(iph)/30
*              rr=idistance(ii,jj,256,256)
*              if (rr.ge.200.and.rr.le.4096) then
*                bcgrate(ig)=bcgrate(ig)+1
*              endif
*            endif
*          endif
*        enddo
*        
*        bcgrate(ig)=bcgrate(ig)/(tend(ig)-tstart(ig))
*        
*      enddo
      
      
c OK, output
      iphot=0
      do iph=1,nphot
        if (xpix(iph).ge.xmin.and.xpix(iph).le.xmax.and.ypix(iph).ge.ymin.and
     ~      .ypix(iph).le.ymax) then
          iphot=iphot+1
          xpix(iphot)=xpix(iph)
          ypix(iphot)=ypix(iph)
          pi(iphot)=pi(iph)
          igti(iphot)=igti(iph)
        endif
      enddo
      
      nphot=iphot
      
      
c Open fits file
      status=0
      call unlink(out)
      call ftinit(ounit,out,0,status)
      if(status.ne.0)then
        call fitsio_print_error(status)
         call exiterror('')
      endif
      
      bitpix=8
      naxis=0
      pcount=0
      gcount=1
      naxes(1)=0
      naxes(2)=0
      call ftphpr(ounit,.true.,bitpix,naxis,naxes,pcount,gcount,.false.,status
     ~    )
      call ftpdef(ounit,bitpix,naxis,naxes,pcount,gcount,status)
      call ftpkys(ounit,'CONTENTS','Photon list','Central FOV',status)
      call ftpkys(ounit,'TELESCOP','ROSAT','Telescop',status)
      call ftpkys(ounit,'INSTRUME','PSPC-B','Instrument',status)
      call ftpkyj(ounit,'DATASET',rseq,'Rosat sequence',status)
      
c Extension with photon list
      call ftcrhd(ounit,status)
      ttype(1)='XSKY'
      ttype(2)='YSKY'
      ttype(3)='PI'
      ttype(4)='IGTI'
      tform(1)='I'
      tform(2)='I'
      tform(3)='B'
      tform(4)='B'
      tunit(1)='sky pixel'
      tunit(2)='sky pixel'
      tunit(3)='PI channel'
      tunit(4)='GTI number'
      tfields=4
      nrows=nphot
      call ftphbn(ounit,nrows,tfields,ttype,tform,tunit,'PHOTLIST',0,status)
      
      call ftgkyd(iunit,'CRVAL1',crval1,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRVL1',crval1,comment,status)
      endif
      
      call ftgkyd(iunit,'CRVAL2',crval2,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRVL2',crval2,comment,status)
      endif
      
      call ftgkyd(iunit,'CRPIX1',crpix1,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRPX1',crpix1,comment,status)
      endif
      call ftgkyd(iunit,'CRPIX2',crpix2,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRPX2',crpix2,comment,status)
      endif
      call ftgkyd(iunit,'CDELT1',cdelt1,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCDLT1',cdelt1,comment,status)
      endif
      call ftgkyd(iunit,'CDELT2',cdelt2,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCDLT2',cdelt2,comment,status)
      endif
      
      call ftpkyd(ounit,'CRVAL1',crval1,10,' ',status)
      call ftpkyd(ounit,'CRVAL2',crval2,10,' ',status)
      call ftpkyd(ounit,'CRPIX1',crpix1,10,' ',status)
      call ftpkyd(ounit,'CRPIX2',crpix2,10,' ',status)
      call ftpkyd(ounit,'CDELT1',cdelt1,10,' ',status)
      call ftpkyd(ounit,'CDELT2',cdelt2,10,' ',status)
      
      call ftbdef(ounit,tfields,tform,0,nrows,status)
       
      call ftpcli(ounit,1,1,1,nrows,xpix,status)
      call ftpcli(ounit,2,1,1,nrows,ypix,status)
      call ftpclb(ounit,3,1,1,nrows,pi,status)
      call ftpclb(ounit,4,1,1,nrows,igti,status)
      

c Extension with GTI info
      call ftcrhd(ounit,status)
      ttype(1)='START'
      ttype(2)='FINISH'
      ttype(3)='USED'
      ttype(4)='GTIQUAL'
      ttype(5)='BKGDRATE'
      tform(1)='D'
      tform(2)='D'
      tform(3)='J'
      tform(4)='J'
      tform(5)='D'
      tunit(1)='seconds'
      tunit(2)='seconds'
      tunit(3)=' '
      tunit(4)='GTI quality'
      tunit(5)='background count rate'
      tfields=5
      nrows=ngti
      call ftphbn(ounit,nrows,tfields,ttype,tform,tunit,'GTIINFO',0,status)
      
      call ftbdef(ounit,tfields,tform,0,nrows,status)
      
      call ftpcld(ounit,1,1,1,nrows,tstart,status)
      call ftpcld(ounit,2,1,1,nrows,tend,status)
      call ftpclj(ounit,3,1,1,nrows,ugti,status)
      call ftpclj(ounit,4,1,1,nrows,gtiqual,status)
      call ftpcld(ounit,5,1,1,nrows,bcgrate,status)

      call ftclos(ounit,status)
      call ftclos(iunit,status)
      if (status.ne.0) then
        call fitsio_write_error (status,0)
      endif

      end
      








