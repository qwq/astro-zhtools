      subroutine specmap
      implicit none
      character data*200,out*200,specgrid*200,buff*200
      
      integer lnblnk
      
c image arrays
      real smap(512,512)
      real totimg(512,512)
      
c fitsio staff
      integer iunit,ounit
      parameter (iunit=10)
      parameter (ounit=11)
      integer status
      
      integer pcount,gcount
      
      character arg*80
      integer blocksize
      character comment*80
      integer length,nrows,tfields
      character ttype(300)*80,tform(300)*80,tunit(300)*80,extname*80
      integer varidat
      integer fx,fy,fpi,ftime
      integer ifield,irow,i,j,ii,jj
      
      integer*2 x,y
      integer*2 pi
      integer ipi
      real*8 time
      integer pimin,pimax
      
      real*8 crval1,crval2,crpix1,crpix2,cdelt1,cdelt2
      integer bitpix
      real*8 bscale,bzero
      integer naxis,naxes(10)
      
      logical qtime,qgood
      real*8 tmin,tmax
      
      real spec1(34), spec2(34)
      real weight(34)
      
      integer cmin,cmax,ch,n
      real par1,par2,w,sum
      integer pi_to_sass
      integer pixel
      
      logical rdf
      integer unit,newunit
c-----------------------------------------------------------------  
      data = ' '
      out  = ' '
      specgrid = ' ' 
      
      call get_command_line_par  ('data',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        data=arg
      endif
      
      call get_command_line_par  ('out',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        out=arg
      endif
      
      call get_command_line_par  ('specgrid',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        specgrid=arg
      endif
      
      
      if (data.eq.' '.or.out.eq.' '.or.specgrid.eq.' ') then
        write(0,*)'Usage: specmap data=events out=keyword specgrid=specgrid \\'
        write(0,*
     ~      )'       [pimin=pimin] [pimax=pimax] [tmin=tmin] [tmax=tmax] \\'
        write(0,*
     ~      )'       [pixel=pixelsz (arcsec)] '
        call exit(1)
      endif
      
      pimin=20
      pimax=240
      rdf=.false.
      pixel=15
      
      call get_command_line_par  ('pimin',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)pimin
      endif
      call get_command_line_par  ('pimax',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)pimax
      endif
      
      tmin=0
      tmax=1.0e10
      qtime=.false.
      call get_command_line_par  ('tmin',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        qtime=.true.
        read (arg,*)tmin
      endif
      
      call get_command_line_par  ('tmax',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        qtime=.true.
        read (arg,*)tmax
      endif
      
      call get_command_line_par  ('pixel',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read (arg,*)pixel
      endif
      
      pixel=pixel*2

      
c read spectral grid file
      unit = newunit()
      open(unit,file=specgrid,status='old',iostat=status)
      if (status.ne.0) then
        write(0,*)'Cannot find spectral grid file'
        call exit(1)
      endif
      
      n=0
      do while (n.lt.3)
        read(unit,'(a)')buff
        if (buff(1:10).eq.'----------') n=n+1
      enddo
      
      read(unit,'(a)')buff
      read(buff(7:),*)par1,par2
      read(unit,'(a)')buff
      
      do i=1,34
        read(unit,*)w,spec1(i),spec2(i)
      enddo
      
      close(unit)
      
c make weights
      cmin=pi_to_sass(pimin)
      cmax=pi_to_sass(pimax)
      
      sum=0.0
      do i=cmin,cmax
        sum=sum+spec1(i)
      enddo
      sum=sum/(cmax-cmin+1)
      do i=cmin,cmax
        spec1(i)=spec1(i)/sum
      enddo
      
      
      sum=0.0
      do i=cmin,cmax
        sum=sum+spec2(i)
      enddo
      sum=sum/(cmin-cmax+1)
      do i=cmin,cmax
        spec2(i)=spec2(i)/sum
      enddo
      
      sum=0
      do i=cmin,cmax
        weight(i)=(spec1(i)-spec2(i))/(par1-par2)
        sum=sum+weight(i)**2
      enddo
      sum=sqrt(sum/(cmax-cmin+1))
      do i=cmin,cmax
        weight(i)=weight(i)/sum
      enddo
      
      
      
      
      pcount=0
      gcount=1
      
      
      
c read FITS file
      status=0
      call ftopen(iunit,data,0,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',data(1:lnblnk(data))
        call exit(1)
      endif
      
      
c read EVENTS
      call move_ext (iunit,'EVENTS',status)
      if (status.ne.0) then
                                ! TRY RDF DATA FORMAT
        status=0
        call move_ext (iunit,'STDEVT',status)
        if (status.ne.0) then
          write(0,*) 'Cannot find EVENTS extension; exit'
          call exit(1)
        endif
        rdf=.true.
      endif
      
      call ftgkyj (iunit,'NAXIS1',length,comment,status)
      call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~    ,status)
      
c necessary byte positions
      ftime=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'time'.and.ttype(ifield).ne.'TIME') then
          if (tform(ifield).eq.'1I') then
            ftime=ftime+2
          else if (tform(ifield).eq.'1J') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1E') then
            ftime=ftime+4
          else if (tform(ifield).eq.'1D') then
            ftime=ftime+8
          else
            write(0,*) 'unknown format:',tform(ifield)(1:lnblnk(tform(ifield
     ~          )))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1D') then
            write (0,*) 'TIME field format: assume 1D, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 20
        endif
      enddo
       call exiterror('TIME column not found')
 20   continue
        
      fx=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'x'.and.ttype(ifield).ne.'X') then
          if (tform(ifield).eq.'1I') then
            fx=fx+2
          else if (tform(ifield).eq.'1J') then
            fx=fx+4
          else if (tform(ifield).eq.'1E') then
            fx=fx+4
          else if (tform(ifield).eq.'1D') then
            fx=fx+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'X field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 21
        endif
      enddo
       call exiterror('X column not found')
 21   continue
      
      fy=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'y'.and.ttype(ifield).ne.'Y') then
          if (tform(ifield).eq.'1I') then
            fy=fy+2
          else if (tform(ifield).eq.'1J') then
            fy=fy+4
          else if (tform(ifield).eq.'1E') then
            fy=fy+4
          else if (tform(ifield).eq.'1D') then
            fy=fy+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'Y field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 22
        endif
      enddo
       call exiterror('Y column not found')
 22   continue
      
      fpi=1
      do ifield=1,tfields
        if (ttype(ifield).ne.'pi'.and.ttype(ifield).ne.'PI') then
          if (tform(ifield).eq.'1I') then
            fpi=fpi+2
          else if (tform(ifield).eq.'1J') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1E') then
            fpi=fpi+4
          else if (tform(ifield).eq.'1D') then
            fpi=fpi+8
          else
            write(0,*) 'unknown format:',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
        else
          if (tform(ifield).ne.'1I') then
            write (0,*) 'PI field format: assume 1I, got ',tform(ifield
     ~          )(1:lnblnk(tform(ifield)))
             call exiterror('')
          endif
          goto 23
        endif
      enddo
       call exiterror('PI column not found')
 23   continue
      
     
      do i=1,512
        do j=1,512
          totimg(i,j)=0
          smap(i,j)=0
        enddo
      enddo

c read EVENTS
      do irow=1,nrows
        call ftgtbb (iunit,irow,fpi,2,pi,status)
        ipi=pi
        ch=pi_to_sass(ipi)
c        decode bytes
        if (ch.ge.cmin.and.ch.le.cmax) then
          if (qtime) then
            call ftgtbb (iunit,irow,ftime,8,time,status)
            qgood=time.ge.tmin.and.time.le.tmax
          else
            qgood=.true.
          endif
          if (qgood) then
            call ftgtbb (iunit,irow,fx,2,x,status)
            call ftgtbb (iunit,irow,fy,2,y,status)
            ii=x/pixel+1
            jj=y/pixel+1
            if(ii.ge.1.and.ii.le.512.and.jj.ge.1.and.jj.le.512)then
              smap(ii,jj)=smap(ii,jj)+weight(ch)
              totimg(ii,jj)=totimg(ii,jj)+1
            endif
          endif
        endif
      enddo
      
      
      bitpix=-32
      
      pcount=0
      gcount=1
      bscale=1.0
      bzero=0.0
      naxis=2
      naxes(1)=nint(512.0*30.0/pixel)
      naxes(2)=nint(512.0*30.0/pixel)
      call unlink(out)
      call ftinit(ounit,out,0,status)
      call ftphpr(ounit,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~    .false.,status)
      call ftpdef(ounit,bitpix,naxis,naxes,pcount,gcount,status)
      call ftpkyd(ounit,'BSCALE',1.0d0,10,' ',status)
      call ftpkyd(ounit,'BZERO',0.0d0,10,' ',status)
      call ftpscl(ounit,bscale,bzero,status)
      
      call ftgkyd(iunit,'CRVAL1',crval1,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRVL1',crval1,comment,status)
      endif
      if (status.ne.0) then
        write(0,*)'Warning: CRVAL1 not found'
        status=0
      endif
      
      
      call ftgkyd(iunit,'CRVAL2',crval2,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRVL2',crval2,comment,status)
      endif
      if (status.ne.0) then
        write(0,*)'Warning: CRVAL2 not found'
        status=0
      endif
      
      call ftgkyd(iunit,'CRPIX1',crpix1,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRPX1',crpix1,comment,status)
      endif
      if (status.ne.0) then
        write(0,*)'Warning: CRPIX1 not found'
        status=0
      endif

      
      call ftgkyd(iunit,'CRPIX2',crpix2,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCRPX2',crpix2,comment,status)
      endif
      if (status.ne.0) then
        write(0,*)'Warning: CRPIX2 not found'
        status=0
      endif

      
      call ftgkyd(iunit,'CDELT1',cdelt1,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCDLT1',cdelt1,comment,status)
      endif
      if (status.ne.0) then
        write(0,*)'Warning: CDELT1 not found'
        status=0
      endif

      
      call ftgkyd(iunit,'CDELT2',cdelt2,comment,status)
      if (status.ne.0) then
        status=0
        call ftgkyd(iunit,'TCDLT2',cdelt2,comment,status)
      endif
      if (status.ne.0) then
        write(0,*)'Warning: CDELT2 not found'
        status=0
      endif

      
      crpix1=crpix1/pixel+1
      crpix2=crpix2/pixel+1
      cdelt1=cdelt1*pixel
      cdelt2=cdelt2*pixel
      
      call ftpkyd(ounit,'CRVAL1',crval1,10,' ',status)
      call ftpkyd(ounit,'CRVAL2',crval2,10,' ',status)
      call ftpkyd(ounit,'CRPIX1',crpix1,10,' ',status)
      call ftpkyd(ounit,'CRPIX2',crpix2,10,' ',status)
      call ftpkyd(ounit,'CDELT1',cdelt1,10,' ',status)
      call ftpkyd(ounit,'CDELT2',cdelt2,10,' ',status)
      
      call ftrdef(ounit,status)
      call ftp2de(ounit,0,512,naxes(1),naxes(2),smap,status)

      
      call ftclos(iunit,status)
      call ftclos(ounit,status)
      
      out=out(1:lnblnk(out))//'.norm'
      call unlink(out)
      call ftinit(ounit-1,out,0,status)
      call ftphpr(ounit-1,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~    .false.,status)
      call ftpdef(ounit-1,bitpix,naxis,naxes,pcount,gcount,status)
      call ftpkyd(ounit-1,'BSCALE',1.0d0,10,' ',status)
      call ftpkyd(ounit-1,'BZERO',0.0d0,10,' ',status)
      call ftpscl(ounit-1,bscale,bzero,status)
      
      call ftpkyd(ounit-1,'CRVAL1',crval1,10,' ',status)
      call ftpkyd(ounit-1,'CRVAL2',crval2,10,' ',status)
      call ftpkyd(ounit-1,'CRPIX1',crpix1,10,' ',status)
      call ftpkyd(ounit-1,'CRPIX2',crpix2,10,' ',status)
      call ftpkyd(ounit-1,'CDELT1',cdelt1,10,' ',status)
      call ftpkyd(ounit-1,'CDELT2',cdelt2,10,' ',status)
      
      call ftpcom(ounit-1,'NORM image',status)
      
      call ftrdef(ounit-1,status)
      call ftp2de(ounit-1,0,512,naxes(1),naxes(2),totimg,status)
      call ftclos(ounit-1,status)

      if (status.ne.0) then
        call fitsio_write_error (status,0)
      endif
      
      
      end
      







