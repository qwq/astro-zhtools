#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

long lnblnk_ (char*);

void read_fits_mask_ (char *, long *, long* );

void make_region_image_ (long *region, char *srcreg, long *ns,
			 char *bkgdreg, long *nb)
{
  int i,j,ii,jj,n;
  int rmfile;
  int x,y,r,r1,r2,x1,x2,y1,y2,rr1,rr2,rr;
  FILE *bkgd, *src;
  char filename[100];
  char type[100];
  int dist;
  float a1,a2,a;
  int quse;
  long region0[262144];
  
  srcreg[*ns]='\0';
  bkgdreg[*nb]='\0';


  n=512*512;
  
  for(i=0;i<n;i++)
    region[i]=0;

  printf("SRC: %s    BKGD: %s\n",srcreg,bkgdreg);

  if (strcmp(srcreg,"none")!=0)
    {
      /* if region is fits mask -- call special program */
      if (strncmp(srcreg,"fits:",5)==0)
	{
	  for(i=0;i<n;i++)
	    region0[i]=0;
	  read_fits_mask_ (srcreg,ns,region0);
	  for (i=0;i<n;i++)
	    if (region0[i]!=0) region[i]=1;
	}
      else
	{
	  /* determine if src is described in a file */  
	  if (strncmp(srcreg,"file:",5)==0)
	    {
	      rmfile=0;
	      strcpy(filename,srcreg+5);
	    }
	  else
	    /* write the string to a temporary file */
	    {
	      sprintf(filename,"/tmp/.srcreg.%d\0",getpid());
	      rmfile=1;
	      src=fopen(filename,"w");
	      if (src==NULL)
		{
		  fprintf(stderr,"Failed to open for writing: %s\n",filename);
		  exit(1);
		}
	      fprintf(src,"%s",srcreg);
	      fclose(src);
	    }
	  src=fopen(filename,"r");
	  if (src==NULL)
	    {
	      fprintf(stderr,"Failed to open file: %s\n",filename);
	      exit(1);
	    }
	  
	  make_region_image_0 (region,src,1);
	  fclose(src);
	  if (rmfile==1)
	    unlink(filename);
	}
    }

  

  if (strcmp(bkgdreg,"none")!=0&&strcmp(bkgdreg,"standard")!=0)
    {
      
      /* if region is fits mask -- call special program */
      if (strncmp(bkgdreg,"fits:",5)==0)
	{
	  read_fits_mask_ (bkgdreg,nb,region0);
	  for (i=0;i<n;i++)
	    if (region0[i]!=0) region[i]=2;
	}
      
      else
	{
	  /* determine if bkgd, is described in a file */  
	  if (strncmp(bkgdreg,"file:",5)==0)
	    {
	      rmfile=0;
	      strcpy(filename,bkgdreg+5);
	    }
	  else
	    /* write the string to a temporary file */
	    {
	      sprintf(filename,"/tmp/.bkgdreg.%d\0",getpid());
	      rmfile=1;
	      bkgd=fopen(filename,"w");
	      if (bkgd==NULL)
		{
		  fprintf(stderr,"Failed to open for writing: %s\n",filename);
		  exit(1);
		}
	      fprintf(bkgd,"%s",bkgdreg);
	      fclose(bkgd);
	    }
	  bkgd=fopen(filename,"r");
	  if (bkgd==NULL)
	    {
	      fprintf(stderr,"Failed to open file: %s\n",filename);
	      exit(1);
	    }
	  
	  make_region_image_0 (region,bkgd,2);
	  fclose(bkgd);
	  if (rmfile==1)
	    unlink(filename);
	}
    }
  
}



int make_region_image_0 (long *img,FILE *src, int val0)
{
  int i,j,ii,jj,n;
  float x,y,r1,r2,x1,x2,y1,y2,rr1,rr2,rr;
  float r;
  char type[100];
  int dist;
  float a1,a2,a;
  int quse;
  float value;
  int nx,ny;
  float xc,yc;
  float ax,ay,angle,cosa,sina,dx,dy,deltax,deltay;
  float sidex,sidey;

  nx=512;ny=512;
  n=nx*ny;
  
  while (fscanf(src,"%s",type)==1)
    {
      printf("%s\n",type);
      switch (type[0])
	{
	  
	case 'p': /* single pixel */
	  {
	    if (fscanf(src,"%f %f %f",&xc,&yc,&value)!=3)
	      {
		fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
		exit(2);
	      }
	    xc=floor(xc);
	    yc=floor(yc);
	    i=xc+(nx)*(yc-1)-1;
	    if (i<n) img[i]=val0;
	    goto ENDLOOP;
	  }
	  
	case 'c': /* circle */
	  {
	    if (fscanf(src,"%f %f %f %f",&xc,&yc,&r,&value)!=4)
	      {
		fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
		exit(2);
	      }
	    xc=floor(xc);
	    yc=floor(yc);
	    printf("%f %f %f %f\n",xc,yc,r,value);
	    rr=r*r;
	    for (i=-r;i<=r;i++)
	      for (j=-r;j<=r;j++)
		{
		  dist=i*i+j*j;
		  if (dist<=rr)
		    {
		      ii=(xc+i)+(nx)*(yc+j-1)-1;
		      if (ii<n) img[ii]=val0;
		    }
		}
	    goto ENDLOOP;
	  }

	case 'b': /* box */
	  {
	    if (fscanf(src,"%f %f %f %f %f %f",&xc,&yc,&sidex,&sidey,&angle,&value)!=6)
	      {
		fprintf(stderr,"wrong structure of region descriptor\n");
		exit(2);
	      }
	    xc=floor(xc);
	    yc=floor(yc);

	    rr=sqrt(sidex*sidex/4+sidey*sidey/4)+1;
	    sina=sin(angle*3.1415926536/180.0);
	    cosa=cos(angle*3.1415926536/180.0);
	    for (i=xc-rr;i<=xc+rr;i++)
	      for (j=yc-rr;j<=yc+rr;j++)
		{
		  dx=i-xc;
		  dy=j-yc;
		  deltax=dx*sina+dy*cosa;
		  deltay=-dx*cosa+dy*sina;
		  if (deltax<0) deltax=-deltax;
		  if (deltay<0) deltay=-deltay;
		  
		  if (deltax<=sidex/2&&deltay<=sidey/2)
		    {
		      ii=i+(nx)*(j-1)-1;
		      if (ii<n) img[ii]=val0;
		    }
		}
	    

	    goto ENDLOOP;
	  }

	case 'e': /* ellipse */
	  {
	    if (fscanf(src,"%f %f %f %f %f %f",&xc,&yc,&ax,&ay,&angle,&value)!=6)
	      {
		fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
		exit(2);
	      }
	    rr=ay;
	    if(ax>ay)rr=ax;
	    sina=sin(angle*3.1415926536/180.0);
	    cosa=cos(angle*3.1415926536/180.0);
	    for (i=xc-rr;i<=xc+rr;i++)
	      for (j=yc-rr;j<=yc+rr;j++)
		{
		  dx=i-xc;
		  dy=j-yc;
		  deltax=dx*sina+dy*cosa;
		  deltay=-dx*cosa+dy*sina;
		  if ((deltax/ax)*(deltax/ax)+(deltay/ay)*(deltay/ay)<1)
		    {
		      ii=i+(nx)*(j-1)-1;
		      if (ii<n) img[ii]=val0;
		    }
		}
	    
	    goto ENDLOOP;
	  }

	case 'a': /*annulus*/
	  {
	    if (fscanf(src,"%f %f %f %f %f",&x,&y,&r1,&r2,&value)!=5)
	      {
		fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
		exit(2);
	      }
	    x=floor(x);
	    y=floor(y);
	    rr1=r1*r1;
	    rr2=r2*r2;
	    for (i=-r2;i<=r2;i++)
	      for (j=-r2;j<=r2;j++)
		{
		  dist=i*i+j*j;
		  if (dist<=rr2 && dist > rr1)
		    {
		      ii=(x+i)+(nx)*(y+j-1)-1;
		      if (ii<n) img[ii]=val0;
		    }
		}
	    goto ENDLOOP;
	  }

	case 's': /*sector*/
	  {
	    if (fscanf(src,"%f %f %f %f %f %f %f",
		       &x,&y,&r1,&r2,&a1,&a2,&value)!=7)
	      {
		fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
		exit(2);
	      }
	    x=floor(x);
	    y=floor(y);
	    rr1=r1*r1;
	    rr2=r2*r2;
	    for (i=-r2;i<=r2;i++)
	      for (j=-r2;j<=r2;j++)
		{
		  dist=i*i+j*j;
		  if (dist<=rr2 && dist > rr1)
		    {
		      quse=0;
		      if (dist == 0)
			{
			  quse=1;
			}
		      else
			{
			  a=acos((i+0.0)/sqrt(dist));
			  if (j<0)
			    {
			      a=2.0*3.1415926536-a;
			    }
			  a=a*180.0/3.1415926536;
			  if (a>a1 && a<=a2)
			    {
			      quse=1;
			    }
			}
		      if (quse==1)
			{
			  ii=(x+i)+(nx)*(y+j-1)-1;
			  if (ii<n) img[ii]=val0;
			}
		    }
		}
	    goto ENDLOOP;
	  }

	default: fprintf(stderr,"unkown region type: %s\n",type),exit(2);
	}
		      
    ENDLOOP:;
	    
    }
  return 1;
}
