      subroutine flipimg (img,nx,ny,imgo,x0,y0,angle,boundval,subpix,poisson
     ~    ,phot)
      implicit none
      integer nx,ny
      real img (nx,ny), imgo(nx,ny)
      real x0,y0,angle
      real boundval
      logical poisson
      integer subpix
      integer phot(subpix,subpix)
      real vx, vy, rx, ry, rv
      integer ii,jj,i,j
      real x,y
      integer ibin,jbin, bin

      bin = subpix

      vx = cos(angle*3.1415926536/180.0)
      vy = sin(angle*3.1415926536/180.0)

      do i=1,nx
        do j=1,ny
          imgo(i,j)=-1e20
        enddo
      enddo

      do i=1,nx
        do j=1,ny
          if (poisson) then
            call scatter_phot(nint(img(i,j)),phot,bin*bin)
          endif
          do ibin=1,bin
            do jbin=1,bin
              rx = i-0.5+(ibin-0.5)/bin - x0
              ry = j-0.5+(jbin-0.5)/bin - y0
              rv = rx*vx+ry*vy
              rx = 2*rv*vx-rx
              ry = 2*rv*vy-ry
              ii = x0+rx
              jj = y0+ry
              if (ii.ge.1.and.ii.le.nx.and.jj.ge.1.and.jj.le.ny) then
                if (imgo(ii,jj).eq.-1e20) imgo(ii,jj)=0.0
                if (poisson) then
                  imgo(ii,jj)=imgo(ii,jj)+phot(ibin,jbin)
                else
                  imgo(ii,jj)=imgo(ii,jj)+img(i,j)/bin**2
                endif
              endif
            enddo
          enddo
        enddo
      enddo

      do i=1,nx
        do j=1,ny
          if (imgo(i,j).ne.-1e20) then
            img(i,j)=imgo(i,j)
          else
            img(i,j)=boundval
          endif
        enddo
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine scatter_phot(k,phot,n)
      implicit none
      integer k,n,phot(n)
      integer i,l
      real ran2

      do i=1,n
        phot(i)=0
      enddo

      do l=1,k
        i = n+1
        do while (i.gt.n)
          i = int(n*ran2())+1
        enddo
        phot(i)=phot(i)+1
      enddo

      return
      end

      
