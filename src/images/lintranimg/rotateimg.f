      subroutine rotate_img_main (img,nx,ny,refx,refy,angle,boundval,subpix
     ~    ,poisson)
      implicit none
c
c See rotation.tex at the bottom and rotate_img_pure
c
c
      integer nx,ny
      real img(nx,ny)
      real refx,refy, angle, boundval
      integer subpix
      logical poisson
      
      real shiftx,shifty, A, rot
      integer irefx,irefy

c 1) Move image so that the rotation will be around the nearest even point
      irefx = nint(refx)
      irefy = nint(refy)
      shiftx = refx-irefx
      shifty = refy-irefy
      
      if (shiftx.ne.0.0.or.shifty.ne.0.0) then
        call rotate_img__shiftxy (img,nx,ny,-shiftx,-shifty,boundval,poisson)
      endif

      
c  2) Translate angle to -180:180 range
      A = angle
      do while (A.lt.-180.0)
        A = A + 360.0
      enddo
      do while (A.gt.180.0)
        A = A - 360.0
      enddo

c  3) Rotate
      if (A.lt.-135.0) then     ! rotate by -180 and then by 180+A
        call rotate_img__180 (img,nx,ny,irefx,irefy,boundval)
        rot = 180.0+A
      else if (A.lt.-45.0) then ! rotate by -90 and then by 90+A
        call rotate_img__minus90 (img,nx,ny,irefx,irefy,boundval)
        rot = 90.0 + A
      else if (A.lt.45.0) then  ! just rotate by A
        rot = A
      else if (A.lt.135.0) then ! rotate by 90 and then by A-90
        call rotate_img__plus90 (img,nx,ny,irefx,irefy,boundval)
        rot = A - 90.0
      else                      ! rotate by 180 and then by A-180
        call rotate_img__180 (img,nx,ny,irefx,irefy,boundval)
        rot = A - 180.0
      endif
      if (abs(rot).gt.0.0) then
        call rotate_img__rot (img,nx,ny,float(irefx),float(irefy),rot,boundval
     ~      ,subpix,poisson)
      endif

c   4) Shift back if necessary
      if (shiftx.ne.0.0.or.shifty.ne.0.0) then
        call rotate_img__shiftxy (img,nx,ny,shiftx,shifty,boundval,poisson)
      endif

      return
      end

**xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*
*      subroutine rotate_img_pure (img,nx,ny,refx,refy,angle, boundval)
*c
*c Rotate the image in-place, around (refx,refy) by 'angle' degrees counter
*c clock-wise
*c
*c The transofrmations made are rotation relative to the ref. point by 
*c angle or, if |angle|>90, by 90-|angle|.
*c If angle > 90, the program also performs y- and x-flips relative to the
*c refpoint. If any of the transformations moves the point outside the image
*c boundary, its value is lost and will be replaced by boundval, if necessary
*c 
*c If this is undesirable, the user should put his data into a large enough
*c frame, rotate it, and extract the rotated data
*c
*      implicit none
*      integer nx,ny
*      real img(nx,ny)
*      real refx,refy, angle
*      real boundval
*
*c
*c  From comp.graphics.algorithms FAQ 
*c       http://www.exaflop.org/docs/cgafaq/ind.html
*c 
*c* Subject 3.01: How do I rotate a bitmap?
*c* 
*c*     The easiest way, according to the comp.graphics faq, is to take the
*c*     rotation transformation and invert it. Then you just iterate over 
*c*     the destination image, apply this inverse transformation and find 
*c*     which source pixel to copy there. A much nicer way comes from the
*c*     observation that the rotation matrix:
*c* 
*c*           R(T) = { { cos(T), -sin(T) }, { sin(T), cos(T) } }
*c*
*c*     is formed my multiplying three matrices, namely:
*c*
*c*           R(T) = M1(T) * M2(T) * M3(T)
*c*
*c*     where
*c*
*c*           M1(T) = { { 1, -tan(T/2) },
*c*                     { 0, 1         } }
*c*           M2(T) = { { 1, 0         },
*c*                     { sin(T), 1    } }
*c*           M3(T) = { { 1, -tan(T/2) },
*c*                     { 0, 1         } }
*c*
*c*     Each transformation can be performed in a separate pass, and because 
*c*     these transformations are either
*c*     row-preserving or column-preserving, anti-aliasing is quite easy.
*c*
*c*     Reference:
*c*
*c*     Paeth, A. W., "A Fast Algorithm for General Raster Rotation", 
*c*     Proceedings Graphics Interface '89, Canadian
*c*     Information Processing Society, 1986, 77-81
*c*     [Note - e-mail copies of this paper are no longer available]
*c*     [Gems I]
*c*
*c------------------------------------------------------------------------
*c
*c The first transformation is 
*c  x' = x - tan(T/2)*y    y' = y
*c
*c The second transformation is
*c  x' = x                 y' = sin(T)*x + y
*c
*c The third transformation is
*c  x' = x - tan(T/2)*y    y' = y
*c
*c
*
*c 180 deg rotation is equivalent to flipy, flipx
*c 90+alpha rotation is equivalent to flipy, flipx, rot by -(90-alpha)
*c -(90+alpha) rotation is equivalent to flipy, flipx, rot by (90-alpha)
*
*      real A, alpha, rot
*
*      A = angle
*
*c Move angle to -180,+180 interval
*      do while (A.lt.-180.0)
*        A = A+360
*      enddo
*
*      do while (A.gt.180.0)
*        A = A-360
*      enddo
*
*      if (abs(A).le.90.0) then
*        call rotate_img__rot (img,nx,ny,refx,refy,A, boundval)
*      else
*        call rotate_img__flipy (img,nx,ny,refx,refy,boundval)
*        call rotate_img__flipx (img,nx,ny,refx,refy,boundval)
*        if (A.lt.0.0) then      ! A = -(90+alpha) => alpha = -A-90
*          alpha = -A-90.0
*          rot   = 90.0-alpha
*        else                    ! A = 90+alpha    => alpha = A - 90
*          alpha  = A-90.0
*          rot    = -(90.0-alpha)
*        endif
*        
*        call rotate_img__rot (img,nx,ny,refx,refy,rot,boundval)
*      endif
*
*      return
*      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__rot (img,nx,ny,refx,refy,Ang, boundval,subpix
     ~    ,poisson)
      implicit none
      integer nx,ny
      real img(nx,ny)
      real refx,refy,Ang,boundval
      integer subpix
      logical poisson
      real a(20000), ashift(20000), b(20000)

      real tgrot, sinrot, shift, step, start
      real pi
      parameter (pi=3.1415926536)
      integer i,j
      integer nbin, ibin
      integer nbinmax
      parameter (nbinmax=20)
      integer apoisson(nbinmax,20000)
      
      tgrot = tan(0.5*Ang*pi/180.0)
      sinrot = sin(Ang*pi/180.0)

      nbin = subpix
      start = 0.5/(nbin)
      step = 1.0/nbin
      



c Transform I:       x' = x - tan(T/2)*y    y' = y
      do j=1,ny
        do i=1,nx
          b(i)=0
          a(i)=img(i,j)
        enddo
        if (poisson) then
          if (nbin.eq.1) then
            do i=1,nx
              apoisson(1,i)=a(i)
            enddo
          else
            do i=1,nx
              call scatter_phot(nint(a(i)),apoisson(1,i),nbin)
            enddo
          endif
        endif
        
        do ibin=1,nbin
          if (poisson) then
            do i=1,nx
              a(i)=apoisson(ibin,i)
            enddo
          endif
          shift = -tgrot*(j-0.5+start+(ibin-1)*step-refy)
          call rotate_img__shift (a,nx,ashift,shift,boundval,poisson)
          do i=1,nx
            b(i)=b(i)+ashift(i)
          enddo
        enddo
        if(.not.poisson) then
          do i=1,nx
            img(i,j)=b(i)/nbin
          enddo
        else
          do i=1,nx
            img(i,j)=b(i)
          enddo
        endif
      enddo


c Transform II:     x' = x                 y' = sin(T)*x + y
      do i=1,nx
        do j=1,ny
          a(j)=img(i,j)
          b(j)=0
        enddo
        if (poisson) then
          if (nbin.eq.1) then
            do j=1,ny
              apoisson(1,j)=a(j)
            enddo
          else
            do j=1,ny
              call scatter_phot(nint(a(j)),apoisson(1,j),nbin)
            enddo
          endif
        endif
        do ibin=1,nbin
          if (poisson) then
            do j=1,ny
              a(j)=apoisson(ibin,j)
            enddo
          endif
          shift = sinrot*(i-0.5+start+(ibin-1)*step-refx)
          call rotate_img__shift (a,ny,ashift,shift,boundval,poisson)
          do j=1,ny
            b(j)=b(j)+ashift(j)
          enddo
        enddo
        if (.not.poisson) then
          do j=1,ny
            img(i,j)=b(j)/nbin
          enddo
        else
          do j=1,ny
            img(i,j)=b(j)
          enddo
        endif
      enddo

c Transform III (same as I): x' = x - tan(T/2)*y    y' = y
      do j=1,ny
        do i=1,nx
          a(i)=img(i,j)
          b(i)=0
        enddo
        if (poisson) then
          if (nbin.eq.1) then
            do i=1,nx
              apoisson(1,i)=a(i)
            enddo
          else
            do i=1,nx
              call scatter_phot(nint(a(i)),apoisson(1,i),nbin)
            enddo
          endif
        endif
        do ibin=1,nbin
          if (poisson) then
            do i=1,nx
              a(i)=apoisson(ibin,i)
            enddo
          endif
          shift = -tgrot*(j-0.5+start+(ibin-1)*step-refy)
          call rotate_img__shift (a,nx,ashift,shift,boundval,poisson)
          do i=1,nx
            b(i)=b(i)+ashift(i)
          enddo
        enddo
        if (.not.poisson) then
          do i=1,nx
            img(i,j)=b(i)/nbin
          enddo
        else
          do i=1,nx
            img(i,j)=b(i)
          enddo
        endif
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__shiftxy (img,nx,ny,shiftx,shifty,boundval,poisson
     ~    )
      implicit none
      integer nx,ny
      real img(nx,ny)
      real shiftx,shifty, boundval
      logical poisson
      real a(20000), ashift(20000)
      integer i,j

      do i=1,nx
        do j=1,ny
          a(j)=img(i,j)
        enddo
        call rotate_img__shift(a,ny,ashift,shifty,boundval,poisson)
        do j=1,ny
          img(i,j)=ashift(j)
        enddo
      enddo

      do j=1,ny
        do i=1,nx
          a(i)=img(i,j)
        enddo
        call rotate_img__shift(a,nx,img(1,j),shiftx,boundval,poisson)
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__plus90 (img,nx,ny,irefx,irefy,boundval)
      implicit none
      integer nx,ny, irefx,irefy
      real boundval
      real img(nx,ny)

      call rotate_img__transpose (img,nx,ny,irefx,irefy,boundval)
      call rotate_img__iflipx    (img,nx,ny,irefx,boundval)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__minus90 (img,nx,ny,irefx,irefy,boundval)
      implicit none
      integer nx,ny, irefx,irefy
      real boundval
      real img(nx,ny)

      call rotate_img__transpose (img,nx,ny,irefx,irefy,boundval)
      call rotate_img__iflipy    (img,nx,ny,irefy,boundval)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__180 (img,nx,ny,irefx,irefy,boundval)
      implicit none
      integer nx,ny, irefx,irefy
      real boundval
      real img(nx,ny)

      call rotate_img__iflipy (img,nx,ny,irefy,boundval)
      call rotate_img__iflipx (img,nx,ny,irefx,boundval)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__iflipx (img,nx,ny,irefx,boundval)
      implicit none
      integer nx,ny, irefx
      real boundval
      real img(nx,ny), a(20000)
      integer l, i,j

      do j=1,ny
        do i=1,nx
          a(i)=img(i,j)
        enddo
        do i=1,nx
          l = irefx+(-(i-irefx))
          if (l.ge.1.and.l.le.nx) then
            img(i,j)=a(l)
          else
            img(i,j)=boundval
          endif
        enddo
      enddo
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__iflipy (img,nx,ny,irefy,boundval)
      implicit none
      integer nx,ny, irefy
      real boundval
      real img(nx,ny), a(20000)
      integer l, i,j

      do i=1,nx
        do j=1,ny
          a(j)=img(i,j)
        enddo
        do j=1,ny
          l = irefy+(-(j-irefy))
          if (l.ge.1.and.l.le.ny) then
            img(i,j)=a(l)
          else
            img(i,j)=boundval
          endif
        enddo
      enddo
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__transpose (img,nx,ny,irefx,irefy,boundval)
      implicit none
      integer nx,ny, irefx,irefy
      real boundval
      real img(nx,ny)
      integer i,j,k,l,xx,yy,ww
      real w   

      if (irefx.ge.irefy) then
        do j=1,ny
          do i=1,min(j-1+irefx-irefy,nx)
            xx = i-irefx
            yy = j-irefy
            ww = xx
            xx = yy
            yy = ww
            k = xx + irefx
            l = yy + irefy
            if (l.ge.1.and.l.le.ny.and.k.ge.1.and.k.le.nx) then
              w = img(i,j)
              img(i,j)=img(k,l)
              img(k,l)=w
            else
              img(i,j)=boundval
            endif
          enddo
        enddo
      else
        do i=1,nx
          do j=1,min(i-1+irefy-irefx,ny)
            xx = i-irefx
            yy = j-irefy
            ww = xx
            xx = yy
            yy = ww
            k = xx + irefx
            l = yy + irefy
            if (l.ge.1.and.l.le.ny.and.k.ge.1.and.k.le.nx) then
              w = img(i,j)
              img(i,j)=img(k,l)
              img(k,l)=w
            else
              img(i,j)=boundval
            endif
          enddo
        enddo
      endif

      return
      end
      

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__flipy (img,nx,ny,refx,refy,boundval)
      implicit none
      integer nx,ny
      real img(nx,ny)
      real refx,refy,boundval
      integer i,j
      real a(20000), ashift(20000)

      do i=1,nx
        do j=1,ny
          a(j)=img(i,j)
        enddo
        call rotate_img__flip  (a,ny,ashift,refy,boundval) ! return value
        do j=1,ny               ! is in 'a'!
          img(i,j)=a(j)
        enddo
      enddo

      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__flipx (img,nx,ny,refx,refy,boundval)
      implicit none
      integer nx,ny
      real img(nx,ny)
      real refx,refy,boundval
      integer i,j
      real a(20000), ashift(20000)

      do j=1,ny
        do i=1,nx
          a(i)=img(i,j)
        enddo
        call rotate_img__flip  (a,nx,ashift,refx,boundval) ! return value
        do i=1,nx               ! is in 'a'!
          img(i,j)=a(i)
        enddo
      enddo

      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__flip (a,n,ashift,ref,boundval)
      implicit none
      integer n
      real a(n),ashift(n)
      real ref
      real boundval
      integer k, i, l
      real shift

      k = nint(ref)
      shift = 2.0*(ref-k)
      
      call rotate_img__shift (a,n,ashift,shift,boundval)
      do i=1,n
        l = k + (k-i)
        if (l.ge.1.and.l.le.n) then
          a(i)=ashift(l)
        else
          a(i)=boundval
        endif
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rotate_img__shift (a,n,ashift,shift,boundval,poisson)
      implicit none
      integer n
      real a(n), ashift(n)
      logical poisson
      real shift, boundval
      integer k, k1, k2, l, m, i
      real w1,w2
      real aw1(10000),aw2(10000)
      
      if (shift.eq.float(nint(shift))) then
        k = nint(shift)
        do i=1,n
          l = i-k
          if (l.ge.1.and.l.le.n) then
            ashift(i)=a(l)
          else
            ashift(i)=boundval
          endif
        enddo
      else
        if (shift.gt.0.0) then
          k1 = int(shift)+1
          k2 = int(shift)
          w1 = shift-int(shift)
          w2 = 1.0 - w1
        else
          k1 = -(int(-shift))
          k2 = k1 - 1
          w2 = (-shift) - int(-shift)
          w1 = 1.0 - w2
        endif
      endif
        
      if (.not.poisson) then
        do i=1,n
          l = i-k1
          m = i-k2
          if (l.ge.1.and.m.le.n) then
            ashift(i)=a(l)*w1+a(m)*w2
          else
            ashift(i)=boundval
          endif
        enddo
      else
        do i=1,n
          call scatter_phot_binom(nint(a(i)),w1,l,m)
          aw1(i)=l
          aw2(i)=m
        enddo
        do i=1,n
          l = i-k1
          m = i-k2
          if (l.ge.1.and.m.le.n) then
            ashift(i)=aw1(l)+aw2(m)
          else
            ashift(i)=boundval
          endif
        enddo
      endif
      
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine scatter_phot_binom(n,w,l,m)
      implicit none
      integer n,l,m
      real w

      integer i
      real ran2

      l = 0
      m = 0
      do i=1,n
        if (ran2(-100).lt.w) then
          l = l + 1
        else
          m = m + 1
        endif
      enddo

      return
      end


*
*\documentclass{article}
*
*\begin{document}
*
*\section{Pure rotation}\label{secrot}
*
*Rotation by angle $\alpha$ ($\alpha>0$ for counter clock-wise rotations) is
*\begin{equation}\label{eqrot}
*M(\alpha) = \left(\begin{array}{rr} \cos\alpha & -\sin\alpha \\
*                                \sin\alpha & \cos\alpha\end{array}\right)
*\end{equation}
*
*This is the product of three matrices:
*\[
*M_1 = \left(\begin{array}{cc} 1 & -\tan(\alpha/2) \\
*                              0 & 1              \end{array}\right)
*\]
*\[
*M_2 = \left(\begin{array}{cc} \sin\alpha & 1 \\
*                              0          & 1 \end{array}\right)
*\]
*\[
*M_3 = M_1 = \left(\begin{array}{cc} 1 & -\tan(\alpha/2) \\
*                              0 & 1              \end{array}\right)
*\]
*
*Because each of $M_1$, $M_2$, and $M_3$ is either row- or column-preserving,
*anti-aliasing is easy
*
*$$M_1: \qquad x' = x - \tan(\alpha/2)\, y \quad y'=y$$
*$$M_2: \qquad x' = x    \quad y'=\sin(\alpha)\,x + y$$
*$$M_3: \qquad x' = x - \tan(\alpha/2)\, y \quad y'=y$$
*
*\section{Rotation around a reference point}
*
*Rotation around the reference point $(x_0,y_0)$ is the product of three
*matrices, 
*
*$$ M = M_1 \otimes M_2 \otimes M_3, $$
*where $M_1$ is the shift matrix,
*$$ M_1 = \left(\begin{array}{cc} -x_0 & 0 \\ 0 & -y_0 \end{array}\right) $$
*$M_2$ is the rotation matrix (eq.~\ref{eqrot}), and $M_3$ is the inverse
*shift
*$$ M_3 = \left(\begin{array}{cc}  x_0 & 0 \\ 0 &  y_0 \end{array}\right) $$
*
*\section{Simplifying rotations}
*
*\subsection{Rotation by 90 degrees}
*
*The matrix of the $90^\circ$ rotation is
*$$M_{90} = 
*\left(\begin{array}{rr} 0 & -1 \\
*                        1 & 0\end{array}\right) = 
*\left(\begin{array}{rr} -1 & 0 \\
*                        0  & 1\end{array}\right) \otimes
*\left(\begin{array}{rr} 0 & 1 \\
*                        1 & 0\end{array}\right),
*                    $$
*i.e., this rotation is equivalent to transposing the image followed by
*flipping $x$
*
*\subsection{Rotation by --90 degrees}
*
*$$M_{-90} = 
*\left(\begin{array}{rr} 0 & 1 \\
*                       -1 & 0\end{array}\right) = 
*\left(\begin{array}{rr} 1 & 0 \\
*                        0  & -1\end{array}\right) \otimes
*\left(\begin{array}{rr} 0 & 1 \\
*                        1 & 0\end{array}\right),
*                    $$
*i.e., this rotation is equivalent to transposing the image followed by
*flipping $y$
*
*
*\subsection{Rotation by 180 degrees}
*
*$$M_{180} = 
*\left(\begin{array}{rr} -1 & 0 \\
*                        0 & -1\end{array}\right)$$
*i.e., this rotation is equivalent to flipping  both $x$ and $y$
*
*rotation by $-180^\circ$ is equivalent to the $+180^\circ$ rotation.
*
*
*\subsection{Recipe}
*
*The recipe is now obvious. Translate angle $\alpha$ into $-180^\circ$ to
*$+180^\circ$ range. Find to which of $\alpha_i=-180^\circ, -90^\circ, ...
*+180^\circ$ alpha is the closest, rotate by $\alpha_i$ and than by
*$(\alpha-\alpha_i)$ using \S\ref{secrot}.
*
*
*
*\end{document}
