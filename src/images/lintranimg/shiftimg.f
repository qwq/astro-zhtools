      subroutine shiftimg (img,nx,ny,shiftx,shifty,boundval,poisson)
      implicit none
      integer nx,ny
      real img(nx,ny)
      real shiftx, shifty
      real boundval
      logical poisson


      call rotate_img__shiftxy (img,nx,ny,shiftx,shifty,boundval,poisson)
      
      return
      end
