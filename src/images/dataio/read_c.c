#include <stdio.h>
#include <stdlib.h>

void read_c_ (void *p, int *N, char *type, char *name)
{
  FILE *in = fopen(name,"r");
  if (in==NULL) {
    perror (name);
    exit(1);
  }

  if (*type=='b'||*type=='B') { fread (p,*N*sizeof(char),1,in); } 
  else if (*type=='i'||*type=='I') { fread (p,*N*sizeof(short),1,in); } 
  else if (*type=='j'||*type=='J') { fread (p,*N*sizeof(long),1,in); } 
  else if (*type=='e'||*type=='E') { fread (p,*N*sizeof(float),1,in); } 
  else if (*type=='d'||*type=='D') { fread (p,*N*sizeof(double),1,in); } 
  
  fclose(in);
}
