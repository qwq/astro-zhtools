      subroutine quadr_surf_class
     ~    (I,J,D,A,Ap,App,Appp,itype)
      implicit none
      real*8 I,J,D,A,Ap,App,Appp
      integer itype
      
      if (A.ne.0.0d0) then
        
        if (A.gt.0.0d0) then
          
          if (Ap*I.gt.0.0d0.and.J.gt.0.0d0) then
            
            if ( D.ne.0.0d0 ) then
              if (D*I.gt.0.0d0.and.J.gt.0.0d0) then
                itype=1
              else
                itype=0
              endif
            else
              itype=0
            endif
            
          else
            itype=0
          endif
          
          
        else
          
          if (D.ne.0.0d0) then
            if (D*I.gt.0.0d0.and.J.gt.0.0d0) then
              itype=1
            else
              itype=0
            endif
          else
            if (J.gt.0.0d0) then
              itype=1
            else
              itype=0
            endif
          endif
        endif
        
      else                      ! A=0
        if (Ap.ne.0.0d0) then
          if (Ap*I.gt.0.0d0.and.J.gt.0.0d0) then
            itype=0
          else
            if (D.ne.0.0d0) then
              if (D*I.gt.0.0d0.and.J.gt.0.0d0) then
                itype=0
              else
                itype=1
              endif
            else
              if (J.gt.0.0d0) then
                itype=1
              else
                itype=0
              endif
            endif
          endif
        else
          itype=1
        endif
      endif
      
      return
      end
      
        



