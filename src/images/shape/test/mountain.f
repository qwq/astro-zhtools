      implicit none

      character data*200
      character class*200,arg*200
      
      integer lnblnk
      
c image arrays
      integer img(1)
      pointer (pimg,img)
      integer classimg(1)
      pointer (pclassimg,classimg)
      integer wksp(1)
      pointer (pwksp,wksp)
      integer malloc
      
c fitsio staff
      integer iunit,ounit, newunit
      integer nx,ny
      integer status
      
      integer nx1,ny1
      
      integer naxis,naxes(10)
      integer bitpix,pcount,gcount
      logical simple,extend,anyf
      integer hdtype

      double precision bscale,bzero
      
      
      character history*80

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      call get_command_line_par ("img",1,data)
      call get_command_line_par ("out",1,class)


      iunit=newunit()
      status=0
      call open_fits (iunit, data, status)
      if(status.ne.0)then
        write(0,*)'>>> Error opening IMAGE file: ',data(1:lnblnk(data))
        call exit (1)
      endif
      call ftmahd (iunit, 1, hdtype, status) ! Goto the primary array  
      call ftghpr(iunit,10,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~    ,status)              ! Get dimensions

      if (naxis.ne.2)then
        write(0,*)'Not 2-D image in the primary header'
        call exit (1)
      endif
      nx=naxes(1)
      ny=naxes(2)
      nx1=nx
      ny1=ny
      
      pimg=malloc(4*nx*ny)
      pclassimg=malloc(4*nx*ny)
      pwksp=malloc(4*nx*ny)
      if (pimg.eq.0.or.pclassimg.eq.0.or.pwksp.eq.0) then
      
        write (0,*)'Not enough memory'
        call exit (1)
      endif
      
      call ftg2de(iunit,0,0.0,nx,nx,ny,img,anyf,status)
      call ftclos(iunit,status)

      if (status.ne.0) then
        call fitsio_print_error (status)
        write(0,*)'Error reading input image 1'
        call exit (1)
      endif

      call mountain (img,classimg,wksp,nx,ny,5e-6)
      
      end
      
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      
      subroutine mountain (img,class,cond,nx,ny,imgmin)
      implicit none
      integer nx,ny
      real img(nx,ny)
      integer class(nx,ny)
      integer cond(nx,ny)
      integer i,j
      real imgmin
      integer iclass,nclass
      real A,B
      real i11,i12,i13,i21,i22,i23,i31,i32,i33,sum
      real f11,f12,f13,f21,f22,f23,f31,f32,f33
      real tol
      real test
      parameter (tol=1e-4)
      integer npos,nneg
      
      integer npeak,npeakmax,ipeak,mpeak,ntest
      parameter (npeakmax=1000)
      integer xpeak(npeakmax),ypeak(npeakmax)
      integer xpeak0(npeakmax),ypeak0(npeakmax)
      real dist(npeakmax),distmin
      integer idistance
      
      
      logical qnew,if_peak,true
      integer iprint
      
      do i=1,nx
        do j=1,ny
          cond(i,j)=0
        enddo
      enddo

      do i=2,nx-1
        do j=2,ny-1
          if (img(i,j).gt.imgmin) then
            cond(i,j)=1
          endif
        enddo
      enddo
      
*      do i=2,nx-1
*        do j=2,ny-1
*          if (img(i,j).gt.imgmin) then
**            i11=alog(img(i-1,j-1))
**            i12=alog(img(i,j-1))
**            i13=alog(img(i+1,j-1))
**            
**            i21=alog(img(i-1,j))
**            i22=alog(img(i,j))
**            i23=alog(img(i+1,j))
**            
**            i31=alog(img(i-1,j+1))
**            i32=alog(img(i,j+1))
**            i33=alog(img(i+1,j+1))
*            i11=img(i-1,j-1)
*            i12=img(i,j-1)
*            i13=img(i+1,j-1)
*            
*            i21=img(i-1,j)
*            i22=img(i,j)
*            i23=img(i+1,j)
*            
*            i31=img(i-1,j+1)
*            i32=img(i,j+1)
*            i33=img(i+1,j+1)
*            
*            sum=abs((i11+i12+i13+i21+i23+i31+i32+i33)/9)
*            i11=i11/sum
*            i12=i12/sum
*            i13=i13/sum
*            
*            i21=i21/sum
*            i22=i22/sum
*            i23=i23/sum
*            
*            i31=i31/sum
*            i32=i32/sum
*            i33=i33/sum
*            
*          
*
*            A=0.5*(i32-i12)     ! di/dx
*            B=0.5*(i23-i21)     ! di/dy
*                  
*            f11=i22-A-B
*            f12=i22-A
*            f13=i22-A+B
*            f21=i22-B
*            f22=i22
*            f23=i22+B
*            f31=i22+A-B
*            f32=i22+A
*            f33=i22+A+B
*            
*            npos=0
*            nneg=0
*
*            if (abs(f11-i11).gt.tol) then
*              if (f11.lt.i11) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*
*            if (abs(f12-i12).gt.tol) then
*              if (f12.lt.i12) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*            
*            if (abs(f13-i13).gt.tol) then
*              if (f13.lt.i13) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*
*            if (abs(f21-i21).gt.tol) then
*              if (f21.lt.i21) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*
*            if (abs(f23-i23).gt.tol) then
*              if (f23.lt.i23) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*
*            if (abs(f31-i31).gt.tol) then
*              if (f31.lt.i31) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*
*            if (abs(f32-i32).gt.tol) then
*              if (f32.lt.i32) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*
*            if (abs(f33-i33).gt.tol) then
*              if (f33.lt.i33) then
*                nneg=nneg+1
*              else
*                npos=npos+1
*              endif
*            endif
*            
*            if (npos.gt.nneg) then
*              cond(i,j)=1
*            endif
*            
*            if (i.eq.137.and.j.eq.235) then
*              print*,npos,nneg
*            endif
*            
**            test = f11+f12+f13+f21+f22+f23+f31+f32+f33 -
**     ~          (i11+i12+i13+i21+i22+i23+i31+i32+i33)
**            
**            if (test.gt.0.02) then
**              cond(i,j)=1
**            endif
*
*
**            if (
**     ~          (i11.le.f11).and.
**     ~          (i12.le.f12).and.
**     ~          (i13.le.f13).and.
**     ~          (i21.le.f21).and.
**     ~          (i23.le.f23).and.
**     ~          (i31.le.f31).and.
**     ~          (i32.le.f32).and.
**     ~          (i33.le.f33)) then
**              
**              cond(i,j)=1
**            endif
*          endif
*        enddo
*      enddo
*      call saotng (cond,nx,ny,'j')
*       call exiterror('')


     
c // find islands      
      do i=1,nx
        do j=1,ny
          class(i,j)=0
        enddo
      enddo
      
      nclass=0
      do i=2,nx-1
        do j=2,ny-1
          if (cond(i,j).eq.1) then
            if (class(i,j).eq.0) then
              if (if_peak(img,nx,ny,i,j)) then
                nclass=nclass+1
                call classj (cond,class,nx,ny,cond(i,j),nclass,i,j)
              endif
            endif
          endif
        enddo
      enddo
      call saotng (class,nx,ny,'j')
      

      npeak=0
      do i=2,nx-1
        do j=2,ny-1
          if (class(i,j).gt.0) then
            if (if_peak(img,nx,ny,i,j)) then
              npeak=npeak+1
              xpeak0(npeak)=i
              ypeak0(npeak)=j
            endif
          endif
        enddo
      enddo
          
      do iclass=1,nclass
        mpeak=0
        do i=1,npeak
          if (class(xpeak0(i),ypeak0(i)).eq.iclass) then
            mpeak=mpeak+1
            xpeak(mpeak)=xpeak0(i)
            ypeak(mpeak)=ypeak0(i)
          endif
        enddo
        
        do i=1,nx
          do j=1,ny
            if (class(i,j).eq.iclass) then
              distmin=1000000
              do ipeak=1,mpeak
                dist(ipeak)=
     ~              sqrt(float(idistance(i,j,xpeak(ipeak),ypeak(ipeak))))
                if (dist(ipeak).lt.distmin) then
                  distmin=dist(ipeak)
                endif
              enddo
              ntest=0
              do ipeak=1,mpeak
                if (dist(ipeak).lt.distmin+2.0) then
                  ntest=ntest+1
                endif
              enddo
              if (ntest.gt.1) then
                cond(i,j)=0
              endif
            endif
          enddo
        enddo
      enddo
              
c // find islands again
      do i=1,nx
        do j=1,ny
          class(i,j)=0
        enddo
      enddo
      
      nclass=0
      do i=2,nx-1
        do j=2,ny-1
          if (cond(i,j).eq.1) then
            if (class(i,j).eq.0) then
              if (if_peak(img,nx,ny,i,j)) then
                nclass=nclass+1
                call classj (cond,class,nx,ny,cond(i,j),nclass,i,j)
              endif
            endif
          endif
        enddo
      enddo
      call saotng (class,nx,ny,'j')

      return
      end
        

      subroutine saotng (img,nx,ny,type)
      implicit none
      real img(*)
      integer nx,ny
      character type*(*)
      character outname*80
      character command*200
      outname='/tmp/show'
      command='xpaset SAOtng < /tmp/show'
      
      call write_fits_image (outname,img,nx,ny,type,type)
      call system (command)
      call sleep(1)
      return
      end
      
      
      subroutine classj (img,classimg,nx,ny,classval,nclass,i,j)
      implicit none
      integer nx,ny
      integer img(nx,ny)
      integer classimg(nx,ny)
      integer classval
      integer nclass
      
      logical adjacent_pixel_j
      
      integer i,j,ii,jj,iclass,i1,i2,j1,j2

      logical qnew,qstep,qnew1
      integer step
      integer nx1,ny1
      
      iclass=nclass
      classimg(i,j)=iclass
      
      step=1
      qstep=.true.
      
      nx1=nx-1
      ny1=ny-1
      
      do while (qstep)
        
        qstep=.false.
        i1=max(i-step,2)
        i2=min(i+step,nx1)
        j1=max(j-step,2)
        j2=min(j+step,ny1)
*        i1=max1(i1,1)
*        j1=max1(j1,1)
*        i2=min1(i2,nx)
*        j2=min1(j2,ny)
        
*        qnew=.true.
        
        do ii=i1,i2
          do jj=j1,j2
            if (classimg(ii,jj).eq.0.and.img(ii,jj).eq.classval) then
              if (
     ~            classimg(ii,jj-1).eq.iclass. or.
     ~            classimg(ii-1,jj).eq.iclass. or.
     ~            classimg(ii+1,jj).eq.iclass. or.
     ~            classimg(ii,jj+1).eq.iclass) then
                qstep=.true.
                if (img(ii,jj).eq.classval) then
                  classimg(ii,jj)=iclass
                endif
              endif
            endif
          enddo
        enddo

        step=step+1
        
      enddo
      
          
      return
      end
