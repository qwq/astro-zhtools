/*
 * fill.c : one page seed fill program, 1 channel frame buffer version
 *
 * doesn't read each pixel twice like the BASICFILL algorithm in
 *      Alvy Ray Smith, "Tint Fill", SIGGRAPH '79
 *
 * Paul Heckbert        13 Sept 1982, 28 Jan 1987
 */

typedef int pixel;

int wx1, wx2, wy1, wy2;  /* screen window */
int *map; int forarray;

struct seg {short y, xl, xr, dy;};      /* horizontal segment of scan line y */
#define MAX 10000               /* max depth of stack */

#define PUSH(Y, XL, XR, DY) \
    if (sp<stack+MAX && Y+(DY)>=wy1 && Y+(DY)<=wy2) \
    {sp->y = Y; sp->xl = XL; sp->xr = XR; sp->dy = DY; sp++;}

#define POP(Y, XL, XR, DY) \
    {sp--; Y = sp->y+(DY = sp->dy); XL = sp->xl; XR = sp->xr;}



/* ---------------------------------------------------------------------- */
void  ini_fill (int *img, int nx, int ny, int for)
{ /* if for=1, the array is in the fortran order */
  wx1=0; wx2=nx-1; 
  wy1=0; wy2=ny-1; 
  map = img;
  forarray = 1;
}

void ini_fill_ (int *img, int *nx, int *ny, int *for)
{
  ini_fill (img, *nx, *ny, *for);
}
/* ---------------------------------------------------------------------- */
  
/* ---------------------------------------------------------------------- */
pixel pixelread (int x, int y)
{
  if (forarray) return map[nx*y+x];
  else return map[ny*x+y];
}

void pixelwrite (int x, int y, int nv)
{
  if (forarray) map[nx*y+x]=nv;
  else map[ny*x+y]=nv;
}
/* ---------------------------------------------------------------------- */

void fill_ (int *x, int *y, int *nv)
{
  fill (*x-1,*y-1,*nv);
}

/*
 * fill: set the pixel at (x,y) and all of its 4-connected neighbors
 * with the same pixel value to the new pixel value nv.
 * A 4-connected neighbor is a pixel above, below, left, or right of a pixel.
 */
fill(x, y, nv)
int x, y;       /* seed point */
pixel nv;       /* new pixel value */
{
    int l, x1, x2, dy;
    pixel ov;   /* old pixel value */
    struct seg stack[MAX], *sp = stack; /* stack of filled segments */

    ov = pixelread(x, y);               /* read pv at seed point */
    if (ov==nv || x<wx1 || x>wx2 || y<wy1 || y>wy2) return;
    PUSH(y, x, x, 1);                   /* needed in some cases */
    PUSH(y+1, x, x, -1);                /* seed segment (popped 1st) */

    while (sp>stack) {
        /* pop segment off stack and fill a neighboring scan line */
        POP(y, x1, x2, dy);
        /*
         * segment of scan line y-dy for x1<=x<=x2 was previously filled,
         * now explore adjacent pixels in scan line y
         */
        for (x=x1; x>=wx1 && pixelread(x, y)==ov; x--)
            pixelwrite(x, y, nv);
        if (x>=x1) goto skip;
        l = x+1;
        if (l<x1) PUSH(y, l, x1-1, -dy);                /* leak on left? */
        x = x1+1;
        do {
            for (; x<=wx2 && pixelread(x, y)==ov; x++)
                pixelwrite(x, y, nv);
            PUSH(y, l, x-1, dy);
            if (x>x2+1) PUSH(y, x2+1, x-1, -dy);        /* leak on right? */
skip:       for (x++; x<=x2 && pixelread(x, y)!=ov; x++);
            l = x;
        } while (x<=x2);
    }
}


