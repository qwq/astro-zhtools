#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

long lnblnk_ (char*);
int nint (float x);
char *shift (char *p, char *word);
int pnpoly (int n, float *x, float *y, float xx, float yy);

int make_sector (float *img, int nx, int ny, 
		  float xx, float yy, float r1, float r2,
		  float a1, float a2, float value);

int make_region_image_0 (float *img, int nx, int ny, FILE *src);

int circle_find_imin_imax (float r, int rr, int j, int x, int nx,
			   int *I_min, int *I_max);

void make_region_image_ (float *img, long *nx0, long *ny0,char *region,
			 int *ns,float *val0)
{
  int i,n;
  int rmfile;
  FILE *src;
  char filename[100];
  int nx,ny;
  float value;
  float *nimg;
  
  region[*ns]='\0';


  nx=*nx0;
  ny=*ny0;
  value=*val0;
  nimg=img;
  
  n=nx*ny;
  
  for(i=0;i<n;i++)
    nimg[i]=value;
  
  if (strcmp(region,"none")!=0)
    {
      /* determine if src is described in a file */  
      if (strncmp(region,"file:",5)==0)
	{
	  rmfile=0;
	  strcpy(filename,region+5);
	}
      else
	/* write the string to a temporary file */
	{
	  sprintf(filename,"/tmp/.region.%d\0",getpid());
	  rmfile=1;
	  src=fopen(filename,"w");
	  if (src==NULL)
	    {
	      fprintf(stderr,"Failed to open for writing: %s\n",filename);
	      exit(1);
	    }
	  fprintf(src,"%s",region);
	  fclose(src);
	}
      if (strcmp(filename,"-")==0) {
	src = stdin;
      } else {
	src=fopen(filename,"r");
      }
      if (src==NULL)
	{
	  fprintf(stderr,"Failed to open file: %s\n",filename);
	  exit(1);
	}
      
      make_region_image_0 (nimg,nx,ny,src);
      fclose(src);
      if (rmfile==1)
	unlink(filename);
    } 

  /*  saoimage_(img,nx0,ny0,"e\0"); */

}








int make_region_image_0 (float *img, int nx, int ny, FILE *src)
{
  int i,j,ii;
  float xx,yy;
  float r1,r2;
  int x,y,rr1,rr2,rr;
  float r;
  char type[100];
  int dist;
  float a1,a2;
  float value,junk;
  int xc,yc;
  float ax,ay,angle,cosa,sina,dx,dy,deltax,deltay;
  float sidex,sidey;
  char buff[2000];
  int nread;
  int qsaoimage;
  int exclude, firsttype;
  char word[80], *p;
  float xpoly[1000], ypoly[1000];
  int npoly,imin,imax,jmin,jmax;
  int wi;
  float wr;

  while (fgets(buff,1000,src)!=NULL)
    {
      if (buff[0]!='#')
	{
	  qsaoimage=saoimage2reg(buff);
	  if (sscanf(buff,"%s",type)==1)
	    {
	      if ( type[0] == '+' || type[0] == '-' ) {
		if ( type[0]=='-' )
		  exclude = 1;
		else
		  exclude = 0;
		firsttype = 1;
	      }
	      else {
		exclude = 0;
		firsttype = 0;
	      }

	      if (strncmp(type+firsttype,"poly",4)==0)
		type[firsttype]='Y';

	      /*	      printf ("region: %c\n",type[firsttype]); */

	      switch (type[firsttype])
		{
		  
		case 'p': /* single pixel */
		  {
		    nread=sscanf(buff,"%s %f %f %f",type,&xx,&yy,&value);
		    if (nread<3||nread>4)
		      {
			fprintf(stderr,
			   "wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    else if (nread==3)
		      {
			value=1;
		      }
		    if (exclude) value = 0;
		    x=nint(xx);
		    y=nint(yy);
		    i=x+(nx)*(y-1)-1;
		    if ( x >=1 && x <= nx && y >=1 && y <=ny ) 
		      img[i]=value;
		    continue;
		  }

		case 'c': /* circle */
		  {
		    nread=sscanf(buff,"%s %f %f %f %f",type,&xx,&yy,&r,&value);
		    if (nread<4||nread>5)
		      {
			fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    else if (nread==4)
		      {
			value=1;
		      }
		    if (exclude) value = 0;
		    rr=r*r;
		    x=nint(xx);
		    y=nint(yy);
		    for (j=-r;j<=r;j++)
		      if (y+j>=1&&y+j<=ny) {
			wi = x+(nx)*(y+j-1)-1;
			circle_find_imin_imax (r,rr,j,x,nx,&imin,&imax);
			for (i=imin;i<=imax;i++)
			  {
			    ii=wi+i;
			    img[ii]=value;
			  }
		      }
		    continue;
		  }
		  
		case 'b': /* box */
		  {
		    printf ("|%s|\n",buff);
		    if (sscanf(buff,"%s %f %f %e %e %e %e",
			       type,&xx,&yy,&sidex,&sidey,&angle,&value)!=7)
		      {
			if (sscanf(buff,"%s %f %f %e %e %e",
				   type,&xx,&yy,&sidex,&sidey,&junk)==6)
			  {
			    /*if (qsaoimage) {value=1; angle=junk;}
			      else           {value=junk;angle=0;}*/
			    value=junk;angle=0;
			  }
			else
			  {
			    fprintf(stderr,
				    "wrong structure of region descriptor\n");
			    exit(2);
			  }
		      }
		    if (exclude) value = 0;
		    xc=nint(xx);
		    yc=nint(yy);
		    rr=sqrt(sidex*sidex/4+sidey*sidey/4)+1;
		    sina=sin(angle*3.1415926536/180.0);
		    cosa=cos(angle*3.1415926536/180.0);
		    for (i=xc-rr;i<=xc+rr;i++)
		      for (j=yc-rr;j<=yc+rr;j++)
			{
			  dx=i-xc;
			  dy=j-yc;
			  deltax=dx*cosa+dy*sina;
			  deltay=-dx*sina+dy*cosa;
			  if (deltax<0) deltax=-deltax;
			  if (deltay<0) deltay=-deltay;
			  
			  if (deltax<=sidex/2&&deltay<=sidey/2)
			    {
			      if (i>=1&&i<=nx&&j>=1&&j<=ny)
				{
				  ii=i+(nx)*(j-1)-1;
				  img[ii]=value;
				  /*if (ii<0||ii>nx*ny-1)
				    fprintf (stderr,"!!! box\n"); */
				}
			    }
			}
		    
		    continue;
		  }

		case 'e': /* ellipse */
		  {
		    if (sscanf(buff,
			       "%s %f %f %e %e %e %e",
			       type,&xx,&yy,&ax,&ay,&angle,&value)!=7)
		      {
			if (sscanf(buff,"%s %f %f %e %e %e",
				   type,&xx,&yy,&sidex,&sidey,&junk)==6)
			  {
			    /*			    if (qsaoimage) {value=1; angle=junk;}
						    else           {value=junk;angle=0;} */
			    value=junk;angle=0;
			  }
			else
			  {
			    fprintf(stderr,
				    "wrong structure of region descriptor %s\n",type);
			    exit(2);
			  }
		      }
		    if (exclude) value = 0;
		    xc=nint(xx);
		    yc=nint(yy);
		    rr=ay;
		    if(ax>ay)rr=ax;
		    sina=sin(angle*3.1415926536/180.0);
		    cosa=cos(angle*3.1415926536/180.0);
		    for (i=xc-rr;i<=xc+rr;i++)
		      for (j=yc-rr;j<=yc+rr;j++)
			{
			  dx=i-xc;
			  dy=j-yc;
			  deltax=dx*cosa+dy*sina;
			  deltay=-dx*sina+dy*cosa;
			  if ((deltax/ax)*(deltax/ax)
			      +(deltay/ay)*(deltay/ay)<1)
			    {
			      if (i>=1&&i<=nx&&j>=1&&j<=ny)
				{
				  ii=i+(nx)*(j-1)-1;
				  img[ii]=value;
				  /*if (ii<0||ii>nx*ny-1)
				    fprintf (stderr,"!!! ellipse\n");*/
				}
			    }
			}
		    continue;
		  }

		case 'a': /*annulus*/
		  {
		    if (sscanf(buff,"%s %f %f %f %f %e",
			       type,&xx,&yy,&r1,&r2,&value)!=6)
		      {
			fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    if (exclude) value = 0;
		    x=nint(xx);
		    y=nint(yy);
		    rr1=r1*r1;
		    rr2=r2*r2;
		    for (i=-r2;i<=r2;i++)
		      for (j=-r2;j<=r2;j++)
			{
			  dist=i*i+j*j;
			  if (dist<=rr2 && dist > rr1)
			    {
			      if (x+i>=1&&x+i<=nx&&y+j>=1&&y+j<=ny)
				{
				  ii=(x+i)+(nx)*(y+j-1)-1;
				  img[ii]=value;
				}
			    }
			}
		    continue;
		  }
		  
		case 's': /*sector*/
		  {
		    if (sscanf(buff,"%s %f %f %f %f %e %e %e",
			       type,&xx,&yy,&r1,&r2,&a1,&a2,&value)!=8)
		      {
			fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    
		    if (exclude) value = 0;

		    while (a2>360.0)
		      a2-=360.0;
		    while (a1>360.0)
		      a1-=360.0;

		    if (a2<a1)
		      {
			/* the user wants to go through 0 */
			make_sector (img,nx,ny,xx,yy,r1,r2,a1,360.0,value);
			make_sector (img,nx,ny,xx,yy,r1,r2,0.0,a2,value);
		      }
		    else
		      {
			make_sector (img,nx,ny,xx,yy,r1,r2,a1,a2,value);
		      }
		    continue;
		  }
		case 'Y': /* polygon */
		  {
		    p = shift(buff,type);
		    npoly = 0;
		    i=0;
		    while ( (p = shift(p,word)) != NULL) {
		      if (sscanf(word,"%f",&value)!=1) break;
		      i++;
		      if ((i/2)*2==i) {
			ypoly[npoly]=value;
			npoly++;
		      } else {
			xpoly[npoly]=value;
		      }
		    }
		    if (npoly*2==i) {
		      value = xpoly[npoly-1];
		      npoly--;
		    } else {
		      value = 1;
		    }
		    if (npoly<2) {
		      fprintf(stderr,
			      "less than 3 vertices in the polynomial\n");
		      exit(2);
		    }
		    if (exclude) value = 0;
		    
		    imin = nx; imax = 1; jmin = ny; jmax=1;
		    for (i=0;i<npoly;i++) {
		      if (imin>xpoly[i]) imin = floor(xpoly[i]);
		      if (imax<xpoly[i]) imax = floor(xpoly[i])+1;
		      if (jmin>ypoly[i]) jmin = floor(ypoly[i]);
		      if (jmax<ypoly[i]) jmax = floor(ypoly[i])+1;
		    }
		    if (imin<1) imin=1;   if (imax>nx) imax=nx;
		    if (jmin<1) jmin=1;   if (jmax>ny) jmax=ny;
		    /*xpoly[npoly]=xpoly[0]; ypoly[npoly]=ypoly[0]; npoly++;*/

		    for (i=imin;i<=imax;i++)
		      for (j=jmin;j<=jmax;j++)
			{
			  xx = i; yy=j;
			  
			  if (pnpoly(npoly,xpoly,ypoly,xx,yy))
			    {
			      ii=i+(nx)*(j-1)-1;
			      img[ii]=value;
			    }
			}
		    continue;
		  }
		      
		default:
		  fprintf(stderr,"unkown region type: %s\n",type),exit(2);
		}
	    }
	}
    }
  /*  saoimage_(img,&nx,&ny,"e\0"); */
  return img[0];
}

int make_sector (float *img, int nx, int ny, 
		  float xx, float yy, float r1, float r2,
		  float a1, float a2, float value)
{
  int rr1,rr2,x,y;
  int i,j,ii,dist;
  int quse;
  float a;

  rr1=nint(r1*r1);
  rr2=nint(r2*r2);
  x=nint(xx);
  y=nint(yy);
  for (i=-r2;i<=r2;i++)
    for (j=-r2;j<=r2;j++)
      {
	dist=i*i+j*j;
	if (dist<=rr2 && dist > rr1)
	  {
	    quse=0;
	    if (dist == 0)
	      {
		quse=1;
	      }
	    else
	      {
		a=acos((i+0.0)/sqrt(dist+0.0));
		if (j<0)
		  {
		    a=2.0*3.1415926536-a;
		  }
		a=a*180.0/3.1415926536;
		if (a2==360.0)
		  {
		    if ((a>a1 && a<=a2)||a==0)
		      {
			quse=1;
		      }
		  }
		else
		  if (a>a1 && a<=a2)
		    {
		      quse=1;
		    }
		
	      }
	    if (quse==1)
	      {
		if (x+i>=1&&x+i<=nx&&y+j>=1&&y+j<=ny)
		  {
		    ii=(x+i)+(nx)*(y+j-1)-1;
		    img[ii]=value;
		  }
	      }
	  }
      }
  return 0;
}
  


int saoimage2reg (char *string)
{

  int n,i;
  if (strchr(string,'(')==NULL)
    return 0;
  
  n=strlen(string);
  if (string[n-1]=='\n') {
    string[n-1]='\0';
    n--;
  }

  for (i=0;i<n;i++)
    {
      switch (string[i])
	{
	case '(':
	  {
	    string[i]=' ';
	    break;
	  }
	case ')':
	  {
	    string[i]=' ';
	    break;
	  }
	case ',':
	  {
	    string[i]=' ';
	    break;
	  }
	case '+':
	  {
	    string[i]=' ';
	    break;
	  }
	default: string[i]=tolower(string[i]);
	}
    }
  strcat (string,"  1");
  return 1;
}

int nint (float x)
{
  register int i;
  i = floor(x);
  if (x-i>0.5) 
    return i+1;
  else
    return i;
}


char *shift (char *p, char *word)
{
  char *s;
  int i;
  
  s=p;
  /* skip leading spaces */
  while (*s){
    if (!isspace(*s)) break;
    s++;
  }

  /* if at end of string, return NULL */
  if ( !(*s) ) return NULL;
  
  i = 0;
  while (*s){
    if (isspace(*s)) break;
    word[i]=*s;
    i++;
    s++;
  }
  word[i]='\0';
  return s;
}
  

int circle_find_imin_imax (float r, int rr, int j, int x, int nx,
			   int *I_min, int *I_max)
{
  float wr;
  int imin,imax;
  wr = r*r-j*j; if (wr<0.0) {wr = 0.0;} else {wr = sqrt(wr);}
  imin = -wr-1;
  while (imin*imin+j*j>rr) imin++;
  imax = wr+1;
  while (imax*imax+j*j>rr) imax--;
  
  if (x+imin<1) imin = 1-x;
  if (x+imax>nx) imax = nx-x;
  *I_min = imin; *I_max=imax;
  return 1;
}
