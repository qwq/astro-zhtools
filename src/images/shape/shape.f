      implicit none
      character arg*80
      integer n,k, lnblnk
      
      call getarg(0,arg)

      n = lnblnk(arg)

      k = max(1,n-7)
      if (arg(k:n).eq.'classify') then
        call classify
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'island') then
        call classify
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'mkedge') then
        call mkedge
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'setval') then
        call setval
        call exit(0)
      endif

      k = max(1,n-4)
      if (arg(k:n).eq.'mkreg') then
        call mkreg
        call exit(0)
      endif

      write(0,*) 'Use as classify or island, mkcond, mkedge, setval, or mkreg'
      call exit(1)
      
      end
      
