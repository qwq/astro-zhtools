#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

int TOWCS;

double tr_fcrval1, tr_fcrval2, tr_fcrpix1, tr_fcrpix2, 
  tr_fcdelt1, tr_fcdelt2,  tr_frot, 
  tr_tcrval1, tr_tcrval2, tr_tcrpix1, tr_tcrpix2, 
  tr_tcdelt1, tr_tcdelt2, tr_trot;
char tr_ftype[20], tr_ttype[20];

int trreg_translate_box (float x, float y, float w, float h, float a,
			  float *xn, float *yn, float *wn, float *hn, float *an
			  );

int trreg_scale_to_scale (float xold, float yold, float sizeold, float aold,
			  float *sizenew);

int trreg_ang_to_ang (float xold, float yold, float aold, float *anew);

int trreg_pix_to_pix (float xold, float yold, float *xnew, float *ynew);

int trreg_pix_to_sky (double xpix, double ypix, double xref, double yref,
		      double xrefpix, double yrefpix, 
		      double xinc, double yinc, double rot,
		      char *type, double *xpos, double *ypos, int *status);

int trreg_sky_to_pix (double xpos, double ypos, double xref, double yref, 
		      double xrefpix, double yrefpix, 
		      double xinc, double yinc, double rot,
		      char *type, double *xpix, double *ypix, int *status);

/************************************************************************/

/* short_format only applies to polygon output to avoid excessively long lines */

int trans_reg_string_ (char *regstring, int *nreg,
		       double *fcrval1, double *fcrval2,
		       double *fcrpix1, double *fcrpix2,
		       double *fcdelt1, double *fcdelt2,
		       double *frot, char *ftype,
		       double *tcrval1, double *tcrval2,
		       double *tcrpix1, double *tcrpix2,
		       double *tcdelt1, double *tcdelt2,
		       double *trot, char *ttype, int *printvalues,
		       int *towcs, int *short_format)
{
  /*
    subroutine trans_reg_string (regstring,len(regstring),
    ~	fcrval1, fcrval2, fcrpix1, fcrpix2, fcdelt1, fcdelt2, fcrota2, ftype,
    ~	tcrval1, tcrval2, tcrpix1, tcrpix2, tcdelt1, tcdelt2, tcrota2, ttype)
  */
  
  char *r,*p,*d;
  char type[21000];
  char buff[21000],wstr[80];
  float x,y,xnew,ynew,x1,x2,y1,y2,x1new,x2new,y1new,y2new;
  float w,h,r1,r2,r1new,r2new,wnew,hnew,a,anew,a1,a2,a1new,a2new;
  float r1in,r2in,r1out,r2out,ain,aout;
  float r1innew,r2innew,r1outnew,r2outnew,ainnew,aoutnew;
  float xv[21000],yv[21000]; int nv;
  float value;
  char prefix[2];
  int nread,i;
  int prval, globprval;
  int sectorformat_zhtools;
  char format[80], wcsstr[80], szunit[2];

  TOWCS = *towcs;
  if (TOWCS) {
    strcpy(wcsstr,"fk5;");
    strcpy(szunit,"\"");
  } 
  else {
    wcsstr[0]='\0';
    szunit[0]='\0';
  }


  globprval=*printvalues;

  regstring[*nreg-1]='\0';

  /* Copy wcs to external vars */
  tr_fcrval1 = *fcrval1; tr_fcrval2 = *fcrval2; 
  tr_fcrpix1 = *fcrpix1; tr_fcrpix2 = *fcrpix2;
  tr_fcdelt1 = *fcdelt1; tr_fcdelt2 = *fcdelt2;
  tr_frot=*frot; strncpy(tr_ftype,ftype,4); tr_ftype[4]='\0';
  tr_tcrval1 = *tcrval1; tr_tcrval2 = *tcrval2; 
  tr_tcrpix1 = *tcrpix1; tr_tcrpix2 = *tcrpix2;
  tr_tcdelt1 = *tcdelt1; tr_tcdelt2 = *tcdelt2;
  tr_trot=*trot; strncpy(tr_ttype,ftype,4); tr_ttype[4]='\0';

  /*
    fprintf (stderr,"%g %g %g %s ",tr_fcrval1,tr_fcrpix1,tr_fcdelt1,tr_ftype);
    fprintf (stderr,"%g %g %g %g\n",tr_fcrval2,tr_fcrpix2,tr_fcdelt2,tr_frot);
    fprintf (stderr,"%g %g %g %s ",tr_tcrval1,tr_tcrpix1,tr_tcdelt1,tr_ttype);
    fprintf (stderr,"%g %g %g %g\n",tr_tcrval2,tr_tcrpix2,tr_tcdelt2,tr_trot);
  */
  r = regstring;

  /* Skip leading blanks */
  while (isspace(*r) && *r != '\0' ) r++;
  if (*r == '\0') { /* Empty string; return */
    return 0;
  }
  
  /* skip indicator of include/exclude */
  if ( *r == '-' || *r == '+' )
    {
      prefix[0]=*r; prefix[1]='\0';
      r += 1;
    }
  else
    prefix[0]='\0';
  

  
  /* If first non-blank is not a letter, return */
  if ( ! isalpha (*r) ) return 0;

  /* Find the first word */
  p = r+1;
  while ( isalpha(*p) ) p++;
  strncpy(type,r,p-r); type[p-r]='\0';
  strcpy(buff,p);

  /* Parse Region type */
  prval = globprval;

  if ( strcasecmp (type,"point") == 0 ||
       strcasecmp (type,"p") == 0 ) {
    /* POINT */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e", &x,&y,&value);
    switch (nread) {
    case 2: prval=0; value=1; break;
    case 3: break;
    default: return 0;
    }
    trreg_translate_box (x,y,1.0,1.0,0.0,
			 &xnew,&ynew,&wnew,&hnew,&anew);
    strcpy(format,"%s%sbox(%f,%f,%f%s,%f%s,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,prefix,xnew,ynew,wnew,szunit,hnew,szunit,
	    anew,value);
    return 1;
  }

  if ( strcasecmp (type,"line") == 0 ) {
    /* LINE */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e", &x1,&y1, &x2, &y2, &value);
    switch (nread) {
    case 4: prval=0; value=1; break;
    case 5: break;
    default:  return 0;
    }
    trreg_pix_to_pix (x1,y1,&x1new,&y1new);
    trreg_pix_to_pix (x2,y2,&x2new,&y2new);
    strcpy(format,"%s%sline(%f,%f,%f,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,prefix,x1new,y1new,x2new,y2new,value);
    return 1;
  }
  
  if ( strcasecmp (type,"polygon") == 0 ) {
    /* POLYGON */
    trreg_rem_signs (buff);
    nv = 0; d=buff;
    while (1) {
      xv[nv] = strtod(d,&p);
      if (p==d) {
	prval=0; value=1;
	break;
      }
      d = p;
      yv[nv] = strtod(d,&p);
      if (p==d) {
	value = xv[nv];
	break;
      }
      d = p;
      nv ++;
    }
    if (nv<3) return 0;
    
    strcpy(buff,"polygon(");
    for (i=0;i<nv;i++) {
      trreg_pix_to_pix (xv[i],yv[i],&xnew,&ynew);
      if (*short_format) {
	sprintf(wstr,"%.2f,%.2f",xnew,ynew);
      } else {
	sprintf(wstr,"%f,%f",xnew,ynew);
      }
      if (i>0) strcat(buff,",");
      strcat (buff,wstr);
    }
    if (prval && globprval)
      sprintf(wstr,") %f\0",value);
    else
      strcpy(wstr,")");
    strcat (buff,wstr);
    strcpy(regstring,wcsstr); 
    strcat(regstring,prefix); 
    strcat(regstring,buff); 
    
    return 1;
  }

  if ( strcasecmp (type,"rectangle") == 0 ||
       strcasecmp (type,"rotrectangle") == 0) {
    /* RECTANGLE */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e %e", &x1,&y1,&x2,&y2,&a,&value);
    switch (nread) {
    case 4: a=0; prval=0; value=1; break;
    case 5: prval=0; value=1; break;
    case 6: break;
    default: return 0;
    }
    trreg_pix_to_pix (x1,y1,&x1new,&y1new);
    trreg_pix_to_pix (x2,y2,&x2new,&y2new);
    trreg_ang_to_ang (x1,x2,a,&anew);
    strcpy (format,"%s%srectangle(%f,%f,%f,%f,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,prefix,x1new,y1new,x2new,y2new,anew,value);
    return 1;
  }
  
  if ( strcasecmp (type,"box") == 0 ||
       strcasecmp (type,"b") == 0 ||
       strcasecmp (type,"rotbox") == 0 ) {
    /* BOX */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e %e", &x,&y,&w,&h,&a,&value);
    switch (nread) {
    case 4: a=0; prval=0; value=1; break;
    case 5: prval=0; value=1; break;
    case 6: break;
    default: return 0;
    }
    trreg_translate_box (x,y,w,h,a,
			 &xnew,&ynew,&wnew,&hnew,&anew);
    strcpy (format,"%s%sbox(%f,%f,%f%s,%f%s,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,prefix,xnew,ynew,wnew,szunit,hnew,szunit,
	    anew,value);
    return 1;
  }
  
  if ( strcasecmp (type,"diamond") == 0    ||
       strcasecmp (type,"rotdiamond") == 0 ||
       strcasecmp (type,"rhombus") == 0    ||
       strcasecmp (type,"rotrhombus") == 0
       ) {
    /* Rhombus */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e %e", &x,&y,&w,&h,&a,&value);
    switch (nread) {
    case 4: a=0; prval=0; value=1; break;
    case 5: prval=0; value=1; break;
    case 6: break;
    default: return 0;
    }
    /* Translation is the same for box and rhombus */
    trreg_translate_box (x,y,w,h,a,
			 &xnew,&ynew,&wnew,&hnew,&anew);
    strcpy (format,"%s%sdiamond(%f,%f,%f%s,%f%s,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,prefix,xnew,ynew,wnew,szunit,hnew,szunit,
	    anew,value);
    return 1;
  }

  if ( strcasecmp (type,"circle") == 0 ||
       strcasecmp (type,"c") == 0
       ) {
    /* CIRCLE */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e", &x,&y,&r1,&value);
    switch (nread) {
    case 3: prval=0; value=1; break;
    case 4: break;
    default:  return 0;
    }
    trreg_pix_to_pix (x,y,&xnew,&ynew);
    trreg_scale_to_scale (x,y,r1,0.0,&w);
    trreg_scale_to_scale (x,y,r1,90.0,&h);
    if (abs(w-h)<0.01*(w+h)/2.0) {
      strcpy (format,"%s%scircle(%f,%f,%f%s)");
      if (prval && globprval) strcat (format," %g");
      sprintf(regstring,format,wcsstr,prefix,xnew,ynew,h,szunit,value);
    }
    else {
      strcpy (format,"%s%sellipse(%f,%f,%f%s,%f%s,0.0)");
      if (prval && globprval) strcat (format," %g");
      sprintf(regstring,format,wcsstr,prefix,xnew,ynew,w,szunit,h,szunit,
	      value);
    }
    return 1;
  }

  if ( strcasecmp (type,"ellipse") == 0 ||
       strcasecmp (type,"e") == 0
       ) {
    /* ELLIPSE */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e %e", &x,&y,&w,&h,&a,&value);
    switch (nread) {
    case 4: a=0; prval=0; value=1; break;
    case 5: prval=0; value=1; break;
    case 6: break;
    default:  return 0;
    }
    trreg_pix_to_pix (x,y,&xnew,&ynew);
    trreg_ang_to_ang (x,y,a,&anew);
    trreg_scale_to_scale (x,y,w,a,&wnew);
    trreg_scale_to_scale (x,y,h,a+90.0,&hnew);
    strcpy (format,"%s%sellipse(%f,%f,%f%s,%f%s,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,prefix,xnew,ynew,wnew,szunit,hnew,szunit
	    ,anew,value);
    return 1;
  }
  
  if ( strcasecmp (type,"annulus") == 0 ||
       strcasecmp (type,"a") == 0
       ) {
    /* ANNULUS */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e", &x,&y,&r1,&r2,&value);
    switch (nread) {
    case 4: prval=0; value=1; break;
    case 5: break;
    default:  return 0;
    }
    trreg_pix_to_pix (x,y,&xnew,&ynew);
    trreg_scale_to_scale (x,y,r2,0.0,&w);
    trreg_scale_to_scale (x,y,r2,90.0,&h);
    if (abs(w-h)<0.01*(w+h)/2.0) {
      strcpy (format,"%s%sannulus(%f,%f,%f%s,%f%s)");
      if (prval && globprval) strcat (format," %g");
      sprintf(regstring,format,wcsstr,prefix,xnew,ynew,w*r1/r2,szunit,
	      w,szunit,value);
    }
    else{
      strcpy (format,"%s%selliptannulus(%f,%f,%f%s,%f%s,%f%s,%f%s,0.0,0.0)");
      if (prval && globprval) strcat (format," %g");
      sprintf(regstring,format,wcsstr,prefix,xnew,ynew,w*r1/r2,szunit,
	      h*r1/r2,szunit,w,szunit,h,szunit,value);
    }
    return 1;
  }

  if ( strcasecmp (type,"elliptannulus") == 0 ) {
    /* ELLIPT ANNULUS */
    trreg_rem_signs (buff);
    nread = sscanf (buff,"%e %e %e %e %e %e %e %e %e", 
		    &x,&y,&r1in,&r2in,&r1out,&r2out,&ain,&aout,&value);
    switch (nread) {
    case 6: ain=0; aout=0; prval=0; value=1; break;
    case 7: aout=ain; prval=0; value=1; break;
    case 8: prval=0; value=1; break;
    case 9: break;
    default:  return 0;
    }
    trreg_pix_to_pix (x,y,&xnew,&ynew);
    trreg_ang_to_ang (x,y,ain,&ainnew);
    trreg_ang_to_ang (x,y,aout,&aoutnew);
    trreg_scale_to_scale (x,y,r1in,ain,&r1innew);
    trreg_scale_to_scale (x,y,r2in,ain+90.0,&r2innew);
    trreg_scale_to_scale (x,y,r1out,aout,&r1outnew);
    trreg_scale_to_scale (x,y,r2out,aout+90.0,&r2outnew);
    strcpy (format,"%s%selliptannulus(%f,%f,%f%s,%f%s,%f%s,%f%s,%f,%f)");
    if (prval && globprval) strcat (format," %g");
    sprintf(regstring,format,wcsstr,
	    prefix,xnew,ynew,r1innew,szunit,r2innew,szunit,r1outnew,szunit,
	    r2outnew,szunit,ainnew,aoutnew,value);
    return 1;
  }
  
  if ( strcasecmp (type,"sector") == 0 ||
       strcasecmp (type,"s") == 0
       ) {
    /* SECTOR */
    trreg_rem_signs (buff);
    sectorformat_zhtools = 1;
    nread = sscanf (buff,"%e %e %e %e %e %e %e", &x,&y,&r1,&r2,&a1,&a2,&value);
    switch (nread) {
    case 4: prval=0; value=1; a1=r1; a2=r2; sectorformat_zhtools = 0; 
      break; /* CFITSIO syntax */
    case 6: prval=0; value=1; break;
    case 7: break;
    default:  return 0;
    }
    trreg_pix_to_pix (x,y,&xnew,&ynew);
    trreg_ang_to_ang (x,y,a1,&a1new);
    trreg_ang_to_ang (x,y,a2,&a2new);
    trreg_scale_to_scale (x,y,r1,0.5*(a1+a2),&r1new);
    trreg_scale_to_scale (x,y,r2,0.5*(a1+a2),&r2new);
    if (sectorformat_zhtools) {
      strcpy (format,"%s%ssector(%f,%f,%f%s,%f%s,%f,%f)");
      if (prval && globprval) strcat (format," %g");
      sprintf(regstring,format,wcsstr,prefix,xnew,ynew,r1new,szunit,r2new,
	      szunit,a1new,a2new,value);
    } else {
      strcpy (format,"%s%ssector(%f,%f,%f,%f)");
      if (prval && globprval) strcat (format," %g");
      sprintf(regstring,format,wcsstr,prefix,xnew,ynew,a1new,a2new,value);
    }
    return 1;
  }
  return 0;
}

/*--------------------------------------------------------------------------*/
int trreg_translate_box (float x, float y, float w, float h, float a,
			  float *xn, float *yn, float *wn, float *hn, float *an
			  )
{
  trreg_pix_to_pix (x,y,xn,yn);
  trreg_ang_to_ang (x,y,a,an);
  trreg_scale_to_scale (x,y,w,a,wn);
  trreg_scale_to_scale (x,y,h,a+90.0,hn);
  return 1;
}


/*--------------------------------------------------------------------------*/
int trreg_scale_to_scale (float xold, float yold, float sizeold, float aold,
			  float *sizenew)
{
  double deg2rad = 3.14159265358979/180.0;
  float x1new,y1new,x2new,y2new;
  float x,y; double dx,dy;

  if (TOWCS) {
    *sizenew = sizeold * 
      sqrt((tr_fcdelt1*tr_fcdelt1+tr_fcdelt2*tr_fcdelt2)/2.0) * 
      3600.0;
      return 0;
  } else {
    trreg_pix_to_pix (xold,yold,&x1new,&y1new);
    x = xold+sizeold*cos(aold*deg2rad); y=yold+sizeold*sin(aold*deg2rad);
    trreg_pix_to_pix (x,y,&x2new,&y2new);
    dx = x2new-x1new; dy = y2new - y1new;
    *sizenew=sqrt(dx*dx+dy*dy);
    return 1;
  }
}

/*--------------------------------------------------------------------------*/
int trreg_ang_to_ang (float xold, float yold, float aold, float *anew)
{
  double deg2rad = 3.14159265358979/180.0;
  float x1new,y1new,x2new,y2new;
  float x,y; double dx,dy;
  if (TOWCS) {
    *anew = aold + tr_frot;
    return 1;
  }

  trreg_pix_to_pix (xold,yold,&x1new,&y1new);
  x = xold+cos(aold*deg2rad); y=yold+sin(aold*deg2rad);
  trreg_pix_to_pix (x,y,&x2new,&y2new);
  dx = x2new-x1new; dy = y2new - y1new;
  *anew = atan2(dy,dx);
  *anew = *anew/deg2rad;
  if (*anew<0) *anew = *anew+360;
  return 1;
}
  
/*--------------------------------------------------------------------------*/
int trreg_pix_to_pix (float xold, float yold, float *xnew, float *ynew)
{
  double xpix,ypix,xpos,ypos;
  int status;
  xpix = xold;
  ypix = yold;
  status = 0;
  trreg_pix_to_sky (xpix, ypix, tr_fcrval1, tr_fcrval2,
		    tr_fcrpix1, tr_fcrpix2, tr_fcdelt1, tr_fcdelt2,
		    tr_frot, tr_ftype, &xpos, &ypos, &status);
  if (status != 0) {
    *xnew=xold; *ynew=yold; return 1;
  }
  
  if (TOWCS) {
    *xnew = xpos;
    *ynew = ypos;
    return 0;
  }
  
  trreg_sky_to_pix (xpos, ypos, tr_tcrval1, tr_tcrval2,
		    tr_tcrpix1, tr_tcrpix2, tr_tcdelt1, tr_tcdelt2,
		    tr_trot, tr_ttype, &xpix, &ypix, &status);
  if (status != 0) {
    *xnew=xold; *ynew=yold; return 1;
  }
  *xnew=xpix; *ynew=ypix;
  return 0;
}

/*--------------------------------------------------------------------------*/
int trreg_rem_signs (char *string)
{
  int n,i;
  n=strlen(string);
  for (i=0;i<n;i++)
    {
      switch (string[i])
        {
        case '(':  string[i]=' '; break;
        case ')':  string[i]=' '; break;
        case ',':  string[i]=' '; break;
        default: ;
        }
    }
  return 1;
}


/*--------------------------------------------------------------------------*/
int trreg_pix_to_sky (double xpix, double ypix, double xref, double yref,
      double xrefpix, double yrefpix, double xinc, double yinc, double rot,
      char *type, double *xpos, double *ypos, int *status)

/*  worldpos.c -- WCS Algorithms from Classic AIPS.
    Copyright (C) 1994
    Associated Universities, Inc. Washington DC, USA.
   
    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.
   
    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.
   
    You should have received a copy of the GNU Library General Public License
    along with this library; if not, write to the Free Software Foundation,
    Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
   
    Correspondence concerning AIPS should be addressed as follows:
           Internet email: aipsmail@nrao.edu
           Postal address: AIPS Group
                           National Radio Astronomy Observatory
                           520 Edgemont Road
                           Charlottesville, VA 22903-2475 USA

                 -=-=-=-=-=-=-

    These two ANSI C functions, worldpos() and xypix(), perform
    forward and reverse WCS computations for 8 types of projective
    geometries ("-SIN", "-TAN", "-ARC", "-NCP", "-GLS", "-MER", "-AIT"
    and "-STG"):

        worldpos() converts from pixel location to RA,Dec 
        xypix()    converts from RA,Dec         to pixel location   

    where "(RA,Dec)" are more generically (long,lat). These functions
    are based on the WCS implementation of Classic AIPS, an
    implementation which has been in production use for more than ten
    years. See the two memos by Eric Greisen

        ftp://fits.cv.nrao.edu/fits/documents/wcs/aips27.ps.Z
	ftp://fits.cv.nrao.edu/fits/documents/wcs/aips46.ps.Z

    for descriptions of the 8 projective geometries and the
    algorithms.  Footnotes in these two documents describe the
    differences between these algorithms and the 1993-94 WCS draft
    proposal (see URL below). In particular, these algorithms support
    ordinary field rotation, but not skew geometries (CD or PC matrix
    cases). Also, the MER and AIT algorithms work correctly only for
    CRVALi=(0,0). Users should note that GLS projections with yref!=0
    will behave differently in this code than in the draft WCS
    proposal.  The NCP projection is now obsolete (it is a special
    case of SIN).  WCS syntax and semantics for various advanced
    features is discussed in the draft WCS proposal by Greisen and
    Calabretta at:
    
        ftp://fits.cv.nrao.edu/fits/documents/wcs/wcs.all.ps.Z
    
                -=-=-=-

    The original version of this code was Emailed to D.Wells on
    Friday, 23 September by Bill Cotton <bcotton@gorilla.cv.nrao.edu>,
    who described it as a "..more or less.. exact translation from the
    AIPSish..". Changes were made by Don Wells <dwells@nrao.edu>
    during the period October 11-13, 1994:
    1) added GNU license and header comments
    2) added testpos.c program to perform extensive circularity tests
    3) changed float-->double to get more than 7 significant figures
    4) testpos.c circularity test failed on MER and AIT. B.Cotton
       found that "..there were a couple of lines of code [in] the wrong
       place as a result of merging several Fortran routines." 
    5) testpos.c found 0h wraparound in xypix() and worldpos().
    6) E.Greisen recommended removal of various redundant if-statements,
       and addition of a 360d difference test to MER case of worldpos(). 
*/

/*-----------------------------------------------------------------------*/
/* routine to determine accurate position for pixel coordinates          */
/* returns 0 if successful otherwise:                                    */
/* 1 = angle too large for projection;                                   */
/* (WDP 1/97: changed the return value to 501 instead of 1)              */
/* does: -SIN, -TAN, -ARC, -NCP, -GLS, -MER, -AIT projections            */
/* anything else is linear                                               */
/* Input:                                                                */
/*   f   xpix    x pixel number  (RA or long without rotation)           */
/*   f   ypiy    y pixel number  (dec or lat without rotation)           */
/*   d   xref    x reference coordinate value (deg)                      */
/*   d   yref    y reference coordinate value (deg)                      */
/*   f   xrefpix x reference pixel                                       */
/*   f   yrefpix y reference pixel                                       */
/*   f   xinc    x coordinate increment (deg)                            */
/*   f   yinc    y coordinate increment (deg)                            */
/*   f   rot     rotation (deg)  (from N through E)                      */
/*   c  *type    projection type code e.g. "-SIN";                       */
/* Output:                                                               */
/*   d   *xpos   x (RA) coordinate (deg)                                 */
/*   d   *ypos   y (dec) coordinate (deg)                                */
/*-----------------------------------------------------------------------*/
 {double cosr, sinr, dx, dy, dz, temp;
  double sins, coss, dect, rat, dt, l, m, mg, da, dd, cos0, sin0;
  double dec0, ra0, decout, raout;
  double geo1, geo2, geo3;
  double cond2r=1.745329252e-2;
  double twopi = 6.28318530717959, deps = 1.0e-5;
  int   i, itype;
  char ctypes[8][5] ={"-SIN","-TAN","-ARC","-NCP", "-GLS", "-MER", "-AIT",
     "-STG"};

  if (*status > 0)
     return(*status);

/*   Offset from ref pixel  */
  dx = (xpix-xrefpix) * xinc;
  dy = (ypix-yrefpix) * yinc;
/*   Take out rotation  */
  cosr = cos(rot*cond2r);
  sinr = sin(rot*cond2r);
  if (rot!=0.0)
    {temp = dx * cosr - dy * sinr;
     dy = dy * cosr + dx * sinr;
     dx = temp;}
/*  find type  */
/* WDP 1/97: removed support for default type for better error checking */
/*  itype = 0;   default type is linear */
  itype = -1;  /* no default type */
  for (i=0;i<8;i++) if (!strncmp(type, ctypes[i], 4)) itype = i+1;
/* default, linear result for error return  */
  *xpos = xref + dx;
  *ypos = yref + dy;
/* convert to radians  */
  ra0 = xref * cond2r;
  dec0 = yref * cond2r;
  l = dx * cond2r;
  m = dy * cond2r;
  sins = l*l + m*m;
  cos0 = cos(dec0);
  sin0 = sin(dec0);
/* process by case  */
  switch (itype) {
    case 0:   /* linear */
      rat =  ra0 + l;
      dect = dec0 + m;
      break;
    case 1:   /* -SIN sin*/ 
      if (sins>1.0) return(*status = 501);
      coss = sqrt (1.0 - sins);
      dt = sin0 * coss + cos0 * m;
      if ((dt>1.0) || (dt<-1.0)) return(*status = 501);
      dect = asin (dt);
      rat = cos0 * coss - sin0 * m;
      if ((rat==0.0) && (l==0.0)) return(*status = 501);
      rat = atan2 (l, rat) + ra0;
      break;
    case 2:   /* -TAN tan */
      if (sins>1.0) return(*status = 501);
      dect = cos0 - m * sin0;
      if (dect==0.0) return(*status = 501);
      rat = ra0 + atan2 (l, dect);
      dect = atan (cos(rat-ra0) * (m * cos0 + sin0) / dect);
      break;
    case 3:   /* -ARC Arc*/
      if (sins>=twopi*twopi/4.0) return(*status = 501);
      sins = sqrt(sins);
      coss = cos (sins);
      if (sins!=0.0) sins = sin (sins) / sins;
      else
	sins = 1.0;
      dt = m * cos0 * sins + sin0 * coss;
      if ((dt>1.0) || (dt<-1.0)) return(*status = 501);
      dect = asin (dt);
      da = coss - dt * sin0;
      dt = l * sins * cos0;
      if ((da==0.0) && (dt==0.0)) return(*status = 501);
      rat = ra0 + atan2 (dt, da);
      break;
    case 4:   /* -NCP North celestial pole*/
      dect = cos0 - m * sin0;
      if (dect==0.0) return(*status = 501);
      rat = ra0 + atan2 (l, dect);
      dt = cos (rat-ra0);
      if (dt==0.0) return(*status = 501);
      dect = dect / dt;
      if ((dect>1.0) || (dect<-1.0)) return(*status = 501);
      dect = acos (dect);
      if (dec0<0.0) dect = -dect;
      break;
    case 5:   /* -GLS global sinusoid */
      dect = dec0 + m;
      if (fabs(dect)>twopi/4.0) return(*status = 501);
      coss = cos (dect);
      if (fabs(l)>twopi*coss/2.0) return(*status = 501);
      rat = ra0;
      if (coss>deps) rat = rat + l / coss;
      break;
    case 6:   /* -MER mercator*/
      dt = yinc * cosr + xinc * sinr;
      if (dt==0.0) dt = 1.0;
      dy = (yref/2.0 + 45.0) * cond2r;
      dx = dy + dt / 2.0 * cond2r;
      dy = log (tan (dy));
      dx = log (tan (dx));
      geo2 = dt * cond2r / (dx - dy);
      geo3 = geo2 * dy;
      geo1 = cos (yref*cond2r);
      if (geo1<=0.0) geo1 = 1.0;
      rat = l / geo1 + ra0;
      if (fabs(rat - ra0) > twopi) return(*status = 501); /* added 10/13/94 DCW/EWG */
      dt = 0.0;
      if (geo2!=0.0) dt = (m + geo3) / geo2;
      dt = exp (dt);
      dect = 2.0 * atan (dt) - twopi / 4.0;
      break;
    case 7:   /* -AIT Aitoff*/
      dt = yinc*cosr + xinc*sinr;
      if (dt==0.0) dt = 1.0;
      dt = dt * cond2r;
      dy = yref * cond2r;
      dx = sin(dy+dt)/sqrt((1.0+cos(dy+dt))/2.0) -
	  sin(dy)/sqrt((1.0+cos(dy))/2.0);
      if (dx==0.0) dx = 1.0;
      geo2 = dt / dx;
      dt = xinc*cosr - yinc* sinr;
      if (dt==0.0) dt = 1.0;
      dt = dt * cond2r;
      dx = 2.0 * cos(dy) * sin(dt/2.0);
      if (dx==0.0) dx = 1.0;
      geo1 = dt * sqrt((1.0+cos(dy)*cos(dt/2.0))/2.0) / dx;
      geo3 = geo2 * sin(dy) / sqrt((1.0+cos(dy))/2.0);
      rat = ra0;
      dect = dec0;
      if ((l==0.0) && (m==0.0)) break;
      dz = 4.0 - l*l/(4.0*geo1*geo1) - ((m+geo3)/geo2)*((m+geo3)/geo2) ;
      if ((dz>4.0) || (dz<2.0)) return(*status = 501);;
      dz = 0.5 * sqrt (dz);
      dd = (m+geo3) * dz / geo2;
      if (fabs(dd)>1.0) return(*status = 501);;
      dd = asin (dd);
      if (fabs(cos(dd))<deps) return(*status = 501);;
      da = l * dz / (2.0 * geo1 * cos(dd));
      if (fabs(da)>1.0) return(*status = 501);;
      da = asin (da);
      rat = ra0 + 2.0 * da;
      dect = dd;
      break;
    case 8:   /* -STG Sterographic*/
      dz = (4.0 - sins) / (4.0 + sins);
      if (fabs(dz)>1.0) return(*status = 501);
      dect = dz * sin0 + m * cos0 * (1.0+dz) / 2.0;
      if (fabs(dect)>1.0) return(*status = 501);
      dect = asin (dect);
      rat = cos(dect);
      if (fabs(rat)<deps) return(*status = 501);
      rat = l * (1.0+dz) / (2.0 * rat);
      if (fabs(rat)>1.0) return(*status = 501);
      rat = asin (rat);
      mg = 1.0 + sin(dect) * sin0 + cos(dect) * cos0 * cos(rat);
      if (fabs(mg)<deps) return(*status = 501);
      mg = 2.0 * (sin(dect) * cos0 - cos(dect) * sin0 * cos(rat)) / mg;
      if (fabs(mg-m)>deps) rat = twopi/2.0 - rat;
      rat = ra0 + rat;
      break;

    default:
      /* fall through to here on error */
      return(*status = 504);
  }

/*  return ra in range  */
  raout = rat;
  decout = dect;
  if (raout-ra0>twopi/2.0) raout = raout - twopi;
  if (raout-ra0<-twopi/2.0) raout = raout + twopi;
  if (raout < 0.0) raout += twopi; /* added by DCW 10/12/94 */

/*  correct units back to degrees  */
  *xpos  = raout  / cond2r;
  *ypos  = decout  / cond2r;
  return(*status);
}  /* End of worldpos */
/*--------------------------------------------------------------------------*/
int trreg_sky_to_pix (double xpos, double ypos, double xref, double yref, 
      double xrefpix, double yrefpix, double xinc, double yinc, double rot,
      char *type, double *xpix, double *ypix, int *status)

/*-----------------------------------------------------------------------*/
/* routine to determine accurate pixel coordinates for an RA and Dec     */
/* returns 0 if successful otherwise:                                    */
/* 1 = angle too large for projection;                                   */
/* 2 = bad values                                                        */
/* WDP 1/97: changed the return values to 501 and 502 instead of 1 and 2 */
/* does: -SIN, -TAN, -ARC, -NCP, -GLS, -MER, -AIT projections            */
/* anything else is linear                                               */
/* Input:                                                                */
/*   d   xpos    x (RA) coordinate (deg)                                 */
/*   d   ypos    y (dec) coordinate (deg)                                */
/*   d   xref    x reference coordinate value (deg)                      */
/*   d   yref    y reference coordinate value (deg)                      */
/*   f   xrefpix x reference pixel                                       */
/*   f   yrefpix y reference pixel                                       */
/*   f   xinc    x coordinate increment (deg)                            */
/*   f   yinc    y coordinate increment (deg)                            */
/*   f   rot     rotation (deg)  (from N through E)                      */
/*   c  *type    projection type code e.g. "-SIN";                       */
/* Output:                                                               */
/*   f  *xpix    x pixel number  (RA or long without rotation)           */
/*   f  *ypiy    y pixel number  (dec or lat without rotation)           */
/*-----------------------------------------------------------------------*/
 {double dx, dy, dz, r, ra0, dec0, ra, dec, coss, sins, dt, da, dd, sint;
  double l, m, geo1, geo2, geo3, sinr, cosr;
  double cond2r=1.745329252e-2, deps=1.0e-5, twopi=6.28318530717959;
  int   i, itype;
  char ctypes[8][5] ={"-SIN","-TAN","-ARC","-NCP", "-GLS", "-MER", "-AIT",
     "-STG"};

  /* 0h wrap-around tests added by D.Wells 10/12/94: */
  dt = (xpos - xref);
  if (dt >  180) xpos -= 360;
  if (dt < -180) xpos += 360;
  /* NOTE: changing input argument xpos is OK (call-by-value in C!) */

/* default values - linear */
  dx = xpos - xref;
  dy = ypos - yref;
/*  dz = 0.0; */
/*  Correct for rotation */
  r = rot * cond2r;
  cosr = cos (r);
  sinr = sin (r);
  dz = dx*cosr + dy*sinr;
  dy = dy*cosr - dx*sinr;
  dx = dz;
/*     check axis increments - bail out if either 0 */
  if ((xinc==0.0) || (yinc==0.0)) {*xpix=0.0; *ypix=0.0; return(*status = 502);}
/*     convert to pixels  */
  *xpix = dx / xinc + xrefpix;
  *ypix = dy / yinc + yrefpix;

/*  find type  */
/* WDP 1/97: removed support for default type for better error checking */
/*  itype = 0;   default type is linear */
  itype = -1;  /* no default type */
  for (i=0;i<8;i++) if (!strncmp(type, ctypes[i], 4)) itype = i+1;
  if (itype==0) return(*status);  /* done if linear */

/* Non linear position */
  ra0 = xref * cond2r;
  dec0 = yref * cond2r;
  ra = xpos * cond2r;
  dec = ypos * cond2r;

/* compute direction cosine */
  coss = cos (dec);
  sins = sin (dec);
  l = sin(ra-ra0) * coss;
  sint = sins * sin(dec0) + coss * cos(dec0) * cos(ra-ra0);
/* process by case  */
  switch (itype) {
    case 1:   /* -SIN sin*/ 
         if (sint<0.0) return(*status = 501);
         m = sins * cos(dec0) - coss * sin(dec0) * cos(ra-ra0);
      break;
    case 2:   /* -TAN tan */
         if (sint<=0.0) return(*status = 501);
 	 m = sins * sin(dec0) + coss * cos(dec0) * cos(ra-ra0);
	 l = l / m;
	 m = (sins * cos(dec0) - coss * sin(dec0) * cos(ra-ra0)) / m;
      break;
    case 3:   /* -ARC Arc*/
         m = sins * sin(dec0) + coss * cos(dec0) * cos(ra-ra0);
         if (m<-1.0) m = -1.0;
         if (m>1.0) m = 1.0;
         m = acos (m);
         if (m!=0) 
            m = m / sin(m);
         else
            m = 1.0;
         l = l * m;
         m = (sins * cos(dec0) - coss * sin(dec0) * cos(ra-ra0)) * m;
      break;
    case 4:   /* -NCP North celestial pole*/
         if (dec0==0.0) 
	     return(*status = 501);  /* can't stand the equator */
         else
	   m = (cos(dec0) - coss * cos(ra-ra0)) / sin(dec0);
      break;
    case 5:   /* -GLS global sinusoid */
         dt = ra - ra0;
         if (fabs(dec)>twopi/4.0) return(*status = 501);
         if (fabs(dec0)>twopi/4.0) return(*status = 501);
         m = dec - dec0;
         l = dt * coss;
      break;
    case 6:   /* -MER mercator*/
         dt = yinc * cosr + xinc * sinr;
         if (dt==0.0) dt = 1.0;
         dy = (yref/2.0 + 45.0) * cond2r;
         dx = dy + dt / 2.0 * cond2r;
         dy = log (tan (dy));
         dx = log (tan (dx));
         geo2 = dt * cond2r / (dx - dy);
         geo3 = geo2 * dy;
         geo1 = cos (yref*cond2r);
         if (geo1<=0.0) geo1 = 1.0;
         dt = ra - ra0;
         l = geo1 * dt;
         dt = dec / 2.0 + twopi / 8.0;
         dt = tan (dt);
         if (dt<deps) return(*status = 502);
         m = geo2 * log (dt) - geo3;
         break;
    case 7:   /* -AIT Aitoff*/
         da = (ra - ra0) / 2.0;
         if (fabs(da)>twopi/4.0) return(*status = 501);
         dt = yinc*cosr + xinc*sinr;
         if (dt==0.0) dt = 1.0;
         dt = dt * cond2r;
         dy = yref * cond2r;
         dx = sin(dy+dt)/sqrt((1.0+cos(dy+dt))/2.0) -
             sin(dy)/sqrt((1.0+cos(dy))/2.0);
         if (dx==0.0) dx = 1.0;
         geo2 = dt / dx;
         dt = xinc*cosr - yinc* sinr;
         if (dt==0.0) dt = 1.0;
         dt = dt * cond2r;
         dx = 2.0 * cos(dy) * sin(dt/2.0);
         if (dx==0.0) dx = 1.0;
         geo1 = dt * sqrt((1.0+cos(dy)*cos(dt/2.0))/2.0) / dx;
         geo3 = geo2 * sin(dy) / sqrt((1.0+cos(dy))/2.0);
         dt = sqrt ((1.0 + cos(dec) * cos(da))/2.0);
         if (fabs(dt)<deps) return(*status = 503);
         l = 2.0 * geo1 * cos(dec) * sin(da) / dt;
         m = geo2 * sin(dec) / dt - geo3;
      break;
    case 8:   /* -STG Sterographic*/
         da = ra - ra0;
         if (fabs(dec)>twopi/4.0) return(*status = 501);
         dd = 1.0 + sins * sin(dec0) + coss * cos(dec0) * cos(da);
         if (fabs(dd)<deps) return(*status = 501);
         dd = 2.0 / dd;
         l = l * dd;
         m = dd * (sins * cos(dec0) - coss * sin(dec0) * cos(da));
      break;

    default:
      /* fall through to here on error */
      return(*status = 504);

  }  /* end of itype switch */

/*   back to degrees  */
  dx = l / cond2r;
  dy = m / cond2r;
/*  Correct for rotation */
  dz = dx*cosr + dy*sinr;
  dy = dy*cosr - dx*sinr;
  dx = dz;
/*     convert to pixels  */
  *xpix = dx / xinc + xrefpix;
  *ypix = dy / yinc + yrefpix;
  return(*status);
}  /* end xypix */
