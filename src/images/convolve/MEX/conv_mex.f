      subroutine conv_mexsquare (rimg,nx,ny,scale)
      implicit undefined (a-z)
      integer nx,ny
      real rimg(nx,ny)
      real scale
      integer nmax
      parameter (nmax=2048)
      pointer (pmex,mex)
      real mex
      complex speqi(nmax),speqm(nmax)
      
      integer malloc
      
      pmex=malloc(4*nx*ny)
      if(pmex.eq.0)then
         call exiterror('can not allocate memory in conv_mex')
      endif
      
      call conv_mexsquare_ini (mex,speqm,nx,ny,scale)
      

      call rlft2(rimg,speqi,nx,ny,1)
      call cmplx_mult(rimg,mex,nx*ny/2)
      call cmplx_mult(speqi,speqm,ny)
      call rlft2(rimg,speqi,nx,ny,-1)
      call mtrx_smult(rimg,nx*ny,1.0/scale)
      call free(pmex)
      return
      end
      
      
      
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine conv_mex (rimg,nx,ny,scale)
      implicit undefined (a-z)
      integer nx,ny
      real rimg(nx,ny)
      real scale
      integer nmax
      parameter (nmax=2048)
      pointer (pmex,mex)
      real mex
      complex speqi(nmax),speqm(nmax)
      
      integer malloc
      
      pmex=malloc(4*nx*ny)
      if(pmex.eq.0)then
         call exiterror('can not allocate memory in conv_mex')
      endif
      
      call conv_mex_ini (mex,speqm,nx,ny,scale)

      call rlft2(rimg,speqi,nx,ny,1)
      call cmplx_mult(rimg,mex,nx*ny/2)
      call cmplx_mult(speqi,speqm,ny)
      call rlft2(rimg,speqi,nx,ny,-1)
      call mtrx_smult(rimg,nx*ny,1.0/scale)
      call free(pmex)
      return
      end



*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine conv_mex_ini (mex,speqm,nx,ny,scale)
      implicit undefined (a-z)
      integer nx,ny
      real mex(nx,ny),scale,fac
      complex speqm(ny)
      integer rr,rmax,idistance
      integer nx2,ny2,i,j
      
      nx2=nx/2+1
      ny2=ny/2+1
      rmax=40*scale**2+1
      
      do i=1,nx
        do j=1,ny
          rr=idistance(i,j,nx2,ny2)
          if(rr.le.rmax)then
            mex(i,j)=(2.0-rr/scale**2)*exp(-0.5*rr/scale**2)
          else
            mex(i,j)=0.
          endif
        enddo
      enddo
      call diffr_w(mex,nx,ny)
      call rlft2(mex,speqm,nx,ny,1)
      fac=2.0/float(nx*ny)
      
      call mtrx_smult(mex,nx*ny,fac)
      call mtrx_smult(speqm,ny*2,fac)
      return
      end




*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine conv_mexsquare_ini (mex,speqm,nx,ny,scale)
      implicit undefined (a-z)
      integer nx,ny
      real mex(nx,ny),scale,fac
      complex speqm(ny)
      integer rr,rmax,idistance
      integer nx2,ny2,i,j
      
      nx2=nx/2+1
      ny2=ny/2+1
      rmax=40*scale**2+1
      
      do i=1,nx
        do j=1,ny
          rr=idistance(i,j,nx2,ny2)
          if(rr.le.rmax)then
            mex(i,j)=((2.0-rr/scale**2)*exp(-0.5*rr/scale**2))**2
          else
            mex(i,j)=0.
          endif
        enddo
      enddo
      call diffr_w(mex,nx,ny)
      call rlft2(mex,speqm,nx,ny,1)
      fac=2.0/float(nx*ny)
      
      call mtrx_smult(mex,nx*ny,fac)
      call mtrx_smult(speqm,ny*2,fac)
      return
      end




