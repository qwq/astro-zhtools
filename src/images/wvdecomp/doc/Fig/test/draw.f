      implicit none
      real x(1000), y(1000), f(1000)
      real img(512,512)
      real tr(6)
      integer n, i
      
      call plotsize(3.0+3.0/8,3.+3.0/8)

      call startplot ('wvdecomp_a','hardcopy')
      call notation(-4,4,-4,4)
      call location(600,3590,600,3590)
      call limits (-75.0, 75.0, -75.0, 75.0)
      call ticksize (.0,0.0,.0,0.0)

      call read_fits_image ('img',img,512,512,'e')
      
      tr(1)=-255.0
      tr(2)=1
      tr(3)=0
      tr(4)=-255.0
      tr(5)=0
      tr(6)=1

      call pggray (img,512,512,1,512,1,512,100.0,0.0,tr)

      call box(1,2,0,0)
      call xlabel ('Xpix')
      call ylabel ('Ypix')

      call endplot

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      call startplot ('wvdecomp_b','hardcopy')
      call limits (-75.0, 75.0, 1.0, 5.0)
      call notation(-4,4,-1,1)
      call ticksize (.0,0.0,-1.0,10.0)

      call datafile ('img.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call histogram (x,y,n)

      call datafile ('g1.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('blue')
      call connect (x,y,n)

      call datafile ('g2.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('red')
      call connect (x,y,n)
      
      call datafile ('img.atrous.5.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('green')
      call connect (x,y,n)

      call datafile ('img.atrous.6.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('green')
      call connect (x,y,n)


      call ctype ('black')
      call box(1,2,0,0)
      call xlabel ('Xpix')
      call ylabel ('Ypix')

      call endplot

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      call startplot ('wvdecomp_c','hardcopy')
      call limits (-75.0, 75.0, 1.0, 5.0)
      call notation(-4,4,-1,1)
      call ticksize (.0,0.0,-1.0,10.0)

      call datafile ('img.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call histogram (x,y,n)

      call datafile ('wv.1.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('red')
      call connect (x,y,n)

      call datafile ('wv.2.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('red')
      call connect (x,y,n)

      call datafile ('wv.3.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('red')
      call connect (x,y,n)

      call datafile ('wv.4.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('blue')
      call connect (x,y,n)

      call datafile ('wv.5.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('blue')
      call connect (x,y,n)

      call datafile ('wv.6.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('blue')
      call connect (x,y,n)


      call ctype ('black')
      call box(1,2,0,0)
      call xlabel ('Xpix')
      call ylabel ('Ypix')

      call endplot

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      call startplot ('wvdecomp_d','hardcopy')
      call limits (-75.0, 75.0, 1.0, 5.0)
      call notation(-4,4,-1,1)
      call ticksize (.0,0.0,-1.0,10.0)

      call datafile ('img.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call histogram (x,y,n)

      call datafile ('wv.p.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('red')
      call connect (x,y,n)

      call datafile ('wv.e.proj','1 2')
      call readfile (n,x,y)
      do i=1,n
        x(i)=x(i)-256
        y(i)=log10(max(y(i),0.01))
      enddo
      call ctype ('blue')
      call connect (x,y,n)

      call ctype ('black')
      call box(1,2,0,0)
      call xlabel ('Xpix')
      call ylabel ('Ypix')

      call endplot

      end
