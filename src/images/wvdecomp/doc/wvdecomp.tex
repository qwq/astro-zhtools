\documentclass{article}
\usepackage{graphicx}
\usepackage{avdefs}
\usepackage{floatflt}
\usepackage{psfrag}

\renewcommand{\topfraction}{.9}
\renewcommand{\bottomfraction}{.7}
\renewcommand{\textfraction}{.1}
\renewcommand{\floatpagefraction}{.66}
\renewcommand{\dbltopfraction}{.66}
\renewcommand{\dblfloatpagefraction}{.66}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}

\tolerance=400

\begin{document}

\title{Wavelet decomposition code}
\author{Alexey Vikhlinin\\avikhlinin@cfa.harvard.edu}
\maketitle

\section{Introduction}

The introduction to the wavelet analysis and its application to the
astronomical images can be found elsewhere. We point the reader to the work
of Grebenev et al.\ \cite{greb:a1367} who use the Mexican Hat to detect and
analyze the small scale structure in A1367. Another interesting work is that
of Slezak et al.\ \cite{slezak:wavelet:restoration}; they discuss an
iterative image restoration using the \emph{\`a trous}\/ wavelet
transform.  Both works have useful bibliographies.

\section{Problem}
\label{gov}

\begin{figure}[htbp]
  \begin{minipage}{0.48\textwidth}
    \vskip -10pt
    \centerline{\includegraphics[width=0.99\textwidth]{Fig/wvdecomp_a.ps}}
    \vskip -15pt
    \caption{Sample image}\label{fig:wvdec:image}
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\textwidth}
    \vskip -10pt
    \centerline{\includegraphics[width=0.99\textwidth]{Fig/wvdecomp_b.ps}}
    \vskip -15pt
    \caption{Simple wavelet analysis}\label{fig:wv:simple}
  \end{minipage}
\end{figure}

The problem solved by the wavelet decomposition can be illustrated by a
sample image containing two sources of very different size
(Fig.~\ref{fig:wvdec:image}), bright narrow ``point source'' and the
fainter, wide ``cluster''. Our goal is to detect both sources and possibly
decompose the image into the original components. These are the tasks that
stimulated the use of the wavelet transform for the astronomical image
analysis. In traditional variants of the wavelet analysis, one convolves the
data with special filters designed to emphasize structures of a particular
size. The traditional method works well in a simple case of isolated
sources, but fails for the test image.  Figure~\ref{fig:wv:simple} shows the
slice of the surface brightness of the test image convolved with the Mexican
Hat filter of different scale (the slice is computed in a band shown by
dashed lines in Fig.~\ref{fig:wvdec:image}). The convolution is dominated
by the point source, and the cluster remains undetected.


The problem of eliminating the contribution of narrow sources to the
large-scale emission, which is being solved by our algorithm, can lead to
significant improvements in understanding the structures in real
astrophysical images. For example, a narrow filamentary structure in the
Coma cluster was not detected by either isophotal analysis
\cite{white:coma:image93} or simple wavelet analysis \cite{v:coma}, but was
found \cite{v:coma:tail} using the wavelet decomposition technique described
here. 



\section{Wavelet Decomposition: The Idea}
\label{sec:kernel}
The wavelet kernel we use is called \textit{\'a trous}. It is described in
detail, for example, by Starck \& Murtagh \cite{starck:atrous:94}. On scale
$i$, this kernel ($k_i$) equals the difference of two functions, $f_i$ and
$f_{i+1}$, each \emph{very approximately}\/ represented by a Gaussian of
width $2^{i-1}$ ($f_1$ is the $\delta$-function).  The convolution
of an image with kernel $k_i$ emphasizes structures with the characteristic
size $\approx 2^{i-1}$.

\begin{figure}[tbp]
  \vskip -10pt
  \centerline{\includegraphics[width=0.75\textwidth]{Fig/wvdecomp_c.ps}}
  \vskip -15pt
  \caption{Wavelet decomposition. Solid curves show flux at scales from 1
    (the narrowest) to 6 (the broadest) }\label{fig:wv:decomp}
\end{figure}

On the largest scale, $n$ the kernel $k_n=f_n$. The original image $I(x,y)$
is very simply related to its convolutions, $w_i$ (called ``wavelet planes''
below), with kernels $k_i$:
\begin{equation}
  \label{eq:wv:restore}
  I(x,y)=\sum_{i=1}^n w_i(x,y).
\end{equation}
Therefore, we can speak of $w_i$ as images
containing ``flux'' on scale $i$; sum of all fluxes yields the original
image. This is the basis of the wavelet decomposition algorithm. The image
is convolved with the wavelet of the first scale. All insignificant features
are zeroed, and the remaining, significant, ones are subtracted from the
data. After subtraction, we turn to the next scale. In other words, we
subtract all significant small-scale features from the data before working
at large scales.  Clearly, this algorithm greatly reduces the interference
of the point source in Fig.~\ref{fig:wvdec:image} with the large-scale
kernels. The example is shown in Fig.~\ref{fig:wv:decomp}. Small-scale
kernels are sensitive to the point source. It gets almost completely
subtracted at scale 3. At larger scales, only cluster flux is left in the
data. The cluster is therefore easily detected by large-scale kernels
(scales 4, 5, and 6).

\begin{figure}[tbp]
  \vskip -10pt
  \centerline{\includegraphics[width=0.75\textwidth]{Fig/wvdecomp_d.ps}}
  \vskip -15pt
  \caption{Restoration of the original image. Solid curves show the combined 
    scales 1--3 and 4--6}\label{fig:wv:cluster:point}
\end{figure}
The described algorithm involved some cleaning of the wavelet planes, i.e.\ 
zeroing all features that are below some predefined
threshold. Interestingly, equation~(\ref{eq:wv:restore}) remains almost
valid if thresholds are chosen correctly. Moreover, we can restore
individual structures of different sizes by combining the appropriate
wavelet planes. For example, Fig.~\ref{fig:wv:cluster:point} shows that the
sum of scales 1--3 and 4--6 almost perfectly restores the original
components in the data.

\section{Thresholding}

\subsection{Detection thresholds}\label{sec:tmax}
The detection thresholds are conceptually very simple. Roughly speaking, we
convolve the image with a filter and want to know the level above which all
maxima in the convolution are statistically significant. This level is
called the detection threshold. To derive the detection threshold, one
should calculate the level that is rarely exceeded in the convolved images
containing just noise.

\subsubsection{Gaussian noise}


The simplest case is the noise with the Gaussian distribution and constant
\emph{rms}\/ across the image. First, consider the Gaussian noise with
\emph{rms}=1. The distribution of image values upon convolution with the
kernel $k_i$ is also Gaussian with the \emph{rms} $\sigma_i$. The values of
$\sigma_i$ for each scale $i$ can be calculated analytically as
\begin{equation}
  \label{eq:threshold:gauss}
  \sigma_i = \left(\sum_{x,y} k_i^2(x,y)\right)^{1/2}.
\end{equation}
For the \emph{\`a trous}\/ kernels, the values of $\sigma_i$ are given in
Table. 
\begin{floatingtable}{
  \begin{tabular}{|cc|}
    \hline
    $i$ & $\sigma_i$\\
    \hline
    1 & 0.89080 \\
    2 & 0.20066 \\
    3 & 0.08551 \\
    4 & 0.04122 \\
    5 & 0.02042 \\
    ... & ... \\
    $j+1$ & $\approx\sigma_j/2$ \\
    \hline
  \end{tabular}}
\end{floatingtable}
Next, consider the uniform Gaussian noise with the \emph{rms}\/ level $n$.
Since the convolution with the wavelet kernel is a linear transformation,
the \emph{rms}\/ level in the convolved image is $n\sigma_i$. The $t$ sigma
significance detection threshold is at the level of $tn\sigma_i$.
Thus, the detection threshold determination is straightforward for the
uniform Gaussian noise.

Suppose now that the image noise is Gaussian, but its \emph{rms}\/ varies
across the image, $n=n(x,y)$. In this case one can still use the $t\hat
n\sigma_i$ relation to calculate the detection threshold, where $\hat n$ is
related to the convolution of the noise pattern with the wavelet kernel as
\begin{equation}
  \label{eq:effnoise}
  \hat n(x,y)=\left(k^2_i \otimes n^2(x,y)\right)^{1/2}.
\end{equation}
In practice, it is inefficient to use this equation because the convolution
with $k_i^2$ cannot be performed with the fast algorithm that is used for
convolution with $k_i$. Instead, we convolve $n^2(x,y)$ with the Gaussian
with $\sigma=2^{i}$ that approximates the negative part of the wavelet
kernel.


\subsubsection{Poisson noise}

This type of noise is usually found in the X-ray astronomy. The general
solution to the problem of finding detection thresholds in the case of
Poisson noise is given by Starck \& Pierre in
\cite{starck:wavelet:thresholds98}. Their approach will be implemented in
future releases of the code. Currently, we use the following approximate
approach. The 1-$\sigma$ error on $N$ counts can be approximated as
$1+(n+0.75)^{1/2}$ \cite{gehrels:poisson:approximation86}. We use this
approximation to estimate the error map as
\begin{equation}
  \label{eq:poisson:error:map}
  \hat n(x,y) = \frac{1+\sqrt{b(x,y)S+0.75}}{S},
\end{equation}
where $S$ is the effective area covered by the wavelet kernel,
$S=1/\sigma_i^2$, and $b(x,y)$ is the background. The detection threshold is
given, as for the Gaussian noise, by $t\hat n\sigma_i$. The background is
estimated by convolving the data with the function representing the negative
part of the wavelet kernel.


\subsection{Filtering thresholds}\label{sec:tmin}

The detection threshold should be high to prevent false detections. To allow
just one false source in a $512\times512$ image, one should should use the
$4.5\sigma$ detection threshold. However, the use this high threshold for
image filtering leads to a significant loss of useful signal.

\begin{floatingfigure}{0.49\textwidth}
  \psfrag{4.5sigma}{$4.5\sigma$}
  \psfrag{2sigma}{$2\sigma$}
  \psfrag{0}{$0$}
  \centerline{\includegraphics[width=0.48\textwidth]{Fig/tmin.ps}}
  \caption{~}
  \label{fig:tmin}
\end{floatingfigure}
This point is illustrated in Fig.~\ref{fig:tmin}. The convolution with the
wide kernel reveals a source which is $>4.5\sigma$ significant. However,
only the very top of the source is above this significance level. If we set
all regions below the $4.5\sigma$ level to zero, we would loose most of the
source flux. Fortunately, we know that the wavelet kernel was wide and
therefore the detected structure \emph{must be broad}. We, therefore, can
keep some region (say, above $2\sigma$) around the detected maximum, and
still be sure that the signal kept is real.

The code realizes this scheme as follows. The region above the filtering
threshold (also known as the minimum threshold) $t_{\rm min}$ is kept if and 
only if it contains a local maximum above the detection threshold (also, the 
maximum threshold) $t_{\rm max}$. 

\begin{floatingfigure}{0.49\textwidth}
  \psfrag{4.5sigma}{$4.5\sigma$}
  \psfrag{2sigma}{$2\sigma$}
  \psfrag{0}{$0$}
  \centerline{\includegraphics[width=0.48\textwidth]{Fig/tmin2.ps}}
  \caption{~}
  \label{fig:tmin2}
\end{floatingfigure}
This algorithm has a drawback if $t_{\rm min}$ is set too low, as
illustrated in Fig.~\ref{fig:tmin2}. The secondary maximum has only $\approx
3\sigma$ significance, and normally should not be detected. However, it is
kept because it is within the ``$2\sigma$'' region around the main peak. The
solution will be to implement an additional constraint: the region kept
should be above $t_{\rm min}$ \emph{and}\/ within some radius of the
$>t_{\rm max}$ region. In practice, the situation in Fig.~\ref{fig:tmin2} is
rarely found if $t_{\rm min}>2.5-3\sigma$.



\section{Implementation}


\subsection{Wavelet kernels}

\texttt{Wvdecomp} supports two families of the wavelet kernels --- the
\emph{\`a trous} kernels \cite{starck:atrous:94} and ``gauss''
kernels. Both families can be thought of as the difference of two Gaussians
of unit normalization. On scale 1, the positive part is the
$\delta$-function, while the negative part is approximately a Gaussian with
$sigma=1\,$pixel. On scale 2, the positive part is approximated by a
$\sigma=1\,$pixel, and the negative part by a $\sigma=2\,$pixel
Gaussians; the widths increase by a factor of 2 on each subsequent scale.

While the approximation by the two Gaussians is close to the truth, it is
not the whole truth. The \emph{\`a trous} family uses a complicated (but
fast) algorithm to compute the kernel. For the ``gauss'' family, the
Gaussian convolution is approximated by three subsequent rectangular box
convolutions. Both families produce very similar results\footnote{I usually
  use the \emph{\`a trous} kernel, but the 160~deg$^2$ cluster survey data
  were reduced using the ``gaussian'' family}.

\subsection{The code}

The program is invoked as \texttt{wvdecomp image output
  [options]}. \texttt{Image} is the input image and \texttt{output} is the
output keyword. The command line options are explained below.
\begin{itemize}
\item [\texttt{kernel=gauss/atrous}] Set the wavelet kernel;
  \texttt{kernel=atrous} is the default.

\item [\texttt{scalemin=smin}] Set the scale at which the decomposition
  process starts (see \S\ref{sec:kernel}). Default value is 1.

\item [\texttt{scalemax=smax}] Set the scale at which the decomposition
  stops. Default is \texttt{scalemax=6}.
  
  The program writes a set of output images, \texttt{output} and
  \texttt{output.i} where ${\tt smin}\le {\tt i} \le {\tt smax+1}$.
  \texttt{output.i} contains flux detected at scale \texttt{i} (see
  \S\ref{sec:kernel} and \S\ref{sec:tmin}), except for \texttt{i=smax+1}.
  ${\tt output}\equiv\sum_{\rm i=smin}^{\rm smax}{\tt
    output.i}$. \texttt{output.[smax+1]} contains is the residual image
  (e.g., \texttt{image-output}) smoothed with the kernel at the largest
  scale (\S\ref{sec:kernel}).
  
\item[\texttt{threshold=t}] Set the detection threshold, in sigma
  (\S\ref{sec:tmax}). Default is \texttt{threshold=3}. However, this is
  insufficient for most applications; for example, this allows for many
  false detections in a $512\times512$ image. \texttt{threshold=4.5-5} is a
  reasonable choice for most applications.

  It is possible to set individual detection threshold for each scale. For
  that, the \texttt{threshold=ta,tb,...} syntax should be used, where
  \texttt{ta} is the threshold at \texttt{smin} and so on.
  
\item[\texttt{thresholdmin=tmin}] This parameter, the filtering threshold,
  is explained in \S\ref{sec:tmin}. Its default value equals the detection
  threshold at the given scale. \texttt{thresholdmin=2.5} or more is a good
  choice. It is possible to set individual \texttt{tmin}'s for all scales
  with the same syntax as for the \texttt{threshold} parameter.

\item[\texttt{iter=niter}] Set the number of iterations at each scale. The
  decomposition process is repeated at the same scale until no new
  structures are detected or until \texttt{niter} iterations are made. This
  parameter defaults to 1. A reasonable choice is \texttt{iter=5}.
  
\item[\texttt{errimg=error\_image}] You can supply the image containing
  standard deviations in the data at each pixel. If this parameter is not
  set, the noise will be estimated from the data assuming either Poisson
  or Gaussian statistics (the statistics used can be controlled by the
  \texttt{stat} parameter).

\item[\texttt{stat=poisson,gauss}] Set the statistics of the image
  noise. This parameter defaults to \texttt{poisson}; however, if
  \texttt{errimg} is supplied, \texttt{stat=gauss} is the default.

\item[\texttt{bg=bgimage}] Sometimes it is practical to subtract the known
  structures from the data before the wavelet decomposition. For example, in 
  the case of the ROSAT PSPC data, one should subtract the normalized
  exposure map to get rid of spurious detections around the PSPC window
  support structure. If the image noise is Gaussian, this ``background'' can 
  be subtracted in advance. However, if it is desirable to use the Poisson
  statistics, this should be done internally in the code. This is the
  purpose of the \textrm{bg} parameter.

\item[\texttt{exp=expimage}] Similar to \texttt{bg}, but uses the exposure
  map to correct the data. Image is divided by the exposure map before
  convolution with the wavelet kernel, and multiplied back by exposure upon
  the convolution. This preserves Poisson statistics, but eliminates false
  detections in the region of strongly varying exposure.

\end{itemize}

All other parameters listed the the \texttt{wvdecomp} help file are
experimental and should be used with caution.

There is no way to change the default values of parameters except changing
the code. However, your usual setting can be specified in a parameter file
which has a form like
\begin{tt}
\obeylines\parindent=0pt\obeyspaces
threshold=5             \# detection threshold
thresholdmin=3          ! filtering threshold
iter=5                  - the number of iterations
\end{tt}
\noindent This parameter file can be supplied to the \texttt{wvdecomp} as
\texttt{@par.file} (e.g., \texttt{wvdecomp img out @par.file}).

\section{Examples}

The use of wavelet decomposition for separation of structures of different
size was already discussed (\S\ref{gov},~\ref{sec:kernel} and
Fig.~\ref{fig:wvdec:image}--~\ref{fig:wv:cluster:point}). Other
applications, which require the ability to detect sources and combine
structures of different size are numerous. Just two examples are shown below.

\subsection{Calculation of the background map}

The best approximation to the background is the smoothed data from which all 
detectable sources were subtracted. This is exactly what the largest scale
of the wavelet decomposition contains. So, the image \texttt{bg.7} created
by 

\texttt{wvdecomp image bg scalemin=1 scalemax=6 threshold=3 $\backslash$ \\
  thresholdmin=2  iter=10}

\noindent is a good approximation to the background map. Note that in this
example, thresholds were set low, because we were interested not in the
confident source detection but rather in a complete source subtraction.


\subsection{Subtraction of point sources to study the large scale brightness 
  gradients}


This is similar to the previous example, with the two exceptions. First, we
should stop the wavelet decomposition at small scale, because we would like 
to subtract only point sources. Second, we may not want to smooth the
resulting image. Suppose, scale 3 is already larger than the point source,
but still much smaller than the structures of interest.

\texttt{wvdecomp image point scalemin=1 scalemax=3 threshold=3 $\backslash$ \\
  thresholdmin=2 iter=10}

creates the image of point sources, \texttt{point}. Subtraction of
\texttt{point} from the \texttt{image} produces the desired map.


\bibliographystyle{plainav}
\bibliography{vikhlinin,clusters,wv}

\end{document}
