      implicit undefined (a-z)
      integer nd
      parameter (nd=512)
      
      integer nscale
      parameter (nscale=7)
      
      
      real G(nd,nd),sig
      double precision sum
      
      integer i,j,iscale
      
      do iscale=1,nscale
        call mset (G,nd*nd,0.0,'e')
        G(nd/2,nd/2)=1
        call gausswv (G,nd,nd,iscale,.false.)
        call saoimage (G,nd,nd,'e')
      enddo
      end
   
      
      subroutine gausswv (z,nx,ny,scale, justsmooth)
c
      implicit none
      integer nx,ny
      real z(nx,ny)
      integer scale
      logical justsmooth
      
      integer w11(7),w12(7),w13(7),w21(7),w22(7),w23(7)

c  scale         1    2    3    4    5    6    7
c  sig1         0.0  1.0  2.0  4.0  8.0  16.  32.
c  sig2         1.0  2.0  4.0  8.0  16.  32.  64.

      data w11 / 1,   3,   3,   9,  15,  31,  65 /
      data w12 / 1,   3,   5,   7,  17,  35,  53 /
      data w13 / 1,   1,   3,   7,  15,  31,  71 /

      data w21 / 3,   3,   9,  15,  31,  65,  127/
      data w22 / 3,   5,   7,  17,  35,  53,  123/
      data w23 / 1,   3,   7,  15,  31,  71,  139/

      real z2(1)
      pointer (pz2,z2)
      integer nn,malloc
      logical firstcall
      save pz2
      
      if (firstcall) then
        nn=1
        pz2=malloc(nn*4)
        firstcall=.false.
      endif
      
      if (.not.justsmooth) then
        if (nn.lt.nx*ny) then   ! Need to allocate more memory
          call free(pz2)
          nn=nx*ny
          pz2=malloc(4*nn)
          if (pz2.eq.0) then
            write(0,*)"failed to allocate memory in gausswv"
            call exit(1)
          endif
        endif
        
        call mcopy(z2,z,nx,ny)
        
        call conv_rect1(z,nx,ny,w11(scale),w11(scale))
        call conv_rect1(z,nx,ny,w12(scale),w12(scale))
        call conv_rect1(z,nx,ny,w13(scale),w13(scale))
        
        call conv_rect1(z2,nx,ny,w21(scale),w21(scale))
        call conv_rect1(z2,nx,ny,w22(scale),w22(scale))
        call conv_rect1(z2,nx,ny,w23(scale),w23(scale))

        call marith(z,"=",z,"-",z2,nx*ny)

      else
        call conv_rect1(z,nx,ny,w11(scale),w11(scale))
        call conv_rect1(z,nx,ny,w12(scale),w12(scale))
        call conv_rect1(z,nx,ny,w13(scale),w13(scale))
      endif
      return
      end
      
      subroutine conv_rect1(d,nx,ny,nrecx,nrecy)
c
c  convolves real d(nx,ny) with rectangle nrecx, nrecy
c
c
c
      implicit none
      integer nx,ny
      integer nrecx,nrecy
      real d(nx,ny)
      integer nmax
      parameter (nmax=2048)
      real row(nmax),wksp(2*nmax)
      integer i,j
      real w
      
      if ((nrecx/2)*2.eq.nrecx.or.(nrecy/2)*2.eq.nrecy) then
         call exiterror('must be odd nrecx in conv_rect1')
        return
      endif
      
c==Columns
      if (nrecy.gt.1) then
        do i=1,nx
          do j=1,ny
            row(j)=d(i,j)
          enddo
          call conv_rect1_line (row,wksp,ny,nrecy)
          do j=1,ny
            d(i,j)=row(j)
          enddo
        enddo
      endif

c==Rows
      if (nrecx.gt.1) then
        do j=1,ny
          call conv_rect1_line (d(1,j),wksp,nx,nrecx)
        enddo
      endif
      
      w=float(nrecx*nrecy)
      w=1.0/w
      
      call msarith (d,"*",w,nx*ny)
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine conv_rect1_line (line,wksp,n,nrec)
      implicit none
      integer n,nrec
      real line(n)
      real wksp(-nrec:n+nrec)
      integer i
      real sum
      integer i1,i2,nrec2
      
      do i=1,n
        wksp(i)=line(i)
      enddo
      
      do i=-nrec,0
        wksp(i)=wksp(1)
      enddo
      
      do i=n+1,n+nrec
        wksp(i)=wksp(n)
      enddo
      
      nrec2=nrec/2
      i1=1-nrec2
      i2=1+nrec2
      sum=0
      do i=i1,i2
        sum=sum+wksp(i)
      enddo
      line(1)=sum
      
      do i=2,n
        i1=i-1-nrec2
        i2=i+nrec2
        line(i)=line(i-1)+wksp(i2)-wksp(i1)
      enddo
      
      return
      end







        
      
      
      
