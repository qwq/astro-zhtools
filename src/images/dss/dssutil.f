*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine sky2dss (ra,dec,xpix,ypix,nx,ny)
      implicit none
      real*8 ra,dec
      real*8 xpix,ypix
      integer nx,ny
      
      real*8 xpixelsz,ypixelsz
      real*8 ppo3,ppo6
      real*8 a(13),b(13)
      real*8 ra0,dec0,pltscale
      real*8 crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      real equinox,optepoch
      integer cnpix1,cnpix2
      logical usedsswcs,usefitswcs
      real*8 crval10, crval20, crpix10, crpix20, cdelt10, cdelt20, crota20
      
      common /dssovercoord/ crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
     ~    ,xpixelsz,ypixelsz,ppo3,ppo6,a,b,ra0,dec0,pltscale,equinox,optepoch
     ~    ,cnpix1,cnpix2,crval10, crval20, crpix10, crpix20, cdelt10, cdelt20
     ~    ,crota20,usedsswcs,usefitswcs
      
      integer mode
      real*8 xpix0,ypix0,ra1,dec1
      common /sky2dsscom/xpix0,ypix0,ra1,dec1,mode
      
*      real*8 sky2dssfunc
*      external sky2dssfunc
      
*      xpix0=nx/2
*      ypix0=ny/2
*      ra1=ra
*      dec1=dec
*      
*      do irep=1,3
*        mode=1
*        xpix0=fmin(1.0d0,dfloat(nx),sky2dssfunc,1.0d-7)
*        mode=2
*        ypix0=fmin(1.0d0,dfloat(ny),sky2dssfunc,1.0d-7)
*      enddo
*
*      xpix=xpix0
*      ypix=ypix0
      
      call platepix(ra,dec,xpix,ypix)
      
      return
      end
      

*      function sky2dssfunc (pix)
*      implicit none
*      real*8 sky2dssfunc,pix
*      real*8 xpixelsz,ypixelsz
*      real*8 ppo3,ppo6
*      real*8 a(13),b(13)
*      real*8 ra0,dec0,pltscale
*      real*8 crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
*      real equinox,optepoch
*      integer cnpix1,cnpix2
*      logical usedsswcs,usefitswcs
*      real*8 crval10, crval20, crpix10, crpix20, cdelt10, cdelt20, crota20
*      
*      common /dssovercoord/ crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
*     ~    ,xpixelsz,ypixelsz,ppo3,ppo6,a,b,ra0,dec0,pltscale,equinox,optepoch
*     ~    ,cnpix1,cnpix2,crval10, crval20, crpix10, crpix20, cdelt10, cdelt20
*     ~    ,crota20,usedsswcs,usefitswcs
*      
*      integer mode
*      real*8 xpix0,ypix0,ra1,dec1
*      common /sky2dsscom/xpix0,ypix0,ra1,dec1,mode
*      real*8 ksi,eta
*      
*      real*8 ra0old,dec0old,cosra,tandec,cosdec
*      save dec0old,ra0old
*      data dec0old /-1000.0d0/
*      
*      
*      real*8 sla_dsep
*      
*      real*8 xpix,ypix
*      real*8 X,Y,xx,yy,ra,dec,nominator,denominator
*      real*8 pi
*      parameter (pi=3.14159265358979d0)
*      
*      if (mode.eq.1) then
*        xpix=pix
*        ypix=ypix0
*      else
*        ypix=pix
*        xpix=xpix0
*      endif
*
*      if (ra0old.ne.ra0.or.dec0old.ne.dec0) then
*        ra0old=ra0
*        dec0old=dec0
*        cosra=dcos(ra0)
*        tandec=dtan(dec0)
*        cosdec=dcos(dec0)
*      endif
*      
*      X=cnpix1+0.5+xpix
*      Y=cnpix2+0.5+ypix
*      xx=(ppo3-xpixelsz*X)/1000.0d0
*      yy=(-ppo6+ypixelsz*Y)/1000.0d0
*      ksi=a(1)*xx+a(2)*yy+a(3)+a(4)*xx**2+a(5)*xx*yy+a(6)*yy**2+a(7)*(xx
*     ~    **2+yy**2)+a(8)*xx**3+a(9)*xx**2*yy+a(10)*xx*yy**2+a(11)*yy**3
*     ~    +a(12)*xx*(xx**2+yy**2)+a(13)*xx*(xx**2+yy**2)**2
*      eta=b(1)*yy+b(2)*xx+b(3)+b(4)*yy**2+b(5)*yy*xx+b(6)*xx**2+b(7)*(yy
*     ~    **2+xx**2)+b(8)*yy**3+b(9)*yy**2*xx+b(10)*yy*xx**2+b(11)*xx**3
*     ~    +b(12)*yy*(yy**2+xx**2)+b(13)*yy*(yy**2+xx**2)**2
*      
*      ksi=ksi*pi/(60.0d0*60.0d0*180.0d0)
*      eta=eta*pi/(60.0d0*60.0d0*180.0d0)
*      
*      ra=datan2(ksi/cosdec,1.0d0-eta*tandec)+ra0
*      nominator=dcos(ra-ra0)
*      denominator=(1.0d0-eta*tandec)/(eta+tandec)
*      
*      dec=datan(nominator/denominator)
*      
*      sky2dssfunc=sla_dsep(ra,dec,ra1,dec1)
*      return
*      end
*      
*




*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine platepix (ra, dec, xpix, ypix)
      implicit none
      
      real*8 ra
      real*8 dec
      
      real*8 xpix
      real*8 ypix

      real*8 xpixelsz,ypixelsz
      real*8 ppo3,ppo6
      real*8 a(13),b(13)
      real*8 ra0,dec0,pltscale
      real*8 crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      real equinox,optepoch
      integer cnpix1,cnpix2
      logical usedsswcs,usefitswcs
      real*8 crval10, crval20, crpix10, crpix20, cdelt10, cdelt20, crota20
      
      common /dssovercoord/ crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
     ~    ,xpixelsz,ypixelsz,ppo3,ppo6,a,b,ra0,dec0,pltscale,equinox,optepoch
     ~    ,cnpix1,cnpix2,crval10, crval20, crpix10, crpix20, cdelt10, cdelt20
     ~    ,crota20,usedsswcs,usefitswcs
      
      real*8 div,xi,eta,x,y,xy,x2,y2,x2y,y2x,x3,y3,x4,y4,x2y2,cjunk,dx,dy
      real*8 sypos,cypos,syplate,cyplate,sxdiff,cxdiff
      real*8  f,fx,fy,g,gx,gy, xmm, ymm
      real*8 conr2s
      real*8 tolerance 
      integer  max_iterations
      integer    i
      real*8 xr, yr
      
      conr2s=206264.8062470964d0
      tolerance=0.0000005
      max_iterations=50

*      cond2r=1.745329252e-2
*      xr = ra * cond2r
*      yr = dec * cond2r

      xr = ra
      yr = dec
      sypos = dsin (yr)
      cypos = dcos (yr)
      syplate = dsin (dec0)
      cyplate = dcos (dec0)
      sxdiff = dsin (xr - ra0)
      cxdiff = dcos (xr - ra0)
      div = (sypos * syplate) + (cypos * cyplate * cxdiff)
      xi = cypos * sxdiff * conr2s / div
      eta = ((sypos * cyplate) - (cypos * syplate * cxdiff)) * conr2s / div
      
      xmm = xi / pltscale
      ymm = eta / pltscale
      
      do i=0,max_iterations-1
        
        xy = xmm * ymm
        x2 = xmm * xmm
        y2 = ymm * ymm
        x2y = x2 * ymm
        y2x = y2 * xmm
        x2y2 = x2 + y2
        cjunk = x2y2 * x2y2
        x3 = x2 * xmm
        y3 = y2 * ymm
        x4 = x2 * x2
        y4 = y2 * y2
        f =
     ~      a(1)*xmm      + a(2)*ymm +
     ~      a(3)          + a(4)*x2  +
     ~      a(5)*xy       + a(6)*y2  +
     ~      a(7)*x2y2     + a(8)*x3  +
     ~      a(9)*x2y      + a(10)*y2x +
     ~      a(11)*y3      + a(12)*xmm*x2y2 +
     ~      a(13)*xmm*cjunk
        fx =
     ~      a(1)          + a(4)*2.0*xmm +
     ~      a(5)*ymm      + a(7)*2.0*xmm +
     ~      a(8)*3.0*x2   + a(9)*2.0*xy +
     ~      a(10)*y2      + a(12)*(3.0*x2+y2)
     ~      + a(13)*(5.0*x4 +6.0*x2*y2+y4)
        
        fy =
     ~      a(2)           + a(5)*xmm +
     ~      a(6)*2.0*ymm   + a(7)*2.0*ymm +
     ~      a(9)*x2        + a(10)*2.0*xy +
     ~      a(11)*3.0*y2   + a(12)*2.0*xy +
     ~      a(13)*4.0*xy*x2y2
        
        g =
     ~      b(1)*ymm       + b(2)*xmm +
     ~      b(3)           + b(4)*y2 +
     ~      b(5)*xy        + b(6)*x2 +
     ~      b(7)*x2y2      + b(8)*y3 +
     ~      b(9)*y2x       + b(10)*x2y +
     ~      b(11)*x3       + b(12)*ymm*x2y2 +
     ~      b(13)*ymm*cjunk
        
        gx =
     ~      b(2)           + b(5)*ymm +
     ~      b(6)*2.0*xmm   + b(7)*2.0*xmm +
     ~      b(9)*y2        + b(10)*2.0*xy +
     ~      b(11)*3.0*x2   + b(12)*2.0*xy +
     ~      b(13)*4.0*xy*x2y2
        
        gy =
     ~      b(1)            + b(5)*2.0*ymm +
     ~      b(5)*xmm        + b(7)*2.0*ymm +
     ~      b(8)*3.0*y2     + b(9)*2.0*xy +
     ~      b(10)*x2        + b(12)*(x2+3.0*y2) +
     ~      b(13)*(5.0*y4 + 6.0*x2*y2 + x4)
        
        f = f - xi
        g = g - eta
        dx = ((-f * gy) + (g * fy)) / ((fx * gy) - (fy * gx))
        dy = ((-g * fx) + (f * gx)) / ((fx * gy) - (fy * gx))
        xmm = xmm + dx
        ymm = ymm + dy
        if ((dabs(dx) .lt. tolerance) .and. (dabs(dy) .lt. tolerance))
     ~      goto 10
        
      enddo
      
 10   continue
      
      x = (ppo3 - xmm*1000.0) / xpixelsz
      y = (ppo6 + ymm*1000.0) / ypixelsz
      
      xpix = x - cnpix1 + 1.0 - 0.5
      ypix = y - cnpix2 + 1.0 - 0.5
      
      return
      end
      
      
      
