      implicit none
      integer status
      integer lnblnk
      
      character imgname*200
      character srcname*200
      
      integer iunit
      
      

      integer cnpix1,cnpix2
      
      real xpix,ypix
      real X,Y
      double precision xx,yy
      
      integer hdtype
      character parname*20
      character comment*100
      
      double precision pi
      parameter (pi=3.14159265358979d0)

      
      character sgn*1,sign*1
      integer ihmsf(4),idmsf(4),ndp
      
      integer i

      double precision xpixelsz,ypixelsz
      double precision ppo3,ppo6
      double precision a(13),b(13)
      integer pltrah,pltram
      real pltras
      character pltdecsn*20
      integer pltdecd,pltdecm
      real pltdecs
      double precision ra0,dec0,ra,dec,ksi,eta
      double precision nominator,denominator
      

      integer narg,iargc
      logical defined
      
      iunit=1
      
      narg=iargc()
      if (narg.lt.1) call zhhelp('dsscoord')
      

c     call getarg(1,imgname)
      call set_default_par ('img','${ARGV[1]}')
      call get_command_line_par ('img',1,imgname)
      if (.not.defined(imgname)) then
        call zhhelp ('dsscoord')
      endif
      
      if (narg.ge.2) then
c       call getarg(2,srcname)
        call set_default_par ('src','${ARGV[2]}')
        call get_command_line_par ('src',1,srcname)
      else
        srcname=' none'
      endif
      
      
c - get pos info
      call open_fits (iunit, imgname, status)
      if(status.ne.0)then
        write(0,*)'>>> Error opening IMAGE file: ',imgname(1:lnblnk(imgname))
        call exit (1)
      endif
      
      call ftmahd (iunit, 1, hdtype, status) ! Goto the primary array  
      
      parname='CNPIX1'
      call ftgkyj (iunit,parname,cnpix1,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='CNPIX2'
      call ftgkyj (iunit,parname,cnpix2,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='XPIXELSZ'
      call ftgkyd (iunit,parname,xpixelsz,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='YPIXELSZ'
      call ftgkyd (iunit,parname,ypixelsz,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PPO3'
      call ftgkyd (iunit,parname,ppo3,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PPO6'
      call ftgkyd (iunit,parname,ppo6,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      
      do i=1,13
        write(parname,'(''AMDX'',i2)')i
        call rmblanks(parname)
        call ftgkyd (iunit,parname,a(i),comment,status)
        if (status.ne.0) then
          write(0,*)'Error reading header: ',parname
        endif
      enddo
      
      do i=1,13
        write(parname,'(''AMDY'',i2)')i
        call rmblanks(parname)
        call ftgkyd (iunit,parname,b(i),comment,status)
        if (status.ne.0) then
          write(0,*)'Error reading header: ',parname
        endif
      enddo
      
      parname='PLTRAH'
      call ftgkyj (iunit,parname,pltrah,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PLTRAM'
      call ftgkyj (iunit,parname,pltram,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PLTRAS'
      call ftgkye (iunit,parname,pltras,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PLTDECSN'
      call ftgkys (iunit,parname,pltdecsn,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PLTDECD'
      call ftgkyj (iunit,parname,pltdecd,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PLTDECM'
      call ftgkyj (iunit,parname,pltdecm,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif
      
      parname='PLTDECS'
      call ftgkye (iunit,parname,pltdecs,comment,status)
      if (status.ne.0) then
        write(0,*)'Error reading header: ',parname
      endif

      call ftclos(iunit,status)
      
      status=0
      
      if (srcname.ne.' none') then
        open(iunit,file=srcname,status='old')
      else
        iunit=5
      endif
      
      call ad_rad_sign (pltrah,pltram,pltras,pltdecsn,pltdecd,pltdecm
     ~    ,pltdecs,ra0,dec0)

      
      do while (status.eq.0)
        read(iunit,*,iostat=status)xpix,ypix
        if (status.eq.0) then
          X=cnpix1-0.5+xpix
          Y=cnpix2-0.5+ypix
          xx=(ppo3-xpixelsz*X)/1000.0d0
          yy=(-ppo6+ypixelsz*Y)/1000.0d0
          ksi=a(1)*xx+a(2)*yy+a(3)+a(4)*xx**2+a(5)*xx*yy+a(6)*yy**2+a(7)*(xx
     ~        **2+yy**2)+a(8)*xx**3+a(9)*xx**2*yy+a(10)*xx*yy**2+a(11)*yy**3
     ~        +a(12)*xx*(xx**2+yy**2)+a(13)*xx*(xx**2+yy**2)**2
          eta=b(1)*yy+b(2)*xx+b(3)+b(4)*yy**2+b(5)*yy*xx+b(6)*xx**2+b(7)*(yy
     ~        **2+xx**2)+b(8)*yy**3+b(9)*yy**2*xx+b(10)*yy*xx**2+b(11)*xx**3
     ~        +b(12)*yy*(yy**2+xx**2)+b(13)*yy*(yy**2+xx**2)**2

          ksi=ksi*pi/(60.0d0*60.0d0*180.0d0)
          eta=eta*pi/(60.0d0*60.0d0*180.0d0)

          ra=datan2(ksi/dcos(dec0),1.0d0-eta*dtan(dec0))+ra0
          nominator=dcos(ra-ra0)
          denominator=(1.0d0-eta*dtan(dec0))/(eta+dtan(dec0))

          dec=datan(nominator/denominator)
          
          if (ra.lt.0.0d0) ra=ra+2.0d0*pi
          ndp=3
          call sla_dr2tf(ndp,ra,sign,ihmsf)
          ndp=2
          call sla_dr2af(ndp,dec,sgn,idmsf)

          print 782,xpix,ypix,ra,dec,(ihmsf(i),i=1,4),sgn,(idmsf(i),i=1,4)
        endif
        
      enddo
      
 782  format(2(f7.1,1x),2x,2(1pe15.8,1x),3x,i2.2,1x,i2.2,1x,i2.2,'.',
     ~    i3.3,2x,a1,i2.2,1x,i2.2,1x,i2.2,'.',i2.2)
      end

