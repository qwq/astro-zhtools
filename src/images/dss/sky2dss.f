      implicit none
      integer status
      integer lnblnk
      character imgname*200
      integer nx,ny
      

      
      integer hdtype
      character parname*20
      character comment*100
      
      double precision pi
      parameter (pi=3.14159265358979d0)

      integer i

      double precision xpixelsz,ypixelsz
      double precision ppo3,ppo6
      double precision a(13),b(13)
      integer pltrah,pltram
      real pltras
      character pltdecsn*20
      integer pltdecd,pltdecm
      real pltdecs
      double precision ra0,dec0
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      real equinox,optepoch
      integer cnpix1,cnpix2
      logical usedsswcs,usefitswcs
      character wcsmode*80
      double precision crval10, crval20, crpix10, crpix20, cdelt10, cdelt20, crota20
      double precision pltscale
      
      common /dssovercoord/ crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
     ~    ,xpixelsz,ypixelsz,ppo3,ppo6,a,b,ra0,dec0,pltscale,equinox,optepoch
     ~    ,cnpix1,cnpix2,crval10,crval20,crpix10,crpix20,cdelt10,cdelt20
     ~    ,crota20,usedsswcs,usefitswcs

      integer bitpix0

      double precision xpix,ypix
      character arg*80
      
      
      double precision ra,dec,ra8,dec8
      integer iunit
      logical defined,qdeg
      
      iunit=1
      
      usedsswcs=.false.
      usefitswcs=.false.

      call get_command_line_par ("img",1,imgname)
      if (.not.defined(imgname)) call zhhelp ('sky2dss')

      wcsmode='dss'
      
c - get pos info
      call open_fits (iunit, imgname, status)
      if(status.ne.0)then
        write(0,*)'>>> Error opening IMAGE file: ',imgname(1:lnblnk(imgname))
        call exit (1)
      endif
      
      call ftmahd (iunit, 1, hdtype, status) ! Goto the primary array  

      
      call ftgkyj (iunit,'NAXIS1',nx,comment,status)
      call ftgkyj (iunit,'NAXIS2',ny,comment,status)
      call ftgkyj (iunit,'BITPIX',bitpix0,comment,status)
      
      if (wcsmode.ne.'dss') then
        parname='CRVAL1'
        call ftgkyd (iunit,parname,crval10,comment,status)
        if (status.ne.0) then
          if (wcsmode.eq.'fits') then
            write(0,*)'Error reading header: ',parname
            call exit(1)
          endif
        endif
        parname='CRVAL2'
        call ftgkyd (iunit,parname,crval20,comment,status)
        if (status.ne.0) then
          if (wcsmode.eq.'fits') then
            write(0,*)'Error reading header: ',parname
            call exit(1)
          endif
        endif
        parname='CRPIX1'
        call ftgkyd (iunit,parname,crpix10,comment,status)
        if (status.ne.0) then
          if (wcsmode.eq.'fits') then
            write(0,*)'Error reading header: ',parname
            call exit(1)
          endif
        endif
        parname='CRPIX2'
        call ftgkyd (iunit,parname,crpix20,comment,status)
        if (status.ne.0) then
          if (wcsmode.eq.'fits') then
            write(0,*)'Error reading header: ',parname
            call exit(1)
          endif
        endif
        parname='CDELT1'
        call ftgkyd (iunit,parname,cdelt10,comment,status)
        if (status.ne.0) then
          if (wcsmode.eq.'fits') then
            write(0,*)'Error reading header: ',parname
            call exit(1)
          endif
        endif
        parname='CDELT2'
        call ftgkyd (iunit,parname,cdelt20,comment,status)
        if (status.ne.0) then
          if (wcsmode.eq.'fits') then
            write(0,*)'Error reading header: ',parname
            call exit(1)
          endif
        endif
      endif

      if (status.ne.0.or.wcsmode.eq.'dss') then
                                ! TRY DSS WCS
        status = 0
        parname='CNPIX1'
        call ftgkyj (iunit,parname,cnpix1,comment,status)
        
        parname='CNPIX2'
        call ftgkyj (iunit,parname,cnpix2,comment,status)
        
        parname='XPIXELSZ'
        call ftgkyd (iunit,parname,xpixelsz,comment,status)
        
        parname='YPIXELSZ'
        call ftgkyd (iunit,parname,ypixelsz,comment,status)
        
        parname='PPO3'
        call ftgkyd (iunit,parname,ppo3,comment,status)
        
        parname='PPO6'
        call ftgkyd (iunit,parname,ppo6,comment,status)
        
        parname='EPOCH'
        call ftgkye (iunit,parname,optepoch,comment,status)
        
        do i=1,13
          write(parname,'(''AMDX'',i2)')i
          call rmblanks(parname)
          call ftgkyd (iunit,parname,a(i),comment,status)
        enddo
        
        do i=1,13
          write(parname,'(''AMDY'',i2)')i
          call rmblanks(parname)
          call ftgkyd (iunit,parname,b(i),comment,status)
        enddo
        
        parname='PLTRAH'
        call ftgkyj (iunit,parname,pltrah,comment,status)
        
        parname='PLTRAM'
        call ftgkyj (iunit,parname,pltram,comment,status)
        
        parname='PLTRAS'
        call ftgkye (iunit,parname,pltras,comment,status)
        
        parname='PLTDECSN'
        call ftgkys (iunit,parname,pltdecsn,comment,status)
        
        parname='PLTDECD'
        call ftgkyj (iunit,parname,pltdecd,comment,status)
        
        parname='PLTDECM'
        call ftgkyj (iunit,parname,pltdecm,comment,status)
        
        parname='PLTDECS'
        call ftgkye (iunit,parname,pltdecs,comment,status)

        parname='PLTSCALE'
        call ftgkyd (iunit,parname,pltscale,comment,status)
        
        if (status.eq.0) then
          usedsswcs=.true.
        else
          status=0
          write(0,*)'Error reading header'
          call exit(1)
        endif
      else
        usefitswcs=.true.
        parname='CROTA2'
        call ftgkyd (iunit,parname,crota20,comment,status)
        if (status.ne.0) then
          status=0
          crota20=0.0
        endif
      endif

      call ad_rad_sign (pltrah,pltram,pltras,pltdecsn,pltdecd,pltdecm
     ~    ,pltdecs,ra0,dec0)
      

      
      if ( .not. ( use dss wcs .or. use fits wcs ) ) then
        write(0,*)'required keywords not read'
        call exit(1)
      endif
      
      
      status=0
      
      call get_command_line_par ('deg',1,arg)
      qdeg = defined (arg)
      
      do while (status.eq.0)
        read (*,*,iostat=status) ra,dec
        if (status.eq.0) then
          if (qdeg) then
            ra8  = ra*3.14159265358979d0/180.0d0
            dec8 = dec*3.14159265358979d0/180.0d0
          else
            ra8=ra
            dec8=dec
          endif
          
          call sky2dss (ra8,dec8,xpix,ypix,nx,ny)
*          print '(f6.1,1x,f6.1)',xpix,ypix
          print *,sngl(xpix),sngl(ypix)
        endif
      enddo
      
      call exit(0)
      end
