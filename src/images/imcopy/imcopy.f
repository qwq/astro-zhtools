      implicit none
      character iname*200, oname*200
      character absiname*200, absoname*200
      integer lnblnk
      integer iunit, ounit, status
      integer naxis,pcount,gcount,blocksize,bitpix
      integer naxes(10)
      integer i
      logical simple,extend
      integer iargc
      
      if (iargc().eq.0) then
        call zhhelp ('imcopy')
      endif

      call command_line_or_read_spar ('i','input image',iname,'s')
      call command_line_or_read_spar ('o','output image',oname,'s')
      
      if (iname.eq.oname) then
        write (0,*) 'Cannot copy to itself'
        call exit(1)
      endif

      call get_absolute_path (iname,absiname)
      call get_absolute_path (oname,absoname)
      if (absiname.eq.absoname) then
        write (0,*) 'Images ',iname(1:lnblnk(iname)),' and ',
     ~      oname(1:lnblnk(oname)),' are the same'
        call exit(1)
      endif

      status=0
c/ get FITSIO units
      call ftgiou (iunit,status)
      if (status.ne.0) then
        call perror_fitsio ("logical unit:",status)
        call exit(1)
      endif
      call ftgiou (ounit,status)
      if (status.ne.0) then
        call perror_fitsio ("logical unit:",status)
        call exit(1)
      endif

c/ open input image and get info
      call ftopen(iunit,iname,0,blocksize,status)
      call ftghpr(iunit,2,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~    ,status)
      
      if (status.ne.0) then
        call perror_fitsio (iname,status)
        call exit(1)
      endif
      
      if (naxis.ne.2) then
        write(0,*)'wrong image dimensions in: ',iname(1:lnblnk(iname))
        write(0,*)(naxes(i),i=1,naxis)
        call exit(1)
      endif

c / open output
      if (oname.ne.'-') call unlink (oname)
      call ftinit(ounit,oname,0,status)
      if (status.ne.0) then
        call perror_fitsio (oname,status)
        call exit(1)
      endif
      
c/  copy
      call ftcopy (iunit,ounit,0,status)
      if (status.ne.0) then
        call perror_fitsio (
     ~      iname(1:lnblnk(iname))//','//oname,
     ~      status)
        call exit(1)
      endif
      
      call ftclos(iunit,status)
      if (status.ne.0) then
        call perror_fitsio (iname,status)
        call exit(1)
      endif

      call ftclos(ounit,status)
      if (status.ne.0) then
        call perror_fitsio (oname,status)
        call exit(1)
      endif
      end

