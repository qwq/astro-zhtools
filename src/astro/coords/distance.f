      program DISTANCE
      implicit none
      integer nwords
      double precision ra1,dec1,ra2,dec2,sla_dsep
      real dist
      integer status
      character format*80
      integer iformat
      logical defined
      
      character string*200
      character words(20)*30
      integer jf
      integer nstr
      
      call get_command_line_par ("format",1,format)
      if (.not.defined(format)) then
        iformat = 1             ! seq.
      else
        if (format(1:3).eq.'seg'.or.format(1:6).eq.'hmsdms') then
          iformat=1
        else if (format(1:3).eq.'deg') then
          iformat=2
        else if (format(1:3).eq.'rad') then
          iformat=3
        else
          write(0,*) 'format should be = hmsdms, deg, or rad'
          call exit(1)
        endif
      endif
      
      write (0,*) 'input coordinates'
      
      status=0
      do while (status.eq.0)
        read (*,'(a)',iostat=status) string
        call replace_characters (string,':',' ')
        if (status.eq.0) then
          if (iformat.eq.1) then
            call splitwords (string,words,nwords)
            if (nwords.eq.12) then
              nstr=1
              call sla_dafin(string,nstr,ra1,jf)
              call sla_dafin(string,nstr,dec1,jf)
              call sla_dafin(string,nstr,ra2,jf)
              call sla_dafin(string,nstr,dec2,jf)
            
              ra1=ra1*15.0d0
              ra2=ra2*15.0d0
            
              dist=sla_dsep(ra1,dec1,ra2,dec2)
            
              print*,dist,' rad    ',dist*180.0/3.1415926536
     ~            ,' deg   ',dist*180.0*60.0/3.1415926536,' arcmin'
            else
              write(0,*)'input: h1 m1 s1  d1 am1 as1  h2 m2 s2 d2 am2 as2'
            endif
          else if (iformat.eq.2) then
            read (string,*,iostat=status) ra1,dec1,ra2,dec2
            if (status.eq.0) then
              ra1=ra1*3.14159265358979d0/180.d0
              ra2=ra2*3.14159265358979d0/180.d0
              dec1=dec1*3.14159265358979d0/180.d0
              dec2=dec2*3.14159265358979d0/180.d0
              
              dist=sla_dsep(ra1,dec1,ra2,dec2)
              
              print*,dist,' rad    ',dist*180.0/3.1415926536
     ~            ,' deg   ',dist*180.0*60.0/3.1415926536,' arcmin'
            else
              write(0,*)'input: ra1 dec1 ra2 dec2'
              status=0
            endif
          else if (iformat.eq.3) then
            read (string,*,iostat=status) ra1,dec1,ra2,dec2
            if (status.eq.0) then
              dist=sla_dsep(ra1,dec1,ra2,dec2)
              
              print*,dist,' rad    ',dist*180.0/3.1415926536
     ~            ,' deg   ',dist*180.0*60.0/3.1415926536,' arcmin'
            else
              write(0,*)'input: ra1 dec1 ra2 dec2'
              status=0
            endif
          endif
            
        endif
      enddo

      end
      
