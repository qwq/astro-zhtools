c% \title{Cosmological formulae}
c% \maketitle


c% arbitrary w
      real function d_a_w(z,oM,oR,w_0,w_a)
      implicit none
      real z, oM, oR, w_0, w_a
      
      double precision Omega_M_0, Omega_R_0
      double precision w0, wa
      common /c_cosm/ w0, wa, Omega_M_0, Omega_R_0
      
      double precision a
      double precision qnc8y
      double precision distance_func
      external distance_func
      double precision R, d

      a = 1/(1+z)
      Omega_M_0 = oM
      Omega_R_0 = oR
      w0 = w_0
      wa = w_a
      d = qnc8y(distance_func,a,1.0d0,1.0d-8)
      if (Omega_R_0.gt.0.0d0) then
        R = 1.0d0/sqrt(Omega_R_0)
        d = R*sinh(d/R)
      else if (Omega_R_0.lt.0.0d0) then
        R = 1.0d0/sqrt(-Omega_R_0)
        d = R*sin(d/R)
      endif


      d_a_w = d/(1+z)
      return
      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      double precision function distance_func (a)
      implicit none
      double precision a
      double precision hubble2

      distance_func = 1/(a**2*sqrt(hubble2(a)))
      return
      end
      
c // (Hubble constant)**2 as a function of a
      double precision function hubble2(a)
      implicit none
      double precision a
      double precision Omega_M_0, Omega_R_0
      double precision w0, wa
      common /c_cosm/ w0, wa, Omega_M_0, Omega_R_0

      double precision qnc8z
      double precision w_over_a
      external w_over_a
      double precision integral
      double precision exp_integral_w
      common /c_save_hubble/ exp_integral_w

      if (abs(a-1).lt.1d-10) then
        integral=0
      else
        integral = qnc8z(w_over_a,1.0d0,a,1.0d-10)
      endif
      exp_integral_w = exp(-3*integral)
      hubble2 = Omega_M_0/a**3 + Omega_R_0/a**2 +
     ~    (1-Omega_M_0-Omega_R_0)/a**3*exp_integral_w

      return
      end

c // w/a
      double precision function w_over_a (a)
      implicit none
      double precision a
      double precision w
      w_over_a = w(a)/a
      return
      end

c // redshift dependence of the equation of state parameter
      double precision function w(a)
      implicit none 
      double precision a
      double precision Omega_M_0, Omega_R_0
      double precision w0, wa
      common /c_cosm/ w0, wa, Omega_M_0, Omega_R_0
      
      w = w0 + wa*(1-a)
      return
      end


c% $H/H_0$ az a function of $z$, $\Omega_M$, and $\Omega_\Lambda$
      function hubble (z,om,ol)
      implicit none
      real hubble, z,om,ol
      real E_func
      
      hubble=E_func(z,om,ol)
      
      return
      end


c%\clearpage
c%\makebox[\textwidth]{\hrulefill}
**XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c%
c% \textit{��������!!!! ������ ��� ������� ���� $d_a (1+z)$}
c%

c% angular size distance: 
c%\begin{equation}
c% y(z) = H_0 a_0 R \sinh 
c%  \left[\frac{1}{H_0 a_0 R} \int_0^z \frac{dz}{E(Z)}\right]
c%\end{equation}

      function d_a(z,om,ol)
      implicit none
      real d_a, z,om,ol
      real*8 qnc8,d_a_func
      external d_a_func
      real omegal,omegam
      common /d_a_com/ omegal,omegam
      real integral
      real h0a0R
      
      if (ol.eq.0.0) then
        
        if (om.gt.0.001) then
          d_a = 2*(2-om+om*z-(2-om)*sqrt(1+om*z))/(om**2*(1+z))
        else
          d_a = z*(1.0+z*0.5)/(1.0+z)
        endif
        
      else
        omegal=ol
        omegam=om
        
        integral = qnc8(d_a_func,0.0d0,dble(z),1.0d-6)
        
        if (om+ol.lt.1.0) then
          h0a0R=1.0/sqrt(1-om-ol)
          d_a = h0a0R * sinh (integral/h0a0R)
        else if (om+ol.eq.1.0) then
          d_a = integral
        else
          h0a0R=1.0/sqrt(om+ol-1)
          d_a = h0a0R * sin (integral/h0a0R)
        endif
        
      endif
      
      d_a=d_a/(1.0+z)

      return
      end
      
      
c%{\small  In fact, $1/E(z)$, 
c% $E(z) = [\Omega_M(1+z)^3+\Omega_R(1+z)^2+\Omega_\Lambda]^{1/2}$}
      function d_a_func (z8)
      implicit none
      real*8 d_a_func,z8
      
      real z,f
      real ol,om
      common /d_a_com/ ol,om
      real E_func
      
      z=z8
      f=1.0/E_func(z,om,ol)
      d_a_func=f
      return
      end
      
      
c% $E(z) = [\Omega_M(1+z)^3+\Omega_R(1+z)^2+\Omega_\Lambda]^{1/2}$
      function E_func (z,om,ol)
      implicit none
      real E_func, z, om, ol
      
      E_func=sqrt(om*(1+z)**3+(1-om-ol)*(1+z)**2+ol)
      
      return
      end


c%\clearpage
c%\makebox[\textwidth]{\hrulefill}
**XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

c%\begin{equation}
c% \frac{d\cal N}{dz} = \frac{(1+z)^2 d_A(z)^2}{E(z)}
c%\end{equation}

      function dndz (z,om,ol)
      implicit none
      
      real dndz, z,om,ol
      
      real E_func, d_a
      
      dndz = (d_a(z,om,ol)*(1.0+z))**2/E_func(z,om,ol)
      
      return
      end
      
      
c%\clearpage
c%\makebox[\textwidth]{\hrulefill}
**XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

c% Look-back time
      function back (z,om,ol)
      real back, z,om,ol
      real*8 qnc8,back_func
      external back_func
      real omegal,omegam
      common /back_com/ omegal,omegam
      
      omegal=ol
      omegam=om
      
      
      back = qnc8(back_func,0.0d0,dble(z),1.0d-5)
      
      return
      end
      
      function back_func (z8)
c
c  In fact, 1/E(z), E(z) = [Om(1+z)**3+Or(1+z)**2+Ol]^1/2
c
c
      implicit none
      real*8 back_func,z8
      
      real z,f
      real ol,om
      common /back_com/ ol,om
      real E_func
      
      z=z8
      f=1.0/((1+z)*E_func(z,om,ol))
      back_func=f
      return
      end



