      implicit none
      real z,Omega, Lambda
      real d_a, d_a_w

      character arg*80
      double precision size
      real angle
      integer l,lnblnk
      logical defined
      real w0, wa

      real c,h0,pi,rad2sec,daconst,pc
      parameter (c=2.9979245620e5) ! km/s
      parameter (pi=3.1415926536)
      parameter (rad2sec = 648000.0/pi)
      parameter (daconst = 108.07477/pi) ! = rad2sec/(c/h0)
      parameter (pc=3.085678e+18)

      logical yespar, defpar
      double precision hubble2


      call get_parameter_value ('z',z,'e')
      if (defpar('wmap')) then
        call get_parameter_value_default ('wmapOmega',omega,0.3,'e')
        call get_parameter_value_default ('wmapLambda',Lambda,0.7,'e')
        call get_parameter_value_default ('wmapH0',h0,72.0,'e')
        w0 = -1
        wa = 0.0
      else
        call get_parameter_value_default ('Omega',omega,1.0,'e')
        call get_parameter_value_default ('Lambda',Lambda,0.0,'e')
        call get_parameter_value_default ('H0',h0,50.0,'e')
        call get_parameter_value_default ('w0',w0,-1.0,'e')
        call get_parameter_value_default ('wa',wa,0.0,'e')
      endif

      call get_command_line_par ('angle',1,arg)
      if (defined(arg)) then
        l = lnblnk(arg)
        if ( (l.ge.4) .and. (arg(l-2:l).eq.'deg') ) then
          read (arg(1:l-3),*) angle
          angle = angle * 3600.0
        else if ( (l.ge.4) .and. (arg(l-2:l).eq.'rad') ) then
          read (arg(1:l-3),*) angle
          angle = angle * rad2sec
        else if ( (l.ge.7) .and. (arg(l-5:l).eq.'arcsec') ) then
          read (arg(1:l-6),*) angle
          angle = angle
        else if ( (l.ge.7) .and. (arg(l-5:l).eq.'arcmin') ) then
          read (arg(1:l-6),*) angle
          angle = angle*60.0
        else
          read (arg,*) angle
        endif
      else
        angle = 1
      endif
      angle = angle / rad2sec
      
      if (abs(w0+1).lt.1e-5.and.abs(wa).lt.1e-5) then
        size = d_a (z,Omega,Lambda) * (c/h0) * 1.0e3 ! in kpc
      else
        size = d_a_w (z,Omega,1-(Omega+Lambda),w0,wa) * (c/h0) * 1.0e3 ! in kpc
      endif

      size = angle*size
      
      if (yespar('dl')) then
        size = size*(1+z)**2
      endif


      call get_command_line_par ('out',1,arg)
      if (defined(arg)) then
        if (arg.eq.'Mpc'.or.arg.eq.'mpc') then
          size = size / 1000.0
        else if (arg.eq.'cm') then
          size = size * 1000.0 * pc
        endif
      endif

      print '(1pe11.5)',size

      if (yespar('print_EZ')) then
        if (abs(w0+1).lt.1e-5.and.abs(wa).lt.1e-5) then
          print '(1pe11.5)',
     ~        sqrt(Omega*(1+z)**3+(1-Omega-Lambda)*(1+z)**2+Lambda)
        else
          print '(1pe11.5)',sqrt(hubble2(1.0d0/(1.0d0+z)))
        endif
      endif
        

      end

