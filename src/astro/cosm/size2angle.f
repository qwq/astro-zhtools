      implicit none
      real z,Omega, Lambda
      real d_a, d_a_w

      character arg*80
      double precision size, distance
      real angle
      integer l,lnblnk
      logical defined, defpar
      real w0, wa

      real c,h0,pi,rad2sec,daconst,pc
      parameter (c=2.9979245620e5) ! km/s
      parameter (pi=3.1415926536)
      parameter (rad2sec = 648000.0/pi)
      parameter (daconst = 108.07477/pi) ! = rad2sec/(c/h0)
      parameter (pc=3.085678e+18)

      character unit*20

      call get_parameter_value ('z',z,'e')
      if (defpar('wmap')) then
        call get_parameter_value_default ('wmapOmega',omega,0.3,'e')
        call get_parameter_value_default ('wmapLambda',Lambda,0.7,'e')
        call get_parameter_value_default ('wmapH0',h0,72.0,'e')
        w0 = -1
        wa = 0.0
      else
        call get_parameter_value_default ('Omega',omega,1.0,'e')
        call get_parameter_value_default ('Lambda',Lambda,0.0,'e')
        call get_parameter_value_default ('H0',h0,50.0,'e')
        call get_parameter_value_default ('w0',w0,-1.0,'e')
        call get_parameter_value_default ('wa',wa,0.0,'e')
      endif

      call get_command_line_par ('size',1,arg)
      if (defined(arg)) then
        l = lnblnk(arg)
        if ( (l.ge.4) .and. (arg(l-2:l).eq.'Mpc') ) then
          read (arg(1:l-3),*) size
          size = size * 1000.0
        else if ( (l.ge.4) .and. (arg(l-2:l).eq.'kpc') ) then
          read (arg(1:l-3),*) size
        else if ( (l.ge.3) .and. (arg(l-1:l).eq.'pc') ) then
          read (arg(1:l-2),*) size
          size = size * 0.001
        else if ( (l.ge.3) .and. (arg(l-1:l).eq.'cm') ) then
          read (arg(1:l-2),*) size
          size = (size/pc) * 0.001
        else
          read (arg,*) size
        endif
      else
        size = 1
      endif
      
      if (abs(w0+1).lt.1e-5.and.abs(wa).lt.1e-5) then
        distance = d_a (z,Omega,Lambda) * (c/h0) * 1.0e3 ! in kpc
      else
        distance = d_a_w (z,Omega,1-(Omega+Lambda),w0,wa) * (c/h0) * 1.0e3 
      endif

      angle = size/distance     ! in rad
      
      call get_command_line_par ('out',1,arg)
      if (defined(arg)) then
        if (arg.eq.'arcsec') then
          angle = angle * rad2sec
          unit = 'arcsec'
        else if (arg.eq.'arcmin') then
          angle = angle * rad2sec / 60.0
          unit = 'arcmin'
        else if (arg.eq.'deg') then
          angle = angle * rad2sec / 3600.0
          unit = 'deg'
        else if (arg.eq.'rad') then
          unit = 'rad'
        endif
      else ! arcsec by default
        angle = angle * rad2sec
        unit = 'arcsec'
      endif

      print '(1pe11.5)',angle
        

      end

