#include <stdlib.h>
#include <zhtools.h>

/* This code derives from g2c.h */
typedef long int  integer;
typedef short int shortint;
typedef char      byte;
typedef float     real;
typedef double    doublereal;
typedef struct { real r, i; } complex;
typedef struct { doublereal r, i; } doublecomplex;
typedef long int  logical;
typedef short int shortlogical;
typedef char      logical1;
typedef char      integer1;

#define ALLOCNAME for_malloc_b_
#define REALLOCNAME for_realloc_b_
#include "alloc_code.h"

#undef ALLOCNAME
#define ALLOCNAME for_malloc_i_
#undef REALLOCNAME
#define REALLOCNAME for_realloc_i_
#include "alloc_code.h"

#undef ALLOCNAME
#define ALLOCNAME for_malloc_j_
#undef REALLOCNAME
#define REALLOCNAME for_realloc_j_
#include "alloc_code.h"

#undef ALLOCNAME
#define ALLOCNAME for_malloc_e_
#undef REALLOCNAME
#define REALLOCNAME for_realloc_e_
#include "alloc_code.h"

#undef ALLOCNAME
#define ALLOCNAME for_malloc_d_
#undef REALLOCNAME
#define REALLOCNAME for_realloc_d_
#include "alloc_code.h"

void for_free_ (void *pointer)
{
  free (pointer);
}

int for_sizeof_(int *c){
  switch(*c){
  case 'b':
    return sizeof(byte);
  case 'i':
    return sizeof(shortint);
  case 'j':
    return sizeof(integer);
  case 'e':
    return sizeof(real);
  case 'd':
    return sizeof(doublereal);
  /* largest possible? */
  default:
    return sizeof(doublereal);
  }
}
