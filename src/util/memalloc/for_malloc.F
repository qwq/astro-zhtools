#include <zhtools.h>

#ifdef F95

C     Thanks to : http://www.star.le.ac.uk/~cgp/f90course/f90.html#tth_sEc5
      FUNCTION realloc_i(p, n)
      INTEGER, POINTER, DIMENSION(:) :: p, realloc_i
      INTEGER, intent(in) :: n
      INTEGER :: nold, ierr
      ALLOCATE(realloc_i(1:n), STAT=ierr)
      IF(ierr /= 0) STOP "reallocate error"
      IF(.NOT. ASSOCIATED(p)) RETURN
      nold = MIN(SIZE(p), n)
      realloc_i(1:nold) = p(1:nold)
      DEALLOCATE(p) 
      END FUNCTION realloc_i

#endif

c Example:
c
c      integer px
c
c      call for_malloc (px,4*10,'e')
c      call for_realloc (px,4*10,'e')
c
c      call work (%val(px),10)
c
c      call for_free (%val(px))
c      
c      end
c
c      subroutine work(x,n)
c      integer n
c      real x(n)
c
c      do i=1,n
c        x(i)=i
c      enddo
c      return
c      end
c     
c--------------------------------------------------------------

      subroutine for_malloc (pointer, size, type)
      implicit none
      ZHPOINTER pointer
      integer size
      character type*(*)
      
      character t*1
      
      t=type(1:1)
      if (ichar(t).le.90) then
        t = char(ichar(t)+32)
      endif
      
      if (t.eq.'b') then
        call for_malloc_b (pointer, size)
      else if (t.eq.'i') then
        call for_malloc_i (pointer, size)
      else if (t.eq.'j') then
        call for_malloc_j (pointer, size)
      else if (t.eq.'e') then
        call for_malloc_e (pointer, size)
      else if (t.eq.'d') then
        call for_malloc_d (pointer, size)
      else
        call exiterror ('unknown type: '//t//' in for_malloc')
      endif
      
      return
      end

C     ---------------------------------------------------------------------

      subroutine for_realloc (oldpointer, pointer, size, type)
      implicit none
      ZHPOINTER oldpointer, pointer
      integer size
      character*(*) type
      
      character*1 t
      
      t=type(1:1)
      if (ichar(t).le.90) then
        t = char(ichar(t)+32)
      endif
      
      if (t.eq.'b') then
        call for_realloc_b (oldpointer, pointer, size)
      else if (t.eq.'i') then
        call for_realloc_i (oldpointer, pointer, size)
      else if (t.eq.'j') then
        call for_realloc_j (oldpointer, pointer, size)
      else if (t.eq.'e') then
        call for_realloc_e (oldpointer, pointer, size)
      else if (t.eq.'d') then
        call for_realloc_d (oldpointer, pointer, size)
      else
        call exiterror ('unknown type: '//t//' in for_realloc')
      endif
      
      return
      end

c
c     xfor_malloc extends for_malloc:
c     it determines the size of an element automatically
c     and also checks for errors 
c
c      call xfor_malloc (px,10,'e')
c
      subroutine xfor_malloc (pointer, size, type)
      implicit none
      integer for_sizeof
      ZHPOINTER pointer
      integer size, isize
      character type*(*)
      
      character t*1
      
      t=type(1:1)
      if (ichar(t).le.90) then
        t = char(ichar(t)+32)
      endif
      
      isize = size * for_sizeof(ichar(t))

      if (t.eq.'b') then
        call for_malloc_b (pointer, isize)
      else if (t.eq.'i') then
        call for_malloc_i (pointer, isize)
      else if (t.eq.'j') then
        call for_malloc_j (pointer, isize)
      else if (t.eq.'e') then
        call for_malloc_e (pointer, isize)
      else if (t.eq.'d') then
        call for_malloc_d (pointer, isize)
      else
        call exiterror ('unknown type: '//t//' in for_malloc')
      endif

      if (pointer.eq.0) call exiterror ('cannot allocate memory')
      
      return
      end

c
c     xfor_realloc extends for_realloc:
c     it determines the size of an element automatically
c     and also checks for errors 
c
c      call xfor_remalloc (opx, px,10,'e')
c
      subroutine xfor_realloc (opointer, pointer, size, type)
      implicit none
      integer for_sizeof
      ZHPOINTER opointer, pointer
      integer size, isize
      character type*(*)
      
      character t*1
      
      t=type(1:1)
      if (ichar(t).le.90) then
        t = char(ichar(t)+32)
      endif
      
      isize = size * for_sizeof(ZHVAL(ichar(t)))

      if (t.eq.'b') then
        call for_realloc_b (opointer, pointer, isize)
      else if (t.eq.'i') then
        call for_realloc_i (opointer, pointer, isize)
      else if (t.eq.'j') then
        call for_realloc_j (opointer, pointer, isize)
      else if (t.eq.'e') then
        call for_realloc_e (opointer, pointer, isize)
      else if (t.eq.'d') then
        call for_realloc_d (opointer, pointer, isize)
      else
        call exiterror ('unknown type: '//t//' in for_malloc')
      endif

      if (pointer.eq.0) call exiterror ('cannot allocate memory')
      
      return
      end

