      function fmin(ax,bx,f,tol)
      implicit none
      
      real*8 ax,bx,f,tol,fmin
      real*8 a,b,c,d,e,eps,xm,p,q,r,tol1,tol2,u,v,w
      real*8 fu,fv,fw,fx,x
      
      c=0.5d0*(3.0d0-dsqrt(5.0d0))
      eps=1.0d0
 10   eps=eps*0.5d0
      tol1=1.0d0+eps
      if(tol1.gt.1.0d0) goto 10
      eps=dsqrt(eps)
      a=ax
      b=bx
      v=a+c*(b-a)
      w=v
      x=v
      e=0.0d0
      fx=f(x)
      fv=fx
      fw=fx
 20   xm=0.5d0*(a+b)
      tol1=eps*dabs(x)+tol/3.0d0
      tol2=tol1+tol1
      if(dabs(x-xm).le.(tol2-0.5d0*(b-a))) goto 90
      if(dabs(e).le.tol1) goto 40
      r=(x-w)*(fx-fv)
      q=(x-v)*(fx-fw)
      p=(x-v)*q-(x-w)*r
      q=q+q-(r+r)
      if(q.gt.0.0d0)p=-p
      q=dabs(q)
      r=e
      e=d
 30   if(dabs(p).ge.dabs(0.5d0*q*r)) goto 40
      if(p.le.q*(a-x)) goto 40
      if(p.ge.q*(b-x)) goto 40
      d=p/q
      u=x+d
      if((u-a).lt.tol2)d=dsign(tol1,xm-x)
      if((b-u).lt.tol2)d=dsign(tol1,xm-x)
      goto 50
 40   if(x.ge.xm)e=a-x
      if(x.lt.xm)e=b-x
      d=c*e
 50   if(dabs(d).ge.tol1)u=x+d
      if(dabs(d).lt.tol1)u=x+dsign(tol1,d)
      fu=f(u)
      if(fu.gt.fx) goto 60
      if(u.ge.x) a=x
      if(u.lt.x) b=x
      v=w
      fv=fw
      w=x
      fw=fx
      x=u
      fx=fu
      goto 20
 60   if(u.lt.x) a=u
      if(u.ge.x) b=u
      if(fu.le.fw) goto 70
      if(w.eq.x) goto 70
      if(fu.le.fv) goto 80
      if(v.eq.x) goto 80
      if (v.eq.w) goto 80
      goto 20
 70   v=w
      fv=fw
      w=u
      fw=fu
      goto 20
 80   v=u
      fv=fu
      goto 20
 90   fmin=x
      return
      end
      
      
      function fmin4(ax,bx,f,tol)
      implicit none
      
      real ax,bx,f,tol,fmin4
      real a,b,c,d,e,eps,xm,p,q,r,tol1,tol2,u,v,w
      real fu,fv,fw,fx,x
      
      c=0.5*(3.0-sqrt(5.0))
      eps=1.0
 10   eps=eps*0.5
      tol1=1.0+eps
      if(tol1.gt.1.0) goto 10
      eps=sqrt(eps)
      a=ax
      b=bx
      v=a+c*(b-a)
      w=v
      x=v
      e=0.0
      fx=f(x)
      fv=fx
      fw=fx
 20   xm=0.5*(a+b)
      tol1=eps*abs(x)+tol/3.0
      tol2=tol1+tol1
      if(abs(x-xm).le.(tol2-0.5*(b-a))) goto 90
      if(abs(e).le.tol1) goto 40
      r=(x-w)*(fx-fv)
      q=(x-v)*(fx-fw)
      p=(x-v)*q-(x-w)*r
      q=q+q-(r+r)
      if(q.gt.0.0)p=-p
      q=abs(q)
      r=e
      e=d
 30   if(abs(p).ge.abs(0.5*q*r)) goto 40
      if(p.le.q*(a-x)) goto 40
      if(p.ge.q*(b-x)) goto 40
      d=p/q
      u=x+d
      if((u-a).lt.tol2)d=sign(tol1,xm-x)
      if((b-u).lt.tol2)d=sign(tol1,xm-x)
      goto 50
 40   if(x.ge.xm)e=a-x
      if(x.lt.xm)e=b-x
      d=c*e
 50   if(abs(d).ge.tol1)u=x+d
      if(abs(d).lt.tol1)u=x+sign(tol1,d)
      fu=f(u)
      if(fu.gt.fx) goto 60
      if(u.ge.x) a=x
      if(u.lt.x) b=x
      v=w
      fv=fw
      w=x
      fw=fx
      x=u
      fx=fu
      goto 20
 60   if(u.lt.x) a=u
      if(u.ge.x) b=u
      if(fu.le.fw) goto 70
      if(w.eq.x) goto 70
      if(fu.le.fv) goto 80
      if(v.eq.x) goto 80
      if (v.eq.w) goto 80
      goto 20
 70   v=w
      fv=fw
      w=u
      fw=fu
      goto 20
 80   v=u
      fv=fu
      goto 20
 90   fmin4=x
      return
      end
      
      
            
      function fmin41(ax,bx,f,tol)
      implicit none
      
      real ax,bx,f,tol,fmin41
      real a,b,c,d,e,eps,xm,p,q,r,tol1,tol2,u,v,w
      real fu,fv,fw,fx,x
      
      c=0.5*(3.0-sqrt(5.0))
      eps=1.0
 10   eps=eps*0.5
      tol1=1.0+eps
      if(tol1.gt.1.0) goto 10
      eps=sqrt(eps)
      a=ax
      b=bx
      v=a+c*(b-a)
      w=v
      x=v
      e=0.0
      fx=f(x)
      fv=fx
      fw=fx
 20   xm=0.5*(a+b)
      tol1=eps*abs(x)+tol/3.0
      tol2=tol1+tol1
      if(abs(x-xm).le.(tol2-0.5*(b-a))) goto 90
      if(abs(e).le.tol1) goto 40
      r=(x-w)*(fx-fv)
      q=(x-v)*(fx-fw)
      p=(x-v)*q-(x-w)*r
      q=q+q-(r+r)
      if(q.gt.0.0)p=-p
      q=abs(q)
      r=e
      e=d
 30   if(abs(p).ge.abs(0.5*q*r)) goto 40
      if(p.le.q*(a-x)) goto 40
      if(p.ge.q*(b-x)) goto 40
      d=p/q
      u=x+d
      if((u-a).lt.tol2)d=sign(tol1,xm-x)
      if((b-u).lt.tol2)d=sign(tol1,xm-x)
      goto 50
 40   if(x.ge.xm)e=a-x
      if(x.lt.xm)e=b-x
      d=c*e
 50   if(abs(d).ge.tol1)u=x+d
      if(abs(d).lt.tol1)u=x+sign(tol1,d)
      fu=f(u)
      if(fu.gt.fx) goto 60
      if(u.ge.x) a=x
      if(u.lt.x) b=x
      v=w
      fv=fw
      w=x
      fw=fx
      x=u
      fx=fu
      goto 20
 60   if(u.lt.x) a=u
      if(u.ge.x) b=u
      if(fu.le.fw) goto 70
      if(w.eq.x) goto 70
      if(fu.le.fv) goto 80
      if(v.eq.x) goto 80
      if (v.eq.w) goto 80
      goto 20
 70   v=w
      fv=fw
      w=u
      fw=fu
      goto 20
 80   v=u
      fv=fu
      goto 20
 90   fmin41=x
      return
      end
      
      
            
