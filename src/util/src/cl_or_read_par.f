      subroutine command_line_or_read_par (name,description,value,type)
      implicit none
      character name*(*),type*(*),description*(*)
      integer value
      
      if (type(1:1).eq.'e'.or.type(1:1).eq.'E') then
        call cl_or_read_epar (name,description,value)
      else if (type(1:1).eq.'d'.or.type(1:1).eq.'D') then
        call cl_or_read_dpar (name,description,value)
      else if (type(1:1).eq.'j'.or.type(1:1).eq.'j') then
        call cl_or_read_jpar (name,description,value)
      else if (type(1:1).eq.'s'.or.type(1:1).eq.'s') then
        write (0,*) "Use command_line_or_read_spar to read a string parameter"
        call exit(0)
      else
        write (0,*) "Use types e, d, or j in command_line_or_read_par"
        call exit(1)
      endif

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine cl_or_read_epar (name,description,value)
      implicit none
      character name*(*),description*(*)
      real value
      character parvalue*80
      integer lnblnk
      logical defined

      call get_command_line_par (name,1,parvalue)
      if (defined(parvalue)) then
        read (parvalue,*) value
      else
        if (description.ne.' ') then
          parvalue = description(1:lnblnk(description))//' ('//
     ~        name(1:lnblnk(name))//'=)'
        else
          parvalue = name
        endif
        call lowstring(parvalue)
        call toupper(parvalue(1:1))
        write(0,'(a,"? ",$)') parvalue(1:lnblnk(parvalue))
        read (*,*) value
      endif
      
      return
      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine cl_or_read_dpar (name,description,value)
      implicit none
      character name*(*),description*(*)
      double precision value
      character parvalue*80
      integer lnblnk
      logical defined

      call get_command_line_par (name,1,parvalue)
      if (defined(parvalue)) then
        read (parvalue,*) value
      else
        if (description.ne.' ') then
          parvalue = description(1:lnblnk(description))//' ('//
     ~        name(1:lnblnk(name))//'=)'
        else
          parvalue = name
        endif
        call lowstring(parvalue)
        call toupper(parvalue(1:1))
        write(0,'(a,"? ",$)') parvalue(1:lnblnk(parvalue))
        read (*,*) value
      endif
      
      return
      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine cl_or_read_jpar (name,description,value)
      implicit none
      character name*(*),description*(*)
      integer value
      character parvalue*80
      integer lnblnk
      logical defined

      call get_command_line_par (name,1,parvalue)
      if (defined(parvalue)) then
        read (parvalue,*) value
      else
        if (description.ne.' ') then
          parvalue = description(1:lnblnk(description))//' ('//
     ~        name(1:lnblnk(name))//'=)'
        else
          parvalue = name
        endif
        call lowstring(parvalue)
        call toupper(parvalue(1:1))
        write(0,'(a,"? ",$)') parvalue(1:lnblnk(parvalue))
        read (*,*) value
      endif
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine command_line_or_read_spar (name,description,value,type)
      implicit none
      character name*(*),description*(*)
      character value*(*),type*(*)
      character parvalue*80
      integer lnblnk
      logical defined

      call get_command_line_par (name,1,parvalue)
      if (defined(parvalue)) then
        value = parvalue
      else
        if (description.ne.' ') then
          parvalue = description(1:lnblnk(description))//' ('//
     ~        name(1:lnblnk(name))//'=)'
        else
          parvalue = name
        endif
        call lowstring(parvalue)
        call toupper(parvalue(1:1))
        write(0,'(a,"? ",$)') parvalue(1:lnblnk(parvalue))
        read (*,'(a)') value
      endif
      
      return
      end

      
