      logical function isfits (file)
      implicit none
      character file*(*)
      integer unit
      integer status,rwmode,blocksize
      
      status=0
      
      call ftgiou(unit,status)
      if (status.ne.0) then
        call perror_fitsio ('logical unit',status)
        call exit(1)
      endif

      rwmode=0
      call ftopen(unit,file,rwmode,blocksize,status)
      
      if (status.eq.0) then
        isfits = .true.
      else
        isfits = .false.
      endif
      status=0
      call ftclos (unit,status)
      status=0
      call ftfiou (unit,status)
      
      return
      end
      
     
