      subroutine get_par_or_arg (parname,iarg,arg)
      implicit none
c
c First tries to read parameter parname;
c If not present, reads command line argument no. iarg
c If the number of arguments is < iarg, returns "undefined"
c
      character parname*(*),arg*(*)
      integer iarg
      
      integer iargc
      logical defined

      call get_cl_par (parname,arg)
      if (defined(arg)) return

      if (iargc().lt.iarg) then
        arg = 'undefined'
        return
      endif

      call getarg (iarg,arg)
      return
      end
      
