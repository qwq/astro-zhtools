c% This program is the driver for the 1-D \'a trous convolution of scale $j$.
c% Its main purpose is to provide the work space and calculate separation for
c% for the \'a trous kernel. The real convolution is performed by
c% \emph{atrousstep\_0} (see {\tt atrousstep\_0.f}).
c%
c%
c% BUGS: the work space has maximum lenght 16384. It is OK for 2-D
c% convolutions as the image size is never that big. For genuine 1-D
c% convolutions this may be a problem

      subroutine  atrousstep (c,n,j)
      implicit none
      integer n
      real c(n)
      integer j
      
      real coeff(-2:2)
      data coeff /0.0625, 0.25, 0.375, 0.25, 0.0625/

      integer nmax
      parameter (nmax=16384)
      real cc(nmax)

      integer step
      
      
      step=2**(j-1)
      
      if (n.gt.nmax-4*step-4) call exiterror('too long array in atrousstep')
      
      call atrousstep_0 (c,cc,n,step,coeff)

      
      return
      end
