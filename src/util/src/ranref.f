      function ranref (r,nr)
c
c  Generates an integer from a refference vector
c
      integer ranref
      integer nr
      real r(nr)
      real x
      integer n
      real ran2
      integer idum
      data idum /0/

      x=ran2(idum)
      n=int(x*r(1))
      n=int(r(n+3))
      if (x.le.r(n)) goto 40
   20 n=n+1
      if (x.gt.r(n)) goto 20
   40 ranref=n+int(r(2))
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function ranref8 (r,nr)
c
c
c  Generates an integer from a refference vector
c
c
      integer ranref8
      integer nr
      real*8 r(nr)
      real*8 x
      integer n
      real*8 ran28
      integer idum
      data idum /0/

      x=ran28(idum)
      n=idint(x*r(1))
      n=idint(r(n+3))
      if (x.le.r(n)) goto 40
   20 n=n+1
      if (x.gt.r(n)) goto 20
   40 ranref8=n+idint(r(2))
      return
      end
