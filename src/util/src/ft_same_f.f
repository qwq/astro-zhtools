      logical function ft_same_files (file1,file2)
      implicit none
      character file1*(*), file2*(*)
      character path1*200,path2*200

      if (file1.eq.'-'.or.file2.eq.'-') then
        ft_same_files = .false.
        return
      endif

      call get_absolute_path (file1,path1)
      call get_absolute_path (file2,path2)
      
      ft_same_files = path1 .eq. path2
      return
      end
