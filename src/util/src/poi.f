      integer function ipoiss (m,flag)
c
c  Returns Poisson random number with mean m.
c
c  logical flag = .true.  - recalculate ref.vector
c                 .false. - do not.
c
c
c  m^1/2 should not exceed 1000
c
c
      implicit none
      real m
      logical flag
      integer nmax
      parameter (nmax=20000)
      real r(nmax)
      integer nr,ifail,ranref
      save nr,r

      if (flag) then
        nr=20.0*sqrt(m)+20.
        if(nr.gt.nmax)  call exiterror('too large mean in ipoiss')
        ifail=0
        call poi_ref (m,r,nr,ifail)
        if (ifail.ne.0) then
          write (0,*) 'ifail=',ifail, '  in poi_ref'
          call exit(1)
        endif
      endif
      ipoiss=ranref(r,nr)
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      integer function ipoiss8 (m,flag)
c
c  Returns Poisson random number with mean m.
c
c  logical flag = .true.  - recalculate ref.vector
c                 .false. - do not.
c
c
c  m^1/2 should not exceed 1000
c
c
      implicit none
      double precision m
      logical flag
      integer nmax
      parameter (nmax=20000)
      double precision r(nmax)
      integer nr,ifail,ranref8
      save nr,r

      if (flag) then
         nr=20.0d0*dsqrt(m)+20.0d0
         if(nr.gt.nmax)  call exiterror('too great mean in ipoiss')
     	 ifail=0
         call poi_ref8 (m,r,nr,ifail)
	 if (ifail.ne.0) then
	   print*,'ifail=',ifail, '  in poi_ref'
	    call exiterror('')
	 endif
      endif
      ipoiss8=ranref8(r,nr)
      return
      end


      function ipoiss82 (m,flag)
c
c  A copy of ipoiss8 under a different name
c
      implicit none
      integer ipoiss82
      double precision m
      logical flag
      integer nmax
      parameter (nmax=20000)
      double precision r(nmax)
      integer nr,ifail,ranref8
      save nr,r

      if (flag) then
         nr=20.0d0*dsqrt(m)+20.0d0
         if(nr.gt.nmax)  call exiterror('too great mean in ipoiss')
     	 ifail=0
         call poi_ref8 (m,r,nr,ifail)
	 if (ifail.ne.0) then
	   print*,'ifail=',ifail, '  in poi_ref'
	    call exiterror('')
	 endif
      endif
      ipoiss82=ranref8(r,nr)
      return
      end





      subroutine poi_ref(t,r,nr,ifail)
c--------------------------------------------------------------------------
c    a. purpose
c    ==========
c
c     poi_ref sets up the reference vector r for a poisson  distribution
c     with mean t.
c
c    b. specification
c    ================
c
c           subroutine poi_ref (t,r,nr,ifail)
c           integer nr,ifail
c           //real// t,r(nr)
c
c    c. parameters
c    =============
c    t - //real//.
c      on entry, t must specify the parameter (mean) t of the distribution.
c      t.ge.0.
c
c    r - //real// array of dimension (nr).
c      on exit, r contains the reference vector.
c
c    nr - integer.
c      on entry, nr must specify the dimension of r. the recommended  value
c      is roughly 20 + 20*sqrt(t). nr must be larger than
c           int(t+7.15*sqrt(t)+8.5) - max(0,int(t-7.15*sqrt(t))+4.
c
c    ifail - integer.
c      before entry, ifail must be assigned a value. for users not familiar
c      with this parameter the recommended value is 0.
c
c      unless  the  routine  detects  an error  (see next  section),  ifail
c      contains 0 on exit.
c
c
c    d. error indicators and warnings
c    ================================
c    ifail = 1
c      on entry, t.lt.0.
c    ifail = 2
c      on entry, nr is too small (see section c).
c---------------------------------------------------------------------
      implicit none
      real t
      integer ifail,nr
      real r(nr)
      real add,const1,const2,const3,one,times,twopi,w,x,y,z,zero
      integer i,ibase,ibot,idiff,ierr,itop,j,k

      data add /8.5/, times /5.0/, zero/0.0/, one /1.0/,
     ~    twopi /6.28318530712/, const1/8.333333333e-2/,
     ~    const2 /2.777777778e-3/,const3 /7.9365079365e-4/

      ierr = 1
      if (t.lt.zero) goto 140
      x=sqrt(t)*times
      itop=int(t+x+add)
      ibot=max(0,int(t-x))
      ierr=2
      if ((itop-ibot+4).gt.nr) goto 140
      idiff=itop-nr
      ibase=ibot-idiff
      if (ibot.gt.0) goto 40

c     use the direct method if t .lt. 50.
      x=zero
      y=exp(-t)
      z=zero
      do i=ibase,nr
         z=z+y
         r(i)=z
         x=x+one
         y=y*t/x
      enddo
      goto 120
c     use stirlings formula if t .gt. 50.
   40 i=int(t)
      x=float(i)
      z=one/(x*x)
      y=((t/x)**i)*exp((x-t)-(const1-(const2-const3*z)*z)/x)/
     ~     sqrt(twopi*x)
      j=i-idiff
      w=x
      z=y
      do k=ibase,j
         i=j+ibase-k
         r(i)=y
         y=y*x/t
         x=x-one
      enddo
      y=zero
      do i=ibase,j
         y=y+r(i)
         r(i)=y
      enddo
      j=j+1
      do i=j,nr
         w=w+one
         z=z*t/w
         y=y+z
         r(i)=y
      enddo
c     finish off in either case.
  120 call refindx(idiff,ibase,r,nr)
      ifail=0
      return
  140 ifail=ierr
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine poi_ref8(t,r,nr,ifail)
c
c     Sets up the reference vector for a poisson distribution.
c     add and times can be changed if a truncation probability of
c     1.0e-12 is regarded as unsatisfactory.
      double precision t
      integer ifail,nr
      double precision r(nr)
      double precision add,const1,const2,const3,one,times,twopi,w,x,y,z,zero
      integer i,ibase,ibot,idiff,ierr,itop,j,k
      data add /8.5d0/, times /7.15d0/, zero/0.0d0/, one /1.0d0/,
     ~    twopi /6.283185307179586d0/, const1/8.333333333333333d-2/,
     ~    const2 /2.777777777777778d-3/,const3 /7.936507936507937d-4/

      ierr = 1
      if (t.lt.zero) goto 140
      x=dsqrt(t)*times
      itop=idint(t+x+add)
      ibot=max(0,idint(t-x))
      ierr=2
      if ((itop-ibot+4).gt.nr) goto 140
      idiff=itop-nr
      ibase=ibot-idiff
      if (ibot.gt.0) goto 40

c     use the direct method if t .lt. 50.
      x=zero
      y=dexp(-t)
      z=zero
      do i=ibase,nr
         z=z+y
         r(i)=z
         x=x+one
         y=y*t/x
      enddo
      goto 120
c     use stirlings formula if t .gt. 50.
   40 i=idint(t)
      x=dfloat(i)
      z=one/(x*x)
      y=((t/x)**i)*dexp((x-t)-(const1-(const2-const3*z)*z)/x)/
     ~     dsqrt(twopi*x)
      j=i-idiff
      w=x
      z=y
      do k=ibase,j
         i=j+ibase-k
         r(i)=y
         y=y*x/t
         x=x-one
      enddo
      y=zero
      do i=ibase,j
         y=y+r(i)
         r(i)=y
      enddo
      j=j+1
      do i=j,nr
         w=w+one
         z=z*t/w
         y=y+z
         r(i)=y
      enddo
c     finish off in either case.
  120 call refindx8(idiff,ibase,r,nr)
      ifail=0
      return
  140 ifail=ierr
      return
      end
