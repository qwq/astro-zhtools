      subroutine sort2(n,ra,rb)
c
c
c
c QuiickSort of ra(n), same rearrangements in rb(n)
c
c
c
c
c
      implicit none
      integer n
      real ra,rb
      integer l,ir,i,j
      dimension ra(n),rb(n)
      real rra,rrb

      l=n/2+1
      ir=n
 10   continue
        if(l.gt.1)then
          l=l-1
          rra=ra(l)
          rrb=rb(l)
        else
          rra=ra(ir)
          rrb=rb(ir)
          ra(ir)=ra(1)
          rb(ir)=rb(1)
          ir=ir-1
          if(ir.eq.1)then
            ra(1)=rra
            rb(1)=rrb
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(ra(j).lt.ra(j+1))j=j+1
          endif
          if(rra.lt.ra(j))then
            ra(i)=ra(j)
            rb(i)=rb(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        ra(I)=rra
        rb(I)=rrb
      goto 10
      end





cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine sort28(n,ra,rb)
c
c
c QuiickSort of ra(n), same rearrangements in rb(n)
c 
c
c
c Same that sort2, but with REAL*8 precision
c
      implicit none
      integer n
      real*8 ra(n),rb(n)
      integer l,ir,i,j
      real*8 rra,rrb

      l=n/2+1
      ir=n
 10   continue
        if(l.gt.1)then
          l=l-1
          rra=ra(l)
          rrb=rb(l)
        else
          rra=ra(ir)
          rrb=rb(ir)
          ra(ir)=ra(1)
          rb(ir)=rb(1)
          ir=ir-1
          if(ir.eq.1)then
            ra(1)=rra
            rb(1)=rrb
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(ra(j).lt.ra(j+1))j=j+1
          endif
          if(rra.lt.ra(j))then
            ra(i)=ra(j)
            rb(i)=rb(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        ra(I)=rra
        rb(I)=rrb
      goto 10
      end
