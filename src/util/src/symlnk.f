      subroutine symlnk (path1, path2)
      implicit none
      character path1*(*), path2*(*)

      character p1*1024, p2*1024
      integer lnblnk, i

      p1 = path1
      p2 = path2
      
      i = lnblnk(p1)
      p1(i+1:i+1)=char(0)
      
      i = lnblnk(p2)
      p2(i+1:i+1)=char(0)

      call symlnk_c (p1,p2)

      return
      end

