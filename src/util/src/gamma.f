      function gamma(x,ifail)
c
c     gamma function
c
      real gamma
      real x
      integer ifail
      
      real g, gbig, t, xbig, xminv, xsmall, y
      integer i, m
      
      data xsmall/1.0e-8/
      
      data xbig,gbig,xminv /34.0e0,8.7e+36,5.9e-39/
      
c     error 1 and 2 test
      t=abs(x)
      if (t.gt.xbig) goto 160
c     small range test
      if (t.le.xsmall) goto 140
c     main range reduction
      m=x
      if (x.lt.0.0) goto 80
      t=x-float(m)
      m=m-1
      g=1.00
      if (m) 20, 120, 40
 20   g=g/x
      goto 120
 40   do i=1,m
        g=g*(x-float(i))
      enddo
      goto 120
 80   t=x-float(m-1)
c     error 4 test
      if (t.eq.1.0) goto 220
      m=1-m
      g=x
      do i=1,m
        g=g*(x+float(i))
      enddo
      g=1.0/g
 120  t=2.0*t-1.0
      
      y=  +8.86226925e-1+t*(  +1.61692007e-2+t*(  +1.03703361e-1+
     a    t*(  -1.34119055e-2+t*(  +9.04037536e-3+t*(  -2.42216251e-3+
     b    t*(  +9.15547391e-4+t*(  -2.98340924e-4+t*(  +1.01593694e-4+
     c    t*(  -3.13088821e-5+t*(  +1.03144033e-5+t*(  -5.48272091e-6+
     d    t*(  +1.88278283e-6))))))))))))
      
      gamma=y*g
      ifail=0
      goto 240
c
c     error 3 test
 140  if (t.lt.xminv) goto 200
      gamma=1.0/x
      ifail=0
      goto 240
c
c     error exits
 160  if (x.lt.0.0) goto 180
      ifail=1
      gamma=gbig
      goto 240
c
 180  ifail=2
      gamma=0.0d0
      goto 240
c
 200  ifail=3
      t=x
      if (x.eq.0.0d0) t=1.0
      gamma=sign(1.0/xminv,t)
      goto 240
c
 220  ifail=4
      gamma=gbig
c
 240  return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function gamma8(x,ifail)
c     gamma function
      implicit none
      real*8 gamma8
      real*8 x
      integer ifail
      real*8 g, gbig, t, xbig, xminv, xsmall, y
      integer i, m
      data xsmall/1.0d-17/
      data xbig,gbig,xminv /57.0d0,7.1d+74,1.4d-76/
      
c     error 1 and 2 test
      t = dabs(x)
      if (t.gt.xbig) go to 160
c     small range test
      if (t.le.xsmall) go to 140
c     main range reduction
      m = x
      if (x.lt.0.0d0) go to 80
      t = x - dfloat(m)
      m = m - 1
      g = 1.0d0
      if (m) 20, 120, 40
   20 g = g/x
      go to 120
   40 do 60 i=1,m
         g = g*(x-dfloat(i))
   60 continue
      go to 120
   80 t = x - dfloat(m-1)
c     error 4 test
      if (t.eq.1.0d0) go to 220
      m = 1 - m
      g = x
      do 100 i=1,m
         g = g*(x+dfloat(i))
  100 continue
      g = 1.0d0/g
  120 t = 2.0d0*t - 1.0d0
      y=  +8.86226925452758013d-1+t*(  +1.61691987244425092d-2+
     at*(  +1.03703363422075456d-1+t*(  -1.34118505705967765d-2+
     bt*(  +9.04033494028101968d-3+t*(  -2.42259538436268176d-3+
     ct*(  +9.15785997288933120d-4+t*(  -2.96890121633200000d-4+
     dt*(  +1.00928148823365120d-4+t*(  -3.36375833240268800d-5+
     et*(  +1.12524642975590400d-5+t*(  -3.75499034136576000d-6+
     ft*(  +1.25281466396672000d-6+t*(  -4.17808776355840000d-7+
     gt*(  +1.39383522590720000d-7+t*(  -4.64774927155200000d-8+
     ht*(  +1.53835215257600000d-8+t*(  -5.11961333760000000d-9+
     it*(  +1.82243164160000000d-9+t*( -6.13513953280000000d-10+
     jt*( +1.27679856640000000d-10+t*( -4.01499750400000000d-11+
     kt*( +4.26560716800000000d-11+
     lt*( -1.46381209600000000d-11)))))))))))))))))))))))

      gamma8 = y*g
      ifail = 0
      go to 240
c
c     error 3 test
  140 if (t.lt.xminv) go to 200
      gamma8 = 1.0d0/x
      ifail = 0
      go to 240
c
c     error exits
  160 if (x.lt.0.0d0) go to 180
      ifail = 1
      gamma8 = gbig
      go to 240
c
  180 ifail = 2
      gamma8 = 0.0d0
      go to 240
c
  200 ifail = 3
      t = x
      if (x.eq.0.0d0) t = 1.0d0
      gamma8 = dsign(1.0d0/xminv,t)
      go to 240
c
  220 ifail = 4
      gamma8 = gbig
c
  240 return
      end
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      function gammln(x)
      real gammln,x
      real g, gbig, lnr2pi, t, xbig, xsmall, xvbig, y
      integer i, m
      data xsmall,xbig,lnr2pi/
     >	1.0e-8,1.2e+3,9.18938533e-1/
      data xvbig,gbig/2.05d36,1.69e38/
      if (x.gt.xsmall) goto 20
c        very small range
      if (x.le.0.0d0) goto 160
      gammln=-alog(x)
      return
   20 if (x.gt.15.0d0) goto 120
c        main small x range
      m=x
      t=x-float(m)
      m=m-1
      g=1.0
      if(m) 40, 100, 60
   40 g=g/x
      goto 100
   60 do i=1,m
         g=g*(x-float(i))
      enddo
  100 t=2.0*t-1.0
      y=(((((((((((  +1.88278283e-6*t  -5.48272091e-6)*t
     ~  +1.03144033e-5)*t  -3.13088821e-5)*t  +1.01593694e-4)*t
     ~  -2.98340924e-4)*t  +9.15547391e-4)*t  -2.42216251e-3)*t
     ~  +9.04037536e-3)*t  -1.34119055e-2)*t  +1.03703361e-1)*t
     ~  +1.61692007e-2)*t  +8.86226925e-1
      gammln=alog(y*g)
      return
  120 if (x.gt.xbig) goto 140
c        main large x range
      t=450.0/(x*x)-1.0
      y=(  +3.89980902e-9*t  -6.16502533e-6)*t  +8.33271644e-2
      gammln=(x-0.5e0)*alog(x)-x+lnr2pi+y/x
      return
c
  140 if (x.gt.xvbig) goto 180
c        asymptotic large x range
      gammln=(x-0.5)*alog(x)-x+lnr2pi
      return
c
c        failure exits
  160 gammln=0.0
      return
  180 gammln=gbig
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function gammln8(x)
      implicit none
      real*8 gammln8,x
      real*8 g, gbig, lnr2pi, t, xbig, xsmall, xvbig, y
      integer i, m
      data xsmall,xbig,lnr2pi/
     ~    1.0d-17,7.7d+7,9.18938533204672742d-1/
      data xvbig,gbig/4.29d+73,7.231d+75/

      if (x.gt.xsmall) goto 20
c        very small range
      if (x.le.0.0d0) go to 160
      gammln8 = -dlog(x)
      go to 200
c
   20 if (x.gt.15.0d0) go to 120
c        main small x range
      m = x
      t = x - dble(m)
      m = m - 1
      g = 1.0d0
      if (m) 40, 100, 60
   40 g = g/x
      go to 100
   60 do 80 i=1,m
         g = g*(x-dfloat(i))
   80 continue
  100 t = 2.0d0*t - 1.0d0

      y=(((((((((((((((((((( -1.243191705600000d-10*t
     * +3.622882508800000d-10)*t -4.030909644800000d-10)*t
     *  +1.265236705280000d-9)*t  -5.419466096640000d-9)*t
     *  +1.613133578240000d-8)*t  -4.620920340480000d-8)*t
     *  +1.387603440435200d-7)*t  -4.179652784537600d-7)*t
     *  +1.253148247777280d-6)*t  -3.754930502328320d-6)*t
     *  +1.125234962812416d-5)*t  -3.363759801664768d-5)*t
     *  +1.009281733953869d-4)*t  -2.968901194293069d-4)*t
     *  +9.157859942174304d-4)*t  -2.422595384546340d-3)*t
     *  +9.040334940477911d-3)*t  -1.341185057058971d-2)*t
     *  +1.037033634220705d-1)*t  +1.616919872444243d-2)*t
     *  +8.862269254527580d-1

      gammln8 = dlog(y*g)

      go to 200
c
  120 if (x.gt.xbig) go to 140
c        main large x range
      t = 450.0d0/(x*x) - 1.0d0

      y=((( +2.002019273379824d-14*t -6.451144077929628d-12)*t
     *  +3.899788998764847d-9)*t  -6.165020494506090d-6)*t
     *  +8.332716440657866d-2

      gammln8 = (x-0.5d0)*dlog(x) - x + lnr2pi + y/x

      go to 200
c
  140 if (x.gt.xvbig) go to 180
c        asymptotic large x range
      gammln8 = (x-0.5d0)*dlog(x) - x + lnr2pi

      go to 200
c
c        failure exits
  160 go to 200
  180 continue
c
  200 continue
      return
c
      end
