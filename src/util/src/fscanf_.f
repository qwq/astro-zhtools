      integer function fscanf (unit,format,
     ~    x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,
     ~    x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21)
      
      integer unit
      character format*(*)
      integer x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,
     ~    x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21
      
      integer getfilep, fscanf_c
      integer ntokens
      
      ntokens=fscanf_c(getfilep(unit),format,
     ~    x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,
     ~    x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21)
      fscanf=ntokens
      return 
      end
      
