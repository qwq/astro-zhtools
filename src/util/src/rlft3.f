      subroutine rlft3 (data,speq,nn1,nn2,nn3,isign)
      implicit none
      integer isign, nn1,nn2,nn3
      complex data(nn1/2,nn2,nn3), speq(nn2,nn3)
      integer i1,i2,i3,j1,j2,j3,nn(3)
      double precision theta, wi, wpi, wpr, wr, wtemp
      complex c1,c2,h1,h2,w
      
      c1=cmplx(0.5,0.0)
      c2=cmplx(0.0,-0.5*isign)
      theta=6.28318530717959d0/dble(isign*nn1)
      wpr=-2.0d0*dsin(0.5d0*theta)**2
      wpi=dsin(theta)
      
      nn(1)=nn1/2
      nn(2)=nn2
      nn(3)=nn3
      
      if (isign.eq.1) then
        call fourn (data,nn,3,isign)
        do i3=1,nn3
          do i2=1,nn2
            speq(i2,i3)=data(1,i2,i3)
          enddo
        enddo
      endif
      
      do i3=1,nn3
        j3=1
        if(i3.ne.1) j3=nn3-i3+2
        wr=1.0d0
        wi=0.0d0
        do i1=1,nn1/4+1
          j1=nn1/2-i1+2
          do i2=1,nn2
            j2=1
            if(i2.ne.1)j2=nn2-i2+2
            if (i1.eq.1) then
              h1=c1*(data(1,i2,i3)+conjg(speq(j2,j3)))
              h2=c2*(data(1,i2,i3)-conjg(speq(j2,j3)))
              data(1,i2,i3)=h1+h2
              speq(j2,j3)=conjg(h1-h2)
            else
              h1=c1*(data(i1,i2,i3)+conjg(data(j1,j2,j3)))
              h2=c2*(data(i1,i2,i3)-conjg(data(j1,j2,j3)))
              data(i1,i2,i3)=h1+w*h2
              data(j1,j2,j3)=conjg(h1-w*h2)
            endif
          enddo
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
          w=cmplx(sngl(wr),sngl(wi))
        enddo
      enddo
      if (isign.eq.-1) then
        call fourn (data,nn,3,isign)
      endif
      return
      end
      
      
      subroutine rlft38 (data,speq,nn1,nn2,nn3,isign)
      implicit none
      integer isign, nn1,nn2,nn3
      double complex data(nn1/2,nn2,nn3), speq(nn2,nn3)
      integer i1,i2,i3,j1,j2,j3,nn(3)
      double precision theta, wi, wpi, wpr, wr, wtemp
      double complex c1,c2,h1,h2,w
      
      c1=dcmplx(0.5d0,0.0d0)
      c2=dcmplx(0.0d0,-0.5d0*isign)
      theta=6.28318530717959d0/dble(isign*nn1)
      wpr=-2.0d0*dsin(0.5d0*theta)**2
      wpi=dsin(theta)
      
      nn(1)=nn1/2
      nn(2)=nn2
      nn(3)=nn3
      
      if (isign.eq.1) then
        call fourn8 (data,nn,3,isign)
        do i3=1,nn3
          do i2=1,nn2
            speq(i2,i3)=data(1,i2,i3)
          enddo
        enddo
      endif
      
      
      do i3=1,nn3
        j3=1
        if(i3.ne.1) j3=nn3-i3+2
        wr=1.0d0
        wi=0.0d0
        do i1=1,nn1/4+1
          j1=nn1/2-i1+2
          do i2=1,nn2
            j2=1
            if(i2.ne.1)j2=nn2-i2+2
            if (i1.eq.1) then
              h1=c1*(data(1,i2,i3)+dconjg(speq(j2,j3)))
              h2=c2*(data(1,i2,i3)-dconjg(speq(j2,j3)))
              data(1,i2,i3)=h1+h2
              speq(j2,j3)=conjg(h1-h2)
            else
              h1=c1*(data(i1,i2,i3)+dconjg(data(j1,j2,j3)))
              h2=c2*(data(i1,i2,i3)-dconjg(data(j1,j2,j3)))
              data(i1,i2,i3)=h1+w*h2
              data(j1,j2,j3)=dconjg(h1-w*h2)
            endif
          enddo
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
          w=dcmplx(wr,wi)
        enddo
      enddo
      if (isign.eq.-1) then
        call fourn8 (data,nn,3,isign)
      endif
      return
      end
      
      
      






