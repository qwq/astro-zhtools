      subroutine ad_rad_sign(h,m,s,sgn,d,am,as,ra,dec)
c
c  converts  h:m:s d:am:as  into ra, dec (rad)
c
c  ra, dec - double precision
c
c      
      implicit none
      integer h,m,d,am
      real s,as
      double precision ra,dec
      character sgn*1
      
      
      double precision s8,as8
      integer d0,am0
      
      integer statusra, statusdec
      
      double precision pi2
      parameter (pi2=3.14159265358979d0/2.0d0)
      
      s8=s
      as8=as
      
      
      
      call sla_dtf2r(h,m,s8,ra,statusra)
      
      d0=d
      am0=am
      if(sgn.eq.'-')then
        d0=-d
        am0=-am
        as8=-as8
      endif
      d0=d0+90
      
      call sla_daf2r(d0,am0,as8,dec,statusdec)
      dec=dec-pi2
      
      if(statusra.ne.0)then
        if(statusra.eq.1)then
          print*,'--!!!-- hours outside of 0-23 in ad_rad'
        elseif(statusra.eq.2)then
          print*,'--!!!-- min outside of 0-59 in ad_rad'
        elseif(statusra.eq.3)then
          print*,'--!!!-- sec outside of 0-59.99... in ad_rad'
        endif
      endif
      
      if(statusdec.ne.0)then
        if(statusra.eq.2)then
          print*,'--!!!-- arcmin outside of 0-59 in ad_rad'
        elseif(statusra.eq.3)then
          print*,'--!!!-- arcsec outside of 0-59.99... in ad_rad'
        endif
      endif
      
      return
      end
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine rad_ad_sign(ra,dec,h,m,s,sgn,d,am,as)
c
c  converts a,d (rad)  into  h:m:s d:am:as 
c
c  a,d - double precision
c
c      
      implicit none
      integer h,m,d,am
      real s,as
      double precision ra,dec
      character sgn*1
      
      integer ndp
      parameter (ndp=4)
      character sign*1
      integer ihmsf(4)

      call sla_dr2tf(ndp,ra,sign,ihmsf)
      h=ihmsf(1)
      m=ihmsf(2)
      s=ihmsf(3)+ihmsf(4)/10.0d0**ndp
      
      call sla_dr2af(ndp,dec,sgn,ihmsf)
      d=ihmsf(1)
      am=ihmsf(2)
      as=ihmsf(3)+ihmsf(4)/10.0d0**ndp
      
      
      return
      end
      








