      function ran28(idum0)
c
c  first call should with negative idum0 !!!
c
c
c
c
      implicit none
      integer idum0,idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,IR2,
     ~    NTAB,NDIV
      double precision ran28,AM,EPS,RNMX, ran28w
      real ran2,eps4,rnmx4,AM4, ran2w
      parameter (IM1=2147483563, IM2=2147483399, AM=1.0d0/IM1,
     ~    IMM1=IM1-1,IA1=40014,IA2=40692, IQ1=53668, IQ2=52774,
     ~    IR1=12211, IR2=3791, NTAB=32, NDIV=1+IMM1/NTAB, AM4=AM)
      integer idum2,j,k,iv(NTAB),iy
      logical flag
      save iv,iy,idum2,flag,idum,EPS,RNMX,eps4,rnmx4
      
      data idum2 /123456789/, iy /0/, flag /.true./

      if(flag) then
        call ini_ran2 (iv,NTAB,iy,idum0,idum,idum2,rnmx4,RNMX,eps4,eps,
     ~      IM1,IR1,IQ1,IA1)
        flag=.false.
      endif
      
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if(idum.lt.0) idum=idum+IM1
      k=idum2/IQ2
      idum2=IA2*(idum2-k*IQ2)-k*IR2
      if(idum2.lt.0) idum2=idum2+IM2
      j=1+iy/NDIV
      iy=iv(j)-idum2
      iv(j)=idum
      if(iy.lt.1)iy=iy+IMM1
      ran28w=min(AM*iy,RNMX)
      ran28=max(ran28w,EPS)
      return
      

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      entry ran2(idum0)
      
      if(flag) then
        call ini_ran2 (iv,NTAB,iy,idum0,idum,idum2,rnmx4,RNMX,eps4,eps,
     ~      IM1,IR1,IQ1,IA1)
        flag=.false.
      endif
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if(idum.lt.0) idum=idum+IM1
      k=idum2/IQ2
      idum2=IA2*(idum2-k*IQ2)-k*IR2
      if(idum2.lt.0) idum2=idum2+IM2
      j=1+iy/NDIV
      iy=iv(j)-idum2
      iv(j)=idum
      if(iy.lt.1)iy=iy+IMM1
      ran2w=min(AM4*iy,rnmx4)
      ran2=max(ran2w,eps4)
      return
      
      end
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine ini_ran2 (iv,NTAB,iy,idum0,idum,idum2,rnmx4,RNMX,eps4,eps,
     ~    IM1,IR1,IQ1,IA1)
      implicit none
      integer NTAB,IM1,IR1,IQ1,IA1,iy,idum0,idum,idum2
      integer iv(NTAB)
      real rnmx4,eps4
      double precision RNMX,EPS
      integer j,k
      double precision EPS1
      real eps14

      do j=1,NTAB
        iv(j)=0
      enddo
      EPS=1.0d0
 10   EPS=0.5d0*EPS
      eps1=1.0d0+EPS
      if(eps1.gt.1.0d0) goto 10
      EPS=2.0d0*EPS
      RNMX=1.0d0-EPS
      eps4=1.0
 20   eps4=0.5*eps
      eps14=1.0+eps4
      if(eps14.gt.1.0) goto 20
      eps4=2.0*eps4
      rnmx4=1.0-eps4
      idum=max(-idum0,1)
      if(idum0.eq.0)idum=100
      idum2=idum
      do j=NTAB+8,1,-1
        k=idum/IQ1
        idum=IA1*(idum-k*IQ1)-k*IR1
        if(idum.lt.0) idum=idum+IM1
        if(j.le.NTAB) iv(j)=idum
      enddo
      iy=iv(1)
      
      return
      end
