#include <stdlib.h>

void isanum_c_ (char *string, int *flag)
{
  char *p;
  double x;
  
  x = strtod(string,&p);
  if (*p != '\0' ) {
    *flag = 0;
  } else {
    *flag = 1;
  }
}
