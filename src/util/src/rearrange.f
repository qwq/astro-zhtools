      subroutine rearrange (x,index,n,type)
      implicit none
      integer x(*)
      integer index(*)
      integer n
      character type*(*)

      
      if       (type(1:1).eq.'b'.or.type(1:1).eq.'B') then
        call rearrange_b (x,index,n)
        
      else if (type(1:1).eq.'i'.or.type(1:1).eq.'I') then
        call rearrange_i (x,index,n)
        
      else if (type(1:1).eq.'j'.or.type(1:1).eq.'J') then
        call rearrange_j (x,index,n)
        
      else if (type(1:1).eq.'e'.or.type(1:1).eq.'E') then
        call rearrange_e (x,index,n)
        
      else if (type(1:1).eq.'d'.or.type(1:1).eq.'D') then
        call rearrange_d (x,index,n)
        
      else
        write (0,*) 'Unknown type: ', type(1:1),' in rearrange'
        call exit (1)
      endif

      return
      end
      
