c
c
c
c    real*8 version of the common ameoba
c
c
c
c
c

      subroutine amoeba8(x0,dx,ndim,ftol,maxit,func,iret)

c---------------------------------------------------------
c     multidimentional simplex minimization
c
c     ndim - number of dimensions
c     func(x,ndim) - function to be minimized
c     x0(ndim) - input : starting vector
c              output: position of minimum
c     dx - step for construction of starting simplex
c     ftol - fractional tolerance to stop minimization
c     maxit - maximum number of iterations
c
c     return code:
c        0 - ok
c        1 - amoeba exeeds maxit
c        2 - too many dimensions
c
c        ########## call amoeba with iret=0
c---------------------------------------------------------
      implicit none
      real*8 x0(*)
      real*8 dx
      integer ndim,maxit
      real*8 ftol
      integer iret
      integer nmax,mpmax
      parameter(nmax=40)
      parameter(mpmax=nmax+1)
      real*8 p(mpmax,nmax),y(mpmax),x(nmax)
 
      real*8 func,env8
      external func
 
      integer iter
      integer mp,np,iv,k
 
*      external ameoba08
 
c==== check dimensions
      if(ndim.gt.nmax) then
         write (0,*) '---> amoeba: too many dimensions'
         iret=2
         return
      endif
 
c==== starting simplex
      mp=ndim+1
      np=ndim
      do 100 iv=1,mp
      do 100 k=1,np
         p(iv,k)=x0(k)
100   continue
      do 110 iv=2,mp
         p(iv,iv-1)=p(iv,iv-1)+dx
110   continue
 
c==== function values
      do 130 iv=1,mp
         do 135 k=1,np
            x(k)=p(iv,k)
135      continue

         if (iret.eq.444) then
           y(iv)=env8(x,ndim, func)
         else
           y(iv)=func(x,ndim)
         end if

130   continue
 
c==== amoeba
      call amoeb08(p,y,mp,np,ndim,ftol,func,maxit,iter,iret)
      if(iret.eq.1) then
         write (0,*) '---> amoeba: exceeding maximum iterations'
      endif
 
c==== minimum position
      do 200 k=1,ndim
         x0(k)=0
         do 210 iv=1,mp
            x0(k)=x0(k)+p(iv,k)
210      continue
         x0(k)=x0(k)/dble(mp)
200   continue
 
 
      return
      end
c=========================================================
      subroutine amoebf8(x0,fx0,dx,ndim0,ftol,maxit,func,iret)
c---------------------------------------------------------
c     multidimentional simplex minimization
c     with fit/no fit  logics
c
c     ndim0 - number of dimensions
c     func(x,ndim0) - function to be minimized
c     x0(ndim0) - input : starting vector
c                 output: position of minimum
c     fx0(ndim0) - fit/not parameter:
c                  if set to 1 fit this parameter, x0(i) - starting val
c                  if set to 0 don't fit, set to x0(i)
c     dx - step for construction of starting simplex
c     ftol - fractional tolerance to stop minimization
c     maxit - maximum number of iterations
c
c     return code:
c        0 - ok
c        1 - amoeba exeeds maxit
c        2 - too many dimensions
c---------------------------------------------------------
      implicit none
      real*8 x0
      dimension x0(*)
      integer fx0(*)
      real*8 dx
      integer ndim0
      real*8 ftol
      integer maxit
      real*8 func,env8
      integer iret
 
      real*8 xw,x
      integer ixno,ndimo,i,k,ndim
      integer nmax
      parameter(nmax=40)
      dimension x(nmax)
      common /entry/ xw(nmax), ixno(nmax), ndimo
 
      external func,env8

      ndimo=ndim0
      iret=0
 
c==== account for fit/no fit logics, prepare for ameoba
      ndim=0
      do 150 i=1,ndim0
         if(fx0(i).eq.0) goto 150
         ndim=ndim+1
         ixno(ndim)=i
         x(ndim)=x0(i)
150   continue
      if(ndim.eq.0) return

      do 100 i=1,ndim0
        xw(i)=x0(i)
 100   continue

c==== ameoba

      iret=444
      call amoeba8(x,dx,ndim,ftol,maxit,func,iret)
      if (iret.eq.444) iret = 0
 
c==== minimum position
      do 200 k=1,ndim
         x0(ixno(k))=x(k)
200   continue
 
 
      return
      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      function env8 (x,ndim,func)
      implicit none
      real*8 env8,func
      integer ndim
      integer nmax
      real*8 xw
      integer ixno,ndim0
      parameter(nmax=40)
      common /entry/ xw(nmax), ixno(nmax), ndim0
      real*8 x(*)
      external func
      integer i

      do 110 i=1,ndim
         xw(ixno(i))=x(i)
110   continue
 
      env8=func(xw,ndim0)

      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine amoeb08(p,y,mp,np,ndim,ftol,func,itmax,iter,iret)
c---------------------------------------------------------
c     multidimentional simplex minimization
c     ground level routine
c     from "numerical recipes"
c---------------------------------------------------------
      implicit none
      integer nmax,mpmax
      real*8 p,y,pr,prr,pbar
      integer mp,np,ndim,itmax,iter,iret
      real*8 ftol,func
      
      external func
 
      parameter(nmax=40)
      parameter(mpmax=nmax+1)
      dimension p(mpmax,nmax),y(mpmax),pr(nmax),prr(nmax),pbar(nmax)
 
      real*8 alpha,beta,gamma
      parameter(alpha=1.0d0,beta=0.5d0,gamma=2.0d0)
      integer mpts,ilo,ihi,inhi,i,j
      real*8 rtol,ypr,yprr
      real*8 env8
 
 
      mpts=ndim+1
      iter=0
 
1     ilo=1
 
c==== find highest, next highest and lowest points
      if(y(1).gt.y(2)) then
         ihi=1
         inhi=2
      else
         ihi=2
         inhi=1
      endif
      do 11 i=1,mpts
         if(y(i).lt.y(ilo)) ilo=i
         if(y(i).gt.y(ihi)) then
            inhi=ihi
            ihi=i
         else
            if(y(i).gt.y(inhi)) then
               if(i.ne.ihi) inhi=i
            endif
         endif
11    continue
 
c==== fractional range from highest to lowest points
c---- calculate
      rtol=2.d0*dabs(y(ihi)-y(ilo))/(dabs(y(ihi))+dabs(y(ilo)))
*      print*,'iteration',iter,'  fract.range',rtol
c---- return if satisfactory
      if(rtol.lt.ftol) return
 
c==== check number of iterations
      if(iter.eq.itmax) then
        iret=1
        return
      endif
 
c==== begin new iteration
      iter=iter+1
c---- center of the face of simplex, opposite to the highest point
      do 12 j=1,ndim
         pbar(j)=0
12    continue
      do 14 i=1,mpts
         if(i.ne.ihi) then
            do 13 j=1,ndim
               pbar(j)=pbar(j)+p(i,j)
13          continue
         endif
14    continue
c---- extrapolate by a factor alpha through the face
      do 15 j=1,ndim
         pbar(j)=pbar(j)/ndim
         pr(j)=(1.+alpha)*pbar(j)-alpha*p(ihi,j)
15    continue
c---- evaluate function in reflected point

      if (iret.eq.444) then
        ypr=env8(pr,ndim, func)
      else
        ypr=func(pr,ndim)
      end if

      if(ypr.le.y(ilo)) then
c------- if better than lowest point, make additional extrapol. by gamm
c....... extrapolate
         do 16 j=1,ndim
            prr(j)=gamma*pr(j)+(1.-gamma)*pbar(j)
16       continue
c....... evaluate function after additional extrapolation
         if (iret.eq.444) then
           yprr=env8(prr,ndim, func)
         else
           yprr=func(prr,ndim)
         end if

         if(yprr.le.y(ilo)) then
c.......... if succeed, replace highest point
            do 17 j=1,ndim
               p(ihi,j)=prr(j)
17          continue
            y(ihi)=yprr
         else
c.......... if additional extrapol. failed, but reflected point ok
            do 18 j=1,ndim
               p(ihi,j)=pr(j)
18          continue
            y(ihi)=ypr
         endif
      else
c------- if refl.point is worse than 2-d highest
         if(ypr.ge.y(inhi)) then
            if(ypr.lt.y(ihi)) then
c.......... but better than highest
               do 19 j=1,ndim
                  p(ihi,j)=pr(j)
19             continue
               y(ihi)=ypr
            endif
c.......... look for intermideate
            do 21 j=1,ndim
               prr(j)=beta*p(ihi,j)+(1.-beta)*pbar(j)
21          continue

            if (iret.eq.444) then
              yprr=env8(prr,ndim, func)
            else
              yprr=func(prr,ndim)
            end if

            if(yprr.lt.y(ihi)) then
c............. if intermidiate ok, accept it
               do 22 j=1,ndim
                  p(ihi,j)=prr(j)
22             continue
               y(ihi)=yprr
            else
c............. if not
               do 24 i=1,mpts
                  if(i.ne.ilo) then
                     do 23 j=1,ndim
                        pr(j)=0.5*(p(i,j)+p(ilo,j))
                        p(i,j)=pr(j)
23                   continue
                     if (iret.eq.444) then
                       y(i)=env8(pr,ndim, func)
                     else
                       y(i)=func(pr,ndim)
                     end if
                  endif
24             continue
            endif
         else
            do 25 j=1,ndim
               p(ihi,j)=pr(j)
25          continue
            y(ihi)=ypr
         endif
      endif
 
c==== goto to the test of result and next iteration
      goto 1
 
      end

