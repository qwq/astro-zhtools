      subroutine indexx(n,arrin,indx)
c
c
c   indexes an array arrin(n), i.e. outputs an array indx(n)
c
c  such that arr(indx(j)) are in ascending order for j=1,..N
c
c  N and ArriN are bot changed on exit
c
c
c
      implicit none
      integer n,i,j,l,ir,indxt,indx
      real arrin
      dimension arrin(n),indx(n)
      real q

      do j=1,n
        indx(j)=j
      enddo

      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          indxt=indx(l)
          q=arrin(indxt)
        else
          indxt=indx(ir)
          q=arrin(indxt)
          indx(ir)=indx(1)
          ir=ir-1
          if(ir.eq.1)then
            indx(1)=indxt
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
          endif
          if(q.lt.arrin(indx(j)))then
            indx(i)=indx(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        indx(i)=indxt
      goto 10
      end




cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine indexx8(n,arrin,indx)
c
c
c   indexes an array arrin(n), i.e. outputs an array indx(n)
c
c  such that arr(indx(j)) are in ascending order for j=1,..N
c
c  N and ArriN are bot changed on exit
c
c  Same that indexx, but with real*8 precision
c
      implicit none
      integer n,i,j,l,ir,indxt,indx(n)
      real*8 arrin(n)
      real*8 q

      do j=1,n
        indx(j)=j
      enddo

      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          indxt=indx(l)
          q=arrin(indxt)
        else
          indxt=indx(ir)
          q=arrin(indxt)
          indx(ir)=indx(1)
          ir=ir-1
          if(ir.eq.1)then
            indx(1)=indxt
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
          endif
          if(q.lt.arrin(indx(j)))then
            indx(i)=indx(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        indx(i)=indxt
      goto 10
      end
      
      
      
      
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine indexxi4(n,arrin,indx)
c
c
c   indexes an array arrin(n), i.e. outputs an array indx(n)
c
c  such that arr(indx(j)) are in ascending order for j=1,..N
c
c  N and ArriN are bot changed on exit
c
c
c
      implicit none
      integer n,i,j,l,ir,indxt,indx
      integer arrin
      dimension arrin(n),indx(n)
      integer q

      do j=1,n
        indx(j)=j
      enddo

      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          indxt=indx(l)
          q=arrin(indxt)
        else
          indxt=indx(ir)
          q=arrin(indxt)
          indx(ir)=indx(1)
          ir=ir-1
          if(ir.eq.1)then
            indx(1)=indxt
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
          endif
          if(q.lt.arrin(indx(j)))then
            indx(i)=indx(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        indx(i)=indxt
      goto 10
      end




