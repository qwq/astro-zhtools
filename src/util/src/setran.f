      subroutine setran1(idum)
      implicit none
      integer idum,idum0
      double precision ran18,w
      idum0=idum
      if(idum0.gt.0)idum0=-idum0
      w=ran18(idum0)
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine setran2(idum)
      implicit none
      integer idum,idum0
      double precision ran28,w
      idum0=idum
      if(idum0.gt.0)idum0=-idum0
      
      w=ran28(idum0)
      
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine ran_setran2
      implicit none
      integer time,n
      character arg*80
      logical defined
      
      call get_command_line_par ('ranseed',1,arg)
      if (defined(arg)) then
        read (arg,*) n
      else
        n=time()
        n=n**10+n**9+n**8+n**7+n**6+n**5+n**4+n**3+n**2+n+n/2+n/3
        n=iabs(n)
      endif

      call setran2(n)
      return
      end
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine ran_setran1
      implicit none
      integer time,n
      character arg*80
      logical defined

      call get_command_line_par ('ranseed',1,arg)
      if (defined(arg)) then
        read (arg,*) n
      else
        n=time()
        n=n**10+n**9+n**8+n**7+n**6+n**5+n**4+n**3+n**2+n+n/2+n/3
        n=iabs(n)
      endif


      n=time()
      n=n**10+n**9+n**8+n**7+n**6+n**5+n**4+n**3+n**2+n+n/2+n/3
      n=iabs(n)
      
      call setran1(n)
      return
      end
