      subroutine diffr_w8(a,nx,ny)
      implicit none
      integer nx,ny,nx2,ny2
      real*8 a(nx,ny)
      real*8 r
      integer i,j
      
      nx2=nx/2
      ny2=ny/2
      
      do i=1,nx
        do j=1,ny2
          r=a(i,j)
          a(i,j)=a(i,j+ny2)
          a(i,j+ny2)=r
        enddo
      enddo
      
      do i=1,nx2
        do j=1,ny
          r=a(i,j)
          a(i,j)=a(i+nx2,j)
          a(i+nx2,j)=r
        enddo
      enddo
      
      return
      end



      subroutine diffr_w(a,nx,ny)
      implicit none
      integer nx,ny,nx2,ny2
      real a(nx,ny)
      real r
      integer i,j
      
      nx2=nx/2
      ny2=ny/2
      
      do i=1,nx
        do j=1,ny2
          r=a(i,j)
          a(i,j)=a(i,j+ny2)
          a(i,j+ny2)=r
        enddo
      enddo
      
      do i=1,nx2
        do j=1,ny
          r=a(i,j)
          a(i,j)=a(i+nx2,j)
          a(i+nx2,j)=r
        enddo
      enddo
      
      return
      end











