      function factln(n)
c
c
c  ln(n!)
c
c      
      implicit none
      real factln
      integer n
      real nr,gammln
      
      integer nmax,i
      parameter (nmax=1000)
      real sample(nmax)
      logical firstcall
      save firstcall,sample
      data firstcall /.true./
      
      if(firstcall)then
        firstcall=.false.
        do i=1,nmax
          nr=float(i)+1.0
          sample(i)=gammln(nr)
        enddo
      endif
      
      if(n.gt.0)then
        if(n.le.nmax)then
          factln=sample(n)
        else
          nr=float(n)+1.
          factln=gammln(nr)
        endif
      else
        factln=0.0
      endif
      return
      end
      
      
      function factln8(n)
c
c
c  ln(n!)
c
c      
      implicit none
      real*8 factln8
      integer n
      real*8 nr,gammln8
      
      integer nmax,i
      parameter (nmax=1000)
      real*8 sample(nmax)
      logical firstcall
      save firstcall,sample
      data firstcall /.true./
      
      if(firstcall)then
        firstcall=.false.
        do i=1,nmax
          nr=dfloat(i)+1.0d0
          sample(i)=gammln8(nr)
        enddo
      endif
      
      if(n.gt.0)then
        if(n.le.nmax)then
          factln8=sample(n)
        else
          nr=dfloat(n)+1.0d0
          factln8=gammln8(nr)
        endif
      else
        factln8=0.0d0
      endif
      return
      end
      
      

      function fact8(n)
c
c
c  n!
c
c      
      implicit none
      real*8 fact8
      integer n
      real*8 nr
      real*8 factln8,gamma8
      integer ifail
      
      integer nmax,i
      parameter (nmax=60)
      real*8 sample(nmax)
      logical firstcall
      save firstcall,sample
      data firstcall /.true./
      
      if(firstcall)then
        firstcall=.false.
        do i=1,nmax
          nr=dfloat(i)+1.0d0
          sample(i)=gamma8(nr,ifail)
        enddo
      endif
      
      if(n.gt.0)then
        if(n.le.nmax)then
          fact8=sample(n)
        else
          nr=factln8(n)
          fact8=dexp(nr)
        endif
      else
        fact8=1.0d0
      endif
      return
      end

