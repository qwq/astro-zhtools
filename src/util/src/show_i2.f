      subroutine show_i2(z,nx,ny)
      implicit none
      integer nx,ny
      integer*2 z(nx,ny)
      
      character command*200
      integer unit,newunit

      unit = newunit()
      
      write(command,782)nx,ny
 782  format('saoimage -skip 4 -i2 ',i5,1x,i5,' /tmp/_show_')
      
      open(unit,file='/tmp/_show_',form='unformatted')
      write(unit)z
      call flush(unit)
      
      call system(command)
      
      close(unit,status='delete')
      
      return
      end
      
      

