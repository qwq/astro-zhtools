      subroutine mtrx_smult(a,n,b)
c
c  a(n)*b
c
c      
      integer n
      real a(n),b
      integer i
      do i=1,n
        a(i)=a(i)*b
      enddo
      return
      end
      subroutine mtrx_smult8(a,n,b)
c
c  a(n)*b
c
c      
      integer n
      real*8 a(n),b
      integer i
      do i=1,n
        a(i)=a(i)*b
      enddo
      return
      end
      
