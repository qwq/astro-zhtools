//#include<stdio.h>
#include<stdlib.h>

int XMIN, XMAX, YMIN, YMAX;
int NX, NY;
int *MAP;
int CONNECT;

int (*getPixel)(int, int);
void (*setPixel)(int, int, int);

/* Fill without recursion */
int *PendingUpList;
int *PendingDownList;
void FloodFill_NoRecurse(int x0, int y0, int fill_color, int original_color);
void RowUp(int row, int fill_color, int original_color);
void RowDown(int row, int fill_color, int original_color);
int FillRow(int fillrow, int refrow, int fill_color, int original_color);
int InkRow(int y, int fill_color, int original_color);
void MarkPendingUp(int row, int flag);
void MarkPendingDown(int row, int flag);
int CheckPendingUp(int row);
int CheckPendingDown(int row);

/**********************************************************************/
int getPixelF (int x, int y)
{
  return MAP[NX*y+x];
}

/**********************************************************************/
int getPixelC (int x, int y)
{
  return MAP[NY*x+y];
}

/**********************************************************************/
void setPixelF (int x, int y, int color)
{
  MAP[NX*y+x]=color;
}

/**********************************************************************/
void setPixelC (int x, int y, int color)
{
  MAP[NY*x+y]=color;
}

/**********************************************************************/
void iniFloodFill (int *img, int nx, int ny, int fortran, int connect)
{
  XMIN = 0; XMAX = nx-1;
  YMIN = 0; YMAX = ny-1;
  NX = nx; NY = ny;
  MAP = img;
  CONNECT = connect;
  if (fortran) {
    getPixel = (int (*)(int,int))getPixelF;
    setPixel = (void (*)(int,int,int))setPixelF;
  } else {
    getPixel = (int (*)(int,int))getPixelC;
    setPixel = (void (*)(int,int,int))setPixelC;
  }
}

/**********************************************************************/
void inifloodfill_ (int *img, int *nx, int *ny, int *fortran, int *connect)
{
  iniFloodFill (img, *nx, *ny, *fortran, *connect);
}


/**********************************************************************/


/* Idea: fill the lines with a loop instead of recursive calls.  */
void FloodFill (int x0, int y0, int fill_color, int original_color)
{
  int color, x, y;
  if (x0<XMIN || x0>XMAX || y0<YMIN || y0>YMAX ) return;
  x = x0; y = y0;
  color = (*getPixel)(x, y);
  if (color == original_color) {
    while (color == original_color) {
      (*setPixel)(x, y, fill_color);
      FloodFill(x, y+1, fill_color, original_color);
      FloodFill(x, y-1, fill_color, original_color);
      if (CONNECT==8) {
	FloodFill(x+1, y+1, fill_color, original_color);
	FloodFill(x+1, y-1, fill_color, original_color);
	FloodFill(x-1, y+1, fill_color, original_color);
	FloodFill(x-1, y-1, fill_color, original_color);
      }
      x = x - 1;
      color = (*getPixel)(x, y);
    }
    x = x0 + 1; y = y0;
    color = (*getPixel)(x, y);
    while (color == original_color) {
      (*setPixel)(x, y, fill_color);
      FloodFill(x, y+1, fill_color, original_color);
      FloodFill(x, y-1, fill_color, original_color);
      if (CONNECT==8) {
	FloodFill(x+1, y+1, fill_color, original_color);
	FloodFill(x+1, y-1, fill_color, original_color);
	FloodFill(x-1, y+1, fill_color, original_color);
	FloodFill(x-1, y-1, fill_color, original_color);
      }
      x = x + 1;
      color=(*getPixel)(x, y);
    }
  }
}

/*****************************************************************************
FloodFill_NoRecurse

Flood fill without using recursion. Crawl image line by line and iterate until
no new pixel fills occur.

Basic description:

1) InkRow paints in the x direction,
2) FillRow paints in the y direction, then calls InkRow on the painted row.
3) RowUp uses FillRow from current row onto row above, then set flag for
   recheck if row is filled (to propagate the newly filled pixels).
4) RowDown does a similar thing but moves down rows.
5, 6, 7, 8) MarkPendingUp, MarkPendingDown, CheckPendingUp, CheckPendingDown
  set and get the pending flags.

Step 0. Zero the pending flags arrays, set flags for row y0.
Step 1. Paint the initial pixel (x0, y0) fill_color
Step 2. InkRow on row y0
Step 3. Starting from YMIN, process all marked rows upward (flags are also
set/unset during the run)
Step 4. Starting from YMAX, process all marked rows downward
Step 5. Iterate steps 3 and 4 until no set flags remain.

2015-10-12 13:02 - Qian
*/
void FloodFill_NoRecurse(int x0, int y0, int fill_color, int original_color)
{
  int color, pxcount, pending, y1, i, n_rows=(YMAX-YMIN+1);
  // Step 0.
  PendingUpList = (int *) malloc(sizeof(int) * n_rows);
  PendingDownList = (int *) malloc(sizeof(int) * n_rows);
  for (i=0; i<=n_rows; i++) PendingUpList[i] = PendingDownList[i] = 0;
  MarkPendingUp(y0,1);
  MarkPendingDown(y0,1);
  // Step 1.
  color = (*getPixel)(x0,y0);
  if (color == original_color || color == fill_color) (*setPixel)(x0,y0,fill_color);
  else return;
  // Step 2.
  pxcount=InkRow(y0,fill_color,original_color);
  do {
    pending=0;
    // Step 3.
    y1=YMIN;
    do {
      if (CheckPendingUp(y1))
      {
        pending++;
        RowUp(y1, fill_color, original_color);
      }
    } while (++y1 < YMAX);
    // Step 4.
    y1=YMAX;
    do {
      if (CheckPendingDown(y1))
      {
        pending++;
        RowDown(y1, fill_color, original_color);
      }
    } while (--y1 > YMIN);
  } while (pending);
  // Free memory!
  free(PendingUpList);
  free(PendingDownList);
}

void RowUp(int row, int fill_color, int original_color)
{
  int fillcount = FillRow(row+1,row,fill_color,original_color);
  if (fillcount == 0) MarkPendingUp(row,0);
  else
  {
    MarkPendingUp(row+1,1);
    MarkPendingDown(row+1,1);
  }
}

void RowDown(int row, int fill_color, int original_color)
{
  int fillcount = FillRow(row-1,row,fill_color,original_color);
  if (fillcount == 0) MarkPendingDown(row,0);
  else
  {
    MarkPendingDown(row-1,1);
    MarkPendingUp(row-1,1);
  }
}

// Blot refrow pixels that have fill_color onto fillrow pixels that have
// original_color, then call InkRow to fill the rest of the row.
int FillRow(int fillrow, int refrow, int fill_color, int original_color)
{
  int i, refcolor, color, x=XMIN, fillcount=0;
  do {
    refcolor = (*getPixel)(x,refrow);
    if (refcolor == fill_color)
    {
      color = (*getPixel)(x,fillrow);
      if (color == original_color)
      {
        (*setPixel)(x,fillrow,fill_color);
        fillcount++;
      }
    }
  } while (XMAX >= ++x);
  fillcount+=InkRow(fillrow, fill_color, original_color);
  return fillcount;
}

int InkRow(int y, int fill_color, int original_color)
{
  int i, color, x=XMIN, fillcount=0, a;
  do {
    color = (*getPixel)(x, y);
    if (color == fill_color)
    { // Go left first
      a = x;
      while (--a >= XMIN)
      {
        color = (*getPixel)(a, y);
        if (color == original_color)
        {
          (*setPixel)(a, y, fill_color);
          fillcount++;
        }
        else
        { // Not original_color or already painted fill_color
          break;
        }
      }
      // Now go right, this time we can increase x itself
      while (++x <= XMAX)
      {
        color = (*getPixel)(x, y);
        if (color == fill_color)
        { // Already filled, keep going
          continue;
        }
        else if (color == original_color)
        { // Fill then continue right
          (*setPixel)(x, y, fill_color);
          fillcount++;
          continue;
        }
        else
        {
          break; // No more on the right; outer do loop will resume searching
          // from the next pixel on the right (if not at edge)
        }
      }
    } // If pixel not fill_color, continue searching to the right.
  } while (++x <= XMAX);
  return fillcount;
}

// Note: Array indices are offsets from YMIN
void MarkPendingUp(int row, int flag)
{
  PendingUpList[row-YMIN] = flag;
}

void MarkPendingDown(int row, int flag)
{
  PendingDownList[row-YMIN] = flag;
}

int CheckPendingUp(int row)
{
  return PendingUpList[row-YMIN];
}

int CheckPendingDown(int row)
{
  return PendingDownList[row-YMIN];
}

//****************************************************************************


void floodfill_ (int *x0, int *y0, int *fill_color, int *original_color)
{
  //printf("Fill@px %d, %d ",*x0,*y0);
  FloodFill_NoRecurse(*x0-1, *y0-1, *fill_color, *original_color);
  //FloodFill (*x0-1, *y0-1, *fill_color, *original_color);
}
