      subroutine move_ext (unit,name,status)
c
c
c Move to the named extension of a fits file.
c
c character name*(*)
c
c status: 0 - OK
c         1 - ext table not found
c
      implicit none
      integer unit
      character name*(*)
      integer status
      
      character name0*80,name1*80,comment*80
      character errtext*50
      integer hdtype
      
      
      name0=name
      status=0
      
      call ftmahd (unit, 1, hdtype, status) ! Goto the primary array
      if (status.ne.0) then
        call ftgerr(status,errtext)
        print*,'FITSIO: ',errtext
        return
      endif
      
      do while (status.eq.0) 
        call ftmrhd (unit, 1, hdtype, status)
        if (status.eq.0) then
          call ftgkys(unit,'EXTNAME',name1,comment,status)
          if (name1.eq.name0) then
            status=-1
          endif
        endif
      enddo
      
      if (status.lt.0) then
        status=0
        return
      else
        status=1
        return
      endif
      
      end
      
      subroutine move_ext_fromhere (unit,name,status)
c
c
c Move to the named extension of a fits file; search beginning from the
c current extension table
c
c character name*(*)
c
c status: 0 - OK
c         1 - ext table not found
c
      implicit none
      integer unit
      character name*(*)
      integer status
      
      character name0*80,name1*80,comment*80
      integer hdtype
      
      
      name0=name
      status=0
      
      
      do while (status.eq.0) 
        call ftmrhd (unit, 1, hdtype, status)
        if (status.eq.0) then
          call ftgkys(unit,'EXTNAME',name1,comment,status)
          if (name1.eq.name0) then
            status=-1
          endif
        endif
      enddo
      
      if (status.lt.0) then
        status=0
        return
      else
        status=1
        return
      endif
      
      end
      
            
      
