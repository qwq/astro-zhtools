      subroutine get_image_size (filename,nx,ny)
      implicit none 
      character filename*(*)
      integer nx,ny
      
      
      integer iunit
      integer status
      integer bitpix
      integer naxis,pcount,gcount,blocksize
      integer naxes(10)
      integer lnblnk
      logical simple,extend
      integer i
 
      
      status=0
      call ftgiou (iunit,status)
      if (status.ne.0) then
        call perror_fitsio ("logical unit:",status)
        call exit(1)
      endif
      
      call ftopen(iunit,filename,0,blocksize,status)
      call ftghpr(iunit,2,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~    ,status)
      
      if (status.ne.0) then
        call perror_fitsio (filename,status)
        call exit(1)
      endif
      
      if (naxis.ne.2) then
        write(0,*)'wrong image dimensions in: ',filename(1:lnblnk(filename))
        write(0,*)(naxes(i),i=1,naxis)
        call exit(1)
      endif
      nx=naxes(1)
      ny=naxes(2)
      
      call ftclos(iunit,status)
      call ftfiou(iunit,status)
      
      if (status.ne.0) then
        call perror_fitsio (filename,status)
        call exit(1)
      endif
      
      return
      end
      









