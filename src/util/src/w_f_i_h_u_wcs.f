      subroutine write_fits_image_hdr_unit_wcs
     ~    (outfile,rimg,nx,ny,imgtype,outtype,headerfrom,headerunit,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,ctype1,ctype2)
      implicit none
      character outfile*(*),headerfrom*(*)
      integer headerunit
      integer nx,ny
      real rimg(nx,ny)
      character outtype*(*),imgtype*(*)
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      character ctype1*(*),ctype2*(*)

      integer ounit
      integer status
      logical defined

      status=0
      call create_fits_image (
     ~    outfile,
     ~    nx,ny,
     ~    outtype,
     ~    1.0d0,0.0d0,
     ~    headerfrom, headerunit,
     ~    '',
     ~    ounit,
     ~    status)


      if (status.ne.0) then
        call perror_fitsio (outfile,status)
        call exit(1)
      endif

      call ftmkyd (ounit,'CRPIX1',crpix1,-8,' ',status)
      status = 0
      call ftmkyd (ounit,'CRPIX2',crpix2,-8,' ',status)
      status = 0
      call ftmkyd (ounit,'CRVAL1',crpix1,-8,' ',status)
      status = 0
      call ftmkyd (ounit,'CRVAL2',crpix2,-8,' ',status)
      status = 0
      call ftmkyd (ounit,'CDELT1',cdelt1,-8,' ',status)
      status = 0
      call ftmkyd (ounit,'CDELT2',cdelt2,-8,' ',status)
      status = 0
      call ftmkyd (ounit,'CROTA2',crota2,-8,' ',status)
      status = 0
      if (defined(ctype1)) call ftmkys (ounit,'CTYPE1',ctype1,' ',status)
      if (defined(ctype2)) call ftmkys (ounit,'CTYPE2',ctype2,' ',status)

      if      (imgtype(1:1).eq.'b'.or.imgtype(1:1).eq.'B') then
        call ftp2db(ounit,0,nx,nx,ny,rimg,status)
      else if (imgtype(1:1).eq.'i'.or.imgtype(1:1).eq.'I') then
        call ftp2di(ounit,0,nx,nx,ny,rimg,status)
      else if (imgtype(1:1).eq.'j'.or.imgtype(1:1).eq.'J') then
        call ftp2dj(ounit,0,nx,nx,ny,rimg,status)
      else if (imgtype(1:1).eq.'e'.or.imgtype(1:1).eq.'E') then
        call ftp2de(ounit,0,nx,nx,ny,rimg,status)
      else if (imgtype(1:1).eq.'d'.or.imgtype(1:1).eq.'D') then
        call ftp2dd(ounit,0,nx,nx,ny,rimg,status)
      else
        write(0,*)'wrong image type: ',imgtype
        call exit(1)
      endif
        
      if (headerunit.ne.ounit) then
        call ftclos(ounit,status)
        call ftfiou(ounit,status)
      else                      ! at least flush the output
        call ftflus(ounit,status)
      endif

      if (status.ne.0) then
        call perror_fitsio (outfile,status)
        call exit(1)
      endif

        
      return
      end


