      function gammp(a,x)
c
c Incomplete gamma-function  P(a,x)=1/G(a)\int_0^x exp(-t)t^(a-1)dt
c
c
c      
      implicit none
      real gammp,a,x,gln,gammcf
      
      if(x.lt.0..or.a.le.0.) call exiterror('invalid parameters in gammp')
      
      if(x.lt.a+1.)then
        call gser(gammp,a,x,gln)
      else
        call gcf(gammcf,a,x,gln)
        gammp=1.-gammcf
      endif
      return
      end
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      function gammp8(a,x)
c
c Incomplete gamma-function  P(a,x)=1/G(a)\int_0^x exp(-t)t^(a-1)dt
c
c
c      
      implicit none
      double precision gammp8,a,x,gln,gammcf
      
      if(x.lt.0.0d0.or.a.le.0.0d0) call exiterror('invalid parameters in gammp8')
      
      if(x.lt.a+1.0d0)then
        call gser8(gammp8,a,x,gln)
      else
        call gcf8(gammcf,a,x,gln)
        gammp8=1.0d0-gammcf
      endif
      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function gammq(a,x)

c
c  Complement of the incomplete gamma-function  Q(a,x)=1-P(a,x)
c
c

      implicit none
      real gammq,a,x
      real gamser,gln

      if(x.lt.0.0.or.a.le.0.0) call exiterror('')
      if(x.lt.a+1.0)then
        call gser(gamser,a,x,gln)
        gammq=1.0-gamser
      else
        call gcf(gammq,a,x,gln)
      endif
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function gammq8(a,x)

c
c  Complement of the incomplete gamma-function  Q(a,x)=1-P(a,x)
c
c

      implicit none
      real*8 gammq8,a,x
      real*8 gamser,gln
      
      if(x.lt.0.0d0.or.a.le.0.0d0)  call exiterror('')
      if(x.lt.a+1.0d0)then
        call gser8(gamser,a,x,gln)
        gammq8=1.0d0-gamser
      else
        call gcf8(gammq8,a,x,gln)
      endif
      
      return
      end
      
      
c-------------------------------------------------------------      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
c_____________________________________________________________      
      subroutine gser(gamser,a,x,gln)
      implicit none
      real gamser,a,x,gln
      integer itmax
      real eps
      parameter (itmax=100,eps=1.e-7)
      real gammln
      real ap,sum,del
      integer n
      


      gln=gammln(a)
      if(x.le.0.)then
        if(x.lt.0.) call exiterror('')
        gamser=0.0
        return
      endif
      ap=a
      sum=1.0/a
      del=sum
      do n=1,itmax
        ap=ap+1.
        del=del*x/ap
        sum=sum+del
        if(abs(del).lt.abs(sum)*eps) goto 1
      enddo
       call exiterror('A too large, ITMAX too small')
1     gamser=sum*exp(-x+a*alog(x)-gln)

      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine gser8(gamser,a,x,gln)
      implicit none
      real*8 gamser,a,x,gln
      integer itmax
      real*8 eps
      parameter (itmax=10000,eps=3.d-12)
      real*8 gammln8
      real*8 ap,sum,del
      integer n
      


      gln=gammln8(a)
      if(x.le.0.d0)then
        if(x.lt.0.d0) call exiterror('')
        gamser=0.d0
        return
      endif
      ap=a
      sum=1.d0/a
      del=sum
      do n=1,itmax
        ap=ap+1.0d0
        del=del*x/ap
        sum=sum+del
        if(dabs(del).lt.dabs(sum)*eps) goto 1
      enddo
       call exiterror('A too large, ITMAX too small')
 1    gamser=sum*dexp(-x+a*dlog(x)-gln)

      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine gcf(gammcf,a,x,gln)
      implicit none
      real gammcf,a,x,gln
      integer itmax
      real eps
      parameter (itmax=100,eps=3.e-7)
      real gammln
      real gold,a0,a1,b0,b1,fac,an,ana,anf,g
      integer n

      gln=gammln(a)
      gold=0.0
      a0=1.0
      a1=x
      b0=0.0
      b1=1.0
      fac=1.0
      dO n=1,itmax
        an=(n)
        ana=an-a
        a0=(a1+a0*ana)*fac
        b0=(b1+b0*ana)*fac
        anf=an*fac
        a1=x*a0+anf*a1
        b1=x*b0+anf*b1
        if(a1.ne.0.)then
          fac=1.0/a1
          g=b1*fac
          if(abs((g-gold)/g).lt.eps) goto 1
          gold=g
        endif
      enddo
      call exiterror('A too large, ITMAX too small')
1     gammcf=exp(-x+a*alog(x)-gln)*g

      return
      end
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine gcf8(gammcf,a,x,gln)
      implicit none
      real*8 gammcf,a,x,gln
      integer itmax
      real*8 eps
      parameter (itmax=300,eps=1.d-12)
      real*8 gammln8
      real*8 gold,a0,a1,b0,b1,fac,an,ana,anf,g
      integer n

      gln=gammln8(a)
      gold=0.0d0
      a0=1.0d0
      a1=x
      b0=0.0d0
      b1=1.0d0
      fac=1.0d0
      dO n=1,itmax
        an=(n)
        ana=an-a
        a0=(a1+a0*ana)*fac
        b0=(b1+b0*ana)*fac
        anf=an*fac
        a1=x*a0+anf*a1
        b1=x*b0+anf*b1
        if(a1.ne.0.d0)then
          fac=1.0d0/a1
          g=b1*fac
          if(dabs((g-gold)/g).lt.eps) goto 1
          gold=g
        endif
      enddo
      call exiterror('A too large, ITMAX too small')
1     gammcf=dexp(-x+a*dlog(x)-gln)*g

      return
      end

