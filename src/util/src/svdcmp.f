      subroutine svdcmp(a,m,n,mp,np,w,v)
c
c    Performs singular value decomposition of a(m,n) with ph. dimensions
c    mp by np        
c               A=UWV(t).  
c
c      U replaces A as output
c      diagonal W -in W(n)
c      V (not V(t)!) is output as V(n,n)
c
      implicit none
      integer m,mp,n,np,nmax
      real a(mp,np), v(np,np), w(np)
      parameter (nmax=100)
      integer i,its,j,k,l,nm
      real anorm,c,f,g,h,s,scale,x,y,z,rv1(nmax),pythag
      
      g=0.0
      scale=0.0
      anorm=0.0
      do i=1,n
        l=i+1
        rv1(i)=scale*g
        g=0.0
        s=0.0
        scale=0.0
        if (i.le.m) then
          do k=i,m
            scale=scale+abs(a(k,i))
          enddo
          if (scale.ne.0.0) then
            do k=i,m
              a(k,i)=a(k,i)/scale
              s=s+a(k,i)*a(k,i)
            enddo
            f=a(i,i)
            g=-sign(sqrt(s),f)
            h=f*g-s
            a(i,i)=f-g
            do j=l,n
              s=0.0
              do k=i,m
                s=s+a(k,i)*a(k,j)
              enddo
              f=s/h
              do k=i,m
                a(k,j)=a(k,j)+f*a(k,i)
              enddo
            enddo
            do  k=i,m
              a(k,i)=scale*a(k,i)
            enddo
          endif
        endif
        w(i)=scale*g
        g=0.0
        s=0.0
        scale=0.0
        if ((i.le.m).and.(i.ne.n)) then
          do k=l,n
            scale=scale+abs(a(i,k))
          enddo
          if (scale.ne.0.0) then
            do k=l,n
              a(i,k)=a(i,k)/scale
              s=s+a(i,k)*a(i,k)
            enddo
            f=a(i,l)
            g=-sign(sqrt(s),f)
            h=f*g-s
            a(i,l)=f-g
            do k=l,n
              rv1(k)=a(i,k)/h
            enddo
            do j=l,m
              s=0.0
              do k=l,n
                s=s+a(j,k)*a(i,k)
              enddo
              do k=l,n
                a(j,k)=a(j,k)+s*rv1(k)
              enddo
            enddo
            do k=l,n
              a(i,k)=scale*a(i,k)
            enddo
          endif
        endif
        anorm=max(anorm,(abs(w(i))+abs(rv1(i))))
      enddo
      do i=n,1,-1
        if (i.lt.n) then
          if (g.ne.0.0) then
            do j=l,n
              v(j,i)=(a(i,j)/a(i,l))/g
            enddo
            do j=l,n
              s=0.0
              do k=l,n
                s=s+a(i,k)*v(k,j)
              enddo
              do k=l,n
                v(k,j)=v(k,j)+s*v(k,i)
              enddo
            enddo
          endif
          do j=l,n
            v(i,j)=0.0
            v(j,i)=0.0
          enddo
        endif
        v(i,i)=1.0
        g=rv1(i)
        l=i
      enddo
      do i=n,1,-1
        l=i+1
        g=w(i)
        if (i.lt.n) then
          do j=l,n
            a(i,j)=0.0
          enddo
        endif
        if (g.ne.0.0) then
          g=1.0/g
          if (i.ne.n) then
            do j=l,n
              s=0.0
              do k=l,m
                s=s+a(k,i)*a(k,j)
              enddo
              f=(s/a(i,i))*g
              do k=i,m
                a(k,j)=a(k,j)+f*a(k,i)
              enddo
            enddo
          endif
          do j=i,m
            a(j,i)=a(j,i)*g
          enddo
        else
          do j= i,m
            a(j,i)=0.0
          enddo
        endif
        a(i,i)=a(i,i)+1.0
      enddo
      do k=n,1,-1
        do its=1,30
          do l=k,1,-1
            nm=l-1
            if ((abs(rv1(l))+anorm).eq.anorm)  go to 2
            if ((abs(w(nm))+anorm).eq.anorm)  go to 1
          enddo
 1        c=0.0
          s=1.0
          do i=l,k
            f=s*rv1(i)
            if ((abs(f)+anorm).ne.anorm) then
              g=w(i)
              h=pythag(f,g)
              w(i)=h
              h=1.0/h
              c= (g*h)
              s=-(f*h)
              do j=1,m
                y=a(j,nm)
                z=a(j,i)
                a(j,nm)=(y*c)+(z*s)
                a(j,i)=-(y*s)+(z*c)
              enddo
            endif
          enddo
 2        z=w(k)
          if (l.eq.k) then
            if (z.lt.0.0) then
              w(k)=-z
              do j=1,n
                v(j,k)=-v(j,k)
              enddo
            endif
            go to 3
          endif
          if (its.eq.30) pause 'no convergence in 30 iterations'
          x=w(l)
          nm=k-1
          y=w(nm)
          g=rv1(nm)
          h=rv1(k)
          f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y)
          g=sqrt(f*f+1.0)
          f=((x-z)*(x+z)+h*((y/(f+sign(g,f)))-h))/x
          c=1.0
          s=1.0
          do j=l,nm
            i=j+1
            g=rv1(i)
            y=w(i)
            h=s*g
            g=c*g
            z=pythag(f,h)
            rv1(j)=z
            c=f/z
            s=h/z
            f= (x*c)+(g*s)
            g=-(x*s)+(g*c)
            h=y*s
            y=y*c
            do nm=1,n
              x=v(nm,j)
              z=v(nm,i)
              v(nm,j)= (x*c)+(z*s)
              v(nm,i)=-(x*s)+(z*c)
            enddo
            z=sqrt(f*f+h*h)
            w(j)=z
            if (z.ne.0.0) then
              z=1.0/z
              c=f*z
              s=h*z
            endif
            f= (c*g)+(s*y)
            x=-(s*g)+(c*y)
            do nm=1,m
              y=a(nm,j)
              z=a(nm,i)
              a(nm,j)= (y*c)+(z*s)
              a(nm,i)=-(y*s)+(z*c)
            enddo 
          enddo
          rv1(l)=0.0
          rv1(k)=f
          w(k)=x
        enddo
 3      continue
      enddo
      return
      end
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx     
      subroutine svdcmp8(a,m,n,mp,np,w,v)
c
c    Performs singular value decomposition of a(m,n) with ph. dimensions
c    mp by np        
c               A=UWV(t).  
c
c      U replaces A as output
c      diagonal W -in W(n)
c      V (not V(t)!) is output as V(n,n)
c
      implicit none
      integer m,mp,n,np,nmax
      real*8 a(mp,np), v(np,np), w(np)
      parameter (nmax=100)
      integer i,its,j,k,l,nm
      real*8 anorm,c,f,g,h,s,scale,x,y,z,rv1(nmax),pythag8
      
      g=0.0d0
      scale=0.0d0
      anorm=0.0d0
      do i=1,n
        l=i+1
        rv1(i)=scale*g
        g=0.0d0
        s=0.0d0
        scale=0.0d0
        if (i.le.m) then
          do k=i,m
            scale=scale+dabs(a(k,i))
          enddo
          if (scale.ne.0.0d0) then
            do k=i,m
              a(k,i)=a(k,i)/scale
              s=s+a(k,i)*a(k,i)
            enddo
            f=a(i,i)
            g=-dsign(dsqrt(s),f)
            h=f*g-s
            a(i,i)=f-g
            do j=l,n
              s=0.0d0
              do k=i,m
                s=s+a(k,i)*a(k,j)
              enddo
              f=s/h
              do k=i,m
                a(k,j)=a(k,j)+f*a(k,i)
              enddo
            enddo
            do  k=i,m
              a(k,i)=scale*a(k,i)
            enddo
          endif
        endif
        w(i)=scale*g
        g=0.0d0
        s=0.0d0
        scale=0.0d0
        if ((i.le.m).and.(i.ne.n)) then
          do k=l,n
            scale=scale+dabs(a(i,k))
          enddo
          if (scale.ne.0.0) then
            do k=l,n
              a(i,k)=a(i,k)/scale
              s=s+a(i,k)*a(i,k)
            enddo
            f=a(i,l)
            g=-dsign(dsqrt(s),f)
            h=f*g-s
            a(i,l)=f-g
            do k=l,n
              rv1(k)=a(i,k)/h
            enddo
            do j=l,m
              s=0.0d0
              do k=l,n
                s=s+a(j,k)*a(i,k)
              enddo
              do k=l,n
                a(j,k)=a(j,k)+s*rv1(k)
              enddo
            enddo
            do k=l,n
              a(i,k)=scale*a(i,k)
            enddo
          endif
        endif
        anorm=max(anorm,(dabs(w(i))+dabs(rv1(i))))
      enddo
      do i=n,1,-1
        if (i.lt.n) then
          if (g.ne.0.0d0) then
            do j=l,n
              v(j,i)=(a(i,j)/a(i,l))/g
            enddo
            do j=l,n
              s=0.0d0
              do k=l,n
                s=s+a(i,k)*v(k,j)
              enddo
              do k=l,n
                v(k,j)=v(k,j)+s*v(k,i)
              enddo
            enddo
          endif
          do j=l,n
            v(i,j)=0.0d0
            v(j,i)=0.0d0
          enddo
        endif
        v(i,i)=1.0d0
        g=rv1(i)
        l=i
      enddo
      do i=n,1,-1
        l=i+1
        g=w(i)
        if (i.lt.n) then
          do j=l,n
            a(i,j)=0.0d0
          enddo
        endif
        if (g.ne.0.0d0) then
          g=1.0d0/g
          if (i.ne.n) then
            do j=l,n
              s=0.0d0
              do k=l,m
                s=s+a(k,i)*a(k,j)
              enddo
              f=(s/a(i,i))*g
              do k=i,m
                a(k,j)=a(k,j)+f*a(k,i)
              enddo
            enddo
          endif
          do j=i,m
            a(j,i)=a(j,i)*g
          enddo
        else
          do j= i,m
            a(j,i)=0.0d0
          enddo
        endif
        a(i,i)=a(i,i)+1.0d0
      enddo
      do k=n,1,-1
        do its=1,100
          do l=k,1,-1
            nm=l-1
            if ((dabs(rv1(l))+anorm).eq.anorm)  go to 2
            if ((dabs(w(nm))+anorm).eq.anorm)  go to 1
          enddo
 1        c=0.0d0
          s=1.0d0
          do i=l,k
            f=s*rv1(i)
            if ((dabs(f)+anorm).ne.anorm) then
              g=w(i)
              h=pythag8(f,g)
              w(i)=h
              h=1.0d0/h
              c= (g*h)
              s=-(f*h)
              do j=1,m
                y=a(j,nm)
                z=a(j,i)
                a(j,nm)=(y*c)+(z*s)
                a(j,i)=-(y*s)+(z*c)
              enddo
            endif
          enddo
 2        z=w(k)
          if (l.eq.k) then
            if (z.lt.0.0d0) then
              w(k)=-z
              do j=1,n
                v(j,k)=-v(j,k)
              enddo
            endif
            go to 3
          endif
          if (its.eq.100) pause 'no convergence in 100 iterations'
          x=w(l)
          nm=k-1
          y=w(nm)
          g=rv1(nm)
          h=rv1(k)
          f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0d0*h*y)
          g=sqrt(f*f+1.0d0)
          f=((x-z)*(x+z)+h*((y/(f+dsign(g,f)))-h))/x
          c=1.0d0
          s=1.0d0
          do j=l,nm
            i=j+1
            g=rv1(i)
            y=w(i)
            h=s*g
            g=c*g
            z=pythag8(f,h)
            rv1(j)=z
            c=f/z
            s=h/z
            f= (x*c)+(g*s)
            g=-(x*s)+(g*c)
            h=y*s
            y=y*c
            do nm=1,n
              x=v(nm,j)
              z=v(nm,i)
              v(nm,j)= (x*c)+(z*s)
              v(nm,i)=-(x*s)+(z*c)
            enddo
            z=dsqrt(f*f+h*h)
            w(j)=z
            if (z.ne.0.0d0) then
              z=1.0d0/z
              c=f*z
              s=h*z
            endif
            f= (c*g)+(s*y)
            x=-(s*g)+(c*y)
            do nm=1,m
              y=a(nm,j)
              z=a(nm,i)
              a(nm,j)= (y*c)+(z*s)
              a(nm,i)=-(y*s)+(z*c)
            enddo 
          enddo
          rv1(l)=0.0d0
          rv1(k)=f
          w(k)=x
        enddo
 3      continue
      enddo
      return
      end


      function pythag(a,b)
c
c
c Computes sqrt(a**2+b**2) without destructive under- or overflow
c
c      
      real pythag,a,b
      real absa,absb
      absa=abs(a)
      absb=abs(b)
      if(absa.gt.absb)then
        pythag=absa*sqrt(1.+(absb/absa)**2)
      else
        if(absb.eq.0.)then
          pythag=0.
        else
          pythag=absb*sqrt(1.+(absa/absb)**2)
        endif
      endif
      return
      end
      
      
      function pythag8(a,b)
c
c
c Computes sqrt(a**2+b**2) without destructive under- or overflow
c
c      
      real*8 pythag8,a,b
      real*8 absa,absb
      absa=dabs(a)
      absb=dabs(b)
      if(absa.gt.absb)then
        pythag8=absa*dsqrt(1.d0+(absb/absa)**2)
      else
        if(absb.eq.0.d0)then
          pythag8=0.d0
        else
          pythag8=absb*dsqrt(1.+(absa/absb)**2)
        endif
      endif
      return
      end

