      subroutine check_command_line (pars,wrong,nwrong)
      implicit none
c--------------------------------------------------------------
c
c  i  character      pars*(*) - "," or space separated list of pars
c  o  character      wrong(*)*(*)   - the list of "wrong" parameters  
c  o  integer            nwrong     - the number of wrong parameters
c
c
c
c--------------------------------------------------------------
      character pars*(*)
      character wrong(*)*(*)
      integer nwrong
      
      character parlist(100)*80
      character parsw*8000
      integer i,npar,lnblnk
      logical exist,ispar
      character testpar*80
      integer iarg,narg,iargc
      character arg*200,arg0*200
      integer neq, whereiseq
      
      
      call strcpy(parsw,pars)
      do i=1,lnblnk(parsw)
        if (parsw(i:i).eq.',') then
          parsw(i:i)=' '
        endif
      enddo
      
      call splitwords(parsw,parlist,npar)
      
      narg=iargc()
      iarg=1
      nwrong=0
      do while (iarg.le.narg)
        call getarg(iarg,arg)
        ispar=.false.
        neq=whereiseq(arg)
        if (neq.gt.0) then
          ispar=.true.
          testpar=arg(1:neq-1)
        else
          call getarg(iarg+1,arg0)
          if (arg0(1:1).eq.'=') then
            ispar=.true.
            testpar=arg
            iarg=iarg+1
          endif
        endif
        if (ispar) then
          exist=.false.
          do i=1,npar
            if (testpar.eq.parlist(i)) then
              exist=.true.
              goto 100
            endif
          enddo
 100      continue
          if (.not.exist) then
            nwrong=nwrong+1
            wrong(nwrong)=testpar
          endif
        endif
        
        iarg=iarg+1
      enddo
            

      
      return
      end
      
      
