      subroutine refvec (p, np, ip, lp, r, nr, ifail)
c-------------------------------------------------------------------------
c   a. purpose
c   ==========
c
c    refvec sets up the reference vector r for a discrete distribution
c    with   pdf    (probability  density   function)  or  cdf  (cumulative
c    distribution function) p  for further use by ranref
c
c   b. specification
c   ================
c
c          subroutine refvec (p,np,ip,lp,r,nr,ifail)
c          integer np,ip,nr,ifail
c          logical lp
c          //real// p(np),r(nr)
c
c   c. parameters
c   =============
c   p - //real// array of dimension (np).
c     before entry, p must contain the pdf or cdf of the distribution.
c
c   np - integer.
c     on entry, np must specify the dimension of p. np must be positive.
c
c   ip - integer.
c     on entry, ip must specify the value of the variate,  assumed to be a
c     whole number, to which the probability in p(1) corresponds.
c
c   lp - logical.
c     on entry, lp indicates the type of information contained in p. if lp
c     is .true., p contains a cumulative  distribution  function (cdf); if
c     lp is .false., p contains a probability density function (pdf).
c
c   r - //real// array of dimension (nr).
c     on exit, r contains  the  reference  vector r (see  section 3 of the
c     library manual routine document).
c
c   nr - integer.
c     on  entry, nr must  specify  the  dimension  of r. the  recommended
c     value  is  roughly  5 + 1.4*np. nr must be larger than np + 2.
c
c     unchanged on exit.
c
c   ifail - integer.
c     before entry, ifail must be assigned a value. for users not familiar
c     with this parameter the recommended value is 0.
c
c     unless  the  routine  detects  an error  (see next  section),  ifail
c     contains 0 on exit.
c
c
c   d. error indicators and warnings
c   ================================
c   ifail = 1
c     on entry, np.lt.1.
c
c   ifail = 2
c     on entry, nr.lt.np + 3.
c
c   ifail = 3
c     if lp  is .true.  on  entry,  then  the  values  in p are not all in
c     non-descending  order, as required by a cdf. if lp is .false.,  then
c     at  least  one of the  probabilities  in p is  negative,  or all the
c     probabilities are zero.
c
c   ifail = 4
c     the total  probability is not 1. in this case, r is set up correctly
c     since the error may be due to larger rounding  errors than expected.
c
c  e example of subroutine use
c  =========================
c        integer i, ifail, ix, nout
c        double precision p(10), r(19)
c        integer ranref
c        data nout /6/
c
c        data p(1), p(2), p(3), p(4), p(5), p(6), p(7), p(8), p(9),p(10) /
c       *0.0d0,0.1d0,0.2d0,0.4d0,0.5d0,0.6d0,0.8d0,0.9d0,1.0d0,1.0d0/
c        ifail = 0
c* Set a ref. vector
c        call refvec8(p, 10, 0, .true., r, 19, ifail)
c
c* Generate 5 integers from given distribution
c        do i=1,5
c           ix = ranref(r,19)
c           write (nout,*) ix
c        enddo
c        stop
c        end
c------------------------------------------------------------------

      implicit none
      integer ifail, ip, np, nr
      logical lp
      real p(np), r(nr)
      real eps, one, x, zero
      integer i, ierr, j, k

      real epsmach

      data one /1.0/, zero /0.0/
      ierr=1
      if (np.lt.1) goto 180
      ierr=2
      if (nr.lt.(np+3)) goto 180
      eps=epsmach(eps)
      ierr=3
      if (lp) goto 60
      j=1
      do i=1,np
         if (p(i).lt.zero) goto 180
         if (p(i).gt.eps) j=i
      enddo
      x=zero
      do i=1,j
         k=nr-j+i
         x=x+p(i)
         r(k)=x
      enddo
      goto 120
   60 x=zero
      j=0
      do i=1,np
         if (p(i).lt.x) goto 180
         x=p(i)
         if (p(i).lt.one) j=i
      enddo
      if (j.lt.np) j=j+1
      do i=1,j
         k=nr-j+i
         r(k)=p(i)
      enddo
  120 if (r(nr).eq.zero) goto 180
      k=ip-nr+j-1
      j=nr-j+1
      do i=j,nr
         if (r(i).gt.eps) goto 160
      enddo
      i=nr

  160 call refindx(k,i,r,nr)
      ifail=0

      return
  180 ifail=ierr
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine refindx (m,n,r,nr)
c
c     Sets up the index part of the reference vector.
c
      implicit none
      integer m, n, nr
      real r(nr)
      real half, one, x, y, zero
      integer i, j, k

      data half /0.5/, zero /0.0/, one /1.0/

      r(1)=float(n-3)
      r(2)=float(m) + half
c     this is in case truncation is towards -infinity.
      if (int(r(2)).gt.m) r(2)=r(2)-one
      x=r(nr)
      do i=n,nr
         r(i)=r(i)/x
      enddo
      x=one/r(1)
      y=zero
      j=n-1
      k=n
      do i=3,j
   20    if (r(k).gt.y) goto 40
         k=k+1
         goto 20
   40    r(i)=float(k) + half
         y=y + x
      enddo
      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine refvec8 (p, np, ip, lp, r, nr, ifail)
c
c     Sets up a reference vector from a pdf or cdf.
c
      implicit none
      integer ifail, ip, np, nr
      logical lp
      real*8 p(np), r(nr)
      real*8 eps, one, x, zero
      integer i, ierr, j, k

      real*8 epsmach8

      data one /1.0d0/, zero /0.0d0/
      ierr=1
      if (np.lt.1) goto 180
      ierr=2
      if (nr.lt.(np+3)) goto 180
      eps=epsmach8(eps)
      ierr=3
      if (lp) goto 60
      j=1
      do i=1,np
         if (p(i).lt.zero) goto 180
         if (p(i).gt.eps) j=i
      enddo
      x=zero
      do i=1,j
         k=nr-j+i
         x=x+p(i)
         r(k)=x
      enddo
      goto 120
   60 x=zero
      j=0
      do i=1,np
         if (p(i).lt.x) goto 180
         x=p(i)
         if (p(i).lt.one) j=i
      enddo
      if (j.lt.np) j=j+1
      do i=1,j
         k=nr-j+i
         r(k)=p(i)
      enddo
  120 if (r(nr).eq.zero) goto 180
      k=ip-nr+j-1
      j=nr-j+1
      do i=j,nr
         if (r(i).gt.eps) goto 160
      enddo
      i=nr

  160 call refindx8(k,i,r,nr)
      ifail=0

      return
  180 ifail=ierr
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine refindx8 (m,n,r,nr)
c
c     Sets up the index part of the reference vector.
c
      implicit none
      integer m, n, nr
      real*8 r(nr)
      real*8 half, one, x, y, zero
      integer i, j, k

      data half /0.5d0/, zero /0.0d0/, one /1.0d0/

      r(1)=dfloat(n-3)
      r(2)=dfloat(m) + half
c     this is in case truncation is towards -infinity.
      if (idint(r(2)).gt.m) r(2)=r(2)-one
      x=r(nr)
      do i=n,nr
         r(i)=r(i)/x
      enddo
      x=one/r(1)
      y=zero
      j=n-1
      k=n
      do i=3,j
   20    if (r(k).gt.y) goto 40
         k=k+1
         goto 20
   40    r(i)=dfloat(k) + half
         y=y + x
      enddo
      return
      end
