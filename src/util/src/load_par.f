      subroutine load_parameters(ifile,argf,npar,nparmax)
      implicit none
      integer ifile
      integer nparmax
      character argf(nparmax)*(*)
      integer npar
      character buff*200
      
      integer iarg,lnblnk,whereiseq,nn
      integer status
      logical iscommentstring
      character word(3)*80
      integer nwords
      common /load_par_com/ word, nwords
      
      iarg=1
      do while (iarg.le.nparmax)
        read(ifile,'(a)',iostat=status)buff
        if (status.ne.0) goto 100

                                ! first blank <=> the line is commented out
        if (iscommentstring(buff)) then
          goto 90
        endif
        
        argf(iarg)=word(1)(1:lnblnk(word(1)))
        
        nn=whereiseq(argf(iarg))
        if (nn.gt.0) then
          if (nn.lt.lnblnk(argf(iarg))) then
                                ! done with this line - the parameter and the
                                ! value is in the first word
            iarg=iarg+1
            goto 90
          endif
        endif
        
        argf(iarg)=argf(iarg)(1:lnblnk(argf(iarg)))
     ~      //word(2)(1:lnblnk(word(2)))
        nn=whereiseq(argf(iarg))
        if (nn.gt.0) then
          if (nn.lt.lnblnk(argf(iarg))) then
            iarg=iarg+1
            goto 90 
          endif
        endif
        
        argf(iarg)=argf(iarg)(1:lnblnk(argf(iarg)))
     ~      //word(3)(1:lnblnk(word(3)))
        iarg=iarg+1
        
 90     continue
      enddo
      
 100  continue
      npar=iarg-1
      
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c
c .true. if the string is not a parameter string
c
c
      logical function iscommentstring (string)
      implicit none
      character string*(*)
      character word(3)*80
      integer nwords
      common /load_par_com/ word, nwords
      
      integer ww
      integer whereiseq
      
                                ! if the first symbol in line is not a letter 
                                ! the string is treated as a comment
      ww=ichar(string(1:1))
      if (.not.((ww.ge.65.and.ww.le.90).or.(ww.ge.97.and.ww.le.122)) ) then
        iscommentstring=.true.
        return
      endif
      
      
      call firstthreewords (string,word,nwords)

      if (whereiseq(word(1)).gt.1) then
        iscommentstring=.false.
        return
      endif
      
      if (nwords.gt.1) then
        if (word(2)(1:1).eq.'=') then
          iscommentstring=.false.
          return
        endif
      endif
      
      iscommentstring=.true.
      return
      end
          
