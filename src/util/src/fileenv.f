      subroutine fileenv(filename,envname)
      implicit none
      character filename*(*),envname*(*)
      character env*80
      integer lnblnk
      integer lenv
      character slash*1
      
      call getenv(envname,env)
      lenv=lnblnk(env)
      slash=env(lenv:lenv)
      if(slash.ne.'/')then
        filename=env(1:lenv)//'/'//filename(1:lnblnk(filename))
      else
        filename=env(1:lenv)//filename(1:lnblnk(filename))
      endif
      return
      end
