      function zeroin(ax,bx,f,tol)
      implicit none
      real*8 ax,bx,f,tol,zeroin
      real*8 a,b,c,d,e,eps,fa,fb,fc,tol1,xm,p,q,r,s
      eps=1.0d0
 10   eps=eps*0.5d0
      tol1=1.0d0+eps
      if(tol1.gt.1.0d0) goto 10
      a=ax
      b=bx
      fa=f(a)
      fb=f(b)
 20   c=a
      fc=fa
      d=b-a
 30   if(dabs(fc).ge.dabs(fb)) goto 40  
      a=b
      b=c
      c=a
      fa=fb
      fb=fc
      fc=fa
 40   tol1=2.0d0*eps*(dabs(b))+0.5d0*tol
      xm=0.5d0*(c-b)
      if(dabs(xm).le.tol1) goto 90
      if(fb.eq.0.0d0) goto 90
      if(dabs(e).lt.tol1) goto 70
      if(dabs(fa).le.dabs(fb)) goto 70
      if(a.ne.c) goto 50
      s=fb/fa
      p=2.0d0*xm*s
      q=1.0d0-s
      goto 60
 50   q=fa/fc
      r=fb/fc
      s=fb/fa
      p=s*(2.0d0*xm*q*(q-r)-(b-a)*(r-1.0d0))
      q=(q-1.0d0)*(r-1.0d0)*(s-1.0d0)
 60   if(p.ge.0.0d0)q=-q
      p=dabs(p)
      if((2.0d0*p).ge.(3.0d0*xm*q-dabs(tol1*q))) goto 70
      if(p.ge.dabs(0.5d0*e*q)) goto 70
      e=d
      d=p/q
      goto 80
 70   d=xm  
      e=d
 80   a=b
      fa=fb
      if(dabs(d).gt.tol1) b=b+d
      if(dabs(d).le.tol1) b=b+dsign(tol1,xm)
      fb=f(b)
      if((fb*(fc/dabs(fc))).gt.0.0d0) goto 20
      goto 30
 90   zeroin=b
      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function zeroin2(ax,bx,f,tol)
      implicit none
      real*8 ax,bx,f,tol,zeroin2
      real*8 a,b,c,d,e,eps,fa,fb,fc,tol1,xm,p,q,r,s
      eps=1.0d0
 10   eps=eps*0.5d0
      tol1=1.0d0+eps
      if(tol1.gt.1.0d0) goto 10
      a=ax
      b=bx
      fa=f(a)
      fb=f(b)
 20   c=a
      fc=fa
      d=b-a
 30   if(dabs(fc).ge.dabs(fb)) goto 40  
      a=b
      b=c
      c=a
      fa=fb
      fb=fc
      fc=fa
 40   tol1=2.0d0*eps*(dabs(b))+0.5d0*tol
      xm=0.5d0*(c-b)
      if(dabs(xm).le.tol1) goto 90
      if(fb.eq.0.0d0) goto 90
      if(dabs(e).lt.tol1) goto 70
      if(dabs(fa).le.dabs(fb)) goto 70
      if(a.ne.c) goto 50
      s=fb/fa
      p=2.0d0*xm*s
      q=1.0d0-s
      goto 60
 50   q=fa/fc
      r=fb/fc
      s=fb/fa
      p=s*(2.0d0*xm*q*(q-r)-(b-a)*(r-1.0d0))
      q=(q-1.0d0)*(r-1.0d0)*(s-1.0d0)
 60   if(p.ge.0.0d0)q=-q
      p=dabs(p)
      if((2.0d0*p).ge.(3.0d0*xm*q-dabs(tol1*q))) goto 70
      if(p.ge.dabs(0.5d0*e*q)) goto 70
      e=d
      d=p/q
      goto 80
 70   d=xm  
      e=d
 80   a=b
      fa=fb
      if(dabs(d).gt.tol1) b=b+d
      if(dabs(d).le.tol1) b=b+dsign(tol1,xm)
      fb=f(b)
      if((fb*(fc/dabs(fc))).gt.0.0d0) goto 20
      goto 30
 90   zeroin2=b
      return
      end



