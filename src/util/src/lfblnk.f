      function lfblnk(str)
c----------------------------------------------------------------
c     return the index of first noncharacter symbol of string "str"
c----------------------------------------------------------------
      implicit none
      integer lfblnk
      character str*(*)
      integer lnblnk,i,len,ichar,nchar
      
      len=lnblnk(str)
      
      do i=1,len
        nchar=ichar(str(i:i))
        if(nchar.lt.33.or.nchar.gt.126)then
          lfblnk=i-1
          return
        endif
      enddo
      lfblnk=len

      return                                                        
      end
