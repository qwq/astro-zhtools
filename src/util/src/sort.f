      subroutine sort(n,ra)
c
c
c  Quick Sort of ra(n) into array of non-descending order 
c
c
c
c
c
c
      implicit none
      integer n
      real ra

      dimension ra(n)

      integer l,ir,i,j
      real rra


      l=n/2+1
      ir=n
 10   continue
      if(l.gt.1)then
        l=l-1
        rra=ra(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1)then
          ra(1)=rra
          return
        endif
      endif
      i=l
      j=l+l
 20   if(j.le.ir)then
        if(j.lt.ir)then
          if(ra(j).lt.ra(j+1))j=j+1
        endif
        if(rra.lt.ra(j))then
          ra(i)=ra(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        goto 20
      endif
      ra(i)=rra
      goto 10
      END
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine sort8(n,ra)
c
c
c  Quick Sort of ra(n) into array of non-descending order 
c
c
c
c
c  Same as sort, but REAL*8 precision
c
      implicit none
      integer n
      real*8 ra(n)
      
            
      integer l,ir,i,j
      real*8 rra
      
      l=n/2+1
      ir=n
 10   continue
      if(l.gt.1)then
        l=l-1
        rra=ra(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1)then
          ra(1)=rra
          return
        endif
      endif
      i=l
      j=l+l
 20   if(j.le.ir)then
        if(j.lt.ir)then
          if(ra(j).lt.ra(j+1))j=j+1
        endif
        if(rra.lt.ra(j))then
          ra(i)=ra(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        goto 20
      endif
      ra(i)=rra
      goto 10
      END
      
      
      
