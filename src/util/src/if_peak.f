      function if_peak (img,nx,ny,i0,j0)
      implicit none
      logical if_peak
      integer nx,ny,i0,j0
      real img(nx,ny),im0
      
      im0=img(i0,j0)
      if_peak= 
     ~    (im0.ge.img(i0+1,j0)) .and.
     ~    (im0.ge.img(i0-1,j0)) .and.
     ~    (im0.ge.img(i0,j0+1)) .and.
     ~    (im0.ge.img(i0+1,j0+1)) .and.
     ~    (im0.ge.img(i0-1,j0+1)) .and.
     ~    (im0.ge.img(i0-1,j0-1)) .and.
     ~    (im0.ge.img(i0+1,j0-1)) .and.
     ~    (im0.ge.img(i0,j0-1))
      
      return
      end
      
