      function poiprob(k,mean)
c
c
c  Prob  i < k
c
c
c      
      implicit none
      integer k
      real poiprob,mean
      real a,x,gammq
      
      if(k.gt.1)then
        a=float(k)
        x=mean
        poiprob=gammq(a,x)
      elseif(k.eq.1)then
        poiprob=exp(-mean)
      else
        poiprob=0.
      endif
      
      return
      end
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprob12(x1,x2,mean)
c
c
c  Prob  x1 <= i < x2
c
c      
      implicit none
      real poiprob12,x1,x2,mean
      real poiprob
      integer i1,i2
      
      i1=int(x1)
      i2=int(x2)
      
      poiprob12=poiprob(i2,mean)-poiprob(i1,mean)
      return
      end
        
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      function poiprob8(k,mean)
c
c
c  Prob  i < k
c
c
c      
      implicit none
      integer k
      real*8 poiprob8,mean
      real*8 a,x,gammq8
      
      if(k.gt.1)then
        a=dfloat(k)
        x=mean
        poiprob8=gammq8(a,x)
      elseif(k.eq.1)then
        poiprob8=dexp(-mean)
      else
        poiprob8=0.
      endif
      
      return
      end
        
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprob128(x1,x2,mean)
c
c
c  Prob  x1 <= i < x2
c
c      
      implicit none
      real*8 poiprob128,x1,x2,mean
      real*8 poiprob8
      integer i1,i2
      
      i1=idint(x1)
      i2=idint(x2)
      
      poiprob128=poiprob8(i2,mean)-poiprob8(i1,mean)
      return
      end
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprob1(ii,mean)
c
c
c    Prob i=ii
c
c      
      implicit none
      real poiprob1,mean
      integer ii
      real factln
      
      poiprob1=exp(ii*alog(mean)-mean-factln(ii))
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprob18(ii,mean)
c
c
c    Prob i=ii
c
c      
      implicit none
      real*8 poiprob18,mean
      integer ii
      real*8 factln8
      
      poiprob18=dexp(ii*dlog(mean)-mean-factln8(ii))
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprob1ln(ii,mean)
c
c
c    ln(Prob i=ii)
c
c      
      implicit none
      real poiprob1ln,mean
      integer ii
      real factln
      
      if (mean.lt.1.0e-7.and.ii.gt.0) then
         poiprob1ln=-10.0
      else if (mean.lt.1.0e-7.and.ii.eq.0) then
         poiprob1ln=0.0
      else
         poiprob1ln=ii*alog(mean)-mean-factln(ii)
      endif
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprob1ln8(ii,mean)
c
c
c    ln(Prob i=ii)
c
c      
      implicit none
      real*8 poiprob1ln8,mean
      integer ii
      real*8 factln8
      if(ii.lt.0) call exiterror('ii.lt.0 in poiprob')
      if(ii.eq.0)then
        poiprob1ln8=-mean
      elseif(ii.eq.1)then
        poiprob1ln8=-mean+dlog(mean)
      else
        poiprob1ln8=ii*dlog(mean)-mean-factln8(ii)
      endif
      return
      end
      

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function poiprobdev (mean,prob)
      implicit none
c
c
c Positive deviation from the mean assosiated with the probability prob
c
c
c
c
c
      real poiprobdev,mean,prob
      real probold
      data probold /-1.0/
      save probold
      
      integer n,i
      parameter (n=3000)
      real mean0(n),dev0(n)
      save mean0,dev0
      real mean1,prob1
      common /poipdevcom/ mean1,prob1
      real*8 zeroin,poiprobdevfunc,poiprobdevfunc2,a,b
      external poiprobdevfunc,poiprobdevfunc2
      real sigma
      
      if (prob.ne.probold) then
        probold=prob
        do i=1,n
          mean0(i)=i*30.0/n
          mean1=mean0(i)
          prob1=prob
          a=mean0(i)
          b=50.0
          dev0(i)=zeroin(a,b,poiprobdevfunc,1.0d-4)-mean0(i)
        enddo
        a=0.0
        b=8.0
        sigma=zeroin(a,b,poiprobdevfunc2,1.0d-4)
      endif
        
      i=nint(mean*n/30.0)
      if (i.gt.n) then
        poiprobdev=sigma*sqrt(mean)
      else
        poiprobdev=dev0(i)
      endif
      
      return
      end

          

      function poiprobdevfunc (x)
      implicit none
      real*8 poiprobdevfunc,x
      real mean1,prob1
      common /poipdevcom/ mean1,prob1
      real y
      real gammp

      y=x
      poiprobdevfunc=prob1-gammp(y,mean1)
      return
      end
      
      
      function poiprobdevfunc2 (x)
      implicit none
      real*8 poiprobdevfunc2,x
      real mean1,prob1
      common /poipdevcom/ mean1,prob1
      real y
      real normc

      y=x
      poiprobdevfunc2=prob1-normc(y)
      return
      end
      
      






