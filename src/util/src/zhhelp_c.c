#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <zhtools.h>

int  zhusrhelp(char *com);
void zhhelp (char *com);
void zhhelp_man (char *com);
void zhhelp_c_man_ (char *com, CHAR_INT *l);

void zhhelp_c_ (char *com, CHAR_INT *l)
{
  char command[1000];

  strncpy (command,com,*l);
  command[*l]='\0';

  if( !zhusrhelp(command) ) zhhelp(command);
  exit(0);
}

void zhhelp_c_man_ (char *com, CHAR_INT *l)
{
  char command[1000];

  strncpy (command,com,*l);
  command[*l]='\0';

  if( !zhusrhelp(command) ) zhhelp_man (command);
  exit(0);
}

void zhhelp (char *com)
{
  char filename[1000], command[1000];
  char zhtools[1000];
  char *s=NULL;

  /* if ZHTOOLS is defined, use it */
  if( (s=getenv("ZHTOOLS")) ){
    sprintf(zhtools, "%s/%s", s, HELPDIR);
  }
  else{
#ifdef ZHMAN_INSTALL_DIR
    /* otherwise use install dir */
    strcpy(zhtools, ZHMAN_INSTALL_DIR);
#else
    /* otherwise we're in trouble */
    fprintf(stderr,
            "ZHTOOLS not initialized: no help is available for %s\n",com);
    exit(1);
#endif
  }

  strcpy(filename,zhtools);
  strcat(filename,"/");
  strcat(filename,com);
  strcat(filename,".help"); 
  if (access(filename,F_OK))
    {
      fprintf(stderr,"no help is available for %s\n",com);
      exit(1);
    }
  strcpy(command,"cat ");
  strcat(command,filename);
  exit(system(command));
}  


void zhhelp_man (char *com)
{
  char command[1000];
  char filename[1000];
  char zhtools[1000];
  char firstarg[1000];
  char *s;

  fprintf (stderr,"!!! Wrong usage of %s. Would you like to see its man page? (y/n) ",com);
  fscanf(stdin,"%s",firstarg);
  if (strcmp(firstarg,"n")==0) {
    exit(1);
  }

  strcpy(firstarg,com);
  
  /* if ZHTOOLS is defined, use it */
  if( (s=getenv("ZHTOOLS")) ){
    strcpy(zhtools, s);
  }
  else{
#ifdef ZHMAN_INSTALL_DIR
    /* otherwise use install dir */
    strcpy(zhtools, ZHMAN_INSTALL_DIR);
#else
    /* otherwise we're in trouble */
    fprintf(stderr,
            "ZHTOOLS not initialized: no help is available for %s\n",com);
    exit(1);
#endif
  }
  strcpy(filename, zhtools);

  strcat(filename, "/");
  strcat(filename, HELPDIR);
  strcat(filename, "/man1/");

  strcat(filename,firstarg);
  strcat(filename,".1"); 
  if (! access(filename,F_OK)) {
    strcpy(command,"man -M ");
    strcpy(command, zhtools);
    strcat(filename, "/");
    strcat(filename, HELPDIR);
    strcat(filename, "/");
    strcat(command,firstarg);
    exit(system(command));
  }

  strcpy(filename, zhtools);
  strcat(filename, "/");
  strcat(filename, HELPDIR);
  strcat(filename, "/");
  strcat(filename,firstarg);
  strcat(filename,".help"); 
  if (access(filename,F_OK))
    {
      fprintf(stderr,"no help is available for %s\n",firstarg);
      exit(1);
    }
  strcpy(command,"cat ");
  strcat(command,filename);
  exit(system(command));
}

#ifndef SZ_LINE
#define SZ_LINE 8192
#endif

int zhusrhelp(char *cmd)
{
  char help[SZ_LINE];
  char tbuf[SZ_LINE];
  char *t=NULL;
  char *parfile=NULL;

  /* look for a user parameter file */
  if( !(parfile=getenv("MY_READPAR_FILE")) ) return 0;
  /* look for a help parameter associated with this command */
  snprintf(help, SZ_LINE-1, "%s.help", cmd);
  if( !get_parameter_file_par(parfile, help, tbuf) ) return 0;
  /* display the user help */
  fprintf(stdout, "%s\n", tbuf);
  fflush(stdout);
  /* query user about more display */
  fprintf(stdout, "Also display canonical zhtools help page(Y/n): ");
  fgets(tbuf, SZ_LINE, stdin);
  for(t=tbuf; *t; t++){
    if( !isspace(*t) ) break;
  }
  /* return 0 if more display is desired */
  return !((*t=='y') || (*t=='Y') || (*t=='\0') || (*t=='\n'));
}
