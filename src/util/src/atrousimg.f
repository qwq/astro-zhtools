c% \`A trous hat image convolution.
      subroutine atrousimg (img,nx,ny,jj)
c% $j\!j$ is the scale of the \`a trous transform
      implicit none
      integer nx,ny
      real img(nx,ny)
      integer jj
      
      integer nmax
      parameter (nmax=16384)
      real array(nmax)
      
      integer i,j
      
c% The convolution is performed first on image rows and then columns. The 1-D
c% convolution is performed by the function \emph{atrousstep} (see {\tt
c% atrousstep.f}). The image column or raw is copied into the 1-D {\tt array}.
c% {\tt Array} is convolved with the 1-D \`a trous hat of scale $j\!j$ and 
c% the put back into the image. 


c rows
      do i=1,nx
        
        do j=1,ny
          array(j)=img(i,j)
        enddo
        
        call atrousstep (array,ny,jj)
        
        do j=1,ny
          img(i,j)=array(j)
        enddo
        
      enddo
      
      
c columns
      
      do j=1,ny
        
        do i=1,nx
          array(i)=img(i,j)
        enddo
        
        call atrousstep (array,nx,jj)
        
        do i=1,nx
          img(i,j)=array(i)
        enddo
        
      enddo
      
      return
      end

