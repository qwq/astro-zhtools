c% The inner loop of the \`a trous kernel convolution. 
      subroutine atrousstep_0 (c,cc,n,step,coeff)
      implicit none
      integer n,step
      real coeff(-2:2)
      real c(n)
      real cc(-2*step-2:n+2*step+2)
      
      integer i,j,jj
      real sum
      
c% Copy the data      
      do i=1,n
        cc(i)=c(i)
      enddo
      
c% Left boundary: equals the first element of the data
      do i=-2*step-2,0
        cc(i)=c(1)
      enddo
c% Right boundary: equals the last element.      
      do i=n+1,n+2*step+2
        cc(i)=c(n)
      enddo
      
      do i=1,n
c%$$c_i = \sum_{j=-2}^2 \mathrm{cc}_{i+j\times\mathrm{step}}\;\mathrm{coeff}_j$$
        sum=0
        jj=i-2*step
        do j=-2,2
          sum=sum+cc(jj)*coeff(j)
          jj = jj + step
        enddo
        c(i)=sum
      enddo
      
      return
      end
