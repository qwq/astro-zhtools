      function gasdev(idum)
      implicit none
      integer idum
      real gasdev,ran2
      integer iset
      real v1,v2,r,fac,gset
      
      data iset/0/
      save iset, gset
      
      if (iset.eq.0) then
 1      v1=2.*ran2(idum)-1.
        v2=2.*ran2(idum)-1.
        r=v1**2+v2**2
        if(r.ge.1..or.r.eq.0.) goto 1
        fac=sqrt(-2.*alog(r)/r)
        gset=v1*fac
        gasdev=v2*fac
        iset=1
      else
        gasdev=gset
        iset=0
      endif
      return
      end

      function gasdev8(idum)
      implicit none
      integer idum
      real*8 gasdev8,ran28
      integer iset
      real*8 v1,v2,r,fac,gset

      data iset/0/
      save iset, gset

      if (iset.eq.0) then
 1      v1=2.0d0*ran28(idum)-1.0d0
        v2=2.0d0*ran28(idum)-1.0d0
        r=v1**2+v2**2
        if(r.ge.1.0d0.or.r.eq.0.0d0) goto 1
        fac=dsqrt(-2.0d0*dlog(r)/r)
        gset=v1*fac
        gasdev8=v2*fac
        iset=1
      else
        gasdev8=gset
        iset=0
      endif
      return
      end
















