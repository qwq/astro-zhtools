      function norm(x)
c
c Cumulative normal distribution p(x)
c
c       p(x)=Prob(t<x)
c
c
      implicit none
      real norm,x
      real rrtwo,erfc4
      data rrtwo/7.07106781e-1/
      norm=0.5*erfc4(-x*rrtwo)
      return
      end     
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function normc(x)
c
c  Complement of cumulative normal distribution q(x)
c
c            q(x) = Prob(t>x)
c
      implicit none
      real normc,x
      real rrtwo,erfc4
      data rrtwo/7.07106781e-1/
      normc=0.5d0*erfc4(x*rrtwo)
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      function normd(mean,sigma,x1,x2)
c
c  Prob ( x1< norm(mean,sigma) <x2 )
c
c      Complement of cumulative normal distribution q(x)
c
c            q(x) = Prob(t>x)
c
      implicit none
      real normc,normd
      real mean,sigma,x1,x2,xx1,xx2
      
      xx1=(x1-mean)/sigma
      xx2=(x2-mean)/sigma
      
      normd=normc(xx1)-normc(xx2)
        
      return
        end      
       
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      function norm8(x)
      implicit none
      real*8 norm8,x
      real*8 rrtwo,erfc8
      data rrtwo/7.071067811865475d-1/
      norm8=0.5d0*erfc8(-x*rrtwo)
      return
      end
      
     
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function normc8(x)
      implicit none
      real*8 normc8,x
      real*8 rrtwo,erfc8
      data rrtwo/7.071067811865475d-1/
      normc8=0.5d0*erfc8(x*rrtwo)
      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      function normd8(mean,sigma,x1,x2)
      implicit none
      real*8 normc8,normd8
      real*8 mean,sigma,x1,x2,xx1,xx2
      
      xx1=(x1-mean)/sigma
      xx2=(x2-mean)/sigma
      
      normd8=normc8(xx1)-normc8(xx2)
      
      return
      end

      
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      
      
      
      
      function erf(x)
      implicit none 
      real erf
      integer ncfc, ncfd, j
      real xup, sqrtpi, zero, one, two, three, twenty,half,
     *    xv, x, x2, bjp2, bjp1, bj
      
      real c(8),d(8)
      data ncfc,ncfd/8,8/,xup/4.0/,sqrtpi/1.772454/
     a    ,c(1),c(2),c(3),c(4),c(5),c(6),c(7),c(8)
     a    /1.944907,4.2019d-2,-1.8687d-2,5.129d-3,-1.068d-3
     a    ,1.74d-4,-2.1d-5,2.0d-6/
     a    ,d(1),d(2),d(3),d(4),d(5),d(6),d(7),d(8)
     a    /1.483110,-3.01071e-1,6.8995e-2,-1.3916e-2,2.421e-3
     a    ,-3.66e-4,4.9e-5,-6.0e-6/
      
      data zero, one, two, three, twenty, half /0.0,1.0,2.0,3.0,
     *    20.0,0.5/
      
      
      xv=abs(x)
      if (xv.ge.xup) go to 120
      if (xv.le.two) go to 60
      x2=two - twenty/(xv+three)
      
      bjp2=zero
      bjp1=c(ncfc)
      j=ncfc - 1
 20   bj=x2*bjp1 - bjp2 + c(j)
      if (j.eq.1) go to 40
      bjp2=bjp1
      bjp1=bj
      j=j - 1
      go to 20
 40   x2=half*(bj-bjp2)/xv*exp(-x*x)/sqrtpi
      erf=(one-x2)*sign(one,x)
      return
c
 60   x2=x*x - two
c     summation
      bjp2=zero
      bjp1=d(ncfd)
      j=ncfd - 1
 80   bj=x2*bjp1 - bjp2 + d(j)
      if (j.eq.1) go to 100
      bjp2=bjp1
      bjp1=bj
      j=j - 1
      go to 80
 100  erf=half*(bj-bjp2)*x
      return
c
 120  erf=sign(one,x)
      return
      end
     

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function erf8(x)
      implicit none
      real*8 erf8,x
      integer ncfc, ncfd, j
      real*8 xup, sqrtpi, zero, one, two, three, twenty, half,
     >    xv, x2, bjp2, bjp1, bj
      real*8 c(18),d(17)
      data ncfc,ncfd/18,17/,xup/6.25d0/,sqrtpi/1.7724538509055160d0/
     A    ,C(1),C(2),C(3),C(4),C(5),C(6),C(7),C(8),C(9),C(10),C(11)
     A    ,C(12),C(13),C(14),C(15),C(16),C(17),C(18)
     A    /1.9449071068178803D0,4.20186582324414D-2,-1.86866103976769D-2
     A    ,5.1281061839107D-3,-1.0683107461726D-3,1.744737872522D-4
     A    ,-2.15642065714D-5,1.7282657974D-6,-2.00479241D-8
     A    ,-1.64782105D-8,2.0008475D-9,2.57716D-11,-3.06343D-11
     A    ,1.9158D-12,3.703D-13,-5.43D-14,-4.0D-15,1.2D-15/
     A    ,D(1),D(2),D(3),D(4),D(5),D(6),D(7),D(8),D(9),D(10),D(11)
     A    ,D(12),D(13),D(14),D(15),D(16),D(17)
     A    /1.4831105640848036D0,-3.010710733865950D-1,6.89948306898316D-2
     A    ,-1.39162712647222D-2,2.4207995224335D-3,-3.658639685849D-4
     A    ,4.86209844323D-5,-5.7492565580D-6,6.113243578D-7
     A    ,-5.89910153D-8,5.2070091D-9,-4.232976D-10,3.18811D-11
     A    ,-2.2361D-12,1.467D-13,-9.0D-15,5.0D-16/
      
      data zero, one, two, three, twenty, half /0.0d0,1.0d0,2.0d0,3.0d0,
     *    20.0d0,0.5d0/
      xv = dabs(x)
      if (xv.ge.xup) goto 120
      if (xv.le.two) goto 60 
      x2 = two - twenty/(xv+three)
C
C     SUMMATION
      bjp2 = zero
      bjp1 = c(ncfc)
      j = ncfc - 1
 20   bj = x2*bjp1 - bjp2 + c(j)
      if (j.eq.1) goto 40 
      bjp2 = bjp1
      bjp1 = bj
      j = j - 1
      goto 20 
 40   x2 = half*(bj-bjp2)/xv*dexp(-x*x)/sqrtpi
      erf8 = (one-x2)*dsign(one,x)
      goto 140 
C
 60   x2 = x*x - two
C     SUMMATION
      bjp2 = zero
      bjp1 = d(ncfd)
      j = ncfd - 1
 80   bj = x2*bjp1 - bjp2 + d(j)
      if (j.eq.1) goto 100 
      bjp2 = bjp1
      bjp1 = bj
      j = j - 1
      goto 80 
 100  erf8 = half*(bj-bjp2)*x
      goto 140 
C
 120  erf8 = dsign(one,x)
 140  continue
      return
      end
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function erfc4(x)
      implicit none
      real erfc4
      real x,y,t,xhi,xlo
      data xhi,xlo/9.5e0,-4.5e0/
      
      
      
      if(x.ge.xhi) go to 10
      if(x.le.xlo) go to 20
      
      t=1.0-7.5/(abs(x)+3.75)
      y=   +1.4558972e-1+t*(  -2.7342192e-1+t*(  +2.2600824e-1+
     a    t*(  -1.6357229e-1+t*(  +1.0260225e-1+t*(  -5.4799165e-2+
     b    t*(  +2.4151896e-2+t*(  -8.2316935e-3+t*(  +1.7866301e-3+
     c    t*(  -6.4127909e-6+t*(  -1.3874589e-4+
     d    t*(  +3.1475326e-5)))))))))))
      
      erfc4=exp(-x*x)*y
      if(x.lt.0.0)erfc4=2.0-erfc4
      return
      
 10   erfc4=0.0
      return
 20   erfc4=2.0
      return
      
      end
      

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function erfc8(x)
c                                         
c     Complement of error function erfc(x) 
c
c     erfc(-1000)=2
c     erfc(0)=1
c     erfc(1000)=0
c
c
c
      implicit none
      real*8 erfc8,w
      real*8 x,y,t,xhi,xlo
      data xhi,xlo/13.0d0,-6.25d0/

      if(x.ge.xhi) goto 10 
      if(x.le.xlo) goto 20 

      t=1.0d0-7.5d0/(dabs(x)+3.75d0)
      y=   +1.455897212750385d-1+t*(  -2.734219314954260d-1+
     At*(  +2.260080669166197d-1+t*(  -1.635718955239687d-1+
     Bt*(  +1.026043120322792d-1+t*(  -5.480232669380236d-2+
     Ct*(  +2.414322397093253d-2+t*(  -8.220621168415435d-3+
     Et*(  +1.802962431316418d-3+t*(  -2.553523453642242d-5+
     Et*(  -1.524627476123466d-4+t*(  +4.789838226695987d-5+
     Ft*(  +3.584014089915968d-6+t*(  -6.182369348098529d-6+
     Gt*(  +7.478317101785790d-7+t*(  +6.575825478226343d-7+
     Ht*(  -1.822565715362025d-7+t*(  -7.043998994397452d-8+
     It*(  +3.026547320064576d-8+t*(  +7.532536116142436d-9+
     Jt*(  -4.066088879757269d-9+t*( -5.718639670776992d-10+
     Kt*( +3.328130055126039d-10))))))))))))))))))))))
      w=dexp(-x*x)*y
      if(x.lt.0.0d0)w=2.0d0-w
      erfc8=w
      return

10    erfc8=0.0d0
      return
20    erfc8=2.0d0
      return
      end      
