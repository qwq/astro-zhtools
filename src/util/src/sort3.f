      subroutine sort3(n,ra,rb,rc,wksp,iwksp)
c
c   
c Sorts an array ra(n) into ascending order, making corresp.
c
c  rearrangements in rb and rc,
c
c  index order is kept in iwksp(i)
c
c
c
      implicit none
      integer n
      real ra(n),rb(n),rc(n),wksp(n)
      integer iwksp(n)
      integer j

      call indexx(n,ra,iwksp)

      do j=1,n
        wksp(j)=ra(j)
      enddo

      do j=1,n
        ra(j)=wksp(iwksp(j))
      enddo

      do j=1,n
        wksp(j)=rb(j)
      enddo

      do j=1,n
        rb(j)=wksp(iwksp(j))
      enddo

      do j=1,n
        wksp(j)=rc(j)
      enddo

      do j=1,n
        rc(j)=wksp(iwksp(j))
      enddo

      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine sort38(n,ra,rb,rc,wksp,iwksp)
c
c   
c Sorts an array ra(n) into ascending order, making corresp.
c
c  rearrangements in rb and rc,
c
c  index order is kept in iwksp(i)
c
c  Same as sort3, but REAL*8 accuracy
c
      implicit none
      integer n
      real*8 ra(n),rb(n),rc(n),wksp(n)
      integer iwksp(n)
      integer j

      call indexx8(n,ra,iwksp)

      do j=1,n
        wksp(j)=ra(j)
      enddo

      do j=1,n
        ra(j)=wksp(iwksp(j))
      enddo

      do j=1,n
        wksp(j)=rb(j)
      enddo

      do j=1,n
        rb(j)=wksp(iwksp(j))
      enddo

      do j=1,n
        wksp(j)=rc(j)
      enddo

      do j=1,n
        rc(j)=wksp(iwksp(j))
      enddo

      return
      end


      subroutine sort3i4(n,ra,rb,rc,wksp,iwksp)
c
c   
c Sorts an array ra(n) into ascending order, making corresp.
c
c  rearrangements in rb and rc,
c
c  index order is kept in iwksp(i)
c
c
c
      implicit none
      integer n
      integer ra(n),rb(n),rc(n),wksp(n)
      integer iwksp(n)
      integer j

      call indexxi4(n,ra,iwksp)

      do j=1,n
        wksp(j)=ra(j)
      enddo

      do j=1,n
        ra(j)=wksp(iwksp(j))
      enddo

      do j=1,n
        wksp(j)=rb(j)
      enddo

      do j=1,n
        rb(j)=wksp(iwksp(j))
      enddo

      do j=1,n
        wksp(j)=rc(j)
      enddo

      do j=1,n
        rc(j)=wksp(iwksp(j))
      enddo

      return
      end

