      subroutine svdksb(u,w,v,m,n,mp,np,b,x)
c
c  Solves AX=B, where A is specified byarrays u, w, v as returned from svdcmp
c
c  m,n - logical dimensions of a; mp and np are physical ones
c  
c  b(m) - input right side
c  x(n) - output solution vector
c
c
c
c
      implicit none
      integer m,mp,n,np,nmax
      real b(mp),u(mp,np),v(np,np),w(np),x(np)
      parameter (nmax=500)
      integer i,j,jj
      real s,tmp(nmax)
      
      do j=1,n
        s=0.
        if(w(j).ne.0.)then
          do i=1,m
            s=s+u(i,j)*b(i)
          enddo
          s=s/w(j)
        endif
        tmp(j)=s
      enddo
      do j=1,n
        s=0.
        do jj=1,n
          s=s+v(j,jj)*tmp(jj)
        enddo
        x(j)=s
      enddo
      return
      end
        
      subroutine svdksb8(u,w,v,m,n,mp,np,b,x)
c
c  Solves AX=B, where A is specified byarrays u, w, v as returned from svdcmp
c
c  m,n - logical dimensions of a; mp and np are physical ones
c  
c  b(m) - input right side
c  x(n) - output solution vector
c
c
c
c
      implicit none
      integer m,mp,n,np,nmax
      real*8 b(mp),u(mp,np),v(np,np),w(np),x(np)
      parameter (nmax=500)
      integer i,j,jj
      real*8 s,tmp(nmax)
      
      do j=1,n
        s=0.0d0
        if(w(j).ne.0.0d0)then
          do i=1,m
            s=s+u(i,j)*b(i)
          enddo
          s=s/w(j)
        endif
        tmp(j)=s
      enddo
      do j=1,n
        s=0.0d0
        do jj=1,n
          s=s+v(j,jj)*tmp(jj)
        enddo
        x(j)=s
      enddo
      return
      end
        
