      subroutine show_c1(z,nx,ny)
      implicit none
      integer nx,ny
      character z(nx,ny)*1
      character command*200
      integer unit,newunit

      unit = newunit()
      
      write(command,782)nx,ny
 782  format('saoimage -skip 4 -u1 ',i4,1x,i4,' /tmp/_show_')
      
      open(unit,file='/tmp/_show_',form='unformatted')
      write(unit)z
      call flush(unit)
      
      call system(command)
      
      close(unit,status='delete')
      
      return
      end
      
      

