      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine get_numbered_parameter_file_par (filename,parkey,num,value)
      implicit none
      character parkey*(*),filename*(*)
      integer num
      character value*(*)
      
      character parname*100
      integer lnblnk
      
      write (parname,'(a,i3.3)') parkey(1:lnblnk(parkey)),num
      
      call get_pf_par (filename,parname,value)
      return
      end
      
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c
c a-la max
c
c
      subroutine get_numbered_param (iret,filename,parkey,num,value)
      implicit none
      character parkey*(*),filename*(*)
      integer num
      character value*(*)
      integer iret
      
      character parname*100
      integer lnblnk
      
      write (parname,'(a,i3.3)') parkey(1:lnblnk(parkey)),num
      
      call get_pf_par (filename,parname,value)
      if (value.eq.'undefined'.or.value.eq.'UNDEFINED') then
        iret=1
      else
        iret=0
      endif
      
      return
      end
      
       
