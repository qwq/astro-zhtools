      integer function ipoidev (xm)
c
c    Returns an integer number drawn from Poissonian distr. of mean XM
c
      implicit none
      real xm,xmw
      integer ipoidev12
      integer ipoiss12,em
      
      integer idum
      data idum /-100/
      
      if (xm.lt.12.)then
        em=ipoidev12(xm)
      else
        xmw=xm
        em=0
 3      continue
        if(xmw.gt.12.)then
          em=em+ipoiss12(idum)
          xmw=xmw-12.
          goto 3
        endif
        em=em+ipoidev12(xmw)
      endif
        
      ipoidev=em
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      integer function ipoidev8(xm)
c
c    Double precision version of ipoidev
c
      implicit none
      double precision xm,xmw
      integer ipoidev128
      integer ipoiss128,em
      
      integer idum
      data idum /-100/

      if (xm.lt.12.0d0)then
        em=ipoidev128(xm)
      else
        xmw=xm
        em=0
 3      continue
        if(xmw.gt.12.0d0)then
          em=em+ipoiss128(idum)
          xmw=xmw-12.0d0
          goto 3
        endif
        em=em+ipoidev128(xmw)
      endif
      ipoidev8=em
      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      integer function ipoiss12(idum)
c
c returns Poisson with mean = 12
c
      implicit none
      integer idum
      logical flag
      integer nmax
      parameter (nmax=150)
      real r(nmax),m
      parameter (m=12.0)
      integer nr,ifail,ranref
      data flag /.true./

      save nr,r,flag

      if (flag) then
        nr=150
        ifail=0
        call poi_ref (m,r,nr,ifail)
        if (ifail.ne.0) then
          write (0,*) 'ifail=',ifail, '  in poi_ref'
          call exit(1)
        endif
        flag=.false.
      endif
      ipoiss12=ranref(r,nr)
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      integer function ipoiss128(idum)
c
c Double precision version of ipoiss12
c
      implicit none
      integer idum
      logical flag
      integer nmax
      parameter (nmax=150)
      double precision r(nmax),m
      parameter (m=12.0d0)
      integer nr,ifail,ranref8
      data flag /.true./
      
      save nr,r,flag

      if (flag) then
        nr=150
        ifail=0
        call poi_ref8 (m,r,nr,ifail)
        if (ifail.ne.0) then
          print*,'ifail=',ifail, '  in poi_ref'
           call exiterror('')
        endif
        flag=.false.
      endif
      ipoiss128=ranref8(r,nr)
      return
      end
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      integer function ipoidev12(xm)
c
c    Returns a  number drawn from Poissonian distr. of mean XM<12
c
c
c
c
      implicit none
      integer iem
      real xm
      real g,oldm,t,ran2
      data oldm /-1./
      integer idum
      data idum /-100/
      
      save g, oldm

      if (xm.ne.oldm) then
        oldm=xm
        g=exp(-xm)
      endif
      iem=-1
      t=1.
 2    iem=iem+1
      t=t*ran2(idum)
      if (t.gt.g) go to 2
      ipoidev12=iem
      return
      end


cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      integer function ipoidev128(xm)
c
c  Double precision version of ipoidev12
c
      implicit none
      integer iem
      double precision xm,pi
      parameter (pi=3.14159265358979d0)
      double precision g,oldm,t,ran28
      save g,oldm
      data oldm /-1.0d0/
      integer idum
      data idum /-100/

      if (xm.ne.oldm) then
        oldm=xm
        g=dexp(-xm)
      endif
      iem=-1
      t=1.0d0
 2    iem=iem+1
      t=t*ran28(idum)
      if (t.gt.g) go to 2
      ipoidev128=iem
      return
      end
