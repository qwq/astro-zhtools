      subroutine ccorr2d (img1,img2,ccf,n,m)
      implicit none
      integer n,m
      real img1(n,m),img2(n,m),ccf(n,m)
      real mult
      integer i,j
      
      mult=1.0/(float(n)*float(m))
      
      call ccorr2d_main (img1,img2,ccf,n,m,.true.,.true.) 
      do i=1,n
        do j=1,m
          ccf(i,j)=ccf(i,j)*mult
        enddo
      enddo
      
      call diffr_w(ccf,n,m)
      
      return
      end
      
      subroutine ccorr2d_2 (img1,img2,ccf,n,m,recalc1,recalc2)
      implicit none
      integer n,m
      real img1(n,m),img2(n,m),ccf(n,m)
      real mult
      integer i,j
      logical recalc1,recalc2
      
      mult=1.0/(float(n)*float(m))
      
      call ccorr2d_main (img1,img2,ccf,n,m,recalc1,recalc2) 
      do i=1,n
        do j=1,m
          ccf(i,j)=ccf(i,j)*mult
        enddo
      enddo
      
      call diffr_w(ccf,n,m)
      
      return
      end
      
      
      
      subroutine ccorr2d_main (img1,img2,ccf,n,m,recalc1,recalc2)
      implicit none
      integer n,m
      complex img1 (n/2,m)
      complex img2 (n/2,m)
      complex ccf (n/2,m)
      integer nmax
      parameter (nmax=16384)
      complex speqi1(nmax),speqi2(nmax),speq(nmax)
      logical recalc1,recalc2
      integer i,j
      
      if (recalc1) then
        call rlft2(img1,speqi1,n,m,1)
      endif
      if (recalc2) then
        call rlft2(img2,speqi2,n,m,1)
      endif
      
      do i=1,n/2
        do j=1,m
          ccf(i,j)=img1(i,j)*conjg(img2(i,j))
        enddo
      enddo
      
      do i=1,m
        speq(i)=speqi1(i)*conjg(speqi2(i))
      enddo
      
      call rlft2(ccf,speq,n,m,-1)
      
      return
      end
      




