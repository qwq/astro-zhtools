      function ran18(idum0)
c
c  first call should with negative idum0 !!!
c
c
c
c
      implicit none
      integer idum0,idum,IA,IM,IQ,IR,NTAB,NDIV
      double precision ran18,AM,EPS,RNMX, ran18w
      real ran1,eps4,rnmx4,AM4, ran1w
      parameter(IA=16807,IM=2147483647,AM=1.0d0/IM,IQ=127773,IR=2836,
     ~    NTAB=32,NDIV=1+(IM-1)/NTAB,AM4=AM)
      
      integer j,k,iv(NTAB),iy
      logical flag
      save iv,iy,idum,EPS,eps4,RNMX,rnmx4,flag
      data flag /.true./
      
      if(flag)then
        idum=max(-idum0,1)
        call ini_ran1 (eps,eps4,RNMX,rnmx4,idum,iv,NTAB,iy,IQ,IR,IA,IM)
        flag=.false.
      endif
      
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if(idum.lt.0) idum=idum+IM
      j=1+iy/NDIV
      iy=iv(j)
      iv(j)=idum
      ran18w=min(AM*iy,RNMX)
      ran18=max(ran18w,EPS)
      return
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      entry ran1(idum0)
      
      if(flag)then
        idum=max(-idum0,1)
        call ini_ran1 (eps,eps4,RNMX,rnmx4,idum,iv,NTAB,iy,IQ,IR,IA,IM)
        flag=.false.
      endif
      
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if(idum.lt.0) idum=idum+IM
      j=1+iy/NDIV
      iy=iv(j)
      iv(j)=idum
      ran1w=min(AM4*iy,rnmx4)
      ran1=max(ran1w,eps4)
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine ini_ran1 (eps,eps4,RNMX,rnmx4,idum,iv,NTAB,iy,
     ~    IQ,IR,IA,IM)
      implicit none
      double precision eps,RNMX
      real eps4,rnmx4
      integer NTAB,iy,IQ,IR,IA,IM
      integer iv(NTAB)
      integer idum
      integer j
      double precision eps1
      real eps14
      integer k
      
      do j=1, NTAB
        iv(j)=0
      enddo
      EPS=1.0d0
 30   EPS=0.5d0*EPS
      eps1=1.0d0+EPS
      if(eps1.gt.1.0d0) goto 30
      EPS=2.0d0*EPS
      RNMX=1.0d0-EPS
      eps4=1.0
 40   eps4=0.5*eps
      eps14=1.0+eps4
      if(eps14.gt.1.0) goto 40
      eps4=2.0*eps4
      rnmx4=1.0-eps4
      do j=NTAB+8,1,-1
        k=idum/IQ
        idum=IA*(idum-k*IQ)-IR*k
        if(idum.lt.0) idum=idum+IM
        if(j.le.NTAB) iv(j)=idum
      enddo
      iy=iv(1)
      
      return
      end

