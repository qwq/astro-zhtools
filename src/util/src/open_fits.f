      subroutine open_fits (unit,fname,status)
c
c
c      
      implicit none
      integer unit,status
      character fname*(*)
      
      
      integer rw,blocksize
      
      status=0
      rw=0                      ! Read-only access
      blocksize=0
      call ftopen (unit,fname,rw,blocksize,status)
      
      return
      end
      
