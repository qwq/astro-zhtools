      function ifloor(x)
      implicit none
      real x
      integer ifloor,y
      if(x.ge.0.)then
        ifloor=int(x)
      else
        y=int(x)
        if (y.ne.x) then
          ifloor=y-1
        else
          ifloor=y
        endif
      endif
      return
      end
      
      function ifloor8(x)
      implicit none
      real*8 x
      integer ifloor8,y
      if(x.ge.0.)then
        ifloor8=idint(x)
      else
        y=idint(x)
        if (y.ne.x) then
          ifloor8=y-1
        else
          ifloor8=y
        endif
      endif
      return
      end
      
      
      function floor(x)
      implicit none
      real floor,x,y
      
      if (x.ge.0.0) then
        floor=int(x)
      else
        y=int(x)
        if (y.ne.x) then
          floor=y-1.0
        else
          floor=y
        endif
      endif
      return
      end
      
      function floor8(x)
      implicit none
      real*8 floor8,x,y
      
      if (x.ge.0.0d0) then
        floor8=dint(x)
      else
        y=dint(x)
        if (y.ne.x) then
          floor8=y-1.0d0
        else
          floor8=y
        endif
      endif
      return
      end
      



