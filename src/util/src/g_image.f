      subroutine g_image_r (unit, name, image, nx, ny, status)
c
c
c  Get real image from the extension NAME
c
c
c
c  Status: 0 - OK
c          1 - ext name not found
c          2 - problems with image header
c          3 - not 2d image
c          4 - problems reading image
c      
c
      implicit none
      integer unit
      character name*(*)
      real image(*)
      integer nx,ny
      integer status
      
      integer naxis,naxes(10)
      integer bitpix,pcount,gcount
      logical simple,extend,anyf
      integer hdtype
      
      if(name.eq.'PRIMARY')then
        call ftmahd (unit, 1, hdtype, status) ! Goto the primary array  
        if(status.ne.0)then
          status=1
          return
        endif
      else
        call move_ext(unit,name,status)
        if(status.ne.0)then
          status=1
          return
        endif
      endif
      
c--Determine dimensions
      call ftghpr(unit,10,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~    ,status)
      if (status.ne.0) then
        status=2
        return
      endif
      
      if (naxis.ne.2)then
        status=3
        return
      endif

      nx=naxes(1)
      ny=naxes(2)
      
      call ftg2de(unit,0,0.0,nx,nx,ny,image,anyf,status)
      if (status.ne.0)then
        status=4
        return
      endif
      
      return
      end
      
      
      
      subroutine g_image_r_foo (unit, name, image, nx, ny, status)
c
c
c  Get real image from the extension NAME
c
c simpler treatment for german images
c
c  Status: 0 - OK
c          1 - ext name not found
c          2 - problems with image header
c          3 - not 2d image
c          4 - problems reading image
c      
c
      implicit none
      integer unit
      character name*(*)
      real image(*)
      integer nx,ny
      integer status
      character comment*200
      
      integer naxes(10)
      logical anyf
      integer hdtype
      
      if(name.eq.'PRIMARY')then
        call ftmahd (unit, 1, hdtype, status) ! Goto the primary array  
        if(status.ne.0)then
          status=1
          return
        endif
      else
        call move_ext(unit,name,status)
        if(status.ne.0)then
          status=1
          return
        endif
      endif
      
c--Determine dimensions
      call ftgkyj(unit,'NAXIS1',naxes(1),comment,status)
      call ftgkyj(unit,'NAXIS2',naxes(2),comment,status)
      if (status.ne.0) then
        status=2
        return
      endif
      
      
      if (nx.ne.naxes(1).or.ny.ne.naxes(2)) then
         call exiterror('wrong dimensions in g_image_r_foo')
      endif
      
      call ftg2de(unit,0,0.0,nx,nx,ny,image,anyf,status)
      if (status.ne.0)then
        status=4
        return
      endif
      
      return
      end
      





