      function epsmach(x)
c
c
c    1.0 + eps = 1
c
c
      real eps,eps1,epsmach,x
      eps=1.0
10    eps=0.5*eps
      eps1=1.0+eps
      if(eps1.gt.1.0) goto 10
      epsmach=2.0*eps

      return
      end

cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      function epsmach8(x)
c
c
c    1.0 + eps = 1
c
c
      real*8 eps,eps1,epsmach8,x
      eps=1.0d0
10    eps=0.5d0*eps
      eps1=1.0d0+eps
      if(eps1.gt.1.0d0) goto 10
      epsmach8=2.0d0*eps

      return
      end
