
*+WT_FERRMSG
      subroutine wt_ferrmsg(status,errstr)
c     ------------------------------------
c --- DESCRIPTION -------------------------------------------------
c
c This routine checks the error flag obtained from FITSIO routines
c and if appropriate uses FTGERR to get the relevant error text.
c Fcecho is used to write to the screen to ensure that the program
c can be used in differant environments.
c
c --- VARIABLES ---------------------------------------------------
c
      character errstr*80
      character errmess*80
      integer status
c
c --- LINKING AND COMPILATION ---
c
c FTOOLS and FITSIO
c 
c --- CALLED ROUTINES ---
c
c subroutine FTGERR     : (FITSIO) Obtains appropriate error text
c subroutine FCECHO     : (FTOOLS) Writes to screen
c
*-Version 1.0
  
       IF (status.NE.0) THEN
         call ftgerr(status,errmess)
         call fcecho(errmess)
         call fcecho(errstr)
       ENDIF
       return
       end
c
c      <<<--- END OF SUBROUTINE WT_FERRMSG --->>>
c
                                               
 
