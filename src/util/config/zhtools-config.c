#include <stdio.h>

#ifndef ZHTOOLS
#define ZHTOOLS ""
#endif
#ifndef ZHBIN
#define ZHBIN ""
#endif
#ifndef ZHINC
#define ZHINC ""
#endif
#ifndef ZHLIB
#define ZHLIB ""
#endif
#ifndef ZHMAN
#define ZHMAN ""
#endif

static void usage (char *fname)
{
  fprintf(stderr,
	  "usage: %s [-b] [-i] [-l] [-m] [-z]\n",
	  fname);
  fprintf(stderr, "optional switches:\n");
  fprintf(stderr, "  -b  # display install bin directory\n");
  fprintf(stderr, "  -i  # display install include directory\n");
  fprintf(stderr, "  -l  # display install lib directory\n");
  fprintf(stderr, "  -m  # display install man directory\n");
  fprintf(stderr, "  -z  # display ZTOOLS variable used in build\n");
  exit(1);
}

int main(int argc, char **argv)
{
  int c;
  if( argc <= 1 ) usage(argv[0]);
  while ((c = getopt(argc, argv, "bilmz")) != -1){
    switch(c){
    case 'b':
      fprintf(stdout, "%s\n", ZHBIN);
      break;
    case 'i':
      fprintf(stdout, "%s\n", ZHINC);
      break;
    case 'l':
      fprintf(stdout, "%s\n", ZHLIB);
      break;
    case 'm':
      fprintf(stdout, "%s\n", ZHMAN);
      break;
    case 'z':
      fprintf(stdout, "%s\n", ZHTOOLS);
      break;
    }
  }
  fflush(stdout);
  return 0;
}
