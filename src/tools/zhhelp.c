#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <zhtools.h>

int zhusrhelp(char * cmd);

int main(int argc, char *argv[])
{
  char *TASKS, *ZHTOOLS_BIN;
  char command[1000];
  char filename[1000];
  char task[100];
  char zhtools[1000];
  char *s;

  int shorthelp = 0;
  int iarg;

  /* if ZHTOOLS is defined, use it */
  if( (s=getenv("ZHTOOLS")) ){
    sprintf(zhtools, "%s/%s", s, HELPDIR);
  }
  else{
#ifdef ZHMAN_INSTALL_DIR
    /* otherwise use install dir */
    strcpy(zhtools, ZHMAN_INSTALL_DIR);
#else
    /* otherwise we're in trouble */
    fprintf(stderr,
    "ERROR: either $ZHTOOLS and install dir are needed for help\n");
    exit(1);
#endif
  }

  for (iarg=1;iarg<argc-1;iarg++) {
    if (strcmp(argv[iarg],"-s") == 0) {
      shorthelp = 1;
    }
  }

  if (argc==1) {
    strcpy (task,"zhtools");
  } else {
    strncpy (task,argv[argc-1],90);
  }

  if ( argc == 3 && strcmp (argv[1],"key") == 0 ) {
    matchtasks (zhtools,task);
    exit(0);
  }
    


  /* try help from the user's file */
  if ( zhusrhelp (task) ) return (0);

  /* Ok, look for standard places */
  if ( ! shorthelp ) {
    strcpy(filename,zhtools);
    strcat(filename,"/man1/");
    strcat(filename,task);
    strcat(filename,".1"); 
    if (! access(filename,F_OK)) {
      strcpy(command,"man -M ");
      strcat(command,zhtools);
      strcat(command,"/ ");
      strcat(command,task);
      exit(system(command));
    }
  }

  strcpy(filename,zhtools);
  strcat(filename,"/");
  strcat(filename,task);
  strcat(filename,".help"); 
  if (access(filename,F_OK))
    {
      fprintf(stderr,"no help is available for %s\n",task);
      exit(1);
    }
  strcpy(command,"cat ");
  strcat(command,filename);
  exit(system(command));
}  

int matchtasks (char *dir, char *task) {
  char command[1024];

  strcpy (command,"ls ");
  strcat (command,dir);
  strcat (command,"/");
  strcat (command," ");
  strcat (command,dir);
  strcat (command,"/man1/");
  strcat (command," | grep -v : | awk -F. \'{print $1}\' | grep ");
  strcat (command,task);
  strcat (command," | sort | uniq");
  exit(system(command));
}
