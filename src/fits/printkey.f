      subroutine printkey
      implicit none
      character data*200
      
      integer lnblnk
      
c fitsio staff
      integer iunit
      parameter (iunit=10)
      integer status
      
      character comment*80

      character parnames*1000
      character parname(1000)*80
      integer npars
      
      character value*1024
      integer i,n,k
      integer iargc
      
      logical print_in_one_line
      logical yespar
      character format*80, format_name*80
      logical defined
      logical print_par_names

c-----------------------------------------------------------------      
      if (iargc().lt.2) call zhhelp ('printkey')

      call set_default_par ('img','${ARGV[1]}')
      call set_default_par ('keys','${ARGV[2]}')

c     call getarg(1,data)
      call get_cl_par ('img',data)
      if (.not.defined(data)) then
        call zhhelp ('printkey')
      endif

c     call getarg(2,parnames)
      call get_cl_par ('keys',parnames)
      if (.not.defined(parnames)) then
        call zhhelp ('printkey')
      endif

      print_in_one_line = yespar ('oneline')
      print_par_names = yespar('parnames')
      if (print_in_one_line) then
        if (print_par_names) then
          format = '(a,$)'
          format_name = '('' '',a,''='',$)'
        else
          format = '(" ",a,$)'
        endif
      else
        format = '(a)'
        format_name = '(a,''='',$)'
      endif
      
      status=0
      call ftnopn(iunit,data,0,status)
      if (status.ne.0) then
        call perror_fitsio(data,status)
        call exit(1)
      endif
      
      
      if (parnames.eq.'-') then ! read list from stdin
        npars = 0
        status = 0
        do while (status.eq.0)
          read (*,'(a)',iostat=status) parname(npars+1)
          if (status.eq.0) npars=npars+1
        enddo
        if (npars.eq.0) call exit(0)
        status = 0
      else
        n =lnblnk(parnames)
        do i=1,n
          if (parnames(i:i).eq.',') parnames(i:i)=' '
        enddo
        call splitwords(parnames,parname,npars)
      endif
      
      do k=1,npars

        if (print_par_names) print format_name,parname(k)(1:lnblnk(parname(k)))

        call ftgkys (iunit,parname(k),value,comment,status)
        if (status.ne.0) then
          print format,'undefined'
          status = 0
        else
          i = lnblnk(value)
          if (value(1:1).eq.'''' .and. value(i:i).eq.'''') then
            value (1:1) = ' '
            value (i:i) = ' '
          endif
          print format,value(1:lnblnk(value))
        endif
      enddo
      
      if (print_in_one_line) print*

      status=0
      call ftclos(iunit,status)
      call exit(0)

      end
