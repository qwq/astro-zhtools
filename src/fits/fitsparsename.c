#include <stdio.h>
#include <stdlib.h>

#define N 1024

main (int argc, char *argv[])
{
  int status;
  char filetype[N], infile[N], outfile[N], extspec[N], filter[N], binspec[N],
    colspec[N];

  if (argc != 2) {
    zhhelp("fitsparsename");
    exit (1);
  }



  status = 0;

  ffiurl (argv[1], filetype, infile, outfile, extspec, filter,
	  binspec, colspec, &status);

  if (status != 0) {
    ffrprt (stderr,status);
    exit(1);
  }
  
  printf ("%s\n",filetype);
  printf ("%s\n",infile);
  printf ("%s\n",outfile);
  printf ("%s\n",extspec);
  printf ("%s\n",filter);
  printf ("%s\n",binspec);
  printf ("%s\n",colspec);
}
