      subroutine gtifits
      implicit none
      integer iargc
      character infile*200, outfile*200
      character string*200
      integer nwords
      character word(100)*80
      double precision tstart(10000), tend(10000)
      integer nt, w, status
      integer iformat
      integer tfields
      character tunit(10)*80, tform(10)*80, ttype(10)*80
      integer unit, newunit
      logical defined

      if (iargc().ne.2) call zhhelp ('gtifits')

c     call getarg(1,infile)
      call set_default_par ('img','${ARGV[1]}')
      call get_command_line_par ('img',1,infile)
      if (.not.defined(infile)) then
        call zhhelp ('gtifits')
      endif

c     call getarg(2,outfile)
      call set_default_par ('out','${ARGV[2]}')
      call get_command_line_par ('out',1,outfile)
      if (.not.defined(outfile)) then
        call zhhelp ('gtifits')
      endif

      unit = newunit()
      open(unit,file=infile,status='old')
      read (unit,'(a)') string
      call splitwords(string,word,nwords)
      if (nwords.eq.2) then
        read (string,*) tstart(1),tend(1)
        iformat=1
        nt=1
      else if (nwords.eq.3) then ! Snowden's valid_time syntax
        read (string,*) w, tstart(1),tend(1)
        iformat=2
        nt=1
      else
        call zhhelp_man ('gtifits')
        call exit(1)
      endif
      status=0
      do while (status.eq.0)
        if (iformat.eq.1) then
          read (unit,*,iostat=status) tstart(nt+1), tend(nt+1)
        endif
        if (iformat.eq.2) then
          read (unit,*,iostat=status) w, tstart(nt+1), tend(nt+1)
        endif
        if (status.eq.0) then
          nt=nt+1
        endif
      enddo
      
      close(unit)
      status=0
      call unlink(outfile)
      call ftgiou(unit,status)
      call ftinit(unit,outfile,0,status)
      call ftphps(unit,8,0,0,status)
      call ftcrhd(unit,status)
      
      tfields=2
      ttype(1)='START'
      tform(1)='1D'
      tunit(1)=' '
      ttype(2)='STOP'
      tform(2)='1D'
      tunit(2)=' '
      
      call ftphbn(unit,nt,tfields,ttype,tform,tunit,'GTI',0,status)
      call ftpcld(unit,1,1,1,nt,tstart,status)
      call ftpcld(unit,2,1,1,nt,tend,status)
      
      call ftclos(unit,status)
      if (status.ne.0) then
        call perror_fitsio (outfile,status)
        call exit(1)
      endif
      
      end
