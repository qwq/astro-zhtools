c fmdfit from ftools modified by AV so that templfile='-' is interpreted as
c stdin
c


C******************************************************************************
C SUBROUTINE:
C      fmdfit_ftools
C
C DESCRIPTION: 
C      Modifies a FITS file header according to instructions in a template file
C
C AUTHOR:  
C      Vidya Sagar Jul '92 
C
C MODIFICATION HISTORY:
C      cleaned up code and documentation -  Sept 1992 - W Pence
C	added change of keyword name only 8/2/93 EAG
C
C NOTES:
C      fmdfit_ftools uses FITSIO calls to read FITS file
C
C USAGE:
C      call fmdfit_ftools(infile,tmpfil,ftstat)
C
C ARGUMENTS:
C      infile   - input FITS file and extension number
C      tmpfil   - input header template file
C
C PRIMARY LOCAL VARIABLES
C      infile     - name of FITS file
C      context    - error message 
C      errstr     - fitsio error message string
C      extnum     - FITS file extension number
C      ftstat     - FITSIO error number
C
C CALLED ROUTINES:
C      subroutine fcasfm - get ASCII format
C      subroutine fcecho - echo message to terminal
C      subroutine fcerrm - report an error number to terminal
C      subroutine fcpars - parse off filename and extension number
C      subroutine ftclos - close a FITS file
C      subroutine ftmrhd - relative move to FITS header
C      subroutine ftopen - open a FITS file
C
C****************************************************************************** 
      subroutine fmdfit_ftools(filnam,tmpfil,ftstat)
      implicit none

      character filnam*(*),tmpfil*(*)
c     character  infile*160
      character context*80
      
      integer ftstat, status
      integer funit
      integer tunit
c     integer block
c     integer extnum
      integer hdtype

      character  kywrd*8
      character card*80

      character tmplte*100
c     character  errstr*80
      character  kyval*80
      
      integer pname1,pname2,pvalue1,pvalue2

C  initialize variables
      status = 0
      funit = 10
      tunit = 11

*********************AV --- put parsing after open file *******************
*C  get the input FITS filename and extension number
*      call fcpars(filnam,infile,extnum,ftstat)
*
*C EAG 8/25/93 default to 1st extension
*      if (extnum .eq. -99) extnum = 1
*
*C  if extension is 0 then give error and exit
*      if (extnum .lt. 0) then
*         context = 'illegal extension number'
*         call fcerr(context)
*         goto 999
*      endif
*********************AV --- put parsing after open file *******************

C  open the input FITS file for read and write
*** AV      call ftopen(funit,infile,1,block,ftstat)
      call ftnopn(funit,filnam,1,ftstat)
      if (ftstat .ne. 0) then
         context = 'unable to open infile'
         call fcerr(context)
         goto 999
      endif

*********************AV --- don't need this because fits file will be
*********************       positioned at the right ext after open

*C  move to the extension number
*      call ftmrhd(funit,extnum,hdtype,ftstat)
*      if (ftstat .ne. 0) then
*         errstr = 'error moving to extension number '
*         write(context,1002) errstr, extnum, ftstat
* 1002    format(a35,i6,i6)
*         call fcerr(context)
*         goto 999
*      endif
*********************AV --- don't need this because fits file will be


************ AV ************************************
      if (tmpfil.eq.'-') then
        tunit = 5 ! stdin
      else
        call faopen (tunit, tmpfil, 1, 0, status)
        if (status .ne. 0) goto 1000
      endif
************ AV ************************************
      
 10   continue

C --- Read each record from the input TEMPLATE file.

      read(tunit,2000,end=999,err=1001) tmplte
 2000 format(a)

C --- Skip comment or blank lines in template file
      if (tmplte(1:1) .eq. '#' .or. tmplte(1:1) .eq. ' ')goto 10

C --- Parse the template string to get back a formatted 80
C --- character length string.

      call ftgthd(tmplte,card,hdtype,ftstat)
      if (ftstat .gt. 0)go to 999

C --- Preserve the first 8 characters of CARD to be checked
C --- for later processing

      kywrd = card(1:8)

      if (hdtype .eq. 0) then
**** AV ---- if parvalue has slashes, we need to treat it as string
        call parse_par_string (tmplte,pname1,pname2,pvalue1,pvalue2)
        if (pvalue2.gt.pvalue1) then
          if (index(tmplte(pvalue1:pvalue2),'/').gt.0) then
            if (tmplte(pvalue1-1:pvalue1-1).ne.'''') then
              tmplte = tmplte(1:pvalue1-1)//''''//tmplte(pvalue1:pvalue2)//
     ~            ''''//tmplte(pvalue2+1:)
              call ftgthd(tmplte,card,hdtype,ftstat)
              if (ftstat .gt. 0) then
                call perror_fitsio (tmplte,ftstat)
                go to 999
              endif
            endif
          endif
        endif
        
C	      modify keyword if it exists, otherwise append new keyword

C             Check to see if keyword is present in the FITS header
ccc  replaced this line of call ftgkey by call ftgcrd
ccc         call ftgkey(funit,kywrd,kyval,comment,ftstat)

        call ftgcrd(funit,kywrd,kyval,ftstat)

         if (ftstat .eq. 202) then
C                 keyword doesn't exist, so append new keyword
            ftstat = 0
            call ftcmsg
            call ftprec(funit,card,ftstat)
         else
C                 modify existing keyword
            call ftmcrd(funit,kywrd,card,ftstat)
         endif
      else if (hdtype .eq. 1) then
C		  simple append new keyword (e.g. COMMENT or HISTORY)
         call ftprec(funit,card,ftstat)

      else if (hdtype .eq. -1) then
C		  delete the record with this keyword.
         call ftdkey(funit,kywrd,ftstat)
         if (ftstat .eq. 202)then
C		    issue warning message that keyword doesn't exist
            call fcecho(' Warning: could not delete '//kywrd)
            ftstat=0
            call ftcmsg
         end if

      else if (hdtype .eq. -2) then
C		change keyword name only
         call ftmnam (funit, card(1:8), card(41:48), ftstat)

      else
C	          we must have hit an END record, so quit
         goto 999
      endif

C	jump out of loop if error was encountered
      if (ftstat .gt. 0) go to 999

C	loop back for next template record
      go to 10

 1000 call fcerr(' Error opening template file')
      ftstat = 104
      go to 999

 1001 call fcerr(' Error reading template file')
      ftstat = 108

 999  continue	
      status = 0
      call ftclos(funit,status)
      close(tunit)

      return
      end


