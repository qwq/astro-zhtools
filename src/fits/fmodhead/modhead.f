c Adopted to the stand-alone routine by AV.
c
c
c
c
C***************************************************************************** 
C SELECTOR TASK:
C      fmodhead
C
C FILE:
C      fmodhead.f 
C
C DESCRIPTION: 
C      Modifies a FITS file header according to instructions in a template file
C
C AUTHOR:  
C      Vidya Sagar. Jul '92
C
C MODIFICATION HISTORY:
C      cleaned up code and documentation - Sept 1992 - W Pence
C      8/29/94 EAG 3.0a - call faopen to open text file
C      9/22/94 EAG 3.0b - add more ftcmsg calls appropriately
C
C NOTES:
C
C ARGUMENTS:
C      none
C
C PRIMARY LOCAL VARIABLES:
C      infile    - input FITS file and extension number
C      tmpfil    - output histogram file
C CALLED ROUTINES:
C      subroutine gmodhd - gets parameters from environment
C      subroutine fmdfit - Modifies read FITS file.
C
C******************************************************************************
*      subroutine fmodhd
      program fmodhd
      implicit none
C
      character infile*160
      character tmpfil*160
      character value*160
      integer iheader
      
      integer         ftstat
      character taskname*40
      common /task/ taskname
      logical defined

      taskname = 'modifyheader'

      call ftcmsg

      infile = ' '
      tmpfil = ' '
      ftstat = 0

*C  get the parameters from the par file
*
*      call gmodhd(infile,tmpfil,ftstat)
*      if (ftstat .ne. 0) goto 999
      
      call set_default_par ('infile','${ARGV[1]}')
      call get_command_line_par ("infile",1,infile)
      if (.not.defined(infile)) then
        call usage
        call exit(1)
      endif

      call get_command_line_par ("templ",1,tmpfil)
      if (.not.defined(tmpfil)) then
        call usage
        call exit(1)
      endif
      
      call get_command_line_par ("header",1,value)
      if (defined(value)) then
        read (value,*,iostat=ftstat) iheader
        if (ftstat.ne.0) then
          call usage
          call exit(1)
        endif
        call fmdfit(infile,tmpfil,iheader,ftstat)
        
      else ! FITSIO specs of the header are given
        call fmdfit_ftools(infile,tmpfil,ftstat)
      endif

 999  continue
      if (ftstat .ne. 0) call fcerrm(ftstat)
      end

      subroutine usage
      write (0,*)
     ~ 'usage: modifyheader infile=inputfile [header=#header] templ=template'
      return
      end


C******************************************************************************
C SUBROUTINE:
C      fmdfit
C
C DESCRIPTION: 
C      Modifies a FITS file header according to instructions in a template file
C
C AUTHOR:  
C      Vidya Sagar Jul '92 
C
C MODIFICATION HISTORY:
C      cleaned up code and documentation -  Sept 1992 - W Pence
C	added change of keyword name only 8/2/93 EAG
C
C NOTES:
C      fmdfit uses FITSIO calls to read FITS file
C
C USAGE:
C      call fmdfit(infile,tmpfil,iheader,ftstat)
C
C ARGUMENTS:
C      infile   - input FITS file and extension number
C      tmpfil   - input header template file
c      iheader  - which header to modify :  AV      
C
C PRIMARY LOCAL VARIABLES
C      infile     - name of FITS file
C      context    - error message 
C      errstr     - fitsio error message string
C      extnum     - FITS file extension number
C      ftstat     - FITSIO error number
C
C CALLED ROUTINES:
C      subroutine fcasfm - get ASCII format
C      subroutine fcecho - echo message to terminal
C      subroutine fcerrm - report an error number to terminal
C      subroutine fcpars - parse off filename and extension number
C      subroutine ftclos - close a FITS file
C      subroutine ftmrhd - relative move to FITS header
C      subroutine ftopen - open a FITS file
C
C****************************************************************************** 
      subroutine fmdfit(filnam,tmpfil,iheader,ftstat)

      character filnam*(*),tmpfil*(*)
      character  infile*160
      character context*80
      character comment*80
      
      integer ftstat, status
      integer funit
      integer tunit
      integer block
      integer extnum
      integer hdtype

      character kywrd*8 
      character card*80

      character tmplte*100
      character errstr*80
      character kyval*80
      
      integer pname1,pname2,pvalue1,pvalue2

C  initialize variables
      status = 0
      funit = 10
      tunit = 11
      ftstat=0

C  get the input FITS filename and extension number
*      call fcpars(filnam,infile,extnum,ftstat)
      infile=filnam
      extnum = iheader

C EAG 8/25/93 default to 1st extension
      if (extnum .eq. -99) extnum = 1

C  if extension is 0 then give error and exit
      if (extnum .lt. 0) then
         context = 'illegal extension number'
         call fcerr(context)
         goto 999
      endif

C  open the input FITS file for read and write
      call ftopen(funit,infile,1,block,ftstat)
      if (ftstat .ne. 0) then
         context = 'unable to open infile'
         call fcerr(context)
         goto 999
      endif

C  move to the extension number
      call ftmrhd(funit,extnum,hdtype,ftstat)
      if (ftstat .ne. 0) then
         errstr = 'error moving to extension number '
         write(context,1002) errstr, extnum, ftstat
 1002    format(a35,i6,i6)
         call fcerr(context)
         goto 999
      endif

      if (tmpfil.eq.'-') then
        tunit = 5 ! stdin
      else
        call faopen (tunit, tmpfil, 1, 0, status)
        if (status .ne. 0) goto 1000
      endif

      
 10   continue

C --- Read each record from the input TEMPLATE file.

      read(tunit,2000,end=999,err=1001) tmplte
 2000 format(a)

C --- Skip comment or blank lines in template file
      if (tmplte(1:1) .eq. '#' .or. tmplte(1:1) .eq. ' ')goto 10

C --- Parse the template string to get back a formatted 80
C --- character length string.

      call ftgthd(tmplte,card,hdtype,ftstat)
      if (ftstat .gt. 0) then
        call perror_fitsio (tmplte,ftstat)
        go to 999
      endif

C --- Preserve the first 8 characters of CARD to be checked
C --- for later processing

      kywrd = card(1:8)

      if (hdtype .eq. 0) then
        call parse_par_string (tmplte,pname1,pname2,pvalue1,pvalue2)
        if (pvalue2.gt.pvalue1) then
          if (index(tmplte(pvalue1:pvalue2),'/').gt.0) then
            if (tmplte(pvalue1-1:pvalue1-1).ne.'''') then
              tmplte = tmplte(1:pvalue1-1)//''''//tmplte(pvalue1:pvalue2)//
     ~            ''''//tmplte(pvalue2+1:)
              call ftgthd(tmplte,card,hdtype,ftstat)
              if (ftstat .gt. 0) then
                call perror_fitsio (tmplte,ftstat)
                go to 999
              endif
            endif
          endif
        endif
C	      modify keyword if it exists, otherwise append new keyword

C             Check to see if keyword is present in the FITS header
         call ftgkey(funit,kywrd,kyval,comment,ftstat)

         if (ftstat .eq. 202) then
C                 keyword doesn't exist, so append new keyword
            ftstat = 0
            call ftcmsg
            call ftprec(funit,card,ftstat)
         else
C                 modify existing keyword
            call ftmcrd(funit,kywrd,card,ftstat)
         endif
      else if (hdtype .eq. 1) then
C		  simple append new keyword (e.g. COMMENT or HISTORY)
         call ftprec(funit,card,ftstat)

      else if (hdtype .eq. -1) then
C		  delete the record with this keyword.
         call ftdkey(funit,kywrd,ftstat)
         if (ftstat .eq. 202)then
C		    issue warning message that keyword doesn't exist
            call fcecho(' Warning: could not delete '//kywrd)
            ftstat=0
            call ftcmsg
         end if

      else if (hdtype .eq. -2) then
C		change keyword name only
         call ftmnam (funit, card(1:8), card(41:48), ftstat)

      else
C	          we must have hit an END record, so quit
         goto 999
      endif

C	jump out of loop if error was encountered
      if (ftstat .gt. 0) go to 999

C	loop back for next template record
      go to 10

 1000 call fcerr(' Error opening template file')
      ftstat = 104
      go to 999

 1001 call fcerr(' Error reading template file')
      ftstat = 108

 999  continue	
      status = 0
      call ftclos(funit,status)
      close(tunit)

      return
      end


C******************************************************************************
C SUBROUTINE:
C      fcecho
C
C DESCRIPTION:
C      This subroutine provides a single point to send text to the
C      terminal. This routine should be modified when a new host
C      environment is used.
C
C AUTHOR/DATE:
C      Kent Blackburn  11/5/91 
C
C MODIFICATION HISTORY:
C
C NOTES:
C      fcecho uses F77/VOS like calls for terminal I/O
C
C USAGE:
C      call fcecho(string)
C
C ARGUMENTS:
C      string - text string sent to terminal
C
C PRIMARY LOCAL VARIABLES:
C
C CALLED ROUTINES:
C      subroutine umsput - put message
C
C******************************************************************************
      subroutine fcecho(string)

      character string*(*)
*     integer dest,prio,irafsts
      integer lnblnk

      write(0,*)string(1:lnblnk(string))
*      dest = 1
*      prio = 0
*      irafsts = 0
*      call logstr(string)
*      call umsput(string,dest,prio,irafsts)
      return
      end

C****************************************************************************
C SUBROUTINE:
C      faopen
C
C DESCRIPTION:
C      Open an ascii file with system dependent options
C
C AUTHOR:  
C       Emily A. Greene
C       Hughes STX
C       August, 1994
C
C MODIFICATION HERSTORY:
C       Ron Zellar -- moved open statements to faopnw
C
C NOTES:
C
C USAGE:
C      call faopen (unit, filename, iomode, recl, status)
C
C ARGUMENTS:
C      unit     - input  - integer - unit number to open
C      filename - input  - string  - name of file to open ! to overwrite
C      iomode   - input  - integer - I/O mode to open file
C                                    1 - readonly
C                                    2 - new file (! overwrites if exists)
C                                    3 - read/write
C                                    4 - append
C      recl     - input  - integer - maximum record length <0 system default
C                                    (133 for VMS, ignored for unix)
C      status   - output - integer - status of operation
C
C PRIMARY LOCAL VARIABLES:
C
C CALLED ROUTINES:
C
C*****************************************************************************
      subroutine faopen (unit, filename, iomode, recl, status)

      integer unit, iomode, recl, status
      character filename*(*)
      logical yespar

      logical killit

      if (status .ne. 0) return
      
      killit = .false.
      if (filename(1:1) .eq. '!') then
        filename = filename(2:)
        killit = .true.
      else if (iomode .ne. 1) then
        killit = yespar ('clobber')
      endif

      if ((iomode .ne. 1) .and. (killit))
     &     call clobber (filename, status)
      if (status .ne. 0) goto 999

C     Open the file
      call faopnw (unit, filename, iomode, recl, status)

 999  return
      end


************************************************************
      subroutine fcerr(string)

      character string*(*)
      integer dest,prio,irafsts
      character taskname*40
      common /task/ taskname
      character buffer1*120,buffer2*120
      integer i, lnblnk

      dest = 2
      prio = 0
      irafsts = 0
      buffer1 = taskname
      i = lnblnk(buffer1)
      buffer2 = buffer1(1:i)//' : '//string

c write to STDERR and logfile
*      call fxwrite (buffer2, 6)
      write(0,'(a)') buffer2(1:lnblnk(buffer2))

c      call logstr(buffer2)
c      call umsput(buffer2,dest,prio,irafsts)
      return
      end
C******************************************************************************
C SUBROUTINE:
C      fcerrm
C
C DESCRIPTION:
C      This subroutine provides an echo of the error status to
C      terminal environment in use.
C
C AUTHOR/DATE:
C      Kent Blackburn  12/5/91      
C
C MODIFICATION HISTORY:
C
C NOTES:
C
C USAGE:
C      call fcerrm(stat)
C
C ARGUMENTS:
C      stat - fitsio returned error status code
C
C PRIMARY LOCAL VARIABLES:
C      context - error message
C      message - error message and code
C
C CALLED ROUTINES:
C      subroutine fcecho - echo message to terminal
C
C******************************************************************************
      subroutine fcerrm(stat)

      integer stat
      character context*40,message*40

      if ((stat .lt. 1).or.(stat .gt.411)) goto 99
      context = 'Error Status Returned : '
      write(message,1000) context,stat
 1000 format(A24,I4)
      call fcerr(message)
      message = ' '
      call ftgerr(stat,message)
      call fcerr(message)
*      call fitsdstk()
 99   return
      end

C****************************************************************************
C SUBROUTINE:
C      faopnw
C
C DESCRIPTION:
C      Open an ascii file with system dependent options on unix platforms
C
C AUTHOR:  
C       Ron Zellar
C       Hughes STX
C       Oct, 1994
C
C MODIFICATION HERSTORY:
C	Original code came from the faopen routine written by Emily Greene
C
C NOTES:
C
C USAGE:
C      call faopnw (unit, filename, iomode, recl, status)
C
C ARGUMENTS:
C      unit     - input  - integer - unit number to open
C      filename - input  - string  - name of file to open ! to overwrite
C      iomode   - input  - integer - I/O mode to open file
C                                    1 - readonly
C                                    2 - new file
C                                    3 - read/write
C                                    4 - append
C      recl     - input  - integer - maximum record length <0 system default
C                                    (133 for VMS, ignored for unix)
C      status   - output - integer - status of operation
C
C PRIMARY LOCAL VARIABLES:
C
C CALLED ROUTINES:
C
C*****************************************************************************
      subroutine faopnw (unit, filename, iomode, recl, status)

      integer unit, iomode, recl, status
      character filename*(*)

      if (status .ne. 0) return

C and open the file!

      if (iomode .eq. 1) then
         open (unit, file=filename, status='old', iostat=status)
      else if (iomode .eq. 2) then
         open (unit, file=filename, status='new', iostat=status)
      else if (iomode .eq. 3) then
         open (unit, file=filename, status='unknown', iostat=status)
      else if (iomode .eq. 4) then
         open (unit, file=filename, status='unknown', iostat=status)

C append access is not supported on all unix systems

 10      read (unit, '(a)', end=999)
         goto 10
      endif

 999  return
      end


C******************************************************************************
C SUBROUTINE:
C     clobber
C
C DESCRIPTION:
C     This routine clears the way to use the file named filenam.
C     It deletes the file named filenam (under VMS, it deletes 
C     the highest numbered version of filenam.) Thus, it leaves
C     filenam available for an OPEN(...,STATUS='NEW') statement.
C
C AUTHOR/DATE:
C     Lawrence Brown 7/12/94
C
C MODIFICATION HISTORY:
C
C NOTES:
C     To add clobber (overwrite) capability to an ftool, put lines like
C     the following in the parameter fetching routine:
C
C      LOGICAL DELEXIST
C      CHARACTER OUTFILE*160
C      INTEGER STATUS
C      CALL UCLGSB('CLOBBER', DELEXIST, STATUS)
C      IF (STATUS .NE. 0) THEN
CC     Probably means there wasn't a clobber field in the .par file
C         STATUS=0
C      ELSE IF(DELEXIST) THEN
C         CALL CLOBBER(OUTFILE,STATUS)
C         IF(STATUS.NE.0) THEN
CC     Do something appropriate. outfile is probably read only.
C      ENDIF
C
C    Then add:
C     
C    clobber,b,h,no,,,"Overwrite existing output file? (CAUTION)"
C
C    to the par file.
C
C USAGE:
C     call clobber(filenam,status)
C
C ARGUMENTS:
C     filenam - the file to be "clobbered"
C     status  - returned error status 
C
C PRIMARY LOCAL VARIABLES:
C     exists - logical for inquire statements
C     lun - logical unit number for clobbering
C
C CALLED ROUTINES:
C
C******************************************************************************
      subroutine clobber(filenam,status)
      character filenam*(*)
      integer status
C
      logical exists,opened
      integer lun

      if(status.ne.0) return
      
      inquire(file=filenam,exist=exists)
      if(exists) then
C     get rid of it
C     first look for a free logical unit number to use to commit the act with
         do 10 lun=99,10,-1
            inquire(lun,opened=opened)
            if(.not.opened) goto 20
 10      continue
C     failed to find free lun 
         status=1
         goto 999
 20      continue
         open(lun,file=filenam,status='old',err=30)
         close(lun,status='delete',err=40)
C     we're done
         goto 999
 30      continue
C     error opening file
         status=2
         goto 999
 40      continue
C     error closing and deleting file (This could really mess up the main 
C     code, so check for it
         status=3
      endif
 999  continue
      return
      end

      subroutine parse_par_string (string,pname1,pname2,pvalue1,pvalue2)
c
c If pname1 == 0, string is not a parameter one
c
c if pvalue2 < pvalue1, parameter is set, but its value is not defined
c
      implicit none
      character string*(*)
      integer pname1,pname2,pvalue1,pvalue2
      
      character word(3)*80
      integer ww,nwords
      integer k,n,l
      integer whereiseq, lnblnk
      logical qstring, quoteread
      
c if the first symbol in line is not a letter, the string is a comment
      ww=ichar(string(1:1))
      if (.not.((ww.ge.65.and.ww.le.90).or.(ww.ge.97.and.ww.le.122)) ) then
        pname1=0
        pname2=0
        pvalue1=0
        pvalue2=0
        return
      endif
      
      call firstthreewords (string,word,nwords)

      pname1 = 1                ! if there is a parameter, its name starts in 
                                ! the first position

c Look for = in the first word
      k = whereiseq(word(1))

      if (k.eq.0) then
c       there is no '=' in the first word; the second one should start with '='
c       for that, 1) there must be more than 1 word
        if (nwords.le.1) then
          pname1=0
          pname2=0
          pvalue1=0
          pvalue2=0
          return
        endif
c       and 2) the second one should start with '='   
        if (word(2)(1:1).ne.'=') then
          pname1=0
          pname2=0
          pvalue1=0
          pvalue2=0
          return
        endif

                                ! Ok, we have a parameter; name ends where
                                ! word(1) ends
        pname2 = lnblnk(word(1))
      
      else
c if k>0, there is '=' in the first word, and so we have a parameter; name
c ends at k-1
        pname2 = k-1
      endif

c Now we know that there is a '=' past the par name; find it     
      k = pname2
      do while (string(k:k).ne.'=')
        k = k + 1
      enddo
      l = k                     ! remember the position of the '='

c Now find where parvalue starts
c   remember where the string ends,
      n = lnblnk(string)
c   start past the '=' sign
      k=k+1
c   and skip all spaces and tabs
      do while (string(k:k).eq.' '.or.string(k:k).eq.'\t')
        k = k + 1
        if ( k .gt. n ) then
c          we came to the end of the string, but have not found the value.
c          This means that parameter is set, but not defined
          pvalue1 = l+1
          pvalue2 = l
          return
        endif
      enddo

c   now, check what is the first character of the value
      if (string(k:k).eq.'''') then
        qstring = .true.
        quoteread = .false.
        pvalue1 = k + 1
      else
        qstring = .false.
        pvalue1 = k
      endif

c
      if (qstring) then         ! Parameter value should be terminated by a 
c single quote. However, the value can contain quotes that are either escaped
c or doubled
        k = pvalue1
        do while ((string(k:k).ne.'''').or.quoteread)
          k = k + 1
c         check if this character is an escape for the next quote
          quoteread = .false.
          if ( string(k:k) .eq. '''' .or. string(k:k) .eq. '\\' ) then
            if ( string(k+1:k+1).eq.'''') then
              k = k + 1
              quoteread = .true.
            endif
          endif
        enddo
        pvalue2 = k - 1
        return
        
      else
        k = pvalue1
        do while (string(k:k).ne.' '.and.string(k:k).ne.'\t')
          k = k + 1
        enddo
        pvalue2 = k - 1
        return
      endif

      return
      end






*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c
c  Short (i.e. find maximum three first words) version of the splitwords
c 
c
      subroutine firstthreewords (string,words,nwords)
      implicit none
      character string*(*)
      character words(*)*(*)
      integer nwords
      integer len,i,j,lnblnk
      
      len=lnblnk(string)
      
      i=1
      
      nwords=0
      
      do while (i.le.len.and.nwords.lt.3)
        do while ((string(i:i).eq.' '.or.string(i:i).eq.'\t').and.i.lt.len)
          i=i+1
        enddo
        if (i.eq.len) then
          if (string(i:i).ne.' '.and.string(i:i).ne.'\t') then
            nwords=nwords+1
            words(nwords)=string(i:i)
          endif
          goto 100
        endif
        
        nwords=nwords+1
        j=1
        words(nwords)=' '
        do while ((string(i:i).ne.' '.and.string(i:i).ne.'\t').and.i.le.len)
          words(nwords)(j:j)=string(i:i)
          i=i+1
          j=j+1
        enddo
      enddo
      
 100  continue
      
      return
      end
      
      function whereiseq (string)
      implicit none
      integer whereiseq 
      character string*(*)
      integer lnblnk
      integer len,i
      
      len=lnblnk(string)
            
      do i=1,len
        if (string(i:i).eq.'=') then
          whereiseq=i
          return
        endif
      enddo
      
      whereiseq=0
      return
      end
