      subroutine copywcs
c
c Copy WCS keywords from one FITS extension to the other
c
c

      character from*200, to*200, arg*200
      logical defined
      integer unitfrom, unitto

      call get_cl_par ('from',from)
      if (.not.defined(from)) call zhhelp ('copywcs')

      call get_cl_par ('to',to)
      if (.not.defined(to)) call zhhelp ('copywcs')

      status = 0
      call ftgiou (unitfrom,status)
      call ftgiou (unitto,status)
      if (status.ne.0) call exit_fitsio ('I/O units',status)

      call ftnopn (unitfrom,from,0,status)
      if (status.ne.0) call exit_fitsio (from,status)

      call ftnopn (unitto,to,1,status)
      if (status.ne.0) call exit_fitsio (to,status)

c Determine extension type for the input file and read keys
      call FTGHDT (unitfrom,type,status)
      if (status.ne.0) call exit_fitsio (from,status)

      if (type.eq.0) then       ! image
        call FTGICS (unit,xrval,yrval,xrpix,yrpix,xinc,yinc,rot,coordtype,
     ~      status)
        if (status.ne.0.and.status.ne.506) call exit_fitsio (from,status)
        status = 0
        
      else                      ! table
        call io_table_wcs_keys (unitfrom,from,'from',
     ~      xrval,yrval,xrpix,yrpix,xinc,yinc,rot,coordtype,'get')
      endif

c Determine the extension type for the output and write keys
      
      call FTGHDT (unitto,type,status)
      if (status.ne.0) call exit_fitsio (to,status)
      if (type.eq.0) then       ! image
        call FTGICS (unit,xrval,yrval,xrpix,yrpix,xinc,yinc,rot,coordtype,
     ~      status)
        if (status.ne.0.and.status.ne.506) call exit_fitsio (from,status)
        status = 0
        
      else                      ! table
        call io_table_wcs_keys (unitfrom,from,'from',
     ~      xrval,yrval,xrpix,yrpix,xinc,yinc,rot,coordtype)
      endif



      subroutine io_table_wcs_keys (unit,file,key,
     ~    xrval,yrval,xrpix,yrpix,xinc,yinc,rot,coordtype,operation)
      implicit none
      double precision xrval,yrval,xrpix,yrpix,xinc,yinc,rot
      character key*(*), file*(*), coordtype*(*), operation*(*)
      integer unit

      character xcolname*80, ycolname*80, word(100)*80
      integer xcol, ycol

      character buff*80, arg*80
      logical defined


      xcolname = 'X'
      ycolname = 'Y'

      buff = key
      call strcat (buff,'cols')
      call get_cl_par (buff,arg)
      if (defined(arg)) then
        call splitwords_char (arg,word,nwords,',')
        if (nwords.ne.2) call zhhelp ('copywcs')
        xcolname = word(1)
        ycolname = word(2)
      endif

      buff = key
      call strcat (buff,'xcol')
      call get_cl_par (buff,arg)
      if (defined(arg)) xcolname = arg
        
      buff = key
      call strcat (buff,'ycol')
      call get_cl_par (buff,arg)
      call get_cl_par (buff,arg)
      if (defined(arg)) ycolname = arg
        
      call ftgcno (unit,.false.,xcolname,xcol,status)
      if (status.ne.0) then
        buff = file
        call strcat (buff,': ')
        call strcat (buff,xcolname)
        call exit_fitsio (buff,status)
      endif
        
      call ftgcno (unitfrom,.false.,ycolname,ycol,status)
      if (status.ne.0) then
        buff = file
        call strcat (buff,': ')
        call strcat (buff,ycolname)
        call exit_fitsio (buff,status)
      endif

      if (operattion.eq.'get') then
        call FTGTCS(unit,xcol,ycol,
     ~      xrval,yrval,xrpix,yrpix,xinc,yinc,rot,coordtype,status)
        if (status.ne.0.and.status.ne.506) call exit_fitsio (file,status)
      endif

      return
      end

        
        
        
          
      
      
      
