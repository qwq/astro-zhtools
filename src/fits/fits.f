      implicit none
      character arg*80
      integer lnblnk
      integer n,k

      call getarg(0,arg)
      n = lnblnk(arg)
      
      k = max(1,n-9)
      if (arg(k:n).eq.'fcolappend') then
        call fcolappend
        call exit(0)
      endif

      k = max(1,n-7)
      if (arg(k:n).eq.'printkey') then
        call printkey
        call exit(0)
      endif

      k = max(1,n-7)
      if (arg(k:n).eq.'chimtype') then
        call chimtype
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'iffits') then
        call iffits
        call exit(0)
      endif

      k = max(1,n-6)
      if (arg(k:n).eq.'gtifits') then
        call gtifits
        call exit(0)
      endif

      k = max(1,n-5)
      if (arg(k:n).eq.'gtiexp') then
        call gtiexp
        call exit(0)
      endif
      
      write(0,*)
     ~    'Use it as fcolappend, printkey, iffits, or chimtype, gtfits, gtiexp'
      call exit(1)

      end
      
