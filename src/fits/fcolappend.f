      subroutine fcolappend
      implicit none
      character data*200,out*200
      
      integer lnblnk
      
c fitsio staff
      integer iunit,ounit
      parameter (iunit=10)
      parameter (ounit=11)
      integer status
      
      integer pcount,gcount
      
      character arg*80
      integer blocksize
      character comment*80
      integer length,nrows,tfields
      character ttype(300)*80,tform(300)*80,tunit(300)*80,extname*80
      integer varidat

      integer keysexist,keysadd,ikey
      character hrec*200
      
      character string*300
      character buff*8192
      
      integer nhdu,hdutype
      integer nrow,nbuff
      integer nbuffmax
      logical copyall
      
      
      character colname*80,unit*80,type*80
      
      real value
      character vstring*10
      integer nvstring
      integer*2 iii
      integer*4 jjj
      character bbb*1
      double precision  ddd
      
      integer n,i,irow,next, iargc
      logical defined
c----------------------------------------------------------------- 
      if (iargc().lt.3) call zhhelp ('fcolappend')

      call set_default_par ('img','${ARGV[1]}')
      call set_default_par ('out','${ARGV[2]}')
      call set_default_par ('col','${ARGV[3]}')

c     call getarg(1,data)
      call get_command_line_par ('img',1,data)
      if (.not.defined(data)) then
        call zhhelp ('fcolappend')
      endif

c     call getarg(2,out)
      call get_command_line_par ('out',1,out)
      if (.not.defined(out)) then
        call zhhelp ('fcolappend')
      endif

c     call getarg(3,colname)
      call get_command_line_par ('col',1,colname)
      if (.not.defined(colname)) then
        call zhhelp ('fcolappend')
      endif

      value=0
      
      call get_command_line_par  ('type',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        type=arg(1:1)
      else
        type='i'
      endif
      
      call get_command_line_par  ('unit',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        unit=arg
      else
        unit=' '
      endif
      
      call get_command_line_par  ('value',1,arg)
      if (arg.ne.'undefined'.and.arg.ne.'UNDEFINED') then
        read(arg,*)value
      endif
      
      call get_command_line_par ('copyall',1,arg)
      if (arg(1:1).eq.'n') then
        copyall=.false.
      endif
      
      if (type(1:1).eq.'b'.or.type(1:1).eq.'B') then
        type='B'
        nvstring=1
        bbb=char(nint(value))
        write(vstring,'(a1)')bbb
        
      else if (type(1:1).eq.'i'.or.type(1:1).eq.'I') then
        type='I'
        nvstring=2
        iii=nint(value)
        write(vstring,'(a2)')iii
        
      else if (type(1:1).eq.'j'.or.type(1:1).eq.'J') then
        type='J'
        nvstring=4
        jjj=nint(value)
        write(vstring,'(a4)')jjj
        
      else if (type(1:1).eq.'e'.or.type(1:1).eq.'E') then
        type='E'
        nvstring=4
        write(vstring,'(a4)')value
        
        
      else if (type(1:1).eq.'d'.or.type(1:1).eq.'D') then
        type='D'
        nvstring=8
        ddd=value
        write(vstring,'(a8)')ddd
        
      else
        write(0,*)'wrong type: ', type
        call exit(1)
      endif
        
        


      pcount=0
      gcount=1


c parse name
      n=lnblnk(data)
      if (data(n:n).eq.']') then
        i=n-1
        do while (i.gt.1.and.data(i:i).ne.'[')
          i=i-1
        enddo
        read(data(i+1:n-1),*)next
        data=data(1:i-1)
      else
        next=1
      endif
      
      
c open output
      call unlink(out)
      call ftinit(ounit,out,0,status)


      
c read FITS file
      status=0
      call ftopen(iunit,data,0,blocksize,status)
      if (status.ne.0) then
        write(0,*)'error opening file: ',data(1:lnblnk(data))
        call exit(1)
      endif
      

c copy other extensions until we're at the desired extension
      nhdu=1
 10   continue
      call ftmahd (iunit,nhdu,hdutype,status)
      if (hdutype.ne.0) then
        call ftgkys (iunit,'EXTNAME',extname,comment,status)
      else
        extname='prime'
      endif

      if (nhdu-1.ne.next) then
        if (copyall.or.extname.eq.'prime') then
          if (nhdu.gt.1) then
            call ftcrhd(ounit,status)
          endif
          
          call ftcopy(iunit,ounit,0,status)
          
        endif
        nhdu = nhdu + 1
        goto 10
      endif
        
      
c read the desired extension
c copy header
      call ftcrhd(ounit,status)
      call ftghsp (iunit,keysexist,keysadd,status)
        
      do ikey=1,keysexist
        call ftgrec(iunit,ikey,hrec,status) 
        call ftprec(ounit,hrec,status)
      enddo
      
      call ftgkyj (iunit,'NAXIS1',length,comment,status)
      call ftghbn (iunit,300,nrows,tfields,ttype,tform,tunit,extname,varidat
     ~    ,status)
      
      tfields=tfields+1
      ttype(tfields)=colname
      tform(tfields)=type
      tunit(tfields)=unit

      call ftpkns (ounit,'TTYPE',tfields,1,ttype(tfields),' ',status)
      call ftpkns (ounit,'TFORM',tfields,1,tform(tfields),' ',status)
      call ftpkns (ounit,'TUNIT',tfields,1,tunit(tfields),' ',status)
      
      call ftmkyj (ounit,'TFIELDS',tfields,' ',status)
      call ftmkyj (ounit,'NAXIS1',length+nvstring,' ',status)
      
      
      call fthdef (ounit,0,status)
      call ftbdef (ounit,tfields,tform,varidat,nrows,status)
      
      nbuffmax=8192/(length+nvstring)

      
      nrow=0
      nbuff=0

c read table
      do irow=1,nrows
        call ftgtbs (iunit,irow,1,length,string,status)
        buff(nbuff*(length+nvstring)+1:nbuff*(length+nvstring)+length+nvstring
     ~      )=string(1:length)//vstring(1:nvstring)
        nbuff=nbuff+1
        if (nbuff.eq.nbuffmax) then
          call ftptbs (ounit,nrow+1,1,(length+nvstring)*nbuffmax,buff,status)
          nrow=nrow+nbuffmax
          nbuff=0
        endif
      enddo
      
      call ftptbs (ounit,nrow+1,1,(length+nvstring)*nbuff,buff,status)
      nrow=nrow+nbuff
      
      if (copyall) then
c copy other extensions until we're at the EVENTS
        nhdu=nhdu+1
 100    continue
        call ftmahd (iunit,nhdu,hdutype,status)
        if (status.ne.0) then
          status=0
          goto 110
        endif
        if (hdutype.ne.0) then
          call ftgkys (iunit,'EXTNAME',extname,comment,status)
        else
          extname='prime'
        endif
        
        if (extname.ne.'EVENTS'.and.extname.ne.'events') then
          if (nhdu.gt.1) then
            call ftcrhd(ounit,status)
          endif
          
          call ftcopy(iunit,ounit,0,status)
          
          nhdu = nhdu + 1
          goto 100
          
        endif
        
      endif
      
 110  continue
      call ftclos(ounit,status)
      if (status.ne.0) then
        write(0,*)'Close file:'
        call fitsio_write_error (status,0)
         call exiterror('')
      endif
        
      
      end
      








