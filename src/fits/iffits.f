      subroutine iffits
      character fname*200
      integer blocksize,status
      logical defined

      status=0
c     call getarg(1,fname)
      call set_default_par ('img','${ARGV[1]}')
      call get_command_line_par ('img',1,fname)
      if (.not.defined(fname)) then
        call zhhelp ('iffits')
      endif
      
      call ftopen(1,fname,0,blocksize,status)
      
      call ftclos(1,status)
      
      if (status.ne.0) then
        status=0
      else
        status=1
      endif
      
      print*,status
      
      end
