      subroutine gtiexp
c
c Prints out exposure associated with the GTI file
c It is the user's responsibility to point to correct extension in the GTI
c file
      
      character gtifile*200
      logical defined
      integer unit,newunit, status
      double precision texp,tstart,tstop
      character string*200
      integer nwords
      character word(100)*80
      
      call get_cl_par ('gti',gtifile)
      if (.not.defined (gtifile)) then
        call get_cl_par ('gtifile',gtifile)
        if (.not.defined (gtifile)) then
          call getarg(1,gtifile)
          if (gtifile.eq.'') then
            call zhhelp ('gtiexp')
          endif
        endif
      endif

      call gtiexp_fits (gtifile) ! it returns only is the gti file is not fits
      
      unit=newunit()
      open(unit,file=gtifile,status='old')
      read (unit,'(a)') string
      call splitwords(string,word,nwords)
      if (nwords.eq.2) then
        read (string,*) tstart,tstop
        iformat=1
        texp=tstop-tstart
      else if (nwords.eq.3) then ! Snowden's valid_time syntax
        read (string,*) w, tstart,tstop
        iformat=2
        texp=tstop-tstart
      else
        call exiterror ('Invalid structure of the text gti file')
      endif
      
      status = 0
      do while (status.eq.0)
        if (iformat.eq.1) then
          read (unit,*,iostat=status) tstart, tstop
        endif
        if (iformat.eq.2) then
          read (unit,*,iostat=status) w, tstart, tstop
        endif
        if (status.eq.0) then
          texp=texp+tstop-tstart
        endif
      enddo
      close(unit)

      print*,sngl(texp)
      return
      end

        
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine gtiexp_fits (gtifile)
      implicit none
      character gtifile*(*)

      integer nt, ntmax
      parameter (ntmax=1000000)
      double precision tstart(ntmax), tstop(ntmax), texp

      character startname*200,stopname*200
      logical defined

      integer unit
      integer status,colnum,i
      logical anyf

      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
      
      call ftnopn (unit,gtifile,0,status)
      if (status.ne.0) then     ! not a fits file
        status=0
        call ftfiou (unit,status)
        return
      endif
      
      call ftgnrw (unit,nt,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
      if (nt.gt.ntmax) then
        write (0,*) 'Too many gti: ',nt,' (',ntmax,' maximum allowed)'
        call exit(1)
      endif

      call get_cl_par ('stop',stopname)
      if (.not.defined(stopname)) stopname = 'STOP'
      call get_cl_par ('start',startname)
      if (.not.defined(startname)) startname = 'START'

      call FTGCNO (unit,.false.,startname,colnum,status)
      call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstart,anyf,status)
      call FTGCNO (unit,.false.,stopname,colnum,status)
      call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstop,anyf,status)

      if (status.ne.0) call exit_fitsio (gtifile,status)
      
      texp = 0
      do i=1,nt
        texp = texp + tstop(i)-tstart(i)
      enddo

      print*,sngl(texp)
      
      call exit(0)
      
      end
