      implicit none
      real T,abund,z

      integer nsmax,ns
      parameter (nsmax=3072)
      real e_bin(0:nsmax), rsspec(nsmax)
      character*80 plmodel, abtable ! Plasma and metal abundance models

      
      double precision bol, soft
      real e
      integer i, ia, it
      


      plmodel = 'mekal'
      abtable = 'angers'


      if (plmodel.eq.'raym') then
        if (abtable.eq.'angers') then
          write (0,*) 'setting abund = xspec'
          call set_abunds ('xspec')
        else
          if (abtable.ne.'allen') then
            write (0,*)
     ~          'Warning: only angers or allen abundance tables for RS'
            write (0,*) '         use allen'
          endif
          call set_abunds ('allen')
        endif
      elseif (plmodel.eq.'mekal') then
        call mekal_set_abund (abtable)
      else
        write (0,*) 'Error: unknown plasma model: ',plmodel
        call exit(1)
      endif

      ns = 3000
      do i=0,ns
        e_bin(i)=0.01+i*0.01
      enddo

      z = 0.05 ! redshift

      do it = 10,100,5
        T = it*0.1
        do ia=0,5
          abund = ia*0.1 ! metallicity, solar units

          if (plmodel.eq.'raym') then
            call RAYM (T, abund, z, ns, e_bin, rsspec)
          elseif (plmodel.eq.'mekal') then
            call mekal (T, abund, z, ns, e_bin, rsspec)
          endif
        
          bol = 0
          soft = 0
          do i=1,ns
            e=0.5*(e_bin(i-1)+e_bin(i))
            bol = bol + rsspec(i)*e
*            print*,i,rsspec(i)
            if (e.gt.0.5.and.e.lt.2.0) then
              soft = soft + rsspec(i)*e
            endif
          enddo
*          print*,soft,bol
          
          print '(f5.1,1x,f5.2,1x,f10.4)',T,abund,soft/bol

        enddo
      enddo
        
      call exit(0)
      

      end
