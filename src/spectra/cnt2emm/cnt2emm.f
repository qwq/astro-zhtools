      implicit none
      real T,abund,z,nH,Omega, Lambda

      character*200 arffile, rmffile, filename
      character*80 arg, colname, matext, hduclas3,rmfversn
      character*80 telescop, instrume, detnam, filter, chantype
      integer nsmax,nchmax, ns,nch
      parameter (nsmax=3072,nchmax=2048)
      real eb1(nsmax),eb2(nsmax),rsp(nchmax,nsmax)
      real e_bin(0:nsmax), rsspec(nsmax), effarea(nsmax)
      real e_min(nchmax), e_max(nchmax)
      integer channel (nchmax)
      integer ngrpmax, grpmax
      parameter (ngrpmax=100)
      integer ngrp(nsmax), F_chan(nsmax,ngrpmax), N_chan(nsmax,ngrpmax)
      real lo_thresh, areascal
      integer flchan, chatter
      double precision modelspec(nsmax), rsspec8(nsmax)
      double precision phmodelspec(nsmax), emodelspec(nsmax)
      double precision phflux,eflux, eeflux
      real femin,femax
      integer cmin,cmax
      real emin,emax
      integer i,j
      real sigism,e
      double precision absorp
      real d_a
      double precision dl
      character*80 plmodel, abtable ! Plasma and metal abundance models

      double precision flux

      real c,h0,pi,rad2sec,daconst
      parameter (c=2.9979245620e5) ! km/s
      parameter (pi=3.1415926536)
      parameter (rad2sec = 648000.0/pi)

      integer unitrmf, unitarf, colnum, nrows, blocksize, status
      logical anyf
      
      logical defined, yespar, defpar

      logical plaw
      real gamma

      plaw = yespar ('powerlaw')

      if (plaw) then
        call get_parameter_value ('gamma',gamma,'e')
        call get_parameter_value ('nH',nH,'e')
      else
        call get_cl_par ('plasma_model',plmodel)
        if (.not.defined(plmodel)) then
          plmodel = 'mekal'
        endif
        
        call get_cl_par ('abundance_table',abtable)
        if (.not.defined(abtable)) then
          abtable = 'angers'
        endif
        call get_parameter_value ('T',T,'e')
        call get_parameter_value ('z',z,'e')
        call get_parameter_value ('nH',nH,'e')
        call get_parameter_value_default ('abund',abund,0.3,'e')
        if (defpar('wmap')) then
          call get_parameter_value_default ('wmapOmega',omega,0.3,'e')
          call get_parameter_value_default ('wmapLambda',lambda,0.7,'e')
          call get_parameter_value_default ('wmapH0',h0,72.0,'e')
        else
          call get_parameter_value_default ('Omega',omega,1.0,'e')
          call get_parameter_value_default ('Lambda',lambda,0.0,'e')
          call get_parameter_value_default ('h',h0,50.0,'e')
        endif
        h0 = h0 / 100.0
*      call get_parameter_value_default ('h',h0,0.5,'e')
*      call get_parameter_value_default ('Omega',omega,1.0,'e')
*      call get_parameter_value_default ('Lambda',lambda,0.0,'e')
        
        if (h0.lt.10.0) h0 = h0 * 100
      endif


      call get_cl_par ('rmf',rmffile)
      if (.not.defined(rmffile)) call exiterror ('rmf=?')
      call get_cl_par ('arf',arffile)
      if (.not.defined(arffile)) call exiterror ('arf=?')
      
      call get_cl_par ('chanmin',arg)
      if (defined(arg)) then
        read (arg,*) cmin
      else
        call get_cl_par ('emin',arg)
        if (.not.defined(arg)) call exiterror ('please set chanmin or emin')
        cmin = -1000
        read (arg,*) emin
      endif

      call get_cl_par ('chanmax',arg)
      if (defined(arg)) then
        read (arg,*) cmax
      else
        call get_cl_par ('emax',arg)
        if (.not.defined(arg)) call exiterror ('please set chanmax or emax')
        cmax = -1000
        read (arg,*) emax
      endif
          
      
c A) Read RMF
      call ftgiou (unitrmf,status)
      filename = rmffile
      call ftopen (unitrmf,filename,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (filename,status)
      chatter = 0
      
      matext = 'SPECRESP MATRIX'
      call ftmnhd (unitrmf,-1,matext,0,status)
      if (status.ne.0) then
        status = 0
        matext='MATRIX'
        call ftmnhd (unitrmf,-1,matext,0,status)
      endif
      if (status.ne.0) 
     ~    call exit_fitsio ('SPECRESP MATRIX, '//filename,status)
      
      call rdrmf3(unitrmf, chatter,matext,
     &    telescop, instrume, detnam, filter, areascal,
     &    chantype, flchan, 
     &    nch, ns, eb1, eb2,
     &    grpmax,ngrp,F_chan, N_chan,
     &    rsp,lo_thresh,nchmax,nsmax,
     &    rmfversn,hduclas3,status)
      if (status.ne.0) call exit_fitsio ('MATRIX, '//filename,status)

      call ftmnhd (unitrmf,-1,'EBOUNDS',0,status)
      if (status.ne.0) call exit_fitsio ('EBOUNDS, '//filename,status)

      call rdebd3(unitrmf,chatter,nchmax, 
     &    telescop,instrume,detnam,filter,areascal, 
     &    chantype, flchan,
     &    nch,channel,e_min,e_max,rmfversn,status)
      
      call ftclos (unitrmf,status)
      call ftfiou (unitrmf,status)
      if (status.ne.0) call exit_fitsio (filename,status)

c B) Read ARF
      if (arffile.eq.'none') then
        do i=1,ns
          effarea(i)=1
        enddo
      else
        call ftgiou (unitarf,status)
        filename=arffile
        call ftopen(unitarf,filename,0,blocksize,status)
        if (status.ne.0) call exit_fitsio (filename,status)
        
        matext = 'SPECRESP'
        call ftmnhd (unitarf,-1,matext,0,status)
        if (status.ne.0) call exit_fitsio (filename,status)
        
        colname='SPECRESP'
        call ftgcno (unitarf,.false.,colname,colnum,status)
        if (status.ne.0) call exit_fitsio (filename,status)
        
        call ftgnrw(unitarf,nrows,status)
        if (status.ne.0) call exit_fitsio (filename,status)
        if (nrows.ne.ns) call exiterror
     ~      ('Different number of energy channels in ARF and RMF')
        
        call ftgcve(unitarf,colnum,1,1,ns,0.0,effarea,anyf,status)
        if (status.ne.0) call exit_fitsio (filename,status)

        call ftclos(unitarf,status)
        call ftfiou(unitarf,status)
        if (status.ne.0) call exit_fitsio (filename,status)
      endif

      if (cmin.lt.0) then
        do i=1,nch
          if (e_max(i).lt.emin) then
            cmin = i+1
          endif
        enddo
        write (0,*) 'min chan = ',cmin
      endif

      if (cmax.lt.0) then
        cmax=nch
        do i=nch,1,-1
          if (e_min(i).gt.emax) then
            cmax = i
          endif
        enddo
        write (0,*) 'max chan = ',cmax
      endif


      e_bin(0)=eb1(1)
      do i=1,ns
        e_bin(i)=eb2(i)
      enddo

      if (plaw) then
        do i=1,ns
          e = 0.5*(eb1(i)+eb2(i))
          rsspec8(i)=e**(-gamma)*(eb2(i)-eb1(i))
        enddo

      else
        if (plmodel.eq.'raym') then
          if (abtable.eq.'angers') then
            write (0,*) 'setting abund = xspec'
            call set_abunds ('xspec')
          else
            if (abtable.ne.'allen') then
              write (0,*)
     ~            'Warning: only angers or allen abundance tables for RS'
              write (0,*) '         use allen'
            endif
            call set_abunds ('allen')
          endif
          call RAYM (T, abund, z, ns, e_bin, rsspec)
        elseif (plmodel.eq.'mekal') then
          call mekal_set_abund (abtable)
          call mekal (T, abund, z, ns, e_bin, rsspec)
        else
          write (0,*) 'Error: unknown plasma model: ',plmodel
          call exit(1)
        endif
        
        call get_cl_par ('dist',arg)
        if (defined(arg)) then
          read (arg,*) dl
          if (dl.lt.1d20) dl = dl*3.085678d+24
        else
          dl = dble(d_a (z,Omega,Lambda)) *(1+z)**2 * (c/h0) * 3.085678d+24
        endif
        
        do i=1,ns
          rsspec8(i)=dble(rsspec(i))*(1+z)/(4*pi*dl**2) ! 1+z factor is present because
                                ! dl is for bolometric luminosities which 
                                ! accounts for the (1+z)^-1 decrease of photon
                                ! energies 
c ���������: ��� ����������� � XSPEC������ �����������:
c K      = 10**-14 / (4 pi (D_A*(1+z))**2) Int n_e n_H dV, where D_A is 
c          the angular size distance to the source (cm), n_e is the electron 
c          density (cm**-3), and n_H is the hydrogen density (cm**-3)

        enddo
      endif

      do i=1,ns
        e = 0.5*(eb1(i)+eb2(i))*1000.0
        absorp = sigism(e)*nH
        if (absorp.lt.50.0) then
          absorp=exp(-absorp)
        else
          absorp=0.0
        endif
        modelspec(i) = absorp*rsspec8(i)*effarea(i)
        phmodelspec(i)=rsspec8(i)
        emodelspec(i)=rsspec8(i)*e*1.60219e-12
      enddo

      flux = 0
      do i=1,ns
        do j=cmin,cmax
          flux=flux+modelspec(i)*dble(rsp(j,i))
        enddo
      enddo

      if (.not.plaw) then
        print '(1pe11.5)',1/flux
      endif

      if (yespar('printflux').or.plaw) then
        call get_parameter_value_default ('femin',femin,0.5,'e')
        call get_parameter_value_default ('femax',femax,2.0,'e')
        phflux=0
        eflux=0
        eeflux=0
        do i=1,ns
          e = 0.5*(eb1(i)+eb2(i))
          if (e.ge.femin.and.e.le.femax) then
            phflux=phflux+phmodelspec(i)
            eflux=eflux+emodelspec(i)
            eeflux=eeflux+e*emodelspec(i)
          endif
        enddo
        print '(3(1x,1pe11.5))',phflux/flux, eflux/flux,eeflux/flux
      endif

          
        
      call exit(0)
      

      end



c%\clearpage
c%\makebox[\textwidth]{\hrulefill}
**XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c%
c% \textit{��������!!!! ������ ��� ������� ���� $d_a (1+z)$}
c%

c% angular size distance: 
c%\begin{equation}
c% y(z) = H_0 a_0 R \sinh 
c%  \left[\frac{1}{H_0 a_0 R} \int_0^z \frac{dz}{E(Z)}\right]
c%\end{equation}

      function d_a(z,om,ol)
      implicit none
      real d_a, z,om,ol
      double precision qnc8,d_a_func
      external d_a_func
      real omegal,omegam
      common /d_a_com/ omegal,omegam
      real integral
      real h0a0R
      
      if (ol.eq.0.0) then
        
        if (om.gt.0.001) then
          d_a = 2*(2-om+om*z-(2-om)*sqrt(1+om*z))/(om**2*(1+z))
        else
          d_a = z*(1.0+z*0.5)/(1.0+z)
        endif
        
      else
        omegal=ol
        omegam=om
        
        integral = qnc8(d_a_func,0.0d0,dble(z),1.0d-6)
        
        if (om+ol.lt.1.0) then
          h0a0R=1.0/sqrt(1-om-ol)
          d_a = h0a0R * sinh (integral/h0a0R)
        else if (om+ol.eq.1.0) then
          d_a = integral
        else
          h0a0R=1.0/sqrt(om+ol-1)
          d_a = h0a0R * sin (integral/h0a0R)
        endif
        
      endif
      
      d_a=d_a/(1.0+z)

      return
      end
      
      
c%{\small  In fact, $1/E(z)$, 
c% $E(z) = [\Omega_M(1+z)^3+\Omega_R(1+z)^2+\Omega_\Lambda]^{1/2}$}
      function d_a_func (z8)
      implicit none
      double precision d_a_func,z8
      
      real z,f
      real ol,om
      common /d_a_com/ ol,om
      real E_func
      
      z=z8
      f=1.0/E_func(z,om,ol)
      d_a_func=f
      return
      end
      
      
c% $E(z) = [\Omega_M(1+z)^3+\Omega_R(1+z)^2+\Omega_\Lambda]^{1/2}$
      function E_func (z,om,ol)
      implicit none
      real E_func, z, om, ol
      
      E_func=sqrt(om*(1+z)**3+(1-om-ol)*(1+z)**2+ol)
      
      return
      end


c%\clearpage
c%\makebox[\textwidth]{\hrulefill}
**XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
