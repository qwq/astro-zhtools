      subroutine readarf (file,ne,elow,ehigh,area,status)
      implicit none
      character file*(*)
      logical exist
      integer ne
      real elow(*),ehigh(*),area(*)
      integer status,status1
      
      integer iunit
      parameter (iunit=11)
      integer blocksize
      character telescop*80,instrume*80,detector*80,filter*80,arfversn*80
      
      inquire (file=file,exist=exist)
      
      if (.not.exist) then
        status=1
        return
      endif
      
      
      status=0
      
c open  fits file
      call ftopen(iunit,file,0,blocksize,status)
      if (status.ne.0) then
        call fitsio_write_error(status,0)
        status=2
        return
      endif

c move to the SPECRESP extension
      call move_ext (iunit,'SPECRESP',status)
      if (status.ne.0) then
        status=0
        call ftclos(iunit,status)
        status=2
        return
      endif
      
      call rdarf1(iunit,0,telescop,instrume,detector,filter,ne,elow,ehigh,area
     ~    ,arfversn,status)
      
      status1=0
      call ftclos(iunit,status1)
      return
      end
      
      
      
c     --------------------------------------------------------
      subroutine rdarf1(iunit,chatter, 
     &    telescop,instrume,detnam,filter,
     &    iebound,e_lo,e_hi,sprsp, arfversn,ierr)
c     --------------------------------------------------------
      IMPLICIT NONE
      integer chatter, ierr
      integer iunit
      integer iebound
      real e_lo(*), e_hi(*)
      real sprsp(*)
      character arfversn*5
      character telescop*20, instrume*20, detnam*20, filter*20
      
c --- DESCRIPTION ------------------------------------------------------
c  Reads the SPECRESP extension for an ARFVERSN=1992a ARF file
c Assumes the extension conforms to HDUVERS2='1.*.*' family
c Currently the following formats are supported -
c HDUVERS2='1.0.0'
c HDUVERS2='1.1.0'
c see OGIP/92-002a
c The HDU CLASS keywords have only been currently introduced thus DO NOT
c have to be present to use this reader.  
c
c  Assumes the FITS is open.
c  !!! Note !!!! File is left open at the end
c      ... close file using FTCLOS, or
c      ... read another extension
c
c  Columns read are ...
c  ENERG_LO    : lower energy bound
c  ENERG_HI    : upper energy bound
c  SPECRESP    : Spectral Response
c
c  Keywords read ...
c  TELESCOP : Mission/Telescope name, NOTE: If not present set to UNKNOWN
c  INSTRUME : Instrument/Detector name NOTE: If not present set to UNKNOWN
c  DETNAM   : Specific detector name NOTE: If not present set to NONE
c  FILTER   : Filter in use, if not present set to NONE
c  ARFVERSN : RMF version
c
c Passed Paramters
c  IUNIT         i   : FORTRAN unit number of open RMF file
c  CHATTER       i   : chattiness flag for o/p (0 quite,10 normal,>20 silly)
c  TELESCOP      o   : String listing telescope/mission
c  INSTRUME      o   : String listing instrument/detector
c  DETNAM        o   : String listing specific detector name
c  FILTER        o   : String listing instrument filter in use
c  AREA          o   : Area scaling factor
c  IEBOUND       o   : No. channels in the full array
c  CHANNEL       o   : Channel array
c  E_MIN         o   : Array containing min nominal energy bound to each chan
c  E_MAX         o   : Array containing max nominal energy bound to each chan
c  RMFVERSN      o   : RMF version
c  IERR          o   : Error Flag, ierr = 0 okay
c
c CALLED ROUTINES 
c  subroutine FTMRHD     : (FITSIO) Move to extension
c  subroutine FTGKYS     : (FITSIO) Read FITS extension header keyword 
c  subroutine FTGCNO     : (FITSIO) Get column number
c  subroutine FTGCVx     : (FITSIO) Read data in x format
c  subroutine WT_FERRMSG : (CALLIB) Dumps FITSIO Error message etc
c
c Compilation & Linking 
c  link with FITSIO & CALLIB & FTOOLS
c
c Authors/Modification History: 
c Ian M George (93 Nov 16) Original
      character version*7
      parameter (version = '1.0.0')
*- 
c INTERNALS
      character errstr*25, wrnstr*25
      character comm*30
      character desc*70, errinfo*70
      integer status
c     integer htype
      integer inull, felem, frow, colnum
c     integer nhdu
      real enull
c     character extname*8,hduclas1*8,hduclas2*8
      logical anyflg, foundcol
c     logical extfind,endfile
c INITIALISATION
      ierr = 0
      status = 0 
      errstr = ' RDARF1 '//version//' ERROR:'
      wrnstr = ' RDARF1 '//version//' WARNING:'
c
c --- USER INFO ---
c
      IF (chatter.GE.15) THEN
        desc = ' ... using RDARF1 ' // version
	call fcecho(desc)
      ENDIF
c
c --- READING DATA ---
c
      
c NAXIS2 ...
      
      status = 0
      call ftgkyj(iunit,'NAXIS2',iebound,comm,status)
      errinfo = errstr//' reading NAXIS2 value '
      IF (status.NE.0) THEN
        ierr = 4
        return
      ENDIF
      
c ARFVERSN ...
      
      status = 0
      ARfversn = '  '
      call ftgkys(iunit,'HDUVERS2',arfversn,comm,status)
      errinfo = wrnstr//' reading HDUVERS2/ARFVERSN'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF 
      IF (arfversn.EQ.'   ') THEN
        status = 0
        call ftgkys(iunit,'ARFVERSN',arfversn,comm,status)
        errinfo = wrnstr//' reading ARFVERSN'
        IF (chatter.GE.30)THEN
          call wt_ferrmsg(status,errinfo)
        ENDIF
      ENDIF
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      
c TELESCOP ...
      
      status = 0
      call ftgkys(iunit,'TELESCOP',telescop,comm,status)
      errinfo = wrnstr//' reading TELESCOP'
      IF (status.EQ.202) THEN
        telescop = 'UNKNOWN'
      ENDIF 
      
c INSTRUME ...
      
      status = 0
      call ftgkys(iunit,'INSTRUME',instrume,comm,status)
      errinfo = wrnstr//' reading INSTRUME'
      IF (status.EQ.202) THEN
        instrume = 'UNKNOWN'
      ENDIF 
      
c FILTER ...
      
      status = 0
      call ftgkys(iunit,'FILTER',filter,comm,status)
      errinfo = wrnstr//' reading FILTER'
      IF (status.EQ.202) THEN
        filter = 'NONE'
      ENDIF
      
c DETNAM ...
      
      status = 0
      call ftgkys(iunit,'DETNAM',detnam,comm,status)
      errinfo = wrnstr//' reading DETNAM'
      IF (status.EQ.202) THEN
        detnam = 'NONE'
      ENDIF   
      
      
      frow = 1
      felem = 1
      inull = 0
c Find and Read ENERG_LO Column 
      status = 0
      foundcol = .true.
      call ftgcno(iunit,.false.,'ENERG_LO',colnum,status)
      IF (status.NE.0) THEN
        foundcol = .false.
      ENDIF
      IF (.NOT.foundcol) THEN
        errinfo = errstr//' ENERG_LO COLUMN not present'
        call fcecho(errinfo)
        ierr = 2
        return
      ENDIF
c ... Reading 
      status = 0 
      enull = 0
      call ftgcve(iunit,colnum,frow,felem,iebound,enull,e_lo,
     &    anyflg,status) 
      IF (status.NE.0) THEN
        errinfo = errstr//' reading ENERG_LO column'
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF      
      
c Find and Read ENERG_HI Column 
      status = 0
      foundcol = .true.
      call ftgcno(iunit,.false.,'ENERG_HI',colnum,status)
      IF (status.NE.0) THEN
        foundcol = .false.
      ENDIF
      IF (.NOT.foundcol) THEN
        errinfo = errstr//' ENERG_HI COLUMN not present'
        call fcecho(errinfo)
        ierr = 2
        return
      ENDIF    
c ... Reading 
      status = 0
      enull = 0
      call ftgcve(iunit,colnum,frow,felem,iebound,enull,e_hi,
     &    anyflg,status)
      IF (status.NE.0) THEN
        errinfo = errstr//' reading ENERG_HI column !'
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF
      
      
      
c Find and Read SPECRESP Column 
      status = 0
      foundcol = .true.
      call ftgcno(iunit,.false.,'SPECRESP',colnum,status)
      IF (status.NE.0) THEN
        foundcol = .false.
      ENDIF
      IF (.NOT.foundcol) THEN
        errinfo = errstr//' SPECRESP COLUMN not present'
        ierr = 2
        return
      ENDIF
c ... reading
      status = 0
      call ftgcve(iunit,colnum,frow,felem,iebound,inull,sprsp,
     &    anyflg,status)
      IF (status.NE.0) THEN
        errinfo = errstr//' reading SPECRESP column'
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF
      
      
      IF (chatter.GE.20) THEN
        desc = '      ... read SPECRESP data '
        call fcecho(desc)
      ENDIF
      
      
      return
      end
      
c --------------------------------------------------------------------   
