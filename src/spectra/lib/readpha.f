      subroutine readpha
     ~    (file,nchan,texp,areascale,backscale,corscale,backfile
     ~    ,corfile,rmffile,arffile,chan,dtype,ipha,pha,qerror,error,qsys
     ~    ,sysfrac,qqual,quality,qgroup,group,poiss,status)
      
      implicit none
c
c  ARG         TYPE        MEANING
c
c file          C          data file name
c nchan         J          number of channels available
c texp          E          exposure, s
c areascale     E          area scaling factor
c backscale     E          background scaling factor
c corscale      E          correction scaling factor
c backfile      C          background file
c corfile       C          correction file
c rmffile       C          RM file 
c arffile       C          Area file
c chan(*)       J          channel numbers 
c dtype         J          data type: 1- counts, 2 - rate
c ipha(*)       J          counts, if dtype = 1
c pha(*)        E          rate, if dtype = 2
c qerror        L          true if statistical errors are present
c error(*)      E          statistical errors if qerror=.true.         
c qsys          L          true if systematic errors are present
c sysfrac(*)    E          systematic fraction if qsys=.true.
c qqual         L          true if quality flags are present          
c quality(*)    J          quality flags (see OGIP memo for defenition)
c qgroup        L          true if grouping is present
c group(*)      J          if qgroup=.true. contain group flag
c poiss         L          true if poisson errors are assumed 
c status        J          - return code
c
c
c
      character file*(*)
      integer nchan
      real texp,areascale,corscale,backscale
      character backfile*(*),corfile*(*),rmffile*(*),arffile*(*)
      integer chan(*)
      integer dtype
      integer ipha(*)
      real pha(*)
      logical qerror
      real error(*)
      logical qsys
      real sysfrac(*)
      logical qqual
      integer quality(*)
      logical qgroup
      integer group(*)
      logical poiss
      integer status

      logical exist
      integer n_xflt
      character xflt(100)*100
      
      
      character telescop*80,instrume*80,detector*80,filter*80,phaversn*80
     ~    ,hduclas2*80,dmode*80,chantype*80
      integer detchans,fchan
c fitsio staff
      integer iunit
      parameter (iunit=11)
      integer blocksize

      
      
      inquire (file=file,exist=exist)
      
      if (.not.exist) then
        status=1
        return
      endif
      
      
      status=0
      
c open  fits file
      call ftopen(iunit,file,0,blocksize,status)
      if (status.ne.0) then
        call fitsio_write_error(status,0)
        status=2
        return
      endif
      
      
c goto the SPECTRUM extension
      call move_ext (iunit,'SPECTRUM',status)
      if (status.ne.0) then
        status=0
        call ftclos(iunit,status)
        status=2
        return
      endif
      
      

      call rdpha1(iunit,20000,nchan,telescop,instrume,detector,
     ~    filter,detchans,texp,areascale,backscale,
     ~    corscale,backfile,corfile,rmffile,arffile,xflt,
     ~    n_xflt,dmode,chantype,phaversn,hduclas2,
     ~    fchan,chan,dtype,
     ~    ipha,pha,qerror,error,qsys,sysfrac,qqual,
     ~    quality,qgroup,group,poiss,status,0)
      
      
      return
      end
      
*+RDPHA1
c     -----------------------------------------------------------------
      subroutine rdpha1(iunit,maxpha,nchan,tlscop,instrum,detnam,
     &                       filter,detchan,texpos,areascal,backscal,
     &		             corscal,backfil,corfil,rmffil,arffil,xflt,
     &                       n_xflt,dmode,chantyp,phaversn,hduclas2,
     &		       	     fchan,chan,dtype,
     &                       ipha,pha,qerror,error,qsys,sysfrc,qqual,
     &                       qualty,qgroup,group,pois,ierr,chatter)
c     -----------------------------------------------------------------
c --- DESCRIPTION -----------------------------------------------------
c
c This subroutine reads the SPECTRUM extension of a PHA file in 
c PHAVERSN = 1992a format. HDUVERS 1
c
c NOTE : The file is assumed to be open. It is assumed that the file
c is at the desired SPECTRUM extension.
c Close file using FTCLOS or read another ext'
c
c KEYWORDS READ ...
c
C TELESCOP   : Telescope name
c INSTRUME   : Instrument name
c FILTER     : Filter in use
c POISSERR   : True if Poission errors apply
c EXPOSURE   : Exposure time
c AREASCAL   : Area scaling factor
c BACKSCAL   : Background scaling factor
c CORRSCAL   : Correction scaling factor
c BACKFILE   : Associated background filename
c CORRFILE   : Associated correction filename
c RESPFILE   : Associated response filename
c ANCRFILE   : Associated ancillary response filename
c XFLTxxxx   : XSPEC filter description
c CHANTYPE   : Channel type - PHA or PI
c DETCHANS   : Number of possible detector channels
c DATAMODE   : Datamode - for ASCA SIS - BRIGHT,FAINT etc 
c PHAVERSN   : PHA file format version
c HDUCLAS2   : Describes SPECTRUM - BKG,TOTAL,NET
c TLMIN1     : First possible value of first column (Channel)
c              if TLMIN not found the default value 1 is assumed
c
c COLUMNS READ (if present) ...
c
c COUNTS/RATE : data in counts OR counts per sec
c STAT_ERR    : Statistical error on data
c SYS_ERR     : Systematic Error
c QUALITY     : Channel Quality flags
c GROUPING    : Channel grouping flags
c
c --- VARIABLES ------------------------------------------------------
c
      IMPLICIT NONE
      integer iunit,chatter,maxpha,nchan,dtype,ierr,n_xflt
      character tlscop*(*),instrum*(*),filter*(*),chantyp*(*),xflt(*)*(*)
      character backfil*(*),corfil*(*),rmffil*(*),dmode*(*),hduclas2*(*)
      character arffil*(*),detnam*(*),phaversn*(*)
      integer group(maxpha),qualty(maxpha),fchan
      integer chan(maxpha),ipha(maxpha),detchan
      real error(maxpha),sysfrc(maxpha),pha(maxpha)
      real texpos,areascal,backscal,corscal
      logical qgroup,qqual,qerror,qsys,pois
c
c --- VARIABLE DIRECTORY ---------------------------------------------
c
c Arguments (only those that are not described in keywords read)...
c
c iunit    int     : Fortran file unit number
c qgroup   logical : true if grouping flags have been set
c qerror   logical : true if statistical errors present
c qsys     logical : true if systematic errors present
c pois     logical : true if poisonian errors
c chatter  int     : chattiness flag (>20 verbose)
c maxpha   int     : Maximum array size
c nchan    int     : Number of channels available
c detchan  int     : Number of possible channels
c dtype    int     : Datatype, 1 - counts, 2 - rate
c ipha     int     : Array used for counts (if dtype = 1)
c pha      real    : Array used for rate  ( if dtype = 2)
c ierr     int     : error flag, 0 is okay ...
c                                1 = failed to find extension
c			         2 = failed to read primary header
c 				 3 = array sizes are not large enough
c				 4 = column not found
c				 5 = error reading data
c 				 6 = error reading keyword
c
c --- CALLED ROUTINES ------------------------------------------------
c
c FTMRHD      : (FITSIO) Move to extension
c FTGHBN      : (FITSIO) Get binary header info
c FTGKYx      : (FITSIO) Get keyword value of format x
c FTGCNO      : (FITSIO) Get column number
c FTGCVx      : (FITSIO) Reads data
c WT_FERRMSG  : (CALLIB) Writes fitsio and routine error message
c FCECHO      : Screen write
c
c --- AUTHORS/MODIFICATION HISTORY -----------------------------------
c
c Rehana Yusaf (1.0.0) : Keith Arnaud's RFTSPF used as a basis
c Rehana Yusaf (1.0.1) Nov 19 1993; Additional arguments - hduclas2
c                        and fchan. Also the extension search is
c                        removed and assumed the file is assumed to
c			 be in the desired position 
c Ian M George (1.0.2: 1994 Mar 11), changed POISERR kywd name to POISSERR
c
      character version*5
      parameter (version = '1.0.2')
*-
c --------------------------------------------------------------------
c 
c --- INTERNALS ---
c
      character errstr*30,wrnstr*30,comm*30
      character errinfo*70,subinfo*70
      integer status,colnum,i,tfields,ivar
c     integer htype
      integer frow,felem,inull
      real enull
      character ttype(6)*20,tunit(6)*20,tlmin*20
c     character extname*8
      character tform(6)*5
      logical anyflg
c     logical extfind
c
c --- USER INFO ---
c
      errstr = ' ERROR:RDPHA1 Ver '//version//':'
      wrnstr = ' WARNING:RDPHA1 Ver '//version//':'
      IF (chatter.GE.15) THEN
        subinfo = ' using RDPHA1 Ver '//version
        call fcecho(subinfo)
      ENDIF 
c
c --- READING KEYWORDS ---
c
      status = 0
      call ftghbn(iunit,6,nchan,tfields,ttype,tform,tunit,
     &            comm,ivar,status)
      errinfo = errstr//' reading binary header info'
      IF (status.NE.0) THEN
        call wt_ferrmsg(status,errinfo)
        ierr = 2
        return
      ENDIF

      IF (nchan.GT.maxpha) THEN
        write(subinfo,'(A,i5)')
     &  'array sizes are too small, they should be ',nchan
        errinfo = errstr//subinfo
        call fcecho(errinfo)
        ierr = 3
        return
      ENDIF          

c DETCHANS ...
 
      call ftgkyj(iunit,'DETCHANS',detchan,comm,status)
      errinfo = errstr//' reading DETCHANS'
      if (status.ne.0) then
        call wt_ferrmsg(status,errinfo)
      endif

c DETERMINE WHICH COLUMNS ARE PRESENT AND SET LOGICALS ACCORDINGLY

      dtype = 0
      qerror = .false.
      qsys = .false.
      qqual = .false.
      qgroup = .false.
      do i=1,6
        IF (ttype(i).EQ.'COUNTS') THEN
          dtype = 1
        ELSEIF (ttype(i).EQ.'RATE') THEN
          dtype = 2
        ELSEIF (ttype(i).EQ.'STAT_ERR') THEN
          qerror = .true.
        ELSEIF (ttype(i).EQ.'SYS_ERR') THEN
          qsys = .true.
        ELSEIF (ttype(i).EQ.'QUALITY') THEN
          qqual = .true.
        ELSEIF (ttype(i).EQ.'GROUPING') THEN
          qgroup =.true.
        ENDIF
      enddo

      IF (dtype.EQ.0) THEN
        ierr = 3
        subinfo = errstr//' reading primary header'
        call fcecho(errinfo)
        return
      ENDIF

c TELESCOPE ...

      status = 0
      tlscop = 'UNKNOWN'
      call ftgkys(iunit,'TELESCOP',tlscop,comm,status)
      errinfo = errstr//' reading TELESCOP'
      if (status.ne.0) then
        call wt_ferrmsg(status,errinfo)
        IF (status.EQ.225) THEN
          tlscop = 'UNKNOWN'
        ENDIF
      endif

c INSTRUME ...

      status = 0
      instrum = 'UNKNOWN'
      call ftgkys(iunit,'INSTRUME',instrum,comm,status)
      errinfo = errstr//' reading INSTRUME'
      if (status.ne.0) then
        call wt_ferrmsg(status,errinfo)
        IF (status.EQ.225) THEN
          instrum = 'UNKNOWN'
        ENDIF     
      endif

c PHAVERSN ...

      status = 0
      phaversn = '  '
      call ftgkys(iunit,'HDUVERS2',phaversn,comm,status)
      errinfo = wrnstr//' reading HDUVERS2/PHAVERSN'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
      IF (phaversn.EQ.'   ') THEN
        status = 0
        call ftgkys(iunit,'PHAVERSN',phaversn,comm,status)
        errinfo = wrnstr//' reading PHAVERSN'
        IF (chatter.GE.30)THEN
          call wt_ferrmsg(status,errinfo)
        ENDIF
      ENDIF
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF 

c HDUCLAS2 ...

      hduclas2 = ' '
      call ftgkys(iunit,'HDUCLAS2',hduclas2,comm,status)
      errinfo = wrnstr//' reading HDUCLAS2'
      IF (chatter.GE.30) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
 
c DETNAM ...

      status = 0
      detnam = '  '
      call ftgkys(iunit,'DETNAM',detnam,comm,status)
      errinfo = wrnstr// ' reading DETNAM keyword'
      IF (chatter.GE.10) THEN
        call wt_ferrmsg(status,errinfo)
      ENDIF
    
c FILTER ...

      status = 0
      filter = ' '
      call ftgkys(iunit,'FILTER',filter,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading FILTER keyword'
        call wt_ferrmsg(status,errinfo)
      ENDIF

c TEXPOS ...

      status = 0
      call ftgkye(iunit,'EXPOSURE',texpos,comm,status)
      IF (chatter.GE.5) THEN
        errinfo = wrnstr//' reading TEXPOS keyword'
        call wt_ferrmsg(status,errinfo)   
      ENDIF
      IF (status.EQ.225) THEN
        texpos = -1
        errinfo = wrnstr//' Exposure time not found'
        call fcecho(errinfo)
        errinfo = wrnstr//' Exposure set to -1'
        call fcecho(errinfo)
      ENDIF
        
c POISSERR ...

      status = 0
      pois = .false.
      call ftgkyl(iunit,'POISSERR',pois,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading POISSERR keyword'
        call wt_ferrmsg(status,errinfo)   
      ENDIF

c AREASCAL ...

      status = 0
      areascal = 1
      call ftgkye(iunit,'AREASCAL',areascal,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading AREASCAL keyword'
        call wt_ferrmsg(status,errinfo)     
      ENDIF

c BACKSCAL ...

      status = 0
      backscal = 1
      call ftgkye(iunit,'BACKSCAL',backscal,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading BACKSCAL keyword'
        call wt_ferrmsg(status,errinfo)     
      ENDIF

c CORRSCAL ...

      status = 0
      corscal = 1
      call ftgkye(iunit,'CORRSCAL',corscal,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading CORRSCAL keyword'
        call wt_ferrmsg(status,errinfo)     
      ENDIF

c BACKFILE ...

      status = 0
      backfil = 'NONE'
      call ftgkys(iunit,'BACKFILE',backfil,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading BACKFILE keyword'
        call wt_ferrmsg(status,errinfo)     
      ENDIF
      IF (backfil.EQ.'   ') THEN
         backfil = 'NONE'
      ENDIF 

c CORRFILE ...

      status = 0
      corfil = 'NONE'
      call ftgkys(iunit,'CORRFILE',corfil,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading CORRFILE keyword'
        call wt_ferrmsg(status,errinfo) 
      ENDIF
      IF (corfil.EQ.'   ') THEN
        corfil = 'NONE'
      ENDIF

c RESPFILE ...

      status = 0
      rmffil ='NONE'
      call ftgkys(iunit,'RESPFILE',rmffil,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading RESPFILE keyword'
        call wt_ferrmsg(status,errinfo) 
      ENDIF
      IF (rmffil.EQ.'   ') THEN
        rmffil = 'NONE'
      ENDIF

c ANCRFILE ...

      status = 0
      arffil = 'NONE'
      call ftgkys(iunit,'ANCRFILE',arffil,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading ANCRFILE keyword'
        call wt_ferrmsg(status,errinfo) 
      ENDIF
      IF (arffil.EQ.'    ') THEN
        arffil = 'NONE'
      ENDIF

c CHANTYPE ...

      status = 0
      chantyp = 'UNKNOWN'
      call ftgkys(iunit,'CHANTYPE',chantyp,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading CHANTYPE keyword'
        call wt_ferrmsg(status,errinfo) 
      ENDIF

c XSPEC FILTER ...

      status = 0
      call ftgkns(iunit,'XFLT',1,9999,xflt,n_xflt,status)
      IF (chatter.GE.30) THEN
        errinfo = wrnstr//' reading XFLT keyword'
        call wt_ferrmsg(status,errinfo) 
      ENDIF

      IF (chatter.GE.20) THEN
        subinfo = '      ... keywords have been read'
        call fcecho(subinfo)
      ENDIF

c DATAMODE ...

      status = 0
      dmode = ' '
      call ftgkys(iunit,'DATAMODE',dmode,comm,status)
      IF (chatter.GE.10) THEN
        errinfo = wrnstr//' reading DATAMODE keyword'
        call wt_ferrmsg(status,errinfo)
      ENDIF     
c
c --- READ DATA ---
c

c CHANNEL ...

      frow = 1
      felem = 1
      status = 0
      call ftgcno(iunit,.false.,'CHANNEL',colnum,status)
      errinfo = errstr//' finding CHANNEL column number'
      IF (status.NE.0) THEN
        call wt_ferrmsg(status,errinfo)
        ierr = 4
        return
      ENDIF

      inull = 0
      status = 0
      call ftgcvj(iunit,colnum,frow,felem,nchan,inull,chan,
     &		  anyflg,status)
      errinfo = errstr//' reading CHANNEL column'
      IF (status.NE.0) THEN
        call wt_ferrmsg(status,errinfo)
        ierr = 5
        return
      ENDIF

c READ TLMINx keyword, where x is CHANNEL COLUMN NUMBER TO DETERMINE
c FIRST POSSIBLE CHANNEL VALUE
      write(tlmin,'(a,i3)') 'TLMIN',colnum
      call rmblanks (tlmin)
      status = 0
      call ftgkyj(iunit,tlmin,fchan,comm,status)
      IF (status.EQ.202) THEN
        IF (chan(1).EQ.0) THEN
          fchan = 0
        ELSE
          fchan = 1
        ENDIF
        IF (chatter.GE.15) THEN
          errinfo = wrnstr//
     &' TLMIN keyword for FCHAN value not found'
          call fcecho(errinfo)
          errinfo = ' ... Default value of FCHAN is 1 '
          call fcecho(errinfo)
        ENDIF
      ENDIF

c COUNTS or RATE ...

      IF (dtype.EQ.1) THEN
        status = 0
        call ftgcno(iunit,.false.,'COUNTS',colnum,status)
        errinfo = errstr//' finding COUNTS column number'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 4
          return
        ENDIF

        inull = 0
        status = 0
        frow = 1
        felem = 1
        call ftgcvj(iunit,colnum,frow,felem,nchan,inull,ipha,
     &            anyflg,status)
        errinfo = errstr//' reading COUNTS column'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 5
          return
        ENDIF                   
      ELSEIF (dtype.EQ.2) THEN
        status = 0
        call ftgcno(iunit,.false.,'RATE',colnum,status)
        errinfo = errstr//' finding RATE column number'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 4
          return
        ENDIF
  
        enull = 0
        status = 0
        frow = 1
        felem = 1
        call ftgcve(iunit,colnum,frow,felem,nchan,enull,pha,
     &            anyflg,status)
        errinfo = errstr//' reading RATE column'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 5
          return
        ENDIF            
      ENDIF

c STAT_ERR ...

      IF (qerror) THEN
        status = 0
        call ftgcno(iunit,.false.,'STAT_ERR',colnum,status)
        errinfo = errstr//' finding STAT_ERR column number'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 4
          return
        ENDIF

        enull = 0
        status = 0
        frow = 1
        felem = 1
        call ftgcve(iunit,colnum,frow,felem,nchan,enull,error,
     &            anyflg,status)
        errinfo = errstr//' reading STAT_ERR column'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 5
          return
        ENDIF             
      ENDIF

c SYS_ERR ...

      IF (qsys) THEN
        status = 0
        call ftgcno(iunit,.false.,'SYS_ERR',colnum,status)
        errinfo = errstr//' finding SYS_ERR column number'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 4
          return
        ENDIF

        enull = 0
        status = 0
        frow = 1
        felem = 1
        call ftgcve(iunit,colnum,frow,felem,nchan,enull,sysfrc,
     &            anyflg,status)
        errinfo = errstr//' reading SYS_ERR column'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 5
          return
        ENDIF             
      ENDIF

c QUALITY ...

      IF (qqual) THEN
        status = 0
        call ftgcno(iunit,.false.,'QUALITY',colnum,status)
        errinfo = errstr//' finding QUALITY column number'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 4
          return
        ENDIF

        inull = 0
        status = 0
        frow = 1
        felem = 1
        call ftgcvj(iunit,colnum,frow,felem,nchan,inull,qualty,
     &            anyflg,status)
        errinfo = errstr//' reading QUALITY column'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 5
          return
        ENDIF             
      ENDIF

c GROUPING ...

      IF (qgroup) THEN
        status = 0
        call ftgcno(iunit,.false.,'GROUPING',colnum,status)
        errinfo = errstr//' finding GROUPING column number'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 4
          return
        ENDIF

        inull = 0
        status = 0
        frow = 1
        felem = 1
        call ftgcvj(iunit,colnum,frow,felem,nchan,inull,group,
     &            anyflg,status)
        errinfo = errstr//' reading GROUPING column'
        IF (status.NE.0) THEN
          call wt_ferrmsg(status,errinfo)
          ierr = 5
          return
        ENDIF             
      ENDIF

      IF (chatter.GE.20) THEN
        subinfo = '       ... data has been read'
        call fcecho(subinfo)
      ENDIF
      return
      end
c -----------------------------------------------------------------------
c     END OF RDPHA1
c -----------------------------------------------------------------------


