#!/bin/bash
INSTALLDIR=/Users/qw/astro/soft/zhtools/ciao-4.11

CIAOLIB=/usr/local/ciao/ciao-4.11/ots
FUNTOOLS=/Users/qw/astro/soft/funtools
WCS=/Users/qw/astro/soft/wcstools/wcstools-3.9.5

GCC=/usr/local/Cellar/gcc/10.2.0/bin
export CC=$GCC/gcc-10
export CXX=$GCC/g++-10
export FC=$GCC/gfortran-10

echo "Warning---variables CC, CXX, FC have been exported."
echo "Run 'make' and 'make install' in this same session."

./configure --prefix=$INSTALLDIR \
    --with-readline=$CIAOLIB \
    --with-funtools=$FUNTOOLS \
    --with-cfitsio=$CIAOLIB \
    --with-wcs=$WCS \
    FFLAGS="-fbackslash -fallow-argument-mismatch"

# -fallow-argument-mismatch is a new flag needed for GCC10, previously off by
# default and had to be turned on with -Wno-argument-mismatch. There's a
# REAL(4) to COMPLEX(4) in corr2d_main that triggers it.
